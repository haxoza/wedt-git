package pl.haxoza.wedt;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import weka.core.Attribute;
import weka.core.FastVector;

public class CorpusBuilder
{
	private List<File> files;
	private HashMap<String, Integer> corpus;
	private Set<String> uniqueWords;
	private Set<String> uniqueStems;
	private final String FILE_NAME = "res/CorpusBuilder.corpus";

	public CorpusBuilder(List<File> files)
	{
		this.files = files;
	}

	public void build()
	{
		resetCorpus();

		for (File file : files)
		{
			List<String> words = getWords(file);
			List<String> steams = makeSteams(words);
			addSteamsToCorpus(steams);
			updateStats(words, steams);
		}
	}

	private void updateStats(List<String> words, List<String> steams)
	{
		uniqueWords.addAll(words);
		uniqueStems.addAll(steams);
	}

	private void resetCorpus()
	{
		corpus = new HashMap<String, Integer>();
		uniqueWords = new HashSet<String>();
		uniqueStems = new HashSet<String>();
	}

	private List<String> makeSteams(List<String> words)
	{
		StemmingFilter stemmingFilter = new StemmingFilter(words);
		stemmingFilter.doStemming();
		return stemmingFilter.getStems();
	}

	private List<String> getWords(File file)
	{
		SensesParser sensesParser = new SensesParser(file);
		sensesParser.parse();
		return sensesParser.getWords();
	}

	private void addSteamsToCorpus(List<String> stems)
	{
		for (String stem : stems)
		{
			int newCount = getCorpus().containsKey(stem) ? getCorpus().get(stem).intValue() + 1 : 1;
			getCorpus().put(stem, new Integer(newCount));
		}
	}

	public HashMap<String, Integer> getCorpus()
	{
		return corpus;
	}
	
	public void limitCorpus(int minWordCount)
	{
		Iterator<Entry<String, Integer>> iterator = corpus.entrySet().iterator();
		
		while (iterator.hasNext())
		{
			Entry<String, Integer> entry = iterator.next();
			if (entry.getValue() < minWordCount)
			{
				iterator.remove();
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void loadFromFile(String fileName)
	{
		try
		{
			FileInputStream fileIS = new FileInputStream(fileName);
			ObjectInputStream objectIS = new ObjectInputStream(fileIS);
			corpus = ((HashMap<String, Integer>) objectIS.readObject());
			objectIS.close();
		} 
		catch (Exception e)
		{
			resetCorpus();
		}
	}
	
	public void loadFromFile()
	{
		loadFromFile(FILE_NAME);
	}

	public void saveToFile(String fileName)
	{
		try
		{
			FileOutputStream fileOS = new FileOutputStream(fileName);
			ObjectOutputStream objectOS = new ObjectOutputStream(fileOS);
			objectOS.writeObject(corpus);
			objectOS.close();
		}
		catch (Exception e) {}
	}
	
	public void saveToFile() throws FileNotFoundException, IOException
	{
		saveToFile(FILE_NAME);
	}
	
	public float getStemsToWordsRatio()
	{
		return uniqueStems.size() / (float)uniqueWords.size();
	}
	
	public FastVector getCorpusAsAttributes()
	{
		FastVector attributes = new FastVector();
		
		for (String stem : corpus.keySet())
		{
			attributes.addElement(new Attribute(FeatureWideCalculator.attributeNameForWord(stem)));
		}
		
		return attributes;
	}
}
