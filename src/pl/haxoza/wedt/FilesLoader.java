package pl.haxoza.wedt;

import java.io.File;
import java.util.ArrayList;

public class FilesLoader
{
	private String[] rawFilesNames;
	
	public FilesLoader(String[] args)
	{
		this.rawFilesNames = args;
	}
	
	public ArrayList<File> loadFiles()
	{
		ArrayList<File> files = new ArrayList<File>();
		
		for (String fileName : rawFilesNames)
		{
			File file = new File(fileName);
			if (file.exists())
			{
				files.add(file);
			}
		}
		
		return files;
	}

}
