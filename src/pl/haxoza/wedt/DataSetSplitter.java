package pl.haxoza.wedt;

import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.instance.RemovePercentage;

public class DataSetSplitter
{
	private final Instances dataSet;
	private Instances trainSet;
	private Instances testSet;

	public DataSetSplitter(Instances dataSet)
	{
		this.dataSet = dataSet;
	}
	
	public void split() throws Exception
	{
		RemovePercentage trainFilter = new RemovePercentage();
		trainFilter.setInputFormat(dataSet);
		trainFilter.setPercentage(60);
		trainSet = Filter.useFilter(dataSet, trainFilter);
		
		RemovePercentage testFilter = new RemovePercentage();
		testFilter.setInputFormat(dataSet);
		testFilter.setInvertSelection(true);
		testFilter.setPercentage(40);
		testSet = Filter.useFilter(dataSet, testFilter);
	}

	public Instances getTrainSet()
	{
		return trainSet;
	}

	public Instances getTestSet()
	{
		return testSet;
	}
}
