package pl.haxoza.wedt;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SparseInstance;

public class DataSetBuilder
{

	private ArrayList<File> files;
	private FastVector attributes;
	private Instances dataSet;
	private int contextWindowSize;

	public DataSetBuilder(ArrayList<File> files, FastVector attributes, int contextWindowSize)
	{
		this.files = files;
		this.attributes = attributes;
		this.contextWindowSize = contextWindowSize;
	}

	public void build()
	{
		initializeDataSet();
		int fileIndex = 0;
		for (File file : files)
		{
			++fileIndex;
			SensesParser sensesParser = new SensesParser(file);
			sensesParser.parse();
			
			List<String> words = sensesParser.getWords();
			StemmingFilter stemmer = new StemmingFilter(words);
			stemmer.doStemming();
			List<String> stems = stemmer.getStems();
			
			List<Sense> senses = sensesParser.getSenses();
			Instance[] extracted = extractFeatureVectors(stems);
			
			for (int instanceNumber = 0; instanceNumber < extracted.length; instanceNumber++)
			{
				Instance instance = extracted[instanceNumber];
				instance.setDataset(dataSet);
				instance.setClassValue(senses.get(instanceNumber).toString());
				dataSet.add(instance);
			}
			
			printFileStat(fileIndex, file, words, stems);
		}
		
		dataSet.compactify();
	}

	private void printFileStat(int fileIndex, File file, List<String> words, List<String> stems)
	{
		Set<String> uniqueWords = new HashSet<String>(words); 
		Set<String> uniqueStems = new HashSet<String>(stems);
		
		System.out.println("File[" + fileIndex + "]: " + file.getName() + ", words: " + uniqueWords.size() + ", stems: " + uniqueStems.size());
	}

	private void initializeDataSet()
	{
		attributes.addElement(new Attribute("sense", Sense.allSenses()));
		dataSet = new Instances("Whole data", attributes, 0);
		dataSet.setClassIndex(getLastAttributeIndex());
	}

	private int getLastAttributeIndex()
	{
		return dataSet.numAttributes() - 1;
	}

	private Instance[] extractFeatureVectors(List<String> text)
	{
		FeatureWideCalculator featureCalculator = new FeatureWideCalculator(convertToAttributeIndex(), text, this.contextWindowSize);
		featureCalculator.calculate();

		double[][] featureVectors = featureCalculator.getFeatureVectors();
		Instance[] instances = new SparseInstance[featureVectors.length];
		for (int i = 0; i < featureVectors.length; i++)
		{
			instances[i] = new SparseInstance(1, featureVectors[i]);
		}

		return instances;
	}
	
	private Map<String, Integer> convertToAttributeIndex()
	{
		Map<String, Integer> attributeToIndex = new HashMap<String, Integer>();
		for (int attributeIndex = 0; attributeIndex < attributes.size(); attributeIndex++)
		{
			attributeToIndex.put(((Attribute) attributes.elementAt(attributeIndex)).name(), attributeIndex);
		}
		return attributeToIndex;
	}
	
	public Instances getDataSet()
	{
		return dataSet;
	}
	
}
