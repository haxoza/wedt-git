package pl.haxoza.wedt;

public class Config
{
	public static final int FEATURE_WIDE_WINDOW_HALF_SIZE = 50; 
	
	protected static Config instance;
		
	protected Config() { /* empty */ }
	
	public static Config getInstance()
	{
		if (instance == null) {
			instance = new Config();
		}
		return instance;
	}
}
