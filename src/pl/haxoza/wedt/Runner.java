package pl.haxoza.wedt;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

import weka.classifiers.Evaluation;
import weka.core.FastVector;
import weka.core.Instances;

public class Runner
{
	public static void main(String[] args) throws Exception
	{
		CommandLine cmd = parseArgs(args);
		
		FilesLoader filesLoader = new FilesLoader(cmd.getArgs());
		ArrayList<File> files = filesLoader.loadFiles();

		System.out.println("Loaded " + files.size() + " data files.");

		CorpusBuilder corpusBuilder = new CorpusBuilder(files);
		
		if (cmd.hasOption("ciread"))
		{
			corpusBuilder.loadFromFile(cmd.getOptionValue("ciread"));
		} else
		{
			corpusBuilder.build();
			System.out.println("Unique stems to words ratio: " + corpusBuilder.getStemsToWordsRatio());
			if (cmd.hasOption("cilimit"))
			{
				corpusBuilder.limitCorpus(Integer.parseInt(cmd.getOptionValue("cilimit")));
			}
			if (cmd.hasOption("ciwrite"))
			{
				corpusBuilder.saveToFile(cmd.getOptionValue("ciwrite"));
			}
		}
		
		FastVector attributes = corpusBuilder.getCorpusAsAttributes();
		
		System.out.println("Corpus size after limit: " + attributes.size());
		
		int contextWindowSize = Config.FEATURE_WIDE_WINDOW_HALF_SIZE;
		if (cmd.hasOption("window"))
		{
			contextWindowSize = Integer.parseInt(cmd.getOptionValue("window"));
		}
		
		DataSetBuilder dataSetBuilder = new DataSetBuilder(files, attributes, contextWindowSize);
		dataSetBuilder.build();
		Instances dataSet = dataSetBuilder.getDataSet();
		System.out.println("Data set instances size:" + dataSet.numInstances());

		DataSetSplitter splitter = new DataSetSplitter(dataSet);
		splitter.split();

		SensesClassifier classifier = new SensesClassifier(splitter.getTrainSet());

		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();
		System.out.println("building classifier... " + dateFormat.format(date));
		classifier.build();
		System.out.println("done building classifier...");

		Evaluation evaluation = classifier.evaluate(splitter.getTestSet());

		printResults(splitter.getTestSet(), evaluation);
	}

	private static void printResults(Instances testSet, Evaluation evaluation) throws Exception
	{
		EvaluationAnalyzer analyzer = new EvaluationAnalyzer(evaluation, testSet);
		
		int numOfNothing = analyzer.getNumberOfNothing();
		System.out.println("#nothing: " + numOfNothing);
		
		System.out.println("Summary: " + evaluation.toSummaryString());
		System.out.println("Conf matrix: " + evaluation.toMatrixString());
		System.out.println("Monkey accuracy: " + (double)numOfNothing / (double)testSet.numInstances());
		System.out.println("FPR(Nothing): " + evaluation.falsePositiveRate(Sense.NOTHING.ordinal()));
		
		int numOfNonNothingCorrect = analyzer.getNumberOfNonNothingCorrect();
		System.out.println("Accuracy for non-nothing tags: " + (double)numOfNonNothingCorrect / (double)(testSet.numInstances() - numOfNothing));
		
		printAverageDistance(analyzer);
	}

	private static void printAverageDistance(EvaluationAnalyzer analyzer)
	{
		analyzer.calculatePrecisionForNotBestScore();
		double[] precisions = analyzer.getPrecisionForNotBestScore();
		
		System.out.println("Good if not best score:");
		int resultsNumber = Math.min(precisions.length, 20);
		for (int i = 1; i < resultsNumber; ++i)
		{
			System.out.format("%5d: %3.8f", i+1, precisions[i]);
			System.out.println();
		}
	}

	public static CommandLine parseArgs(String[] args)
	{
		Options options = new Options();
		options.addOption("window", true, "Feature context window size.");
		options.addOption("ciread", true, "Corpus index filename to load.");
		options.addOption("ciwrite", true, "Corpus index filename to write.");
		options.addOption("cilimit", true, "Minimal words occurence in corpus.");
		
		CommandLineParser parser = new PosixParser();
		CommandLine cmd = null;
		
		try
		{
			cmd = parser.parse(options, args);
		}
		catch (ParseException e)
		{
			System.err.println(e);
			System.exit(-1);
		}
		
		if (cmd.getArgs().length == 0)
		{
			System.err.println("No input data files... exiting.");
			System.exit(-1);
		}
		
		return cmd;
	}
}
