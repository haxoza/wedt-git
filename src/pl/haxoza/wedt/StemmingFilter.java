package pl.haxoza.wedt;

import java.util.ArrayList;
import java.util.List;

import weka.core.stemmers.SnowballStemmer;

public class StemmingFilter
{
	private List<String> words;
	private List<String> stems;

	public StemmingFilter(List<String> words)
	{
		this.words = words;
	}
	
	public void doStemming()
	{
		stems = new ArrayList<String>();
		weka.core.stemmers.Stemmer stemmer = new SnowballStemmer();
		for (String word : words)
		{
			stems.add((String) stemmer.stem(word));
		}
	}
	
	public List<String> getStems()
	{
		return stems;
	}
}
