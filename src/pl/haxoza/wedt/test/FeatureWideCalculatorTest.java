package pl.haxoza.wedt.test;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import pl.haxoza.wedt.FeatureWideCalculator;

public class FeatureWideCalculatorTest
{
	@Test
	public void testCalculate()
	{
		String[] words = { "a", "b", "c", "d", "e" };
		Map<String, Integer> index = convertToAttributeIndex(words);
		String[] text = { "a", "b", "c", "d", 
				          "a", "b", "c", "d", };
		int windowSize = 4;
		FeatureWideCalculator wide = new FeatureWideCalculator(index, Arrays.asList(text), windowSize);

		wide.calculate();

		double[][] featureVectors = wide.getFeatureVectors();
		double[][] expected = { { 1, 1, 1, 1, 0 }, { 2, 1, 1, 1, 0 }, { 2, 2, 1, 1, 0 }, { 2, 2, 2, 1, 0 },
								{ 1, 2, 2, 2, 0 }, { 1, 1, 2, 2, 0 }, { 1, 1, 1, 2, 0 }, { 1, 1, 1, 1, 0 } };
		for (int instance = 0; instance < featureVectors.length; ++instance)
		{
			assertArrayEquals(featureVectors[instance], expected[instance], 0.1);
		}
	}

	private void assertArrayEquals(double[] actual, double[] expected, double delta)
	{
		for (int i = 0; i < actual.length; ++i)
		{
			assertEquals(expected[i], actual[i], delta);
		}
	}

	private Map<String, Integer> convertToAttributeIndex(String[] attributes)
	{
		Map<String, Integer> index = new HashMap<String, Integer>();
		for (int i = 0; i < attributes.length; ++i)
		{
			index.put(FeatureWideCalculator.attributeNameForWord(attributes[i]), i);
		}
		return index;
	}
}
