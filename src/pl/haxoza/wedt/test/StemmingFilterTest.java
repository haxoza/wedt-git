package pl.haxoza.wedt.test;

import java.util.Arrays;
import static org.junit.Assert.*;

import org.junit.Test;

import pl.haxoza.wedt.StemmingFilter;

public class StemmingFilterTest
{
	@Test
	public void shouldGiveStems()
	{
		String[] text = "for example compressed and compression are both accepted as equivalent to compress".split(" ");
		StemmingFilter filter = new StemmingFilter(Arrays.asList(text));
		
		filter.doStemming();
		
		String[] expected = "for exampl compress and compress ar both accept a equival to compress".split(" ");
		assertArrayEquals(null, expected, filter.getStems().toArray());
	}
}
