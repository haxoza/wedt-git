package pl.haxoza.wedt.test;

import static org.junit.Assert.assertArrayEquals;

import java.io.FileInputStream;
import java.io.IOException;

import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;

import org.junit.Test;

public class OpenNLPTests
{
	@Test
	public void tokenizing() throws IOException
	{
		//given
		TokenizerModel model = new TokenizerModel(new FileInputStream("res/en-token.bin"));
		TokenizerME tokenizer = new TokenizerME(model);
		String sentence = "Pierre Vinken, 61 years old, will join the board as a nonexecutive director Nov. 29. Mr. Vinken is chairman of Elsevier N.V., the Dutch publishing group.";
		//when
		String[] tokenized = tokenizer.tokenize(sentence);
		//then
		String[] expected = "Pierre Vinken , 61 years old , will join the board as a nonexecutive director Nov. 29 . Mr. Vinken is chairman of Elsevier N.V. , the Dutch publishing group .".split(" ");
		assertArrayEquals(expected, tokenized);
	}
	
	@Test
	public void posTagging() throws IOException
	{
		//given
		POSModel posModel = new POSModel(new FileInputStream("res/en-pos-maxent.bin"));
		POSTaggerME tagger = new POSTaggerME(posModel);
		TokenizerModel tokenizerModel = new TokenizerModel(new FileInputStream("res/en-token.bin"));
		TokenizerME tokenizer = new TokenizerME(tokenizerModel);
		String sentence = "Pierre Vinken, 61 years old, will join the board as a nonexecutive director Nov. 29. Mr. Vinken is chairman of Elsevier N.V., the Dutch publishing group.";
		//when
		String[] tagged = tagger.tag(tokenizer.tokenize(sentence));
		//then
		String[] expected = "NNP NNP , CD NNS JJ , MD VB DT NN IN DT JJ NN NNP CD . NNP NNP VBZ NN IN NNP NNP , DT JJ NN NN .".split(" ");
		assertArrayEquals(expected, tagged);
	}
}
