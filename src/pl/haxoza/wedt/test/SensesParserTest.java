package pl.haxoza.wedt.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

import pl.haxoza.wedt.Sense;
import pl.haxoza.wedt.SensesParser;

public class SensesParserTest
{

	@Test
	public void testSingleWord()
	{
		SensesParser sensesParser = new SensesParser("haxoza");
		
		sensesParser.parse();
		
		ArrayList<String> words = sensesParser.getWords();
		String[] excepted = {"haxoza"};
		assertEquals(Arrays.asList(excepted), words);
	}
	
	@Test
	public void testFewWords()
	{
		SensesParser sensesParser = new SensesParser("haxoza is the best");

		sensesParser.parse();
		
		ArrayList<String> words = sensesParser.getWords();
		String[] excepted = {"haxoza", "is", "the", "best"};
		assertEquals(Arrays.asList(excepted), words);
	}
	
	@Test
	public void testRemoveHtmlTags()
	{
		SensesParser sensesParser = 
				new SensesParser("<b>haxoza</b> is <font size=3>the</font> best");

		sensesParser.parse();
		
		ArrayList<String> words = sensesParser.getWords();
		String[] excepted = {"haxoza", "is", "the", "best"};
		assertEquals(Arrays.asList(excepted), words);
	}
	
	@Test
	public void testRemoveSenseTags()
	{
		SensesParser sensesParser = 
				new SensesParser("haxoza [email]haxoza@haxoza.pl[/email] email");

		sensesParser.parse();
		
		ArrayList<String> words = sensesParser.getWords();
		String[] excepted = {"haxoza", "haxoza@haxoza.pl", "email"};
		assertEquals(Arrays.asList(excepted), words);
	}

	@Test
	public void testNothingSense()
	{
		SensesParser sensesParser = 
				new SensesParser("haxoza best");
		
		sensesParser.parse();
		
		ArrayList<Sense> senses = sensesParser.getSenses();
		Sense[] excepted = {Sense.NOTHING, Sense.NOTHING};
		assertEquals(Arrays.asList(excepted), senses);
	}
	
	@Test
	public void testExtractEmailSense()
	{
		SensesParser sensesParser = 
				new SensesParser("haxoza [email]haxoza@haxoza.pl[/email] email");
		
		sensesParser.parse();
		
		ArrayList<Sense> senses = sensesParser.getSenses();
		Sense[] excepted = {Sense.NOTHING, Sense.EMAIL, Sense.NOTHING};
		assertEquals(Arrays.asList(excepted), senses);
	}
	
	@Test
	public void testExtractEmailAndPhoneSense()
	{
		SensesParser sensesParser = 
				new SensesParser("[email]haxoza@haxoza.pl[/email] email [phone]123[/phone]");
		
		sensesParser.parse();
		
		ArrayList<Sense> senses = sensesParser.getSenses();
		Sense[] excepted = {Sense.EMAIL, Sense.NOTHING, Sense.PHONE};
		assertEquals(Arrays.asList(excepted), senses);
	}
	
	@Test
	public void testExtractSenseWithFakeTag()
	{
		SensesParser sensesParser = 
				new SensesParser("this is [pdf]"); // this, is, [pdf, ]

		sensesParser.parse();
		
		ArrayList<Sense> senses = sensesParser.getSenses();
		Sense[] excepted = {Sense.NOTHING, Sense.NOTHING, Sense.NOTHING, Sense.NOTHING};
		assertEquals(Arrays.asList(excepted), senses);
	}
	
	@Test
	public void testDeprecatedNestedTags()
	{
		SensesParser sensesParser = 
				new SensesParser("[contactinfo] haxoza [email]haxoza@haoxza.pl[/email][/contactinfo]");
		
		sensesParser.parse();
		
		ArrayList<Sense> senses = sensesParser.getSenses();
		Sense[] excepted = {Sense.NOTHING, Sense.EMAIL};
		assertEquals(Arrays.asList(excepted), senses);
	}
	
	@Test
	public void testDeprecatedPicTag()
	{
		SensesParser sensesParser = 
				new SensesParser("haxoza [pic]<IMAGE src=\"defaul3.jpg\" alt=\"\"/>[/pic] best");
		
		sensesParser.parse();
						
		ArrayList<Sense> senses = sensesParser.getSenses();
		Sense[] excepted = {Sense.NOTHING, Sense.NOTHING};
		assertEquals(Arrays.asList(excepted), senses);
	}
	
}
