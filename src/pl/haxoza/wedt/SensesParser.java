package pl.haxoza.wedt;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;

public class SensesParser
{

	private Scanner scanner;
	private ArrayList<String> words;
	private ArrayList<Sense> senses;
	
	private static final int END_SIGN_GROUPE = 1;
	private static final int RAW_SENSE_GROUPE = 2;
	private static final String END_SIGN = "/";
	private static final String TAG_REGEX = "\\[(/?)(\\w+)]";
	private static final String HTML_REGEX = "\\<.*?>";

	public SensesParser(String string)
	{
		this.scanner = new Scanner(string);
	}

	public SensesParser(File file)
	{
		try
		{
			this.scanner = new Scanner(file);
		} catch (FileNotFoundException e)
		{
			// impossible
			e.printStackTrace();
		}
	}

	public void parse()
	{
		this.words = new ArrayList<String>();
		this.senses = new ArrayList<Sense>();
		
		TokenizerModel model = null;
		try
		{
			model = new TokenizerModel(new FileInputStream("res/en-token.bin"));
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		TokenizerME tokenizer = new TokenizerME(model);
		Stack<Sense> senseStack = new Stack<Sense>();
		senseStack.push(Sense.NOTHING);
		
		while (scanner.hasNextLine())
		{
			String line = scanner.nextLine();
			line = removeHtmlTags(line);

			int startIndex = 0;
			Pattern pattern = Pattern.compile(TAG_REGEX);
			Matcher matcher = pattern.matcher(line);
			
			while (matcher.find())
			{
				if (Sense.isSense(matcher.group(RAW_SENSE_GROUPE)))
				{
					addWordsAndSense(tokenizer.tokenize(line.substring(startIndex, matcher.start())), senseStack.lastElement());
					pushOrPopSenseFromStack(senseStack, matcher);
					startIndex = matcher.end();
				}
			}
			
			addWordsAndSense(tokenizer.tokenize(line.substring(startIndex)), senseStack.lastElement());
		}
	}

	private void pushOrPopSenseFromStack(Stack<Sense> senseStack,
			Matcher matcher)
	{
		String rawSense = matcher.group(RAW_SENSE_GROUPE);
		Sense sense = Sense.fromRaw(rawSense);
		
		if (!sense.isDeprecated())
		{
			if (matcher.group(END_SIGN_GROUPE).equals(END_SIGN))
			{
				senseStack.pop();
			} 
			else
			{
				senseStack.push(sense);
			}
		}
	}
	
	private void addWordsAndSense(String[] rawWords, Sense sense)
	{
		words.addAll(Arrays.asList(rawWords));
		senses.addAll(Collections.nCopies(rawWords.length, sense));
	}

	private String removeHtmlTags(String line)
	{
		return line.replaceAll(HTML_REGEX,"");
	}

	public ArrayList<String> getWords()
	{
		return words;
	}
	
	public ArrayList<Sense> getSenses()
	{
		return senses;
	}

}
