package pl.haxoza.wedt;

import java.util.Arrays;

import weka.core.FastVector;

public enum Sense
{
	// special
	NOTHING,	 
	// annotation tags
	ADDRESS, 
	AFFILIATION, 
	BSDATE, 
	BSMAJOR, 
	BSUNIV, 
	EMAIL, 
	FAX, 
	MSDATE, 
	MSMAJOR, 
	MSUNIV, 
	PHDDATE, 
	PHDMAJOR, 
	PHDUNIV, 
	PHONE, 
	POSITION,
	PIC,
	// block annotation tags
	CONTACTINFO,
	INTRODUCTION,
	EDUCATION,
	PUBLICATION,
	RESINTERESTS;
	
	final static Sense[] DEPRECATED = { PIC, CONTACTINFO, INTRODUCTION,
			EDUCATION, PUBLICATION, RESINTERESTS };
	
	public static boolean isSense(String rawSense)
	{
		try
		{
			Sense.valueOf(rawSense.toUpperCase());
			return true;
		}
		catch (IllegalArgumentException e) {
			return false;
		}
	}

	public static Sense fromRaw(String rawSense)
	{
		return Sense.valueOf(rawSense.toUpperCase());
	}

	public boolean isDeprecated()
	{
		return Arrays.asList(DEPRECATED).contains(this);
	}

	public static FastVector allSenses()
	{
		FastVector result = new FastVector();
		Sense[] senses = values();
		for (int i = 0; i < senses.length; i++)
		{
			if (!senses[i].isDeprecated())
			{
				result.addElement(senses[i].toString());
			}
		}
		return result;
	}
}
