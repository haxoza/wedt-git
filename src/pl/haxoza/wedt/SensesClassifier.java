package pl.haxoza.wedt;

import weka.classifiers.Evaluation;
import weka.classifiers.functions.SMO;
import weka.core.Instances;

public class SensesClassifier
{
	private final Instances trainSet;
	private SMO svm;

	public SensesClassifier(Instances trainSet)
	{
		this.trainSet = trainSet;
	}
	
	public void build() throws Exception
	{
		svm = new SMO();
		svm.buildClassifier(trainSet);
	}
	
	public Evaluation evaluate(Instances testSet) throws Exception
	{
		Evaluation evaluation = new Evaluation(trainSet);
		evaluation.evaluateModel(svm, testSet);
		return evaluation;
	}
}
