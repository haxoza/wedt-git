package pl.haxoza.wedt;

import java.util.Arrays;
import java.util.Comparator;

import weka.classifiers.Evaluation;
import weka.classifiers.evaluation.NominalPrediction;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

public class EvaluationAnalyzer
{
	private Evaluation evaluation;
	private Instances testSet;
	
	private double[] precisionForNotBestScore;
	
	public EvaluationAnalyzer(Evaluation evaluation, Instances testSet)
	{
		this.evaluation = evaluation;
		this.testSet = testSet;
		this.precisionForNotBestScore = null;
	}
	
	public void calculatePrecisionForNotBestScore()
	{
		FastVector predictions = evaluation.predictions();
		double nothing = 0;
		precisionForNotBestScore = new double[predictions.size()];
		int[] precisionCounters = new int[predictions.size()];
		
		for (int predictionIndex = 0; predictionIndex < predictions.size(); predictionIndex++)
		{
			NominalPrediction nominalPrediction = (NominalPrediction) predictions.elementAt(predictionIndex);
			final double[] distribution = nominalPrediction.distribution();
			Integer [] indexes = new Integer[distribution.length];
			
			for (int i = 0; i < indexes.length; i++)
			{
				indexes[i] = i;
			}
			
			Arrays.sort(indexes, new Comparator<Integer>()
			{
				@Override
				public int compare(Integer o1, Integer o2)
				{
					Double d1 = distribution[o1];
					Double d2 = distribution[o2];
					return -d1.compareTo(d2);
				}
			});
			
			Instance instance = testSet.instance(predictionIndex);
			int realClass = (int) instance.value(instance.classIndex());
			
			for (int classIndexInResult = 0; classIndexInResult < indexes.length; ++classIndexInResult)
			{
				if (indexes[classIndexInResult].intValue() == realClass)
				{
					if (realClass != Sense.NOTHING.ordinal())
					{
						++precisionCounters[classIndexInResult];
					}
					else
					{
						++nothing;
					}
					break;
				}
			}
		}
		
		for (int i = 0; i < precisionForNotBestScore.length; ++i)
		{
			precisionForNotBestScore[i] = (100 * ((double)precisionCounters[i])  / (predictions.size() - nothing));
		}
	}
	
	public int getNumberOfNothing()
	{
		int numOfNothing = 0;
		for (int i = 0; i < testSet.numInstances(); ++i)
		{
			Instance curr = testSet.instance(i);
			if ((int)curr.classValue() == Sense.NOTHING.ordinal())
			{
				++numOfNothing;
			}
		}
		return numOfNothing;
	}
	
	public int getNumberOfNonNothingCorrect() {
		int numOfNonNothingCorrect = 0;
		FastVector predictions = evaluation.predictions();
		for (int i = 0; i < predictions.size(); ++i)
		{
			NominalPrediction prediction = (NominalPrediction) predictions.elementAt(i);
			if ((int)prediction.actual() != Sense.NOTHING.ordinal() && 
				prediction.predicted() == prediction.actual())
			{
				++numOfNonNothingCorrect;
			}
		}
		return numOfNonNothingCorrect;
	}
	
	public double[] getPrecisionForNotBestScore()
	{
		return precisionForNotBestScore;
	}
}
