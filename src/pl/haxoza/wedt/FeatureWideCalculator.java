package pl.haxoza.wedt;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FeatureWideCalculator
{
	private final List<String> text;
	private final Map<String, Integer> attributeNameToIndex;
	private double[][] featureVectors;
	private int windowSize;

	public FeatureWideCalculator(final Map<String, Integer> attributeNameToIndex, final List<String> text, final int windowSize)
	{
		this.attributeNameToIndex = attributeNameToIndex;
		this.text = text;
		this.windowSize = windowSize;
		initEmptyFeatureVectors();
	}
	
	public void calculate()
	{
		for (int wordIndex = 0; wordIndex < text.size(); ++wordIndex)
		{
			List<String> window = getWindow(wordIndex);
			calculateFeatureVector(wordIndex, window);
		}
	}
	
	private void calculateFeatureVector(int wordIndex, List<String> window)
	{
		for (String word : window)
		{
			if (attributeNameToIndex.containsKey(attributeNameForWord(word)))
			{
				++featureVectors[wordIndex][attributeNameToIndex.get(attributeNameForWord(word))];
			}
		}
	}

	private List<String> getWindow(final int index)
	{
		int leftEdge = countLeftEdge(index);
		int rightEdge = countRightEdge(index);
		List<String> leftWindow = text.subList(leftEdge, index);
		List<String> rightWindow = text.subList(index + 1, rightEdge);
		List<String> window = new ArrayList<String>(leftWindow);
		window.addAll(rightWindow);
		return window;
	}
	
	private int countLeftEdge(final int index)
	{
		int leftEdge = index - windowSize;
		return leftEdge >= 0 ? leftEdge : 0;
	}
	
	private int countRightEdge(final int index)
	{
		int rightEdge = index + windowSize + 1;
		int size = text.size();
		return rightEdge < size ? rightEdge : size;
	}

	private void initEmptyFeatureVectors()
	{
		featureVectors = new double[text.size()][attributeNameToIndex.size()]; 
	}
	
	public static String attributeNameForWord(String word)
	{
		return "#" + word;
	}
	
	public static String wordFromAttributeName(String attr)
	{
		return attr.substring(1);
	}

	public double[][] getFeatureVectors()
	{
		return featureVectors;
	}
}
