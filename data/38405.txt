<IMAGE src="images/index_01.gif" alt=""/> <IMAGE src="images/index_02.jpg" alt=""/> <IMAGE src="images/index_03.gif" alt=""/>

<IMAGE src="images/index_04.gif" alt=""/>

<IMAGE src="images/head_01.jpg" alt=""/> <IMAGE src="images/head_02.jpg" alt=""/>

<IMAGE src="images/index_06.gif" alt=""/>

<IMAGE src="images/index_07.gif" alt=""/>

Home Benefits FAQs Team 1031 Exchange Properties Partners Contact Us

<IMAGE src="images/index_09.gif" alt=""/>



<b>DINESH GUPTA, MANAGING MEMBER</b>

[pic]<IMAGE src="images/dinesh.jpg" alt=""/>[/pic]

[contactinfo]E-mail: [email]dgupta@firstguardiangroup.com[/email][/contactinfo]

[introduction]<b>Dinesh Gupta</b> is a [position]Managing Member and co-founder[/position] of [affiliation]First Guardian Group[/affiliation] and focuses on [interests]transaction structuring and financing[/interests], [interests]strategic planning[/interests], [interests]investor reporting[/interests], [interests]asset and portfolio management[/interests], and [interests]investment selection and disposition[/interests].

Mr. Gupta has directly been involved in the selection and management of over $85M of retail and commercial properties on behalf of Tenants-in-Common and 1031 Exchange Investors. He has developed numerous financing relationships with top tier banks and institutions that assist the firm in providing financial and due diligence resources for the firm's real estate acquisitions.

Mr. Gupta has extensive experience in managing investment funds on behalf of individuals and institutions. Prior to founding First Guardian Group, he co-founded SVC Ventures, LLC and D&B Venture Management that collectively managed investments of over $50M both on its own account and on behalf of numerous high net worth individuals. Mr. Gupta was responsible for developing numerous banking and financing relationships that resulted in over $1 billion of co-investment in his firms' portfolio assets.

From 1978 to 1999, Mr. Gupta was Business Development Manager at Raychem Corporation (acquired by Tyco International). He led an aggressive acquisition team whose efforts resulted in six acquisitions by Tyco and contributed to sales growth from $270M to approximately $500M per year. Mr. Gupta also led global projects as General Manager with full P&L responsibility of a worldwide group of 60 people.

Mr. Gupta holds an MBA from the [msuniv]University of Santa Clara[/msuniv] and a Bachelor's Degree in [bsmajor]Mechanical Engineering[/bsmajor] from the [bsuniv]University of Delhi[/bsuniv].[/introduction]

Copyright ?2003 - 2005 First Guardian Group | Site by Hire Matt Web Design

<IMAGE src="images/index_12.gif" alt=""/>

<IMAGE src="images/index_13.gif" alt=""/> <IMAGE src="images/index_14.gif" alt=""/> <IMAGE src="images/index_15.gif" alt=""/>
