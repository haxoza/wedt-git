<IMAGE src="../Images/Corner.gif" alt="Skip Navigation Links"/><IMAGE src="../Images/Banner.gif" alt="UW - Neuroscience Training Program"/>

<IMAGE src="../Images/Program.gif" alt="Program"/><IMAGE src="../Images/Research.gif" alt="Research"/><IMAGE src="../Images/Admissions.gif" alt="Admissions"/><IMAGE src="../Images/Resources.gif" alt="Resources"/><IMAGE src="../Images/Events.gif" alt="Events"/><IMAGE src="../Images/Undergrads.gif" alt="Undergrads"/><IMAGE src="../Images/University.gif" alt="University"/>



[contactinfo][pic]<IMAGE src="student-gifs/HAI.JPG" alt="Hai Chen "/>[/pic]Hai Chen

E-mail: [email]h.chen@neurosurg.wisc.edu[/email][/contactinfo]


Research Project:

My project is the role of [interests]NKCC1[/interests] in ischemic cell death in CNS. NKCC1 transports a net influx of Na+, K+, and Cl- in cells under physiological conditions and maintains the ion concentration in the cell. Using knock out mice and pharmacological methods, I am looking at the role NKCC1 could play in cell swelling, the alteration of ion homeostasis in cerebral ischemia, and neuron damage. I am also looking at the possible relationship of excitatory amino acid glutamate release and function of NKCC1 in the cell during the ischemic cell death.


[publication]Abstracts and Publications:

  * Chen, H. and D. Sun. 2005. The role of Na-K-Cl coransporter in cerebral ischemia. Neurol. Res. 27: 280-286.

  * Chen, H., J. Luo, D.B. Kintner, G.E. Shull, and D. Sun. 2005. Na(+)-dependent chloride transporter (NKCC1)-null mice exhibit less gray and white matter damage after focal cerebral ischemia. J. Cereb. Blood Flow Metab. 25: 54-66.

  * Luo, J., H. Chen, D.B. Kintner, G.E. Shull, and D. Sun. 2005. Decreased neuronal death in Na+/H+ exchanger isoform 1-null mice after in vitro and in vivo ischemia. J. Neurosci. 25: 11256-11268.

  * Luo, J., H. Chen, D.B. Kintner, G.E. Shull, and D. Sun. 2004. A role of Na+/H+ exchanger isoform 1 in ischemic neuronal damage. Soc. Neurosci. Abstr.

  * Chen, H., Y. Yan, G. E. Shull, and D. Sun. 2003. Decreased infarction and edema in NKCC1 mutant mice following focal cerebral ischemia. Soc. Neurosci. Abstr.[/publication]

<IMAGE src="../spacer.gif" alt=""/> <IMAGE src="../spacer.gif" alt=""/> <IMAGE src="../spacer.gif" alt=""/>

[contactinfo][address]<font size=2>7225 Medical Sciences Center
1300 University Avenue
</font><font size=2>Madison, WI 53706-1532[/address]

</font><font size=2>Tel: [phone](608) 262-4932[/phone] </font>[/contactinfo]

UW Home Graduate School Site Map

Copyright ?2003 The Board of Regents of the University of Wisconsin System
Page Created June 3, 2003 | Last Updated May 19, 2006
Question or Comments, Please Contact ntp@mhub.neuroscience.wisc.edu
