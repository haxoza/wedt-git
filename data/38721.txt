[pic]<IMAGE src="echigo.jpg" alt="Tomio ECHIGO"/>[/pic]
<b><font size=4>Tomio Echigo</font></b>

  * E-mail



go to Japanese

go to YAGI Lab.

go to ISIR

go to OSAKA UNIVERSITY



[contactinfo]Tomio Echigo<b>[position]Visiting Professor[/position]
[affiliation]The Institute of Scientific and Industrial Research, Osaka University[/affiliation]
Ph. D. [phddate]2003[/phddate], [phduniv]Osaka University[/phduniv]</b>

[address]8-1, Mihogaoka, Ibaraki, Osaka, 567-0047 Japan.[/address]
TEL: [phone]+81-6-6879-8421[/phone]
E-mail: [email]echigoam.sanken.osaka-u.ac.jp[/email][/contactinfo]

[education]<b><font size=4>EDUCATION</font></b>
      Ph. D. [phdmajor]Engineering Science[/phdmajor], [phduniv]Osaka University[/phduniv], [phddate]03/2003[/phddate]

      Master of Engineering, [msmajor]Electrical Engineering[/msmajor], [msuniv]University of Osaka Prefecture[/msuniv], [msdate]03/1982[/msdate]

      Bachelor of Engineering, [bsmajor]Electrical Engineering[/bsmajor], [bsuniv]University of Osaka Prefecture[/bsuniv], [bsdate]03/1980[/bsdate][/education]

[introduction]<b><font size=4>EMPLOYMENT</font></b>
      06/2003 - present [position]Visiting Professor[/position], The [affiliation]Institute of Scientific and Industrial Research, Osaka University[/affiliation]

      04/1991 - 05/2003 Advisory researcher, IBM Research, Tokyo Research Laboratory

      01/1987 - 03/1991 Researcher, IBM Research, Tokyo Research Laboratory

      04/1982 - 12/1986 Manufacturing Engineer, Yasu Plant, IBM Japan Ltd.
[/introduction]
<b><font size=4>RESEARCH ACTIVITIES</font></b>

  * Medical Imaging Technology: Visibility Improvement of Medical Images and Diagnosis Assistance

  * Gait Recognition

  * Image Based Sensing: Multiple Compound Mirrors, Laser Range Sensor, and

  * Intelligent Media
    Personalized Video Digest: MPEG-7 authoring system and generation of video digest
    Video Enrichment: Object based video annotation scheme and search
    Region Segmentation: Unsupervised Region Segmentation of Colored Texture Images by Using Multiple GMRF Models and Region Based Image Coding


<b><font size=4>PUBLICATIONS AND PATENTS</font></b>

  * Publications

  * Patents
