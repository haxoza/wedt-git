Welcome to the home page of
Ajay K. Gupta

[pic]<IMAGE src="Ajay_Final5.JPG" alt=""/>[/pic]<IMAGE src="SBLOGO.JPG" alt=""/> <IMAGE src="Ajay_car.jpg" alt=""/>

RESUME

[introduction]I am currently employed as [position]Sr. Programmer/Analyst[/position] with the [affiliation]Department of Computer Science at The State University of New York[/affiliation] at Stony Brook. I am also employed as an Adjunct Faculty. I am continuing my research in Cluster Technologies, and in [affiliation]Network and Systems Security at the Secure Systems Laboratory of the Stony Brook University[/affiliation].

Prior to 1996, I was employed as Deputy Director, Planning and Development Division, Indira Gandhi National Open University (Govt. of India), New Delhi, India[/introduction]

[education]<b>EDUCATION</b>

Doctorate: PhD in [phdmajor]Computer Science[/phdmajor] (Network and Systems Security), [phduniv]State University of New York at Stony Brook[/phduniv], NY.
Graduate: MS in [msmajor]Computer Science[/msmajor], [msuniv]State University of New York at Stony Brook[/msuniv], NY.
Graduate: MS in [msmajor]Electrical & Computer Engineering[/msmajor] [msuniv]University of Massachusetts[/msuniv], Amherst, MA.
Undergraduate: BE in [bsmajor]Electronics Engineering[/bsmajor], [bsuniv]BITS[/bsuniv], Pilani, INDIA.
High School: St. Xavier's High School, Ahmedabad, INDIA.[/education]

[resinterests]<b>Current Research:</b> [interests]Clusters and Grid Computing[/interests]: [interests]Research and Development of high performance computing clusters[/interests], [interests]parallel programming[/interests], [interests]distributed architectures[/interests], and [interests]application design and development[/interests], with emphasis on [interests]computer security[/interests].

Previous Research areas:

1. [interests]Network and Systems Security[/interests]: Intrusion Detection Systems, Development of Simulation Testbed using Specification-based anomaly detection technique for detection of application based attacks, as applied to various client-server & peer-to-peer based technologies.

Doctoral Dissertation: The dissertation research work, "Simulation and Detection of Self-Propagating Worms and Viruses", develops a simulation testbed using a new method that blends specification-based and anomaly-based intrusion detection techniques. The technique is successfully applied to Network Intrusion Detection systems, Worms, Email Viruses and other server attacks.

2. [interests]Dynamic Routing in Low Earth Orbit Satellites[/interests]
3. [interests]Indirect JAVA - Applet Security[/interests]
4. [interests]Low Delay Video Encoding using MPEG with 4-modem Link for Video-Conferencing[/interests].

Master's Dissertation: [interests]Design and Implementation of a short channel MOSFET transistor model into MOTIS-C[/interests], a MOS Timing Simulator, and comparison of the results with experimental values and those obtained from SPICE.2.G circuit simulator program.[/resinterests]

Hobbies/Interests:
Outdoor sports like Lawn Tennis, Skiing, Ice Skating, Camping, Cricket etc.
Indoor Games like Table Tennis, Chess, Bridge and other card games etc.
General Reading and Travelling.

[contactinfo]Personal Details:

Department/Lab Address :

Current Office/Work Address:

Home Address:

[address]Security Lab,
2311, CSE Building,
State Univ of New York,
Stony Brook, NY 11794[/address]

[address]Department of Computer Science ,
2305, CSE Building,
State Univ of New York,
Stony Brook, NY 11794[/address]

[address]45, Cabriolet Lane,
Melville, NY 11747.[/address]

Phone : [phone](631)-632-8469[/phone]

Phone : [phone](631)-632-1537[/phone][/contactinfo]
