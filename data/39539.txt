<font size=3>

<IMAGE src="images/my-qmul-text.jpg" alt=""/>


[contactinfo]Christof Monz [position]Lecturer[/position]
[affiliation]Department of Computer Science
Queen Mary, University of London[/affiliation]
[address]Mile End Road
London, E1 4NS
United Kingdom[/address]

Office: [address]324[/address]
E-mail: [email]"first name" at dcs.qmul.ac.uk[/email]
Phone: [phone]+44 (0)20-7882-7978[/phone]
Fax: [fax]+44 (0)20-8980-6533[/fax][/contactinfo]

[pic]<IMAGE src="images/christof2small.jpg" alt="Yup, that's me."/>[/pic]

Here's my CV.


News Flash

  * I recently organized the HLT-NAACL 2006 Workshop on Statistical Machine Translation together with Philipp Koehn.


[publication]Publications<b>2006</b>

  *  Christof Monz. <b>Statistical Machine Translation and Cross-Language IR: QMUL at CLEF 2006</b>. In Working Notes for the CLEF 2006 Cross Language Evaluation Campaign, 2006. [ PDF ]

  *  Nizar Habash, Bonnie J. Dorr and Christof Monz. <b>Challenges in Building an Arabic-English GHMT System with SMT Components</b>. In Proceedings of the 7th Biennial Meeting of the Association for Machine Translation in the Americas (AMTA-06). Pages 56-65, 2006. [ PDF ]

  *  Philipp Koehn and Christof Monz. <b>Manual and Automatic Evaluation of Machine Translation between European Languages</b>. In Proceedings on the HTL-NAACL Workshop on Statistical Machine Translation. Pages 102-121, 2006. [ PDF ]

  *  Philipp Koehn and Christof Monz (Eds.). <b>Proceedings of the HLT-NAACL Workshop on Statistical Machine Translation</b> [ Online Proceedings ]

<b>2005</b>

  *  Necip Fazil Ayan, Bonnie J. Dorr, Christof Monz. <b>Alignment Link Projection using Transformation-Based Learning</b>. In Proceedings of HLT-EMNLP 2005. Pages 185-192, 2005. [ PDF ]

  *  Necip Fazil Ayan, Bonnie J. Dorr, Christof Monz. <b>NeurAlign: Combining Word Alignments Using Neural Networks</b>. In Proceedings of HLT-EMNLP 2005. Pages 65-72, 2005. [ PDF ]

  *  David Chiang, Adam Lopez, Nitin Madnani, Christof Monz, Philip Resnik and Michael Subotin. <b>The Hiero Machine Translation System: Extensions, Evaluation, and Analysis</b>. In HLT-EMNLP 2005. Pages 779-786, 2005. [ PDF ]

  *  David Zajic, Bonnie Dorr, Richard Schwartz, Christof Monz, Jimmy Lin. <b>A Sentence-Trimming Approach to Multi-Document Summarization</b>. In Proceedings of the HLT-EMNLP 2005 Workshop on Text Summarization. 2005. [ PDF ]

  *  Bonnie J. Dorr, Christof Monz, Stacy President and Richard Schwartz. <b>A Methodology for Extrinsic Evaluation of Text Summarization: Does ROUGE Correlate?</b> In Proceedings of the Association for Computational Linguistics Workshop on Intrinsic and Extrinsic Evaluation Measures for MT and/or Summarization. Pages 1-8, 2005. [ PDF ]

  *  Philipp Koehn, Joel Martin, Rada Mihalcea, Christof Monz and Ted Pedersen (Eds.) <b>Proceedings of the ACL Workshop on Building and Using Parallel Texts</b>. 2005. [ Online Proceedings ]

  *  Philipp Koehn and Christof Monz. <b>Shared Task: Statistical Machine Translation between European Languages</b>. In Proceedings of the Association for Computational Linguistics Workshop on Building and Using Parallel Texts. Pages 119-124, 2005. [ PDF ]

  *  Christof Monz and Bonnie J. Dorr. <b>Iterative Translation Disambiguation for Cross-Language Information Retrieval</b>. In Proceedings of the 28th Annual International ACM SIGIR Conference on Research and Development in Information Retrieval. Pages 520-527, 2005. [ PDF ]

<b>2004</b>

  * Christof Monz. <b>Minimal Span Weighting Retrieval for Question Answering</b>. In: Rob Gaizauskas, Mark Greenwood, and Mark Hepple (eds.) Proceedings of the SIGIR Workshop on Information Retrieval for Question Answering, p. 23-30, 2004. [ PDF ]

  * Vera Hollink, Jaap Kamps, Christof Monz, and Maarten de Rijke. <b>Monolingual Document Retrieval for European Languages</b>. Information Retrieval 7(1), p. 33-52. [ PDF ]

<b>2003</b>

  * Christof Monz. <b>From Document Retrieval to Question Answering</b>. PhD Thesis. University of Amsterdam, 2003. [ PDF ]

  * Jaap Kamps, Christof Monz, Maarten de Rijke, and Bökur Sigurbjörnsson. <b>Language-dependent and language-independent approaches to cross-lingual text retrieval.</b> In Carol Peters, Julio Gonzalo, Martin Braschler, and Michael Kluck, editors, Comparative Evaluation of Multilingual Information Access Systems, CLEF 2003, LNCS 3237, p. 152-165. Springer, 2004. [ PDF ]

  * Jaap Kamps, Christof Monz, Maarten de Rijke, and Börkur Sigurbjörnsson. <b>Monolingual document retrieval: English versus other European languages.</b> In Arjen P. de Vries, editor, Proceedings of the Fourth Dutch-Belgian Information Retrieval Workshop (DIR 2003), p. 35-39, 2003. [ PDF ]

  * Jaap Kamps, Christof Monz, Maarten de Rijke, and Börkur Sigurbjörnsson. <b>Approaches to Robust and Web Retrieval</b>. In: Proceedings of TREC 2003, p. 594-600. [ PDF ]

  * Valentin Jijkoun, Gilad Mishne, Christof Monz, Maarten de Rijke, Stefan Schlobach, and Oran Tsur. <b>The University of Amsterdam at the TREC 2003 Question Answering Track</b> In: Proceedings of TREC 2003, p. 586-593. [ PDF ]

  * Christof Monz. <b>Document Retrieval in the Context of Question Answering</b> In: F. Sebastiani (Ed.) Proceedings of the 25th European Conference on Information Retrieval Research (ECIR-03), Lecture Notes in Computer Science 2633, Springer, 2003, pages 571-579. [ PDF ]

<b>2002</b>

  *  Marco Aiello, Christof Monz, Leon Todoran and M. Worring. <b>Document Understanding for a Broad Class of Documents</b> International Journal of Document Analysis and Recognition 5(1), 2002, pages 1-16 [ PDF ]

  * Christof Monz, Jaap Kamps, and Maarten de Rijke. <b>The University of Amsterdam at TREC 2002</b> In E. M. Voorhees and D. K. Harman, editors The Eleventh Text REtrieval Conference (TREC 2002) National Institute of Standards and Technology [ PDF ]

  * Christof Monz and Maarten de Rijke. <b>Shallow Morphological Analysis in Monolingual Information Retrieval for Dutch, German and Italian.</b> In: Post-Conference Proceedings of the Cross Language Evaluation Forum Workshop (CLEF 2001), Springer Lecture Notes in Computer Science [ PDF ]

  * Christof Monz, Jaap Kamps, and Maarten de Rijke. <b>The University of Amsterdam at CLEF 2002</b> In: C. Peters, editor, Working Notes for the CLEF 2002 Workshop,pages 73-84 [ PDF ]

  * Jaap Kamps, Christof Monz, and Maarten de Rijke <b>Combining Morphological and Ngram Evidence for Monolingual Document Retrieval</b> In: M.-F. Moens, R. De Busser, D. Hiemstra, W. Kraaij, editors, Proceedings of the Third Dutch Information Retrieval Workshop (DIR 2002), pages 47-51 [ PDF ]

  * Jaap Kamps, Maarten Marx, Christof Monz, and Maarten de Rijke. <b>Exploiting Structure for Information Retrieval</b> In: M.-F. Moens, R. De Busser, D. Hiemstra, W. Kraaij, editors, Proceedings of the Third Dutch Information Retrieval Workshop (DIR 2002), pages 19-26
    [ PDF ]

<b>2001</b>

  * Christof Monz and Maarten de Rijke. <b>Tequesta: The University of Amsterdam's Texual Question Answering System</b> In: Proceedings of Tenth Text Retrieval Conference (TREC-10), 2001, pp. 513-522. [ PDF ]

  * Christof Monz and Maarten de Rijke. <b>University of Amsterdam at CLEF 2001</b> In: Working Notes for the Cross Language Evaluation Forum Workshop (CLEF 2001), 2001, ERCIM-01-W04, pp. 165-169. [ PDF ]

  * Christof Monz. <b>Document Fusion for Comprehensive Event Description</b> In: M. Maybury (ed.) Proceedings of the ACL 2001 Workshop on Human Language Technology and Knowledge Management, 2001. [ PDF ]

  * Christof Monz and Maarten de Rijke. <b>Light-Weight Entailment Checking for Computational Semantics</b> In: P. Blackburn and M. Kohlhase, editors, Proceedings Inference in Computational Semantics (ICoS-3), 2001, pages 59-72. [ PDF ]

  * Christof Monz and Maarten de Rijke. <b>Deductions with meaning.</b> In: M. Moortgat (ed.) Logical Aspects in Computational Linguistics (LACL 98), LNAI 2014, Springer, 2001, pp. 1-10. [ PDF ] Copyright ?by Springer

  * Leon Todoran, Marco Aiello, Christof Monz and Marcel Worring. <b>Logical Structure Detection for Heterogeneous Document Classes.</b> In: Proceedings of Document Recognition and Retrieval VIII, SPIE, 2001, pp. 99-110 [ PDF ]

<b>2000</b>

  * Christof Monz. <b>Computational Semantics and Information Retrieval.</b> In: J. Bos and M. Kohlhase (eds.) Proceedings of the 2nd Workshop on Inference in Computational Semantics (ICoS-2), 2000. pp. 1-5. [ PDF ]

  * Christof Monz, and Maarten de Rijke. <b>Inference in Computational Semantics.</b> Language and Computation 1(2). pp. 151-156, 2000. [ PDF ]

  * Marco Aiello, Christof Monz and Leon Todoran. <b>Combining Linguistic and Spatial Information for Document Analysis.</b> In: J. Mariani and D. Harman (Eds.) Proceedings of RIAO'2000 Content-Based Multimedia Information Access, CID, 2000. pp. 266-275. [ PDF ]

<b>1999</b>

  * Christof Monz. <b>Modeling Ambiguity in a Multi-Agent System.</b> In: P. Dekker (Ed.) Proceedings of the 12th Amsterdam Colloquium, Amsterdam, 1999. pp. 43-48. [ PDF ]

  * Carlos Areces, Christof Monz, Hans de Nivelle, and Maarten de Rijke. <b>The Guarded Fragment: Ins and Outs.</b> In: JFAK. Essays Dedicated to Johan van Benthem on the Occasion of his 50th Birthday, Vossiuspers, Amsterdam University Press, 1999. [ PDF ]

  * Christof Monz. <b>Contextual inference in Computational Semantics.</b> In: P. Bouquet, P. Brézillon, L. Serafini, M. Benerecetti, F. Castellani (Eds.) 2nd International and Interdisciplinary Conference on Modeling and Using Context (CONTEXT'99). LNAI 1688, Springer, 1999, pp. 242-255. [ PDF ] Copyright ?by Springer

  * Christof Monz and M. der Rijke (Eds.). <b>Proceedings of the First Workshop on Inference in Computational Semantics (ICoS-1).</b> Institute for Logic, Language and Computation, Amsterdam, 1999.

  * Christof Monz. <b>Computing presuppositions by contextual reasoning.</b> In: P. Brézillon, R. Turner, J-C. Pomerol and E. Turner (Eds.) Proceedings of the AAAI-99 Workshop on Reasoning in Context for AI Applications, AAAI Press, 1999, pp. 75-79. PDF ]

  * Christof Monz and Maarten de Rijke. <b>A tableau calculus for pronoun resolution.</b> In: N.V. Murray (ed.) Automated Reasoning with Analytic Tableaux and Related Methods, TABLEAUX'99, LNAI 1617, Springer, 1999, pages 247-262. [ PDF ] Copyright ?by Springer.

  * Christof Monz. <b>Review of A. Franz: Automatic ambiguity resolution in natural language processing: An empirical approach.</b> Journal of Logic, Language and Information 8(1), pp. 111-114, 1999. [ PDF ]

<b>1998</b>

  * Christof Monz and Maarten de Rijke. <b>Inference in natural language semantics.</b> In: Proceedings of Logical Aspects in Computational Linguistics (LACL'98). 1998. [ PDF ]

  * Christof Monz and Maarten de Rijke. <b>A resolution calculus for dynamic semantics.</b> In: J. Dix, L. F. del Cerro, and U. Furbach (eds.) Logics in Artificial Intelligence (JELIA'98). Lecture Notes in Artificial Intelligence 1489, Springer, 1998, pp. 184-198. [ PDF ] Copyright ?by Springer

  * Christof Monz and Maarten de Rijke. <b>Labeled resolution for discourse semantics.</b> In: Proceedings of the First International Workshop on Labelled Deduction (LD'98). Dept. of Computer Science, Freiburg, Germany, 1998. [ PDF ]

  * Christof Monz. <b>Dynamic semantics and underspecification.</b> In: H. Prade. Proceedings of the 13th European Conference on Artificial Intelligence (ECAI'98). John Wiley & Sons, 1998, pp. 201-202. [ PDF ]

  * Christof Monz. <b>Dynamic semantics and ambiguity.</b> In: I. Kruijff-Korbayov?(ed.): Proceedings of the 3rd ESSLLI Student Session (ESSLLI'98), 1998, pp. 212-222. [ PDF ]

  * Christof Monz and Maarten de Rijke. <b>A tableaux calculus for ambiguous quantification.</b> In: H. de Swart (ed.) Automated Reasoning with Analytic Tableaux and Related Methods, TABLEAUX'98, LNAI 1397, Springer, 1998, pp. 232-246. [ PDF ] Copyright ?by Springer[/publication]

<IMAGE src="http://nl.nedstatbasic.net/cgi-bin/nedstat.gif?name=stoffig" alt=""/> </font>
