<IMAGE src="/images/abakus35x35.gif" alt="Informatik-Logo"/>

Fakultät für Informatik - Technische Universität München

Lehrstuhl für Effiziente Algorithmen <IMAGE src="/images/tum-logo60x32_tr.gif" alt="TUM-Logo"/> <IMAGE src="/images/german.gif" alt="deutsch"/>


Sven Kosub

[contactinfo][pic]<IMAGE src="passphoto.jpg" alt=""/>[/pic]

eMail: [email]kosub@in.tum.de[/email]

vMail: [phone]+49-89-289-17730[/phone] (phone)

[fax]+49-89-289-17707[/fax] (fax)

pMail: [address]Institut für Informatik

Technische Universität München

Boltzmannstraße 3

D-85748 Garching b. München, Germany[/address]

fMail: [address]03.09.057 (I14), FMI Building[/address][/contactinfo]

<b>Office hours:</b> Friday, 10:30 to 11:30, and by appointment

<IMAGE src="wij_logo.gif" alt=""/> <IMAGE src="ADW_logo.jpg" alt=""/> <IMAGE src="wij_stoerer.gif" alt=""/>

Events

<IMAGE src="dot.gif" alt=""/> InnovaTUM Information Security Group - Coordinated Research and Education in Information Security, Technische Universität München

<IMAGE src="dot.gif" alt=""/> Algorithm of the Week - A Fakultätentag Informatik Initiative for Science Year 2006: Informatics Year

<IMAGE src="dot.gif" alt=""/> Ferien-Akademie Course on "Inside Google - Algorithmics of Search Engines", Sarentino Valley (South Tyrol, Italy), September 18-30, 2005.

<IMAGE src="dot.gif" alt=""/> Ferien-Akademie Course on "Polynomials - Efficient Algorithms and Applications", Sarentino Valley (South Tyrol, Italy), September 19 through October 1, 2004.

<IMAGE src="dot.gif" alt=""/> GI-Dagstuhl Seminar on Game-Theoretic Analyses of the Internet, IBFI Schloss Dagstuhl (Wadern, Germany), August 30 through September 3, 2004.

<IMAGE src="dot.gif" alt=""/> GI-Dagstuhl Seminar on Network Analysis, IBFI Schloss Dagstuhl (Wadern, Germany), April 12-16, 2004.

<IMAGE src="dot.gif" alt=""/> 50th GI-Workshop on Complexity Theory, Data Structures, and Efficient Algorithms, Technische Universität München, March 9, 2004.

[resinterests]Research

<IMAGE src="dot.gif" alt=""/> [interests]Complexity Theory and Algorithms[/interests]

<IMAGE src="dot.gif" alt=""/> [interests]Network Analysis and Discrete Network Dynamics[/interests]

<IMAGE src="dot.gif" alt=""/> [interests]Internet Algorithmics and Game Theory[/interests][/resinterests]

Teaching

<IMAGE src="dotwhite.gif" alt=""/> <b>Spring 2007</b> Fundamentals of Algorithms and Data Structures (Course)

<IMAGE src="dot.gif" alt=""/> <b>Fall 2006</b> Internet Algorithmics (Course)

Algorithm Engineering (Advanced Seminar)

Fundamentals of Local Self-Stabilization (Seminar)

<IMAGE src="dotwhite.gif" alt=""/> <b> Spring 2006</b> Fundamentals of Algorithms and Data Structures (Course)

Text Algorithms (Seminar)

Diploma Theses

<IMAGE src="dot.gif" alt=""/> Benjamin Hummel, Automata-based IP Packet Classification, June 2006

<IMAGE src="dot.gif" alt=""/> Tobias Haensse, Dynamic and Persistent Iterators for Spanning Trees, January 2006

<IMAGE src="dot.gif" alt=""/> Angelika Kneidl, Maintaining View in Dynamic Hierarchies, July 2005

<IMAGE src="dot.gif" alt=""/> Anna-Gwendolyn Huber, Elevator Control under Direct-Travel Conditions, April 2005

<IMAGE src="dot.gif" alt=""/> Zakaria Jabara, Experimental Analysis of Algorithms for Inferring Internet Hierarchies, March 2005

<IMAGE src="dot.gif" alt=""/> Melanie Liebl, Algorithmically Analyzing the Attractiveness of Pure-Strategy Nash Equilibria of Symmetric Congestion Games, January 2005

<IMAGE src="dot.gif" alt=""/> Georgios Mertzios, Improved Algorithms for the Constant-Excess Subgraph Problem and Applications, December 2004

Student Projects

<IMAGE src="dot.gif" alt=""/> (with Stefan Pfingstl) Zlatina Savova, Wrapper Integration in Automated Reference Generation, October 2004

<IMAGE src="dot.gif" alt=""/> Georgios Mertzios, Implementation of an Algorithm for Identifying Subgraphs with Constant Edge Excess, August 2004

<IMAGE src="dot.gif" alt=""/> Matthias Hanitzsch, Automated Generation of References from Electronic Journals, February 2003

Publications

Presentations

Curriculum Vitae

Sven Kosub, October/23/2006.
