Jan Broersen

[contactinfo]Contact Education Research Links

[pic]<IMAGE src="foto1.jpg" alt="Jan Broersen in the train"/>[/pic] <b>Postal address:</b>

J.M. Broersen[affiliation]Intelligent Systems Group
Department of Information and
Computing Sciences
Universiteit Utrecht[/affiliation]
[address]PO Box 80.089
3508 TB  UTRECHT
The Netherlands[/address]
<b>Email:</b>

[email]broersen@cs.uu.nl[/email]
<b>
Visiting address:</b>

[address]Room A122 (centrumgebouw Noord)
Padualaan 14, De Uithof
3584 CH UTRECHT
The Netherlands[/address]

<b>Numbers:</b>

Telephone: [phone]+31 30 2533193[/phone]
Fax: [fax]+31 30 251 3791[/fax][/contactinfo]

Additional information can be found on the department's page.




















Jan Broersen

Contact Education Research Links

<b>Courses:</b>

2006/2007

Inleiding Intelligente Systemen
Overdragen van de informatica
Logica voor AI
Filosofie van de Informatica

2005/2006

Inleiding Intelligente Systemen
Games and Agents
Logica voor AI

2004/2005

Softwareproject
Inleiding Intelligente Systemen
Logica voor AI

2003/2004

Overdragen van de informatica
Inleiding Intelligente Systemen
Logica voor AI

2002/2003

Model-based Reasoning
Programming in Prolog

<b>Master Projects:</b>


























Jan Broersen

Contact Education Research Links


[publication]2006<b>Embedding Alternating-time Temporal Logic in Strategic STIT Logic of Agency</b>, Jan Broersen, Andreas Herzig and Nicolas Troquard, Journal of Logic and Computation, 2006.

<b>A STIT-extension of ATL</b>, Jan Broersen, Andreas Herzig and Nicolas Troquard, Proceedings Tenth European Conference on Logics in Artificial Intelligence (JELIA'06), Lecture Notes in Artificial Intelligence 4160, 69-81, Springer, 2006.

Strategic Deontic Temporal Logic as a Reduction to ATL, with an Application to Chisholm's Scenario, Jan Broersen, Proceedings Eighth International Workshop on Deontic Logic in Computer Science (DEON'06), Lecture Notes in Computer Science 4048, Springer, 53-68, 2006.


2005Semantic Analysis of Chisholm's Paradox, Jan Broersen and Leendert van der Torre, Proceedings of the 17th Belgium-Netherlands Artificial Intelligence Conference, 2005.

<b>From Coalition Logic to STIT</b>, Jan Broersen, Andreas Herzig and Nicolas Troquard, Proceedings LCMAS 2005, Electronic Notes in Theoretical Computer Science.

Beliefs in Agent Implementation, Laurens Winkelhagen, Mehdi Dastani, and Jan Broersen, Proceedings DALT 2005, Lecture Notes in Computer Science 3904, Springer, 2006.
<b>
Beliefs, Obligations, Intentions and Desires as Components in an Agent Architecture</b>, Jan Broersen, Mehdi Dastani and Leendert van der Torre, International Journal of Intelligent Systems, volume 20, issue 9, 893-920, 2005


2004<b>On the Logic of `Being Motivated to Achieve Rho, before Delta'</b>, Jan Broersen, Proceedings Ninth European Conference on Logics in Artificial Intelligence (JELIA'04), Lecture Notes in Artificial Intelligence 3229, 334-346, Springer, 2004.

<b>Specifying Multiagent Organizations</b>, Leendert van der Torre, Joris Hulstijn, Mehdi Dastani, Jan Broersen, Proceedings Seventh International Workshop on Deontic Logic in Computer Science (DEON'04), Lecture Notes in Computer Science 3065, 243-257, Springer, 2004.

<b>Designing a Deontic Logic of Deadlines</b>, Jan Broersen, Frank Dignum, Virginia Dignum, John-Jules Meyer, Proceedings Seventh International Workshop on Deontic Logic in Computer Science (DEON'04), Lecture Notes in Computer Science 3065, 43-56, Springer, 2004.

<b>Meeting the Deadline: Why, When and How</b>, Frank Dignum, Jan Broersen, Virginia Dignum and John-Jules Meyer, Proceedings Third NASA-Goddard/IEEE Workshop on Formal Approaches to Agent-Based Systems (FAABS III), Lecture Notes in Computer Science 3228, 30-40, Springer, 2004.

<b>Action Negation and Alternative Reductions for Dynamic Deontic Logics</b>, Jan Broersen, Journal of Applied Logic, volume 2, issue 1, 153-168, 2004.


2003<b>What an Agent Ought To Do: a Review of John Horty's `Agency and Deontic Logic'</b>, Jan Broersen, Leendert van der Torre, AI and Law, volume 11, 45-61, 2003.

<b>Relativized Action Complement for Dynamic Logics</b>, J.M. Broersen, Advances in Modal Logic, volume 4, 51-70, King's College Publications 2003.

<b>BDIO-CTL: Obligations and the Specification of Agent Behavior</b>, J. Broersen, M. Dastani, and L. van der Torre,  In Proceedings of Eighteenth International Joint Conference on Artificial Intelligence (IJCAI2003), 1389--1390, 2003.

<b>Modal Action Logics for Reasoning about Reactive Systems</b>, Jan Broersen, PhD-thesis Vrije Universiteit Amsterdam, january 2003.


2002<b>Goal Generation in the BOID Architecture</b>, Jan Broersen, Mehdi Dastani, Joris Hulstijn and Leendert van der Torre, Cognitive Science Quarterly, Volume 2, Issue 3-4, 2002.

<b>Realistic Desires</b>, Jan Broersen and Mehdi Dastani and Leendert van der Torre, Journal of Applied Non-Classical Logics, Volume 12, No. 2, 2002.

<b>Trust and Commitment in Dynamic Logic</b>, Jan Broersen and Mehdi Dastani and Zisheng Huang and Leendert van der Torre, Proceedings of the First EurAsian Conference on Advances in Information and Communication Technology (EURASIA-ICT 2002), Lecture Notes in Computer Science 2510, 677-684, Springer, 2002.

<b>Logical Specification of Agent Architecture Components</b>, Jan Broersen and Mehdi Dastani and Leendert van der Torre, Proceedings of the 14th Belgium-Netherlands Artificial Intelligence Conference, 2002.

<b>A new action base for dynamic deontic logics</b>, J.M. Broersen, Pre-proceedings Sixth International Workshop on Deontic Logic in Computer Science (DEON'02), 2002.

<b>The mutual exclusion problem in reasoning about action and change</b> , J.M. Broersen and R.J. Wieringa and J.-J.Ch. Meyer, Pre-proceedings 9th International Workshop on Non-Monotonic Reasoning (NMR2002), editors: Salem Benferhat and Enrico Giunchiglia, 365-371, 2002.


2001<b>A Fixed-point Characterization of a Deontic Logic of Regular Action</b>, J.M. Broersen and R.J. Wieringa and J.-J.Ch. Meyer, Fundamenta Informaticae 48, special issue on deontic logic in computer science, november 2001.

<b>Resolving Conflicts between Beliefs, Obligations, Intentions and Desires</b>,  J. Broersen, M.Dastani and L. van der Torre, Symbolic and Quantitative Approaches to Reasoning and Uncertainty. Proceedings of the ECSQARU'01, Lecture Notes in Computer Science 2143, 568-579, Springer, 2001.

<b>The BOID architecture</b>, J. Broersen and M. Dastani and Z. Huang and J. Hulstijn and L. van der Torre, Proceedings of  the fifth international conference on autonamous agents (AA2001), 9-16, ACM Press, 2001.

<b>An alternative classification of agent types based on BOID conflict resolution</b>, Jan Broersen and Mehdi Dastani and Zisheng Huang and Joris Hulstijn and Leendert van der Torre, Proceedings of the 13th Belgium-Netherlands Artificial Intelligence Conference, 79--87, 2001.

<b>Wishful Thinking</b>, J. Broersen, M. Dastani and L. van der Torre, Proceedings of DGNMR01, 2001.


2000<b>A Semantics for Persistency in Propositional Dynamic Logic</b>, J.M. Broersen and R.J. Wieringa and J.-J.Ch. Meyer, Proceedings First International Conference on Computational Logic (CL2000), Lecture Notes in Computer Science 1861, Springer, 2000.

<b>The BOID project</b>, Jan Broersen and Mehdi Dastani and Zisheng Huang and and Leendert van der Torre, Workshop on Practical Reasoning Agents, Imperial College London, 2000.

<b>Commitment and Trust in Dynamic Logic</b>, Jan Broersen, Mehdi Dastani and Leendert van der Torre, Proceedings of the Twelfth Belgium-Netherlands Artificial Intelligence Conference (BNAIC'00), editors: Antal van den Bosch and Hans Weigand, 2000.

<b>Leveled commitment and trust in negotiation</b>, J. Broersen and M. Dastani and L. van der Torre, Proceedings of the autonomous Agents 2000 Workshop on Deception, Fraud and Trust in Agent Societies, Barcelona 2000.

<b>Mu-calculus-based Deontic Logic for Regular Actions</b>, J.M. Broersen and R.J. Wieringa and J.-J.Ch. Meyer, Pre-proceedings Fifth International Workshop on Deontic Logic in Computer Science (DEON'00), editors: Robert Demolombe and Risto Hilpinen, 2000.


1999<b>A Logic for the Specification of Multi-Object Systems</b>, J.M. Broersen and R.J. Wieringa, Formal Methods for Open Object-Based Distributed Systems, editors: Paolo Ciancarini, Alessandro Fantechi, Roberto Gorrieri, Kluwer Academic Publishers, 1999.


1998<b>Minimal Transition System Semantics for Lightweight Class- and Behavior Diagrams</b>, R.J. Wieringa and J.M. Broersen, Proceedings PSMT-Workshop om Precise Semantics for Software Modeling Techniques, technical report TUM-I9803, Institut für Informatik, technische universität München, 1998.

<b>Preferential Semantics for Action Specifications in First-order Modal Action Logic</b>, J.M. Broersen and R.J. Wieringa, Proceedings of the ECAI'98 Workshop on Practical Reasoning and Rationality (PRR'98), 1998.


1997<b>Minimal Semantics for Action Specifications in PDL</b>, J.M. Broersen and R.J. Wieringa and R.B. Feenstra, Proceedings
Accolade '96, Dutch Graduate School in Logic, Department of Mathematics and Computer Science, University of Amsterdam, editors: Joeri Engelfriet and Martijn Spaan, 1997.

<b>Minimal Semantics for Action Specifications in First-order Dynamic Logic</b> , J.M. Broersen and R.J. Wieringa, , technical report IR-410, Faculty of Mathematics and Computer Science, Vrije Universiteit Amsterdam, 1997.


1996<b>Minimal Semantics for Action Specifications in a Multi-modal Logic</b> , Jan Broersen, Remco Feenstra and Roel Wieringa, Integrity in Databases 1996, 6. Int. Workshop on Foundations of Models and Languages for Data and Objects, Schloss Dagstuhl, Germany, September 16-20, 1996.

<b>Minimal Semantics for Transaction Specifications in a Multi-modal Logic</b>, J.M. Broersen and R.J. Wieringa and R.B. Feenstra, technical report IR-410, Faculty of Mathematics and Computer Science, Vrije Universiteit Amsterdam, 1996.


<b>Drafts / Recent submissions</b><b>Alternative Definitions for Realism in BDI-CTL</b>, J.M. Broersen

<b>Intended Model Semantics for Modal Action Descriptions</b>, J.M. Broersen and J.-J.Ch. Meyer and R.J. Wieringa[/publication]




Jan Broersen

Contact Education Research Links

<b>Things I (co-)organize(d) or am / was otherwise involved in:</b>

The ISIS homepage

The DEON'06 homepage

The BOID homepage

The AAMAS'05 homepage

<b>Two columns I wrote for Writers Block Magazine about Dutch elections:</b>

Column about the 2002 elections (in Dutch)

Column about the 2003 elections (in Dutch) <b>
A song of the band Nanacht of which I am a member:
</b>
Broken Tears (mp3, 1,8 MB)
<b>
Links just for myself:</b>

Our university's webmail

Looking for books in the university library

Looking for journals in the library of mathematics and computer science

Looking for journals in the library of philosophy

Family and Friends:

For "cursussen Nederlandse spelling, grammatica en correspondentie" visit my girlfriend's website

For "tulpen, lelies, zantedesias, etc" visit my brother's website

For "computers, programmatuur, programmeerklussen, etc." visit my sister's boyfriend's website

For "tolk- en vertaalwerk Russisch-Nederlands" visit my girlfriend's brother's website.
