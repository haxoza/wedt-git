<b> </b>

[contactinfo]<b>Ping Chen</b><b>
</b>

<b>[position]Assistant Professor[/position]
[affiliation]Computer and Mathematical Sciences Department
University of Houston-Downtown[/affiliation]
[address]One Main Street
Houston, TX 77002[/address] </b>

<b>Voice: [phone](713)221-2764[/phone]  Fax: [fax](713)221-8086[/fax]
Email: [email]chenp at uhd dot edu[/email]</b>[/contactinfo]

<b>[pic]<IMAGE src="img_lib/ping_2003.jpg" alt=""/>[/pic]</b>

Biosketch | Research | Teaching | Publications | Professional Services | Grants | Research Positions

[introduction]<b>Biosketch</b>

Dr. Ping Chen is an [position]Assistant Professor[/position] in [affiliation]Department of Computer and Mathematics Science of University of Houston-Downtown[/affiliation]. His research interests include computational semantics and medical text mining.  Dr. Ping Chen received his BS degree on [bsmajor]Information Science and Technology[/bsmajor] from [bsuniv]Xi'an Jiao Tong University[/bsuniv], MS degree in [msmajor]computer science[/msmajor] from [msuniv]Chinese Academy of Sciences[/msuniv], and Ph.D degree on [phdmajor]Information Technology[/phdmajor] at [phduniv]George Mason University[/phduniv]. more[/introduction]

[resinterests]<b>Research Interests and Projects</b>

    * [interests]Computational Semantics[/interests]

    * [interests]Intelligent Information Retrieval[/interests]

    * [interests]Data Mining[/interests][/resinterests]

<b>Teaching Interests</b>

  * Data Mining and Warehousing

  * Text Mining

[publication]<b>Selected Recent Publications</b> <b>(DBLP)</b>

<b>Journal Papers and Book Chapters</b>

1. D. Barbara, P. Chen, âFractal Miningâ? in "Data Mining and Knowledge Discovery Handbook: A Complete Guide for Practitioners and Researchers", published by Kluwer Academic Publishers, Page 627-647, 2005.

2. E. Deeba, A. de Korvin, P. Chen, âGenerating and applying rules for web documents retrievalâ? Far East Journal of Applied Mathematics, vol. 16(3)(2004) P249 - 272.

3. P. Chen, W. Ding, "Knowledge Management for Agent-Based Tutoring Systems" in "Designing Distributed Learning Environments: With Intelligent Software Agents", Editor Fuhua Oscar Lin, Page 146-161, published by Idea Group, 2004.

4. K. Yue, T. A. Yang, W. Ding, P. Chen, âOpen Courseware and Computer Science Educationâ? Journal of Computing Sciences in Colleges. Volume 20, Issue 1. October 2004.

5. T. Yang, K. Yue, M. Liaw, G. Collins, J. Venkatraman, S. Achar, K. Sadasivam, P. Chen, âDesign of Distributed Computer Security Labâ? Journal of Computing Sciences in Colleges. Volume 20, Issue 1. October 2004.

6. H. Lynn, P. Chen, C. Hu, Y. Simon, âHigh-dimensionality 3D seismic data visualization and interpretation: Simultaneous interpretation of nine co-rendered volumesâ? the Canadian Society of Exploration Geophysicists Recorder, Page 28-33, June 2003.

7. D. Barbara, P. Chen, âUsing self-similarity to cluster large data sets", Journal of Knowledge Discovery and Data Mining, 7, Page 123 â?152, 2003.

<b>Conference Papers </b>

1. P. Chen, W. Ding, C. Ding, "SenseNet: A Knowledge Representation Model for Computational Semantics", In Proceedings of The 5th IEEE International Conference on Cognitive Informatics, July 2006, Beijing, China.

2. P. Chen, R. Verma, âA Query-based Medical Information Summarization System Using Ontology Knowledgeâ? In Proceedings of the 19th IEEE International Symposium on Computer-Based Medical Systems, Page 37-42, June 2006, Salt Lake City, Utah.

3. P. Chen, H. Al-Mubaid, âContext-based Term Disambiguation in Biomedical Literatureâ? In Proceedings of The 19th International FLAIRS Conference, May 2006, Melbourne, Florida.

4. C. Ding, P. Chen, âMining Executive Compensation Data from SEC Filingsâ? In Proceedings of ICDE Workshop on Challenges in Web Information Retrieval and Integration, Page 49-53, April, 2006, Atlanta, Georgia.

5. H. Al-Mubaid, P. Chen, âBiomedical Term Disambiguation: An Application to Gene-Protein Name Disambiguationâ? In Proceedings of Third International Conference on Information Technology: New Generations, page 606-612, April 2006, Las Vegas, Nevada.

6. X. Wang, P. Chen, âWeb-Based Interactive Visualization of Data Cubesâ? In Proceedings of The 2005 International Conference on Modeling, Simulation and Visualization Methods, Page 136-143, June 2005, Las Vegas, Nevada.

7. A. de Korvin, P. Chen, C. Hu, âA Genetic Algorithm Approach for Analyzing Network Intrusion Hyperalertsâ? In Proceedings of The 11th World Congress of International Fuzzy Systems Association, July 2005, Beijing, China.

8. H. Al-Mubaid, P. Chen, âContext-Based Similar Words Detection and Its Application in Specialized Search Enginesâ? In Proceedings of International Conference on Intelligent User Interfaces, Page 260-264, January 2005, San Diego, CA.

9. A. de Korvin, P. Chen, C. Hu, âGenerating and Applying Rules for Interval Valued Fuzzy Observationsâ? Lecture Notes in Computer Science, Vol. 3177, Page 279-284, Springer-Verlag, 2004.

10. D. Barbara, P. Chen, âSelf-similar Mining of Time Association Rulesâ? In Proceedings of The Eighth Pacific-Asia Conference on Knowledge Discovery and Data Mining (PAKDDâ?4), Page 86-95, Sydney, Australia, May 2004.

11. K. Yue, T. Yang, W. Ding, P. Chen, âA model for open content communities to support effective learning and teachingâ? In Proceedings of International Conference on Web Based Communities, Lisbon, Portugal, March 2004.

12. P. Chen, C. Hu, W. Ding, H. Lynn, "Icon-based Visualization of Large High-Dimensional Datasets", In Proceedings of Third IEEE International Conference on Data Mining (ICDM'03), Page 505-508, Melbourne, Florida, November 2003.

13. P. Chen, C. Hu, H. Lynn, Y. Simon, âVisualizing High Dimensional Dataâ? In Proceedings of Conference on Applied Research in Data Engineering 2002, Little Rock, AR, November 2002.

14. P. Chen, A. de Korvin, C. Hu, âAssociation Analysis with Interval Valued Fuzzy Sets and Body of Evidenceâ? In Proceedings of 2002 IEEE International Conference on Fuzzy Systems, Page 518-523, Honolulu, HI, May 2002.

15. D. Barbara, P. Chen, âTracking Clusters in Evolving Data Setsâ? In Proceedings of FLAIRS'2001, Page 239-243, Key West, FL, May 2001.

16. P. Chen, D. Wijesekera, âHierarchical and Modular Model Checking of Finite State Machines", In Proceedings of 8th Annual IEEE International Conference and Workshop on the Engineering of Computer Based Systems, Vienna, Virginia. April 2001.

17. D. Barbara, P. Chen, "Using the Fractal Dimension to Cluster Datasets,'' In Proceedings of 2000 ACM SIGKDD International Conference on Knowledge Discovery and Data Mining, Page 260-264, Boston, MA. August 2000.[/publication]

[resactivity]<b>Professional Activities and Services </b>

1. [position]Member[/position] of [affiliation]SIGKDD[/affiliation]

2. [position]Referee[/position] for:

    * The [affiliation]Journal of VLSI Signal Processing-Systems for Signal, Image, and Video Technology[/affiliation]

    * Fifth International Conference on Intelligent Data Engineering and Automated Learning (IDEAL'04)

    * [affiliation]IIE Transactions[/affiliation]

3. Program Committee member for:

    * Workshop on Privacy and Security Aspects of Data Mining Held in Conjunction with the Fifth IEEE International Conference on Data Mining (ICDM 2005)

    * Workshop on Privacy and Security Aspects of Data Mining Held in Conjunction with the Fourth IEEE International Conference on Data Mining (ICDM 2004)

    * International Conference on Intelligent Techniques 2004 (InTech04)[/resactivity]

<b>Grants</b>

  <b>Internal</b>

    * Faculty Development Grant, 2002, UHD

    * Faculty Development Grant, 2003, UHD

    * Faculty Development Grant, 2004, UHD

    * Faculty Development Grant, 2005, UHD

    * Organized Research Grant, 2002, UHD

    * Organized Research Grant, 2003, UHD

  <b>External</b>

    * âModule-Based Computer Security Courses and Laboratory for Small and Medium Sized Universitiesâ? NSF Grant, PI, 6/2003-5/2006

    * âMRI: Acquisition of a Computational Cluster Grid for Research and Education in Science and Mathematicsâ? NSF, Co-PI (PI: H. Lin), 9/2006-8/2009

<b>Research Positions</b>

  1. Research assistant

    * Design, develop software for text mining projects

  2. Administrator of Distributed Security Lab

    * Network configuration

    * Project development

    * Lab supervision

Last updated in October 2006.
