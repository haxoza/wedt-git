[pic]<IMAGE src="images/803a-am-crp.jpg" alt=""/>[/pic]



[contactinfo]Amitabha Mukerjee

[position]Professor[/position]


[affiliation]Department of Computer Science and Engineering
Indian Institute of Technology, Kanpur[/affiliation]
[address]Kanpur- 208 016, India[/address].

[position]Head[/position], [affiliation]Center for Robotics and Mechatronics, IIT Kanpur[/affiliation].

Phone: [phone]+91 512 2597489[/phone] (O) [phone]2597995[/phone] (Lab)
Fax: [fax]+91 512 2597995[/fax], [fax]2590725[/fax][/contactinfo]


[resinterests]Research Interests: [interests]Developmental Intelligence[/interests]:

Intelligence arises as a result of a developmental process involving sensory interpretation that lead to incrementally detailed models of the world, leading to motor and linguistic cognition. We are tackling this problem at several levels:

  1. [interests]Perceptual skills[/interests] such as Tracking of objects, and Foreground-Background discrimination.

  2. [interests]Unsupervised Recognition of Agents[/interests] with static cameras (surveillance)

  3.  [interests]Unsupervised Identification of Agents[/interests] with moving cameras (mobile robots)[pdf]

  4.  [interests]Identifying and Tracking agents under occlusion[/interests]. See this [movie] showing pan-tilt camera tracking a person as he moves around, even under occlusion.

  5.  [interests]Attentive processes underlie Cognitive focus in Language Acquisition[/interests]. [pdf]

  6. [interests]Grounded Learning of Nouns, Prepositions, and Verbs from multimodal perception[/interests].

  * [interests]Language Acquisition from real image sequences[/interests].

    Broadly this work is related to Spatial Reasoning, Spatial Reconstruction; earlier I was also interested in Story Animation from hand-constructed Linguistic Models.

    In the long run, intelligence can only arise in embodied systems. These systems must be able to grasp and touch objects, and may even be able to play soccer or drive cars.[/resinterests] .

    . .


    Robotics in Education

    <b>Hands-On Digital</b>. Schooling, especially in India, is driven too much by examinations based on rote learning. Are you ready to take the challenge of learning by doing? Check out our Build Robots, Create Science (BRICS) program for schools, which promotes learning through hands-on model building.

    Publications Projects Teaching Students Storytelling Science Poetry/Prose Who am I?

    <font size=1>CSE Home Page</font> <font size=1>IITKHome</font> <font size=1> </font> <font size=1>Center for Robotics</font> <font size=1>CSE Faculty</font>


    <font size=1>Major update: March 13, 2005</font>
