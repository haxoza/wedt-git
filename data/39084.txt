<font size=6>Centro Brasileiro de Pesquisas Físicas - CBPF MCT</font>

[pic]<IMAGE src="schulze.gif" alt=""/>[/pic]


<b><font size=6>Bruno Schulze</font></b>

<font size=2>SBC</font>

<IMAGE src="acnvrule.gif" alt=""/>



[education]<b><font size=4>Education:</font></b>

<IMAGE src="acnvbul1.gif" alt=""/> Ph.D. ([phdmajor]Computer Science[/phdmajor]), [phduniv]State University of Campinas - IC / UNICAMP[/phduniv]

<IMAGE src="acnvbul1.gif" alt=""/> M.Sc. ([msmajor]Electrical Engineering[/msmajor]), [msuniv]Federal University of Rio de Janeiro - COPPE / UFRJ[/msuniv][/education]



[resinterests]<b><font size=4>Areas of Interest:</font></b>

<IMAGE src="acnvbul1.gif" alt=""/> [interests]Engineering and Computing in Scientific Experimental Environments[/interests]

<IMAGE src="acnvbul1.gif" alt=""/> [interests]Mobile Agent Technologies[/interests]

<IMAGE src="acnvbul1.gif" alt=""/> [interests]Open Distributed Processing[/interests]

<IMAGE src="acnvbul1.gif" alt=""/> [interests]Neural Networks[/interests][/resinterests]

<IMAGE src="acnvbul1.gif" alt=""/> Publications



<b><font size=4>Projects:</font></b>

<IMAGE src="acnvbul1.gif" alt=""/> [interests]Multiware Platform[/interests]

<IMAGE src="acnvbul1.gif" alt=""/> [interests]Rede Rio[/interests]

<IMAGE src="acnvbul1.gif" alt=""/> [interests]REMAV-RJ[/interests]

<IMAGE src="acnvbul1.gif" alt=""/> Former Projects

[contactinfo]<b><font size=4>Address</font></b>

<font size=2> Bruno Schulze</font>

<font size=2> [address]CBPF MCT</font>

<font size=2> R. Dr. Xavier Sigaud 150, Urca</font>

<font size=2> 22290-180 Rio de Janeiro - RJ</font>

<font size=2> Brazil[/address]</font>

<font size=2>Room : [address]3rd Floor, CAT[/address]
Phone: [phone]+55 (21) 586-7142[/phone]
Fax : [fax]+55 (21) 542-7499[/fax]
E-mail: [email]schulze@cbpf.br[/email]
[email]schulze@ic.unicamp.br[/email]</font>[/contactinfo]

<IMAGE src="acnvrule.gif" alt=""/>

<font size=2>This page last updated on </font><font size=2>14 Jul 2000 </font>
