HOME

COURSES

RESEARCH

PUBLICATIONS

LANGUAGES AND MACHINES

CSE

WSU

[contactinfo][pic]<IMAGE src="tsudkamp_s.jpg" alt=""/>[/pic]

[affiliation]<b>Wright State University</b><b>
<b>Department of Computer Science[/affiliation] </b>
</b>[address]3640 Colonel Glenn Hwy.
Dayton, Ohio 45435-0001 USA[/address]
Voice: [phone](937) 775-5118[/phone]
Fax [fax](937)775-5133[/fax]
E-mail: [email]thomas.sudkamp@wright.edu[/email][/contactinfo]

<b>Thomas A. Sudkamp</b>

[introduction]I am a [position]Professor[/position] in the [affiliation]Department of Computer Science and Engineering at Wright State University[/affiliation].  My research interests have included the fields of [interests]approximate reasoning[/interests], [interests]artificial intelligence[/interests], and [interests]mathematical logic[/interests].  Current interests are in the [interests]application of soft computing techniques for modeling[/interests] and [interests]decision making in complex problem domains[/interests].  I am also interested in [interests]machine learning with uncertain and imprecise information and knowledge discovery[/interests].

I am currently an [position]associate editor[/position] for the [affiliation]IEEE Transactions on Systems, Man, and Cybernetics and the IEEE Transactions on Fuzzy Systems[/affiliation]. I am also a [position]member of the editorial board[/position] of [affiliation]Fuzzy Sets and Systems and The International Journal of Computational Intelligence[/affiliation]. I have served as president of the North American Fuzzy Information Processing Society (NAFIPS) and vice president of the International Fuzzy Systems Association (IFSA).[/introduction]

Courses

Current Research

Publications

Books:

Languages and Machines

Similarity and Compatibility in Fuzzy Systems

Computer Science & Engineering homepage

Wright State University homepage

<b> </b>
