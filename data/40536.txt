<font size=5> <font size=4><b>brygg ullmer @ lsu</b></font> </font>

[introduction]Greetings! I am an [position]assistant professor[/position] at [affiliation]LSU[/affiliation], jointly in the [affiliation]computer science department[/affiliation] and at the [affiliation]Center for Computation & Technology (CCT)[/affiliation]. My research group is named Tangible Visualization.

I'm the conference co-chair for a new conference called Tangible and Embedded Interaction (TEI), held this year as the 14th Mardi Gras conference, coming up on February 15-17 in Baton Rouge, Louisiana. Papers spanning related topics in interaction, tools, design, use, and art are due October 20. We have a really exciting program committee, with more than 60 members spanning six continents and diverse, complementary domains, and hope many folks will choose to participate!

I've recently co-created Tangint, a wiki with aspirations to service the international research, design, and arts communities engaged in [interests]tangible interfaces[/interests] and related areas.

In spring 2005 and 2006, I have taught Programming Embedded Interfaces. In fall 2005, I taught CSC 4103 (Operating Systems).

I currently have more information online at my previous homepage at ZIB. In particular, my recent activities; recent and past projects; & and papers, patents, & videos are online there. You can also view my old homepage at MIT.[/introduction]

[contactinfo]Brygg Ullmer ([email]ullmer@cct.lsu.edu[/email])[/contactinfo]

<b>jump to</b> [ research plan | mini-bio @ LSU ]

page last updated july 2006

<font size=5> <font size=4><b>research plan</b></font> </font>

My research group is named <b>Tangible Visualization</b>. Our broad interest is in developing new kinds of physical interaction devices (tangible interfaces) to simplify, strengthen, and extend computer visualizations, especially in collaborative and immersive environments.

[resinterests]I envision four components in my efforts toward this:

  1. Develop <b>[interests]viz tangibles[/interests]</b> for facilitating interactive visualization, focusing on collaborative (e.g., meeting- and classroom) and immersive (VR) environments.

  *  Develop <b>[interests]tangible visualizations[/interests]</b> offering interactive, physically-embodied representations of domain-specific content.

  *  Develop <b>[interests]electronic and programmatic tools[/interests]</b> for supporting the design and implementation of viz tangibles and tangible visualizations.

  *  Develop supporting web- and VR-based software <b>[interests]visualization tools[/interests]</b> which bootstrap and drive the end-usage of viz tangibles and tangible visualizations.[/resinterests]

More details are at the Tangible Visualization web site.

<font size=5> <font size=4><b>teaching</b></font> </font>

In spring 2005 and 2006 , I taught Programming Embedded Interfaces. In fall 2005, I taught CSC 4103 (Operating Systems).

<font size=5> <font size=4><b>mini-bio</b></font> </font>

[introduction]Brygg Ullmer is an [position]assistant professor[/position] at [affiliation]LSU[/affiliation], jointly in [affiliation]computer science and the Center for Computation and Technology (CCT)[/affiliation]. He completed his [phddegree]Ph.D.[/phddegree] in the Tangible Media Group of [phduniv]MIT[/phduniv]'s Media Laboratory in [phddate]2002[/phddate], where his research focused on "[interests]tangible user interfaces[/interests]." He held a postdoctoral position in the visualization department of the Zuse Institute Berlin, internships at Interval Research (Palo Alto) and Sony CSL (Tokyo), and has been a visiting lecturer at Hong Kong Polytechnic's School of Design. His research interests include [interests]tangible interfaces[/interests], [interests]visualization[/interests], [interests]programming languages for networked[/interests] and [interests]embedded systems[/interests], [interests]RFID[/interests], [interests]grid computing[/interests], and [interests]rapid physical and electronic prototyping[/interests]. He also has a strong interest in [interests]computationally-mediated arts and crafts[/interests], rooted in the traditions and material expressions of specific regions and cultures. He has begun applying his work at LSU with collaborators in chemistry, biology, astrophysics, CFD, network monitoring, and environmental monitoring & simulation.[/introduction]

[contactinfo][pic]<IMAGE src="ullmer.jpg" alt=""/>[/pic] Brygg / [email]Ullmer@lsu.edu[/email][/contactinfo]
