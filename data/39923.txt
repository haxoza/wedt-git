[contactinfo][pic]<IMAGE src="ubcganesh.jpg" alt=""/>[/pic] <b>Ganesh Ramesh</b>
<b>Ph.D.</b>
(Academic Page) (Personal Page)
Affiliation: [affiliation]UBC Database Systems Lab
Department of Computer Science
University of British Columbia[/affiliation]
[address]Vancouver, B.C. V6T 1Z4[/address]
[phone](604) 822-0557[/phone] 
[fax](604) 822-5485[/fax] (FAX)[/contactinfo]

Education Research Teaching Publications Links Quotes Personal


  [education]EDUCATION*  Ph.D.[phdmajor]Computer Science[/phdmajor] - [phduniv]SUNY Albany[/phduniv], [phddate]2003[/phddate]

  *  M.S. [msmajor]Computer Science and Engineering[/msmajor] - [msuniv]Indian Institute of Technology[/msuniv], Madras, (1997/[msdate]2000[/msdate])

  *  B.E. [bsmajor]Computer Science and Engineering[/bsmajor] - [bsuniv]University of Madras[/bsuniv],[bsdate]1994[/bsdate][/education]


  [resinterests]RESEARCH

  My research interests delve into answering fundamental questions that arise in [interests]data management[/interests]. There is an explosive growth in the amount of data that is being collected by organizations. A need has emerged for efficiently storing, retrieving, analyzing, summarizing and maintaining such data. Solutions range from developing databases, query languages and solving classical problems in database theory to developing efficient methods for computing succinct summaries or models of such massive datasets. Also included in the process of finding solutions is a suite of techniques from other fields - some being direct applications while others are adaptations of the existing methods to model the domain. The fields include [interests]combinatorics[/interests], [interests]graph theory[/interests], [interests]algorithms and data structures[/interests], [interests]statistics and AI[/interests] to name a few.[/resinterests]


  TEACHING


  Courses I taught at Albany

  *  Systems Programming - A Senior Undergraduate Course - Summer 2000, 2002, 2003 - SUNY Albany

  *  Discrete Mathematics - A 2nd Year Undergraduate Course - Summer 2000 - SUNY Albany


  [publication]PUBLICATIONS

  <IMAGE src="blueball.gif" alt=""/> "<b>A Methodology for Cross-Document Coreference Over Degraded Data Sources</b> ",Amit Bagga, Breck Baldwin and <b>Ganesh Ramesh</b>, in RANLP-2001.

  <IMAGE src="blueball.gif" alt=""/> "<b>A Text-based Method for Detection and Filtering of Commercial Segments in Broadcast News</b>",<b>Ganesh Ramesh</b> and Amit Bagga, in International Conference on Language Resources and Evaluation (LREC)-2002.

  <IMAGE src="blueball.gif" alt=""/> "<b>Multi-source Combined-Media Video Tracking for Summarization</b>", with Amit Bagga, Jianying Hu and Jialin Zhong, in International Conference on Pattern Recognition (ICPR)-2002.

  <IMAGE src="blueball.gif" alt=""/> "<b>Indexing and Data Access Methods for Database Mining</b>", <b>Ganesh Ramesh</b>, William A. Maniatty and Mohammed J. Zaki, in 2002 ACM SIGMOD Workshop on Research Issues in Data Mining and Knowledge Discovery (DMKD 2002). Postscript version, PDF version .A more detailed version is also available as a Technical Report - SUNYA-CS-01-01 , University at Albany, June 2001.The slides for the workshop presentation are Available here.

  <IMAGE src="blueball.gif" alt=""/> "<b>Feasible Itemset Distributions in Data Mining: Theory and Application</b>", <b>Ganesh Ramesh</b>, William A. Maniatty and Mohammed J. Zaki,<b> in 2003 ACM Conference on Principles of Database Systems, PODS 2003. </b>[/publication]


  LINKS* ACM

  * DBLP - Bibliography

  * Citeseer

  * DBWORLD

  * CORR - COmputing Research Repository

  * The Collection of Computer Science Bibliographies

  * IEEE Data Engineering Bulletin

  * SIGKDD Explorations

  * KDD Nuggets

  * The Elements of Style - William Strunk Jr.


  QUOTES

  <IMAGE src="blueball.gif" alt=""/> "<b>A mathematician is a device for turning coffee into theorems.</b>" <b> - Paul Erdos</b>

  <IMAGE src="blueball.gif" alt=""/> " <b>Experience is a hard teacher because she gives the test first, the lesson afterwards.</b>" <b> - Unknown</b>

  <IMAGE src="blueball.gif" alt=""/> "<b>The pioneers of a warless world are the youth who refuse military service. </b>"<b> - Albert Einstein</b>

  <IMAGE src="blueball.gif" alt=""/> "<b>Success is getting what you want. Happiness is wanting what you get.</b>" <b> - Dave Gardner</b>


  PERSONAL

TOP

<IMAGE src="http://www.google.com/logos/Logo_40wht.gif" alt="Google"/>

Last Updated - Jan 28, 2004


[contactinfo]Address [address]Department of Computer Science
University of British Columbia, Vancouver[/address][/contactinfo]
