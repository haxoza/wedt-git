<b>Announcements</b>

  * Preliminary call for 2007 I3P Fellowships

<b>My Links</b>

  * My Home Page

  * Books

  * Classes

  * Papers

  * Reports and Notes

  * Research

  * Security in Programming

  * Service

  * Students

  * Talks

  * Miscellaneous

  * My Family

  * About Me (CV and such)

<b>Other Links</b>

  * MyUCDavis

  * Computer Security Lab

  * Dept. of Computer Science

  * College of Engineering

  * University of California, Davis

  * City of Davis

  * County of Yolo

  * State of California

  * United States of America

<b>This Quarter’s Classes</b>

<b>Office Hours for This Quarter</b>

<b>Contacting Me</b>



Matt Bishop<IMAGE src="images/top.gif" alt=""/>

[pic]<IMAGE src="images/Bishop_head.jpg" alt="Picture of me in my office"/>[/pic]

PGP key information here

[contactinfo][affiliation]Department of Computer Science
University of California, Davis[/affiliation]
[address]One Shields Ave.
Davis, CA 95616-8562
United States of America[/address]

Office: [address]3059 John D. Kemper Hall[/address]
Telephone: [phone]+1 (530) 752-8060[/phone]
Fax: [fax]+1 (530) 752-4767[/fax]
Email: [email]bishop at cs dot ucdavis dot edu[/email][/contactinfo]

[introduction]<b>Graduate Admissions</b>. Click here for information on being admitted to do graduate work in information assurance or computer security. <b>Please do not write me asking about your chances of getting in, or if I will admit you.</b> I get too many such letters to answer them, and my answers are the same for everyone: I don't know, and I do not have the authority to, respectively.

I am a [position]professor[/position] in the [affiliation]Department of Computer Science at the University of California at Davis[/affiliation]. I am one of the [position]co-directors[/position] of the [affiliation]Computer Security Laboratory[/affiliation] here.

I do research in [interests]computer security[/interests]. I am especially interested in [interests]vulnerabilities analysis[/interests], the [interests]design of secure systems and software[/interests], [interests]network security[/interests], [interests]formal models of access control[/interests] (especially the Take-Grant Protection Model), and [interests]user authentication[/interests]. I also work on the security of the UNIX operating system.

My book Computer Security: Art and Science was published by Addison-Wesley Professional in December 2002. A somewhat different version, Introduction to Computer Security, was published two years later, again by Addison-Wesley Professional. Check them out and see which you prefer.[/introduction]

<b>There once was an old man from Esser,
Whose knowledge grew lesser and lesser.
It at last grew so small,
He knew nothing at all,
And now he's a college professor.</b>

<IMAGE src="./images/valid-html401.png" alt="Valid HTML 4.01 Transitional"/> <IMAGE src="./images/built_with_bbedit_01.gif" alt="Built with BBEdit"/>

Last updated on Wednesday, December 13, 2006 at 10:51:03AM PST
