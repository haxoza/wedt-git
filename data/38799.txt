[contactinfo][pic]<IMAGE src="Shaffer02.jpg" alt="Picture of Cliff Shaffer"/>[/pic] Clifford A. Shaffer [position]Associate Professor[/position]
[address]2000A Torgerson
Department of Computer Science
Virginia Tech
Blacksburg, VA 24061[/address]
[phone](540) 231-4354[/phone][/contactinfo]
()

For information related to the book "A Practical Introduction to Data Structures and Algorithm Analysis" see the book homepage.

See a listing of my recent PhD and Masters Thesis students.

See many of the papers that I have written or co-authored.

See information about my ongoing and past research projects.


Courses I often teach:

  * CS2606: Data Structures and Object Oriented Design II (formerly CS2604: Data Structures and File Processing)

  * CS4104: Data and Algorithm Analysis

  * CS5014: Research Methods in Computer Science

  * CS5114: Theory of Algorithms

  * CS6704: Design Patterns and Component Frameworks

Short vitae
Full vitae

[Last updated: 12/15/2006]
