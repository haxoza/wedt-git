<IMAGE src="http://irt-sdc.stanford.edu/dcswhw62100000431enfb5sop_8c4w/njs.gif?dcsuri=/nojavascript&WT.js=No" alt=""/>

<IMAGE src="/Templates/frd/spacer.gif" alt=""/> <IMAGE src="/Templates/frd/spacer.gif" alt=""/> <IMAGE src="/Templates/frd/spacer.gif" alt=""/> <IMAGE src="/Templates/frd/spacer.gif" alt=""/> <IMAGE src="/Templates/frd/spacer.gif" alt=""/> <IMAGE src="/Templates/frd/spacer.gif" alt=""/> <IMAGE src="/Templates/frd/spacer.gif" alt=""/> <IMAGE src="/Templates/frd/spacer.gif" alt=""/> <IMAGE src="/Templates/frd/spacer.gif" alt=""/> <IMAGE src="/Templates/frd/spacer.gif" alt=""/> <IMAGE src="/Templates/frd/spacer.gif" alt=""/>

<IMAGE src="/Templates/frd/institutions-redbar.gif" alt=" "/> <IMAGE src="/Templates/frd/spacer.gif" alt=""/>

<IMAGE src="/Templates/frd/brace-left.gif" alt=" "/> <IMAGE src="/Templates/frd/logo.gif" alt=" "/> <IMAGE src="/Templates/frd/search-spacer1.gif" alt=" "/> <IMAGE src="/Templates/frd/brace-right.gif" alt=" "/> <IMAGE src="/Templates/frd/spacer.gif" alt=""/>

<IMAGE src="/Templates/frd/spacer.gif" alt=""/>

<IMAGE src="/Templates/frd/search-spacer2.gif" alt=" "/> <IMAGE src="/Templates/frd/spacer.gif" alt=""/>

<IMAGE src="/Templates/frd/spacer.gif" alt=""/>

<IMAGE src="/Templates/frd/brand.gif" alt="Faculty & Researcher Directory"/> <IMAGE src="/Templates/frd/spacer.gif" alt=""/>

<IMAGE src="/Templates/frd/tab-brace-lt.gif" alt=" "/> <IMAGE src="/Templates/frd/tab-edu.gif" alt="Education"/> <IMAGE src="/Templates/frd/tab-brace-1-2.gif" alt=" "/> <IMAGE src="/Templates/frd/tab-res.gif" alt="Research"/> <IMAGE src="/Templates/frd/tab-brace-2-3.gif" alt=" "/> <IMAGE src="/Templates/frd/tab-pc.gif" alt="Patient Care"/> <IMAGE src="/Templates/frd/tab-brace-3-4.gif" alt=" "/> <IMAGE src="/Templates/frd/tab-com.gif" alt="Community"/> <IMAGE src="/Templates/frd/tab-brace-rt.gif" alt=" "/> <IMAGE src="/Templates/frd/spacer.gif" alt=""/>

Medical School Home > Research > Community Academic Profiles

<IMAGE src="/Templates/images/spacer.gif" alt=" "/> <IMAGE src="/Templates/images/spacer.gif" alt=" "/>

Seung Kim Info

<IMAGE src="/Templates/images/spacer.gif" alt=" "/> <IMAGE src="/Templates/images/spacer.gif" alt=" "/>

<IMAGE src="/Templates/images/spacer.gif" alt=" "/> <IMAGE src="/Templates/images/spacer.gif" alt=" "/>

<IMAGE src="/Templates/images/arrow-red-fwd.gif" alt=">"/> Profile Home

<IMAGE src="/Templates/images/arrow-red-fwd.gif" alt=">"/> Education

<IMAGE src="/Templates/images/arrow-red-fwd.gif" alt=">"/> Research Interests

<IMAGE src="/Templates/images/arrow-red-fwd.gif" alt=">"/> Publications

<IMAGE src="/Templates/images/spacer.gif" alt=" "/> <IMAGE src="/Templates/images/spacer.gif" alt=" "/>

<IMAGE src="/Templates/images/spacer.gif" alt=" "/>

Find a Profile

<IMAGE src="/Templates/images/spacer.gif" alt=" "/> <IMAGE src="/Templates/images/spacer.gif" alt=" "/>

<IMAGE src="/Templates/images/spacer.gif" alt=" "/> <IMAGE src="/Templates/images/spacer.gif" alt=" "/>

<IMAGE src="/Templates/images/arrow-red-fwd.gif" alt=">"/> Home

<IMAGE src="/Templates/images/arrow-red-fwd.gif" alt=">"/> Profiles by Name

<IMAGE src="/Templates/images/arrow-red-fwd.gif" alt=">"/> Profiles by Department

<IMAGE src="/Templates/images/spacer.gif" alt=" "/> <IMAGE src="/Templates/images/spacer.gif" alt=" "/>

(search for any phrase or name)

<IMAGE src="/Templates/images/circle18-questionmark-w.gif" alt="?"/> Help

<IMAGE src="/Templates/images/spacer.gif" alt=" "/>

<IMAGE src="/Templates/images/spacer.gif" alt=" "/>

<IMAGE src="/Templates/images/spacer.gif" alt=" "/>

<IMAGE src="/Templates/images/keyhole-login.gif" alt="login"/> Update Your Profile
SUNet ID required

<IMAGE src="/Templates/images/spacer.gif" alt=" "/>

Seung K. Kim [msdegree]M.D.[/msdegree], Ph.D.
Email:
<b>Profile: </b>[homepage]http://med.stanford.edu/profiles/Seung_Kim/[/homepage]

[contactinfo]Alternate Contact:
<b>Name: </b>Susan Elliott
<b>Title: </b>[position]Administrative Assistant[/position]
<b>Email: </b>[email]sue.elliott@stanford.edu[/email]
<b>Phone: </b>[phone]650-498-7301[/phone][/contactinfo]

[introduction]Academic AppointmentsAppointmentOrganization

[position]Associate Professor[/position] Developmental Biology

[position]Associate Professor[/position] (By courtesy) Medicine

[position]Member[/position] [affiliation]Comprehensive Cancer Center[/affiliation]

Graduate & Fellowship Program Affiliations Developmental Biology
Medicine
Neonatology and Developmental Biology[/introduction]
Printer-friendly Version <IMAGE src="/Templates/images/icon-printer10.gif" alt=""/>[pic]<IMAGE src="/profiles/viewImage?facultyId=4726&type=big&showNoImage" alt=""/>[/pic]

Honors & AwardsTitleOrganizationDate(s)

Pew Biomedical Research ScholarThe Pew Charitable Trusts1999-2003

Named Investigator AwardStanford-NIH Digestive Diseases Center2000

Career Development AwardAmerican Diabetes Association1999-2003

Faculty Scholar AwardDonald E. and Delia B. Baxter Foundation1999-2001

Henry J. Kaiser Family Foundation Award for Excellence in Preclinical TeachingStanford University School of Medicine2002

8 Honors & Awards: view full listAdministrative AppointmentsTitleOrganizationStart YearEnd Year

MemberMedical Science Review Board, Juvenile Diabetes Research Foundation2002 -

Associate DirectorStanford Medical Scientist Training Program2001 -

External AdvisorNIDDK-NIH Beta Cell Biology Consortium2002 2006

Professional EducationDegreeAwarding InstitutionField of StudyYear of Graduation

[education]A.B.[bsuniv]Harvard University[/bsuniv] [bsmajor]Biochemical Sciences[/bsmajor] [bsdate]1985[/bsdate]

M.D.[msuniv]Stanford University[/msuniv] [msmajor]Medicine[/msmajor] [msdate]1992[/msdate]

Ph.D.[phduniv]Stanford University[/phduniv] [phdmajor]Biochemistry[/phdmajor] [phddate]1992[/phddate][/education]

Web Site LinksResearch/Lab website: Kim Lab Website [resinterests]Research Interests

Organ development requires mechanisms to establish an integrated, stereotyped tissue pattern from multiple distinct cellular components. Many vital organs derive from the endodermal and mesodermal germ layers to form the gastrointestinal and respiratory tracts, yet little is known about the genetic programs that coordinate steps culminating in proper organ morphogenesis and axial position, cell differentiation and physiologic function. Our goal is to identify and understand the pathways that govern organogenesis of the pancreas, a vital organ with endocrine and exocrine functions.


We are using Drosophila, chicks and mice, organisms accessible to embryological, genetic and molecular methods, to identify cell interactions and signaling pathways that regulate early steps in pancreatic islet development. Some of the pathways active during ontogeny also regulate pancreatic growth during adulthood, and we are studying the role of these genetic pathways in growth control and function of the mature pancreas in mice. Armed with an understanding of the mechanisms regulating normal development of insulin-producing cells and other islet cells, we have been able to differentiate functional glucose-responsive islets from embryonic stem cells and other cell lines. These are capable of rescuing glucose regulation and survival in experimental animal models of diabetes mellitus. We are now using this in vitro culture system to isolate candidate islet stem/precursor populations from adult human stem cell populations. We are also using Drosophila to study neuroendocrine cells that govern metabolism. We have discovered that two cell types, one which produces insulin, the other which produces a glucagon-like peptide called AKH, are crucial regulators of glucose homeostasis in Drosophila. Genetic, biochemical, and electrophysiologic studies are being used to elucidate the programs that control development and function of these cells, which comprise the Drosophila endocrine 'pancreas'. In turn, we expect that these studies will identify important conserved functions that govern islet cell biology.[/resinterests]

[publication]Publications

  * <b> Heit JJ, Apelqvist AA, Gu X, Winslow MM, Neilson JR, Crabtree GR, Kim SK "Calcineurin/NFAT signalling regulates pancreatic beta-cell growth and function." Nature 2006; 443: 7109: 345-9 </b> More <IMAGE src="/Templates/images/arrow-red-fwd.gif" alt="more"/>

  * <b> Sugiyama T, Rodriguez RT, McLean GW, Kim SK "Conserved markers of fetal pancreatic epithelium permit prospective isolation of islet progenitor cells by FACS." Proc Natl Acad Sci U S A 2006; </b> More <IMAGE src="/Templates/images/arrow-red-fwd.gif" alt="more"/>

  * <b> Karnik SK, Hughes CM, Gu X, Rozenblatt-Rosen O, McLean GW, Xiong Y, Meyerson M, Kim SK "Menin regulates pancreatic islet growth by promoting histone methylation and expression of genes encoding p27Kip1 and p18INK4c." Proc Natl Acad Sci U S A 2005; </b> More <IMAGE src="/Templates/images/arrow-red-fwd.gif" alt="more"/>

  * <b> Kim SK, Rulifson EJ "Conserved mechanisms of glucose sensing and regulation by Drosophila corpora cardiaca cells." Nature 2004; 431: 7006: 316-20 </b> More <IMAGE src="/Templates/images/arrow-red-fwd.gif" alt="more"/>

  * <b> Rulifson EJ, Kim SK, Nusse R "Ablation of insulin-producing neurons in flies: growth and diabetic phenotypes." Science 2002; 296: 5570: 1118-20 </b> More <IMAGE src="/Templates/images/arrow-red-fwd.gif" alt="more"/>

34 publications: view full list[/publication]

<IMAGE src="/Templates/images/shim.gif" alt=" "/>

<IMAGE src="/Templates/images/shim.gif" alt=" "/> <IMAGE src="/Templates/images/shim.gif" alt=" "/>

<IMAGE src="/Templates/images/shim.gif" alt=" "/> ?2007  Stanford University School of Medicine | Terms of Use <IMAGE src="/Templates/images/pbirt.gif" alt="Powered by IRT"/>

<IMAGE src="/Templates/images/shim.gif" alt=" "/>
