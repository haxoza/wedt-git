<IMAGE src="/images/template/logo_uni.gif" alt="Home"/>

<IMAGE src="/images/template/logoscr.gif" alt="Home"/>

<IMAGE src="/images/template/inf5.jpg" alt="Faculty Home"/> <font size=4> <font size=5>F</font>ACULTY OF <font size=5>C</font>OMPUTER <font size=5>S</font>CIENCE
</font>

<font size=4> <font size=5>F</font>REE <font size=5>U</font>NIVERSITY OF <font size=5>B</font>OZEN - <font size=5>B</font>OLZANO</font>

Italiano Deutsch <b>Home</b> Info Site map Intranet Print Version

Home Unibz > Faculty of Computer Science > About Us > Staff > <b>Johann Gamper<b> </b></b>


<font size=2></font>


[contactinfo]Johann Gamper

<b>Position</b> [position]Assistant Professor (DIS)[/position]

<b>Address</b> [address]Faculty of Computer Science 

Free University of Bozen-Bolzano

Dominikanerplatz 3, I-39100 Bozen, Italy[/address]

[address]<b>Room No.</b> 209[/address]

<b>Office Hours</b> Tuesday 11:00-13:00, and by appointment via e-mail 

<b>Phone</b> [phone]+39 0471 016140[/phone] 

<b>Fax</b> [fax]+39 0471 016009[/fax] 

<b>E-Mail</b> [email]Johann.Gamper@unibz.it[/email]

[pic]<IMAGE src="/web4archiv/objects/images/standard/gamper1.jpeg" alt=""/>[/pic]


<b>Courses</b> Database Management Systems; Temporal and Spatial Databases 

   E-mail: [email]webmaster@unibz.it[/email][/contactinfo]

© Copyright 2002-2007 Free University of Bozen-Bolzano   
