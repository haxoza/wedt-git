[pic]<IMAGE src="picture.gif" alt="picture"/>[/pic]



Krishnan Kumaran <font size=5>

[introduction]I am a [position]member[/position] of the [affiliation]Mathematics of Networks and
Systems Research Dept.[/affiliation] - a component of the
[affiliation]Mathematics Research Center at Bell Labs[/affiliation], Murray
Hill, NJ. [/introduction][contactinfo]I can be contacted using the following
information.

</font><b><font size=7> </font><font size=5>email:</font></b> <font size=5>[email]kumaran@research.bell-labs.com[/email]</font> <font size=5> <b>phone:</b> [phone]908 582 4182[/phone] <b>fax:</b> [fax]908 582 3340[/fax] <b>office:</b> [address]Rm. 2C-313 Bell Laboratories Lucent Technologies 700 Mountain Av. Murray Hill, NJ 07974-0636[/address][/contactinfo]</font> <font size=5>[resinterests]My current research aims to [interests]design algorithms[/interests] that characterize, and possibly improve, performance of communication networks.[/resinterests] Please see my </font><font size=5>Publications</font> <font size=5>for further details. For more information about me, see my </font><font size=6>resume</font><font size=1>. </font><font size=5> Links to my department, center and above: </font>

<font size=4><b>

Up

</b>[ </font><font size=4>Net. and Sys. Dept.</font><font size=4> | </font><font size=4>Math Center </font><font size=4>| </font><font size=4>Bell Labs </font><font size=4>| </font><font size=4>Lucent Technologies </font><font size=4>] </font>

<font size=4>

Last modified: 02 May 1996

</font>
