    [pic]<IMAGE src="zg2.jpg" alt="picture"/>[/pic]

    [introduction]<font size=4><b> Zoubin Ghahramani </b> </font>

    [position]Professor[/position] of Information Engineering
    [affiliation]Department of Engineering
    University of Cambridge[/affiliation]

    <IMAGE src="icons/cam2.png" alt="Cambs"/>

    [position]Associate Research Professor[/position]
    [affiliation]Machine Learning Department
    Carnegie Mellon University[/affiliation]

    <IMAGE src="icons/scslogo2.png" alt="CMU SCS logo"/>

    [position]Adjunct Faculty[/position]
    [affiliation]Gatsby Computational Neuroscience Unit
    University College London[/affiliation][/introduction]
    <IMAGE src="icons/uclbluelogo2.jpg" alt="UCL logo"/>


    Research Papers (Chronological)


    Research Interests


    Software


    Former Students and Postdocs


    Students and Postdocs: <font size=4>

      * Arik Azran

      *  Frederik Eaton

      * Katherine Heller

      * Iain Murray

      * Pedro Ortega

      * Ricardo Silva

      * Ed Snelson

      * JaeMo Sung

      *  Sinead Williamson

    </font>


    News<font size=4>

      *  I will be serving as the Program Chair for the International Conference on Machine Learning (ICML 2007)

      *  Machine Learning Reading Group @ CUED

      *  Online Image Retrieval and Annotation Demo

      *  Microsoft Live Labs Research Award

      * Advanced Tutorial Lecture Series on Machine Learning, Thursdays from 4pm-6pm, this term.

      *  I'm teaching a new course on Machine Learning (4F13), this Michaelmas term (Fall 2006) at Cambridge

      *  New PhD students: Frederik Eaton, Pedro Ortega, and Sinead Williamson

    <font size=4>


    [resinterests]Research Areas and Papers by Topic

    <IMAGE src="icons/gp.png" alt="GP"/> [interests]Gaussian Processes[/interests] <IMAGE src="icons/clustering.png" alt="Clusters"/> [interests]Clustering[/interests] <IMAGE src="icons/gm.png" alt="Graphical Models"/> Graphical Models

    <IMAGE src="icons/mcmc.png" alt="MCMC"/> [interests]Monte Carlo Methods[/interests] <IMAGE src="icons/ssl.png" alt="SSL"/> [interests]Semi-Supervised[/interests]
    [interests]Learning <IMAGE src="icons/nonparam.png" alt="Non Param"/> Non-parametric[/interests]
    [interests]Bayesian Methods[/interests]

    <IMAGE src="icons/approx.png" alt="Approx"/> [interests]Approximate Inference[/interests] <IMAGE src="icons/bioinf.png" alt="DNA"/> [interests]Bioinformatics <IMAGE src="icons/ir.png" alt="IR"/> Information Retrieval[/interests]

    <IMAGE src="icons/motor.png" alt="Control"/> [interests]Sensorimotor Control[/interests]
    and [interests]Robotics[/interests] <IMAGE src="icons/timeseries.png" alt="Time Series"/> [interests]Time Series Models[/interests] <IMAGE src="icons/reviews.png" alt="Reviews"/>[/resinterests] Review Articles
    and Tutorials


    [publication]Talks and Tutorials:

      *  UAI 2005 Tutorial on Nonparametric Bayesian Methods

      * ICML 2004 Tutorial on Bayesian Machine Learning

      * NIPS 1999 Tutorial on Probabilistic Models for Unsupervised Learning

      *  An Old Talk [ps] [pdf][/publication]


    Teaching:

      *  Machine Learning (4F13), Michaelmas 2006, Cambridge

      *  Signal and Pattern Processing (3F3), Lent 2006, Cambridge

      *  Unsupervised Learning Course, Fall 2005, UCL

      *  Gatsby Machine Learning Qualifying Exam Topic List

      *  Statistical Approaches to Learning and Discovery, Spring 2002, CMU

      *  Old Course Websites


    Conferences and Workshops:

      * Open Issues in Gaussian Processes for Machine Learning, NIPS Workshop, Vancouver, Dec 10, 2005

      *  The 10th International Workshop on Artificial Intelligence and Statistics, Barbados, Jan 6-8, 2005

      *  Workshop on Learning Theoretic and Bayesian Inductive Principles, London, July 19-21, 2004

      *  NIPS Conference


    Other:

      *  Very Brief Introduction to Bayesian Machine Learning[/publication]

      *  www.Variational-Bayes.org website

      *  Interested in a PhD in Machine Learning? Apply to the Engineering Department at the University of Cambridge.

      *  CV [ps] [pdf]

      *  Short Bio

      *  Miscellaneous


    Academic Links:

      *  My web page at the Machine Learning Department, Carnegie Mellon University

      *  Machine Learning Group at the Department of Computer Science, University College London

      *  Journal of Machine Learning Research

      * Bayesian Analysis

      * IEEE Transactions on Pattern Analysis and Machine Intelligence

      * Machine Learning

      * PASCAL Research Network

      * Many other machine learning links


    [contactinfo]Contact Information:

      * <b> Mail: </b>

        [address]Department of Engineering
        University of Cambridge
        Trumpington Street
        Cambridge CB2 1PZ, UK[/address]

    </font></font>

    * <b>Office:</b> [address]422B[/address]

    * <b> Tel: </b>

      Office  [phone]+44 (0)1223 764 093[/phone]
      Fax     [fax]+44 (0)1223 332 662[/fax]

    * <b> Email: </b>

      [email]zoubin - at - eng.cam.ac.uk[/email]
      (replace -at- by @)[/contactinfo]

<IMAGE src="http://t1.extreme-dm.com/i.gif" alt="eXTReMe Tracker"/><IMAGE src="http://e1.extreme-dm.com/s10.g?login=zoubing&j=n&jv=n" alt=""/>
