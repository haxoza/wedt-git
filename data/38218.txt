[pic]<IMAGE src="yonit.gif" alt="Mugshot"/>[/pic] Yonit Kesten

  Dr. Yonit Kesten


  [contactinfo][address]Dept. of  Information Systems Engineering
  Ben Gurion University
  Beer-Sheva, Israel[/address]

  e-mail: [email]ykesten@bgumail.bgu.ac.il[/email], Home-phone: [phone]+972-7-6469-988[/phone]

  Affiliated with the "Minerva Center for Verification of Reactive Systems" at the [affiliation]Weizman Institute[/affiliation].
  e-mail: [email]yonit@wisdom.weizmann.ac.il[/email][/contactinfo]


[resinterests]Research interests [interests]Formal verification of VLSI and Communication Systems[/interests], [interests]Proof methods for formal verification[/interests], [interests]Combining Algorithmic and Deductive Verification Methods[/interests], [interests]Compositional Verification and Abstractions[/interests], [interests]Temporalllogic and Automata[/interests], [interests]Non-Procedural and High Level Languages for Specification and Modeling of Concurrent systems[/interests].[/resinterests]


Links List of Publications
