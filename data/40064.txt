<IMAGE src="index_files/sitehead.gif" alt=""/>

[pic]<IMAGE src="index_files/gvj.jpg" alt="Guy-Vincent Jourdan"/>[/pic]


[introduction]Guy-Vincent Jourdan<b>[position]Associate</b>
<b>Professor[/position]</b> <b><font size=4>[/introduction][resinterests]Research Interests: </font></b>

  * [interests]Distributed systems[/interests]

  * [interests]Software verification[/interests], [interests]validation and testing[/interests]

  * [interests]Partially ordered sets[/interests]

  * [interests]Software security[/interests] 

  * [interests]Algorithms[/interests]

  * [interests]Data visualization[/interests][/resinterests]

<b><font size=4>Courses</font></b>

  * Fall 2006

      * CSI4538 / CEG4794

      * EMP5102

      * EMP5117

  * Winter 2007

      * CSI3504

<b><font size=4>Projects</font></b>

  * See my undergraduate projects page

<IMAGE src="index_files/line2.gif" alt=""/>

  <b><font size=4>Background</font></b>

    [education]* Education

        * [phddegree]Ph.D.[/phddegree] ([phdmajor]Computer Science[/phdmajor]) [phduniv]Universit?de Rennes, France[/phduniv][/education]

    [introduction]* Biography

        * Guy-Vincent Jourdan joined [affiliation]SITE[/affiliation] as an [position]associate professor[/position] in June 2004, after 7 years in the private sector as C.T.O. and then C.E.O. of Ottawa based Decision Academic Graphics. He received his PhD from [phduniv]l'universit?de Rennes/INRIA[/phduniv] in France in [phddate]1995[/phddate] in the area of [phdmajor]distributed systems analysis[/phdmajor]. His research interests include [interests]distributed systems modeling and analysis[/interests], [interests]software engineering[/interests], [interests]software security[/interests], [interests]ordered sets and data visualization[/interests].[/introduction]

<IMAGE src="index_files/laddress.gif" alt=""/>

  [contactinfo]Guy-Vincent Jourdan, Ph.D.


  [affiliation]School of Information Technology and Engineering (SITE)
  University of Ottawa[/affiliation]
  [address]800 King Edward Avenue
  P.O. Box 450, Station A
  Ottawa, Ontario, Canada K1N 6N5[/address]

  Office: [address]SITE, Room 5-110[/address]
  Tel: [phone](613) 562-5800 ext. 6686[/phone]
  Fax: [fax](613) 562-5664[/fax]
  Email: [email]gvj@site.uottawa.ca[/email][/contactinfo]
