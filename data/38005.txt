[pic]<IMAGE src="myPic.jpg" alt=""/>[/pic]


[introduction]Natalio Krasnogor, Ph.D.


[position]Associate Professor[/position] at the [affiliation]School of Computer Science and I.T.
University of Nottingham
ASAP Research Group[/affiliation][/introduction]

[resinterests]My main research interests include:
o [interests]Bioinformatics and Chemoinformatics[/interests]
o [interests]Artificial Intelligence[/interests]
o [interests]Memetic Algorithms (click here to download a tutorial)[/interests], [interests]Memetic theory[/interests]
o [interests]Evolutionary Computation in general[/interests].
o [interests]Multi-Agent systems[/interests], [interests]Cellular Automata[/interests], [interests]Artificial Life[/interests]
o [interests]Natural computation and its counterpart[/interests], [interests]Computational nature (NCCN)[/interests]
o[interests] Complexity Theories (e.g., Information/Kolmogorov and Computational ) and emergence as applied to NCCN[/interests].
o [interests]Physical[/interests], [interests]Chemical and Biological Computation[/interests]
o [interests]Systems Self-Assembly (click here to visit ASAP's Special Interest Group on Self-Assembly Research (SIGSAR))[/interests].

One of the domains where I use memetic algorithms (and other modern AI techniques) is bioinformatics, specifically in the Protein Structure Prediction and Protein Structure Comparison problems. I work closely with the Computational Biophysics and Chemistry Group in the School of Chemistry. The bioinformatics theme is an important aspect of the interdiciplinary lab within ASAP. If you wish to look at our current projects and collaborators please visit here

My research in NCCN and complexity theories is aimed at better understanding how to construct and deploy Self-Reconfigurable Software (SRS). If you want to learn more about SRS you can download a powerpoint presentation of a talk on the subject. A web-page about SRS is also availabe.[/resinterests]

[introduction]I am the [position]leading scientist[/position] of [affiliation]ASAP's Special Interest Group on Self-Assembly Research[/affiliation].[/introduction] Detail of our work appears here

A link to a news article on my work is to be found here and here


ADMIN:

I coordinate the School's seminars, if you would like to visit our school and give one please e-mail me. Past seminars are found here.

I am also responsible for new PhD students research practice induction


TEACHING:

Bioinformatics (G53BIO)

High Level Languages (G6HLL)

Introduction to Computer Programming (G6DICP)

Introduction to Computer Programming (G51PRG)

Final year (and group project) research topics

Is there a role for Bioinformatics in the teaching curricula of the University of Nottingham?

Developing a Core Bioinformatics Syllabus( power point version here )


RESEARCH:

My Publications

My Externally Funded Projects (Grants)

Self-Reconfigurable Software

HP-model based protein instances solved to optimality for several lattices

Comparisons of Protein Structures: Models, Measures, Metrics and Methods

Protein Structure Comparison Protocol Based on the Universal Similarity Metric

Protein Structure Comparison Protocol Based on Generalized Fuzzy Contact Maps Overlap


My Research Team

Undergraduate Students

Kun Jiang, implemented Ecological Multi-Agent Simulation for Continuous Optimisation

Graduate Students

Lin Lin (researching into systems self-assembly,click here to see some samples )

German Terrazas Angulo (researching into software self-assembly, click here to see some samples)

Peter Siepman (researching into evolvable chellware, click here for ChellNet web-site)

Mike Stout (researching into protein structure prediction and AI, click here to see some samples)

Pawel Widera (researching into protein structure prediction, CASP, CAFASP)

Azhar A. Shah (researching into protein structure comparison, GRID technology, Bioinformatics)

James Smaldon (researching into the modeling and computation of artificial chemical cells)

Scott Littlewood (researching into protein structure prediction and ant colony approaches, 2nd sup.)

Adam Sweetman (researching into nano computation, 1st supervisor is Prof. P. Moriarty while I am the 2nd supervisor)

Linda Fiaschi (researching into bioinformatics and datamining)

PostDocs

Jaume Bacardit researching in Protein Structure Prediction and AI and (local use) Bibliography and Ontology Server Data Base

Daniel Barthel researching in Protein Structure Comparison and Similarity and (public web-server) Protein Structure Similarity and Comparison Server

Journal Club

The ASAP Special Interest Group on the Computational Foundations of NanoSciece runs a Journal Club

If you would like to join please mail me.


[publication]My Books

Nature Inspired Cooperative Strategies for Optimisation

Nature Inspired Cooperative Strategies for Optimisation
D.A. Pelta and N. Krasnogor (Eds)
<IMAGE src="nicso-cover.bmp" alt=""/>

Recent Advances in Memetic Algorithms
W.E. Hart, N. Krasnogor, J.E. Smith (Eds)
Studies in Fuzziness and Soft Computing
Springer <IMAGE src="ramarst.jpg" alt=""/>[/publication]


Memetic Algorithms

Memetic Algorithms Discussion List

Join the Memetic Algorithms Discussion List

International Workshop on Memetic Algorithms (WOMA) :

Workshop on Memetic Algorithms - an International Series

Memetic Theory in Artificial Systems and Societies (METAS)

Call for Papers for an Interdisciplinary Event!

Memetic Algorithms Framework

The Memetic Algorithms Framework has been recently recognized as one of the six superlative sources of information about the subject of "Memetic Algorithms and Evolutionary Computation:

<IMAGE src="infogsupbig.gif" alt=""/>


Others:

The Relativistic Newspaper Paradox, any insights will be wellcomed!

See also also ICSC web pages and The Adaptive Systems Group at VUB.


[contactinfo]CONTACT INFORMATION:

This page was created by natalio.krasnogor-replace all this by at simbol-nottingham.ac.uk . Last updated 1 October, 2003

 NATALIO KRASNOGOR, Ph.D. 

 Lecturer                       [affiliation]Automatic Scheduling and Planning (ASAP) group

                              ?School of Computer Sciences and IT 

                                Jubilee Campus

                              ?University of Nottingham[/affiliation]

 Tel.: [phone]+44 - 0115 - 8467592[/phone]     [address]Nottingham, NG8 1BB

                              ?United Kingdom[/address]



 Fax.: [fax]+44 - 0115 -8467066[/fax] 

 SkypeIn: [phone]+44 - 020 - 81235798[/phone] 

 URL: [homepage]http://www.cs.nott.ac.uk/~nxk/[/homepage] 



e-mail: [email]Natalio.Krasnogor -replace all this by at simbol- nottingham.ac.uk[/email][/contactinfo]

 

<IMAGE src="http://s11.sitemeter.com/meter.asp?site=s11natkra&refer=&hours=15&minutes=52&rtype=4" alt=""/>
