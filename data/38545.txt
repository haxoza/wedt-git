[introduction]<b>Bill MacCartney</b> [position]Doctoral Candidate[/position]
[affiliation]Natural Language Processing Group
Computer Science Department
Stanford University[/affiliation]
papers  ? resume  ? personal


I'm a [position]Ph.D. student[/position] in the [affiliation]Computer Science Department at Stanford University[/affiliation]. I work with Prof. Chris Manning and the NLP research group. I'm primarily interested in [interests]probabilistic approaches to computational semantics[/interests].

I also volunteer as a [position]tutor[/position] in physics and calculus at the [affiliation]Eastside School[/affiliation] in East Palo Alto.[/introduction]


[publication]Papers

<b>Learning to recognize features of valid textual entailments</b> [pdf]
Bill MacCartney, Trond Grenager, Marie-Catherine de Marneffe, Daniel Cer, Christopher D. Manning
Proceedings of the Human Language Technology Conference of the North American Chapter of the Association for Computational Linguistics (HLT-NAACL 2006)

<b>Generating Typed Dependency Parses from Phrase Structure Parses</b> [pdf]
Marie-Catherine de Marneffe, Bill MacCartney, Christopher D. Manning
5th International Conference on Language Resources and Evaluation (LREC 2006)

<b>Learning to distinguish valid textual entailments</b> [pdf]
Marie-Catherine de Marneffe, Bill MacCartney, Trond Grenager, Daniel Cer, Anna Rafferty, and Christopher D. Manning
Second Pascal RTE Challenge Workshop

<b>Robust Textual Inference using Diverse Knowledge Sources</b> [pdf]
Rajat Raina, Aria Haghighi, Christopher Cox, Jenny Finkel, Jeff Michels, Kristina Toutanova, Bill MacCartney, Marie-Catherine de Marneffe, Christopher D. Manning, Andrew Y. Ng
Proceedings of the First PASCAL Challenges Workshop, 2005

<b>Solving Logic Puzzles: From Robust Processing to Precise Semantics</b> [pdf]
Iddo Lev, Bill MacCartney, Christopher D. Manning, Roger Levy
Proceedings of the ACL-04 Workshop on Text Meaning and Interpretation, July 2004

<b>Practical Partition-Based Theorem Proving for Large Knowledge Bases</b> [pdf, ps]
Bill MacCartney, Sheila A. McIlraith, Eyal Amir, Tomas Uribe
Proceedings of the 18th International Joint Conference on Artificial Intelligence (IJCAI-03), August 2003

<b>The Cycic Friends Network: Getting Cyc agents to reason together</b> [pdf, ps]
James Mayfield, Tim Finin, Rajkumar Narayanaswamy, Chetan Shah, William MacCartney & Keith Goolsbey
Proceedings of the ACM CIKM-95 Intelligent Information Agents Workshop, December 1995


Presentations

<b>Learning to recognize features of valid textual entailments</b> [ppt]
Bill MacCartney, Trond Grenager, Marie-Catherine de Marneffe, Daniel Cer, Christopher D. Manning
Presented at NAACL-06

<b>NLP Lunch Tutorial: Smoothing</b> [pdf]
Bill MacCartney
April 2005

<b>Practical Partition-Based Theorem Proving for Large Knowledge Bases</b> [ppt]
Bill MacCartney, Sheila A. McIlraith, Eyal Amir, Tomas Uribe
18th International Joint Conference on Artificial Intelligence (IJCAI-03), August 2003

<b>Partition-Based Logical Reasoning</b> [ppt, html/ie]
Bill MacCartney, Sheila A. McIlraith, Eyal Amir, Tomas Uribe
RKF Principal Investigators Meeting, Hilton Head SC, 13-15 November 2002[/publication]


[introduction]Other stuff I did before

<b>Knowledge Systems Lab, Stanford University</b>, Researcher, 2002-2003
AI research. Development & testing of algorithms for partition-based reasoning in large knowledge bases. More...

<b>SayIt, Inc.</b>, VP Production, 1999-2000
Website production: process management, UI design, information architecture, user experience analysis. More...

<b>D. E. Shaw & Co.</b>, Quant/Trader, 1996-1998
Fixed-income arbitrage trading, computational modeling. Managed $5B of Japanese bonds & derivatives. More...

<b>Cycorp, Inc.</b>, Researcher, Knowledge Engineer, 1994-1995
AI research. Led effort to deploy Cyc in a multi-agent architecture to achieve distributed inferencing. More...

<b>TestTakers, Inc.</b>, Head of Research & Development, 1991-1994
Small business, many roles. Led curriculum development. Built core database app. Lots of teaching. More...

<b>J. W. Goethe Universität Frankfurt</b>, Fulbright scholar, 1990-1991
One year of graduate study in philosophy of language. Developed German language skills. More...

<b>Princeton University</b>, B.A. in Philosophy, summa cum laude, 1986-1990
Prize for best senior thesis in logic and epistemology. GPA 4.0/3.9. GRE 800/800/800. LSAT 180. More...[/introduction]

<IMAGE src="i/spacer.gif" alt=""/>

[pic]<IMAGE src="i/office-small.jpg" alt=""/>[/pic]

<IMAGE src="i/spacer.gif" alt=""/>


[contactinfo]Contact info

<b>Email:</b> <IMAGE src="i/wcmac-email-color.jpg" alt="my domain is cs.stanford.edu and my username is wcmac"/>

<b>Phone:</b> [phone]650-723-3796[/phone]

<b>Office:</b>
[address]Computer Science Department
Gates Building, Room 114
Stanford University
Stanford, CA 94305-9020[/address][/contactinfo]



We never perform a computation ourselves, we just hitch a ride on the great Computation that is going on already. Tommaso Toffoli



Yes, we have a soul. But it's made of lots of tiny robots. Giulio Giorelli



Research is the process of going up alleys to see if they are blind. Marston Bates
