[pic]<IMAGE src="muthian-photo-small.jpg" alt="Muthian Sivathanu"/>[/pic]

  <font size=5> <b>
  [contactinfo]MUTHIAN SIVATHANU </b> </font>


  [address]1228 Vicente Dr, Apt H
  Sunnyvale CA 94086[/address]
  Phone: [phone]650-961-2059[/phone]
  [email]muthian@cs.wisc.edu[/email][/contactinfo]


[   Research |  Publications |  Talks |  Internships |  Resume |  About me |  Contact ]

[introduction]Hi! I graduated with a Ph.D. from the [phduniv]University of Wisconsin Madison[/phduniv] in [phddate]May 2005[/phddate]. I now work at [affiliation]Google Inc.[/affiliation] A copy of my Ph.D. dissertation on "Semantically-smart Disk Systems" can be found here.

My old job application materials are still available online.[/introduction]

<b><font size=3>Research</font></b>

My research interests span the broad boundaries of [interests]operating systems[/interests] and [interests]distributed systems[/interests].

For my Ph.D., I worked with Prof. Andrea Arpaci-Dusseau and Prof. Remzi Arpaci-Dusseau .

My thesis research proposed and evaluated a new class of storage systems that are aware of how the file system or DBMS is using them, and exploits this knowledge to implement new optimizations and functionality that are currently impossible to implement. A straightforward way to making storage systems semantically-aware is to change the interface to storage to convey richer information; however, modifying an interface as basic as SCSI is not very pragmatic, since it requires broad industry consensus and raises various legacy issues. In contrast, semantically-smart storage systems automatically track higher level semantic information about the file system or DBMS, by careful observation of block level reads and writes from underneath an unmodified SCSI interface. By requiring no changes to the existing interface, semantically-smart disks enable seamless deployment and adoption of new functionality. I demonstrated the utility of semantic knowledge within a storage system by design and prototyping of a variety of new functionality aimed at improving the availability, security, and performance of storage systems. In each of these cases, semantic knowledge was shown to enable functionality that is otherwise impossible to provide in the storage hierarchy. More details are available in my research statement.

Earlier to this, I worked on Scriptable RPC, a framework that enables distributed system services to take advantage of the increasing level of intelligence in system components (be it disk, network interface or memory), resulting in extensible, robust and high-performance distributed systems.

In the past, I have also worked on a host of projects including distributed file systems for network attached storage, distributed RAID algorithms over network storage and security and anonymity architectures for network storage. Some of my old projects can be found here.

[publication]<b><font size=3>Publications</font></b>

<b><font size=3>Journal Publications</font></b>

  * <b>Improving Storage System Availability with D-GRAID</b>
    Muthian Sivathanu , Vijayan Prabhakaran, Andrea C. Arpaci-Dusseau , Remzi H. Arpaci-Dusseau
    ACM Transactions on Storage <b>(TOS '05)</b>, Volume 1, Issue 2, May 2005, pages 133--170.

    <b><font size=3>Conference Publications</font></b>

  * <b>A Logic of File Systems</b>
    Muthian Sivathanu, Andrea C. Arpaci-Dusseau, Remzi H. Arpaci-Dusseau, Somesh Jha
    The Fourth USENIX Conference on File and Storage Technologies <b>(FAST '05) </b>
    December 2005, San Francisco, CA

  * <b>Database-Aware Semantically-Smart Storage</b>
    Muthian Sivathanu, Lakshmi Bairavasundaram, Andrea C. Arpaci-Dusseau, Remzi H. Arpaci-Dusseau
    The Fourth USENIX Conference on File and Storage Technologies <b>(FAST '05) </b>
    December 2005, San Francisco, CA

  * <b>Life or Death at Block Level</b>
    Muthian Sivathanu, Lakshmi Bairavasundaram, Andrea C. Arpaci-Dusseau, Remzi H. Arpaci-Dusseau
    The 6th Symposium on Operating Systems Design and Implementation <b>(OSDI '04)</b>
    December 2004, San Francisco, CA

  * <b> X-RAY: A Non-Invasive Exclusive Caching Mechanism for RAIDs </b>
    Lakshmi Bairavasundaram, Muthian Sivathanu, Andrea C. Arpaci-Dusseau, Remzi H. Arpaci-Dusseau
    Proceedings of the 31st International Symposium on Computer Architecture <b>(ISCA '04)</b>
    June 2004 Munich, Germany

  * <b> Improving Storage System Availability with D-GRAID</b>
    Muthian Sivathanu , Vijayan Prabhakaran, Andrea C. Arpaci-Dusseau , Remzi H. Arpaci-Dusseau
    Proceedings of the Third USENIX Conference on File and Storage Technologies <b>(FAST '04)</b>
    March 2004 San Francisco, CA
    <b>Best Student Paper Award</b>

  * <b>Semantically-Smart Disk Systems</b>
    Muthian Sivathanu , Vijayan Prabhakaran, Florentina Popovici, Timothy Denehy, Andrea C. Arpaci-Dusseau , Remzi H. Arpaci-Dusseau
    The Second USENIX Conference on File and Storage Technologies <b>(FAST '03)</b>
    March 2003 San Francisco, CA

  * <b>Evolving RPC for Active Storage</b>
    Muthian Sivathanu , Andrea C. Arpaci-Dusseau , Remzi H. Arpaci-Dusseau
    Architectural Support for Programming Languages and Operating Systems <b>(ASPLOS - X)</b> , pages 264--276
    October 2002 San Jose, CA

  * <b>Block Asynchronous I/O: A Flexible Infrastructure For User-Level Filesystems</b>
    Muthian Sivathanu , Venkateshwaran Venkataramani , Remzi H. Arpaci-Dusseau
    The International Conference on High-Performance Computing <b>(HiPC '01)</b>
    LNCS 2228 Springer 2001, pages 249--261 December 2001 India

  * <b>Self-determination of Optimal Granularity of Distributed Objects</b>
    S.Muthian, S.Swaminathan, R.Muralikrishna, Arul Siromoney, V.Uma Maheswari,
    Poster Session, The International Conference on High Performance Computing <b>(HiPC '99)</b> , December 1999, Calcutta, India

    <b><font size=3>Technical Reports </font></b>

  * <b>Adaptive Control for Anonymous Network-Attached Storage</b>
    Muthian Sivathanu, Andrea C. Arpaci-Dusseau, Remzi C. Arpaci-Dusseau,
    University of Wisconsin-Madison, February 2002.

  * <b>The WiND Filesystem (WFS)</b>
    Muthian Sivathanu
    Masters' Project Report, University of Wisconsin-Madison
    December 2001

    <b><font size=3>Selected Talks</font></b>


    March 2004: <b>"Improving Storage System Availability with D-GRAID"</b>, FAST 2004, San Francisco.
    August 2003: <b>"Improving Storage System Availability with D-GRAID"</b>, HP Labs, Palo Alto (Invited Talk)
    August 2003: <b>"Improving Storage System Availability with D-GRAID"</b>, IBM Research, Almaden(Invited Talk)
    August 2003: <b>"Performance Isolation on Google Servers"</b>, Google Inc.(Intern Talk)
    October 2002: <b>"Evolving RPC for Active Storage"</b>, ASPLOS 2002, San Jose
    August 2002: <b>"Evolving RPC for Active Storage"</b>, Hewlett Packard Labs, Palo Alto (Invited Talk)
    August 2002: <b>"Scalable Resource Management for Wide-Area Storage"</b>, IBM Almaden (Intern Talk)
    August 2001: <b>"Scalable Fault-tolerant Replication in Wide-Area Storage"</b> , HP labs, Palo Alto (Intern Talk)
    June 2001: <b> "The WiND Filesystem"</b>, HP labs, Palo Alto (Intern Intro Talk)

    <b><font size=3>Internships</font></b>

    <b>Summer 2004</b>

    Microsoft , Redmond, WA (Advanced Operating Systems group)
    Details confidential.

    <b>Summer 2003</b>

    Google Inc. , Mountain View (Infrastructure group)
    Designed and implemented mechanisms for effective performance isolation on Google servers.

    <b>Summer 2002</b>

    IBM  Almaden Research Center , San Jose ( Storage Systems group )
    Designed an architecture for scalable, decentralized resource management in wide-area storage.   The architecture,  targeted at wide-area storage service providers,  facilitates, inter alia,  optimal replication of competing client virtual disks, both in terms of the number of replicas and their placement across the globe.   The policies ensure that individual disks take mostly autonomous decisions, while still working towards globally optimal resource allocation.

    <b>Summer 2001</b>

    Hewlett Packard Labs, Palo Alto (Storage Systems program )
    Designed  a protocol for scalable, fault-tolerant wide area replication that permits concurrent conflicting updates from all replicas in a geographically distributed storage system.   The work resulted in a patent disclosure.   A prototype system was built with an iSCSI device driver at the client talking to multiple disks interacting through rpc.[/publication]

    <b><font size=3>Resume</font></b>

    PDF

    [introduction]<b><font size=3>About Me</font></b>

    For the customary two-line history info, I was born in 1979 in Chennai (aka Madras), India. I did my schooling in St Johns MHSS, Chennai. A four-year stint at [bsuniv]Anna University[/bsuniv] yielded me a B.E. in [bsmajor]Computer Science and Engineering[/bsmajor]. Since August 2000, I have been a graduate student here at [msuniv]UW[/msuniv].

    Besides Computer Science, which is my primary interest, I am interested in [interests]Law and Business[/interests], mainly from the influence of my father, a leading lawyer in India. This also partly explains my interest in choosing Business as my Ph.D. minor.

    I have been taking quite a few fun courses during my stay at UW, including Ice Skating, Swimming and Tennis. In addition, I also play badminton in my spare time.

    I like collecting quotes; Here is a collection of quotes I like.[/introduction]

    <IMAGE src="http://cgi.cs.wisc.edu/scripts/muthian/counter.pl" alt=""/>
