<IMAGE src="/aboutus/images/hdrimg_csbuilding.jpg" alt=""/>   Universität Karlsruhe (TH)
  Department of Computer Science
 System Architecture Group
<IMAGE src="/images/transpixel.gif" alt=""/>

Home About Us Teaching Research Power Management P2P L4Ka Project Search Deutsch  <IMAGE src="/images/de.gif" alt="de"/>

People Contact Us History Open Positions

<IMAGE src="/images/transpixel.gif" alt=""/>

<IMAGE src="/images/transpixel.gif" alt=""/>
<IMAGE src="/images/yellowarrow-d.gif" alt=""/> Head

<IMAGE src="/images/transsquare.gif" alt=""/> Prof. Dr. Frank Bellosa

<IMAGE src="/images/transpixel.gif" alt=""/>
<IMAGE src="/images/yellowarrow-d.gif" alt=""/> Secretary

<IMAGE src="/images/transsquare.gif" alt=""/> Andrea Engelhart

<IMAGE src="/images/transpixel.gif" alt=""/>
<IMAGE src="/images/yellowarrow-d.gif" alt=""/> Professors

<IMAGE src="/images/transsquare.gif" alt=""/> Prof. Dr.
   Jochen Liedtke <IMAGE src="/aboutus/people/images/kreuz.gif" alt="kreuz"/>

<IMAGE src="/images/transsquare.gif" alt=""/> Prof. Dr.
    Horst Wettstein <IMAGE src="/aboutus/people/images/kreuz.gif" alt="kreuz"/>

<IMAGE src="/images/transpixel.gif" alt=""/>
<IMAGE src="/images/yellowarrow-d.gif" alt=""/> Research Staff

<IMAGE src="/images/transsquare.gif" alt=""/> Simon Kellner

<IMAGE src="/images/transsquare.gif" alt=""/> Joshua LeVasseur

<IMAGE src="/images/transsquare.gif" alt=""/> Gerd Liefländer

<IMAGE src="/images/transsquare.gif" alt=""/> Andreas Merkel

<IMAGE src="/images/transsquare.gif" alt=""/> Raphael Neider

<IMAGE src="/images/transsquare.gif" alt=""/> Jan Stöß

<IMAGE src="/images/transpixel.gif" alt=""/>
<IMAGE src="/images/yellowarrow-d.gif" alt=""/> Research Staff (P2P)

<IMAGE src="/images/transsquare.gif" alt=""/> Pengfei Di

<IMAGE src="/images/transsquare.gif" alt=""/> Johannes Eickhold

<IMAGE src="/images/transsquare.gif" alt=""/> Dr. Thomas Fuhrmann

<IMAGE src="/images/transsquare.gif" alt=""/> M. Yaser Houri

<IMAGE src="/images/transsquare.gif" alt=""/> Kendy Kutzner

<IMAGE src="/images/transsquare.gif" alt=""/> Carola Kämpfe

<IMAGE src="/images/transsquare.gif" alt=""/> Sean O'Donoghue

<IMAGE src="/images/transsquare.gif" alt=""/> Bjoern Saballus

<IMAGE src="/images/transpixel.gif" alt=""/>
<IMAGE src="/images/yellowarrow-d.gif" alt=""/> Technical Staff

<IMAGE src="/images/transsquare.gif" alt=""/> James McCuller

<IMAGE src="/images/transsquare.gif" alt=""/> Heinz Zoller

<IMAGE src="/images/transpixel.gif" alt=""/>
<IMAGE src="/images/yellowarrow-r.gif" alt=""/>Students

<IMAGE src="/images/transpixel.gif" alt=""/>
<IMAGE src="/images/yellowarrow-r.gif" alt=""/>Former Staff

<IMAGE src="/images/transpixel.gif" alt=""/>
<IMAGE src="/images/yellowarrow-r.gif" alt=""/>F. Staff (Prof. Wettstein)

<IMAGE src="/images/transpixel.gif" alt=""/>
<IMAGE src="/images/yellowarrow-r.gif" alt=""/>Former Students

<IMAGE src="/images/transpixel.gif" alt=""/>
<IMAGE src="/images/blackpixel.gif" alt=""/> <IMAGE src="/images/transpixel.gif" alt=""/>
Search for Person

<IMAGE src="/images/transpixel.gif" alt=""/>

[name]Joshua LeVasseur[/name]


[pic]<IMAGE src="/aboutus/people/images/levasseur.jpg" alt="Joshua LeVasseur"/>[/pic] <IMAGE src="/images/transpixel.gif" alt=""/>

[contactinfo]<b>Postal Address:</b>

[address]Lehrstuhl Systemarchitektur
Universität Karlsruhe
D 76128 Karlsruhe[/address]

<b>Contact:</b>

Phone: [phone]+49 (721) 608-3836[/phone]

Fax: [fax]+49 (721) 608-7664[/fax]

eMail: [email]levasseur<IMAGE src="/images/funnysign.png" alt=" at "/>ira.uka.de[/email]

<IMAGE src="/images/transpixel.gif" alt=""/>

<b>Visiting Address:</b>

[address]Room 163
Building 50.34 (Am Fasanengarten 5)
Universität Karlsruhe[/address][/contactinfo]

<b>Personal Website:</b>

[homepage]http://i30www.ira.uka.de/~joshua/[/homepage]

[introduction]<b>Position:</b>   [position]Research Staff Member and Ph.D. Student[/position][/introduction]


<b>Teaching (Last 2 Terms): </b>

WT 06/07: Group Seminar (Seminar)
ST 2006: Microkernel Construction (Lecture)
ST 2006: System Design and Implementation (Lecture)
ST 2006: System Design and Implementation (Practical Work)
ST 2006: Group Seminar (Seminar)

<b>Teaching (More Terms)</b>


<b>Research: </b>

L4Ka Project


<b>Talks</b> <b>Student Projects</b>

<IMAGE src="/images/transpixel.gif" alt=""/> <IMAGE src="/images/transpixel.gif" alt=""/> <IMAGE src="/images/transpixel.gif" alt=""/>


[publication]<b>Publications:</b>


<b>Pre-Virtualization: Soft Layering for Virtual Machines</b>
Joshua LeVasseur, Volkmar Uhlig, Matthew Chapman, Peter Chubb, Ben Leslie, and Gernot Heiser
Technical Report 2006-15, Fakultät für Informatik, Universität Karlsruhe, July 2006

[pdf] [bib]


<b>Pre-Virtualization: Slashing the Cost of Virtualization</b>
Joshua LeVasseur, Volkmar Uhlig, Matthew Chapman, Peter Chubb, Ben Leslie, Gernot Heiser
Fakultät für Informatik, Universität Karlsruhe (TH), Technical Report 2005-30, November 2005

[pdf] [bib]


<b>Pre-Virtualization: Uniting Two Worlds</b>
Joshua LeVasseur, Volkmar Uhlig, Ben Leslie, Matthew Chapman and Gernot Heiser
Poster session of 20th ACM Symposium on Operating Systems Principles (SOSP-20), October 23-26, 2005, Brighton, United Kingdom

[pdf] [bib]


<b>Are Virtual-Machine Monitors Microkernels Done Right?</b>
Gernot Heiser, Volkmar Uhlig, Joshua LeVasseur
National ICT Australia, Technical Report, October 2005

[pdf] [bib]


<b>Unmodified Device Driver Reuse and Improved System Dependability via Virtual Machines</b>
Joshua LeVasseur, Volkmar Uhlig, Jan Stoess, and Stefan Götz
Proceedings of the Sixth Symposium on Operating Systems Design and Implementation (OSDI '04), December 6-8, 2004, San Francisco, CA

[pdf] [bib]


<b>A Sledgehammer Approach to Reuse of Legacy Device Drivers</b>
Joshua LeVasseur and Volkmar Uhlig
Proceedings of the 11th ACM SIGOPS European Workshop, September 19-22, 2004, Leuven, Belgium

[pdf] [bib]


<b>Towards Scalable Multiprocessor Virtual Machines</b>
Volkmar Uhlig, Joshua LeVasseur, Espen Skoglund, and Uwe Dannowski
Proceedings of the 3rd Virtual Machine Research & Technology Symposium (VM'04), May 6-7, 2004, San Jose, CA

[pdf] [ps] [bib]


<b>Flexible and Scalable Virtual Machines</b>
Volkmar Uhlig, Joshua LeVasseur, Espen Skoglund, and Uwe Dannowski
Poster session of 19th ACM Symposium on Operating Systems Principles (SOSP-19), October 19-22, 2003, Bolton Landing, NY

[pdf] [bib]


<b>High-End Workstation Compute Farms Using Windows NT</b>
Srinivas Nimmagadda, Joshua LeVasseur, and Rumi Zahir
Proceedings of the 3rd USENIX Windows NT Symposium, Seattle, Washington, pp. 11-20, USENIX Association, July 12-15, 1999

[pdf] [bib]


<b>Case Study: Visualizing Internet Resources</b>
Nahum Gershon, Joshua LeVasseur, Joel Winstead, James Croall, Ari Pernick, and William Ruh
Proceedings of the First Information Visualization Symposium, pp. 122-128, Atlanta, GA, October 30-31, 1995[/publication]

[pdf] [bib]


Mail to webmaster
