<IMAGE src="pictures/TUELogo.gif" alt=""/> <IMAGE src="pictures/AlgoLogo.gif" alt=""/>

<IMAGE src="pictures/blank.gif" alt=""/> <IMAGE src="pictures/blank.gif" alt=""/> <IMAGE src="pictures/blank.gif" alt=""/>

<IMAGE src="pictures/blank.gif" alt=""/><IMAGE src="pictures/blank.gif" alt=""/>[pic]<IMAGE src="pictures/pasfoto.gif" alt=""/>[/pic]<IMAGE src="pictures/blank.gif" alt=""/><IMAGE src="pictures/blank.gif" alt=""/> <b><font size=5>Bettina Speckmann</font></b>

<IMAGE src="pictures/blank.gif" alt=""/>

[contactinfo]<b>email</b> [email]speckman@win.tue.nl[/email]
<b>phone</b> [phone]+31-40-247-3076[/phone]
<b>fax</b> [fax]+31-40- 247-5361[/fax]

<IMAGE src="pictures/blank.gif" alt=""/>

[affiliation]Department of Mathematics and Computer Science[/affiliation]
[address]TU Eindhoven, HG 7.34
P.O. Box 513
5600 MB Eindhoven[/address][/contactinfo]

<IMAGE src="pictures/blank.gif" alt=""/>

Publications

Java Demos

Rectangular Cartograms

<IMAGE src="Cartograms/pictures/WorldTiny.gif" alt=""/>

Geometric Algorithms

Advanced Algorithms

Graph Drawing


<IMAGE src="pictures/blank.gif" alt=""/>

<font size=2><IMAGE src="pictures/blank.gif" alt=""/></font>

[introduction]I am an [position]assistant professor[/position] ("universitair docent" in Dutch) at the [affiliation]Department of Mathematics and Computer Science of the Technical University of Eindhoven[/affiliation] and a [position]member[/position] of the [affiliation]Algorithms group[/affiliation].[/introduction]

[resinterests]<b>Research Interests</b>: [interests]Algorithms and data structures[/interests]; [interests]discrete and computational geometry[/interests]; [interests]applications of computational geometry to geographic information systems[/interests][/resinterests].


<b>Teaching</b>: Advanced Algorithms (2IL40) in the winter trimester 2005/2006, Geometric Algorithms (2IL20) in the spring trimester 2006, and a seminar course on Graph Drawing (2IL90) in the fall trimester 2006/2007.

<IMAGE src="pictures/DutchFlag.jpeg" alt=""/>We are organizing the 3rd Dutch Computational Geometry Day on July 3, 2006, in Eindhoven.

<b>
[resactivity]</b><b>PC member</b>:

  * 21st ACM Symposium on Computational Geometry (SoCG 2005, June 6-8, Pisa, Italy)

  * 13th European Symposium on Algorithms - Engineering and Applications Track (ESA 2005, October 3-6, Mallorca, Spain)

  * 18th Canadian Conference on Computational Geometry (CCCG 2006, August 14-16, Kingston, Canada)

  * 14th International Symposium on Graph Drawing (GD 2006, September 18-20, Karlsruhe, Germany)<b> </b>[/resactivity]


[introduction]<b>Historical Remarks</b>: Before I came to Eindhoven I was a postdoc in the Combinatorics, Geometry and Computation European Graduate Program at ETH Zurich. In Zurich I was a member of the Theory of Combinatorial Algorithms group headed by Emo Welzl. I received my [phddegree]Ph.D.[/phddegree] from the [phduniv]University of British Columbia[/phduniv] in Vancouver, Canada, where I was supervised by David Kirkpatrick and Jack Snoeyink.[/introduction]

<IMAGE src="pictures/blank.gif" alt=""/>last modified: 21-Nov-2006 contact: Bettina Speckmann<IMAGE src="pictures/blank.gif" alt=""/>

<IMAGE src="pictures/blank.gif" alt=""/>

<IMAGE src="pictures/blank.gif" alt=""/>
