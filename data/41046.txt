[contactinfo][pic]<IMAGE src="images/hu.jpg" alt=""/>[/pic] <b>Hu Haodong</b><font size=6> </font>
<font size=2> [affiliation]Computer Science Department
State University of New York at Stony Brook[/affiliation] </font><font size=3>
</font> <font size=2> [address]Stony Brook, NY 11794-4400
USA[/address]</font><font size=3> </font>

<font size=2>Tel.: [phone]+1 (631) 902-9668[/phone] 
E-mail: [email]huhd@cs.sunysb.edu[/email]</font>[/contactinfo]

<IMAGE src="images/logo.gif" alt=""/>

<b> Major:</b> Algorithm <b> Advisor: </b> Michael Bender <b> Interesting Fields: </b>

  * <font size=2>[interests]memory hierarchy[/interests]</font>

  * <font size=2>[interests]cache-oblivious tree[/interests]</font>

  * <font size=2>[interests]computational biology[/interests]</font>

  * <font size=2>[interests]scheduling[/interests]</font>

<b><font size=5> Vita</font></b>

<b> =></b>

[education]<b> Education:</b>

  * <b> [affiliation]State University of New York at Stony Brook[/affiliation]</b>
    <font size=2>[position]PhD student[/position] 2003-present in [phdmajor]Computer Science[/phdmajor]
    Advisor: Michael Bender</font>

  * <b> [msuniv]State University of New York at Stony Brook[/msuniv]</b>
    <font size=2> MA in [msmajor]mathematics[/msmajor], 2001-[msdate]2003[/msdate]</font>

  * <b> [msuniv]Nankai Institute of Mathematics[/msuniv]</b>, P. R. China
    <font size=2>MA in [msmajor]financial mathematics[/msmajor], 1999-[msdate]2001[/msdate]</font>

  * <b> [bsuniv]Nankai University[/bsuniv]</b>, P. R. China
    <font size=2>BA in [bsmajor]mathematics[/bsmajor], 1995-[bsdate]1999[/bsdate]</font>[/education]

<b> Teaching Experience: </b>

  * <font size=2>Teaching Assistant in CSE 328 Fundamentals of Computer Graphics (Sping 2006) </font>

  * <font size=2>Research Assistant in Algorithm Lab (Spring 2006)</font>

  * <font size=2>Teaching Assistant in CSE 373 Undergraduate Algorithm (Fall 2005)  </font>

  * <font size=2>Research Assistant in Algorithm Lab (Fall 2005)</font>

  * <font size=2>Research Assistant in Algorithm Lab (Spring 2005)</font>

  * <font size=2>Teaching Assistant in CSE540 Graduate Theory of Computation (Fall 2004)</font>

  * <font size=2>Teaching Assistant in CSE548 Graduate Analysis of Algorithms (Fall 2004)</font>

  * <font size=2>Teaching Assistant in CSE303 Undergraduate Theory of Computation (Spring 2004)</font>

  * <font size=2>Teaching Assistant in CSE548 Graduate Analysis of Algorithms (Fall 2003)</font><font size=2></font>

[publication]<b> Papers:</b>

  1.  M. A. Bender, H. Hu. "An Adaptive Packet-Memory Array." <font size=2>In</font><font size=2> </font><font size=2> Preceedings of the 25th ACM SIGACT-SIGMOD-SIGART Symposium on Principles of Database Systems (2006PODS). </font><b>
    Best Newcomer Award</b><font size=2> in this year!</font>

  2.  F. Swidan, M. A. Bender, D. Ge, S. He, H. Hu, and R. Pinter. "Sorting by length-weighted reversals: Dealing with signs and circularity." In <font size=2>Proceedings of the 15th Annual Combinatorial Pattern Matching Symposium</font> (CPM), volume 3109 of Lecture Notes in Computer Science, pages 32-46, 2004.

  3. <font size=2> M. A.</font> Bender, D. Ge,  S. He,  H. Hu,  R. Pinter,  S. Skiena, F. Swidan. "Improved Bounds on Sorting with Length-Weighted Reversals."<font size=2>In Proceedings of the 15th Annual ACM-SIAM Symposium on Discrete Algorithms (SODA04), </font> <font size=2>pages 912-921, 2004.</font>

  4.  M. A. Bender, G. S. Brodal, R. Fagerberg, D. Ge, S. He, H. Hu, J. Iacono, and A. Lopez-Ortiz. "The Cost of Cache-Oblivious Searching." <font size=2>In</font> <font size=2>Proceedings of the 44th Annual Symposium on Foundations of Computer Science (FOCS), pages 271-280, 2003.</font>[/publication]

¡¡

[contactinfo]<font size=2><b>Contact: </b> </font> [email]huhd@cs.sunysb.edu[/email][/contactinfo]
<font size=2> Last modified
Copyrights@2003 </font> <font size=2> Haodong Hu, All Rights Reserved </font>

<IMAGE src="http://mystatus.skype.com/balloon/haodong" alt="My status"/>
