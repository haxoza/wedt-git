<IMAGE src="logo.gif" alt=""/><IMAGE src="left_beave1.jpg" alt=""/><IMAGE src="gradstudent.jpg" alt=""/><IMAGE src="right_sun.jpg" alt=""/><IMAGE src="logo.gif" alt=""/>

<font size=5>
</font>

<font size=5> [pic]<IMAGE src="Tiwari_webpage.JPG" alt=""/>[/pic]
</font>

[introduction]<b><font size=6>Abhishek Tiwari</font></b>

<font size=5>[position]Graduate student[/position]</font>
[affiliation]<font size=5>Department of Electrical Engineering</font>
<font size=5>California Institute of Technology</font>[/affiliation][/introduction]

<font size=5>
</font>

[introduction]<b><font size=5>Education and past research</font></b>

  * <font size=4>BTech [bsmajor]Electrical Engineering[/bsmajor], [bsuniv]Indian Institute of Technology Kanpur[/bsuniv], [bsdate]2001[/bsdate].</font>

  * <font size=4>MS [msmajor]Electrical Engineering[/msmajor], [msuniv]California Institute of Technology[/msuniv], Pasadena CA [msdate]2003[/msdate]. </font>

  * <font size=4>PhD Student, [phduniv]California Institute of Technology[/phduniv], Pasadena CA since October 2001.</font>

  * <font size=4>Worked at the Center for Robotics, IIT Kanpur to develop a 'Robot Soccer' team. Represented IIT Kanpur in the FIRA MIROSOT championships held in Singapore in May'2000. Visit the Robot Soccer web page @ IIT Kanpur.</font>

  * <font size=4>Summer Intern at the Autonomous Systems Laboratory, Department of Micro technique, EPF Lausanne, Switzerland in June-July'2000. I worked with the cute little robot "ALICE" developed at ASL. I developed reactive rule based collaborative strategies for 6 ALICEs playing soccer on a field of size same as an A4 sheet. Visit the ALICE Soccer Page for some cool pictures and mpegs of ALICEs performing under rule based strategies.</font>

  * <font size=4>Autonomous Navigation of a Lego robot using optical flow techniques in IIT Kanpur. click here for details.</font>

  * <font size=4>From July 2001 - September 2001, I worked with the Collective Robotics group at Caltech, on an infrared sensor belt for the Moorebot.</font>

  * <font size=4>Developed a PWM based motor controller and a serial port and USB interface for the Kelly vehicles</font><font size=4> under the Multi Vehicle Wireless Testbed Project.  </font>[/introduction]


[resinterests]<b><font size=5>Research and Publications
</font></b><font size=4>I am interested in [interests]Spatio-temporal planning problems[/interests] like those of [interests]Multi Agent Rendezvous[/interests] and [interests]Dynamic Sensor Coverage[/interests]. 
</font>

<font size=4>Rendezvous problem is one of cooperative control, where the designer is required to design control laws under which multiple agents can be made to arrive at a common location at the same time. The problem and its variants finds applications in Spacecraft Docking, Cooperative Strike, Autonomous Formation Flight, Ballistic Missile Interception. I am trying to answer analysis questions such as
</font><font size=4>
"Given a cooperative control law, is it possible to say with a cetain level of confidence that the rendezvous problem will be successfully solved in the presence of uncertainty?"

The tools I have used for my analysis are Lyapunov Stability Theory, Invariance of regions on the phase plane. I am also trying to use SOSTOOLs to automate the process of generating certificates.
</font>

<font size=4>Dynamic Sensor Coverage problem falls more under the category of Situational Awaremess. The problem is to deploy a "few", "limited range", "mobile" sensors in a time varying uncertain environment</font><font size=4> so as to maintian appreciable estimates of the states of the environment at all time.

Applications can be Weather Monitoring, Automatic Surveillance, Reconnaissance, Sensor Networks.

The tools used are Kalman Filter theory and Markov chains.
</font>

<font size=4>In addition to this, I am also interested in the nexus between Control theory and Communication which have been traditionally studied seperately. This includes questions like...</font>

<font size=4>What happens if the feedback channel in a typical closed loop control system is imperfect?</font>
<font size=4>How is the performance of a control system affected if its various components are linked via a communication network?</font>
<font size=4>Such questions find application in real life. The formation flight of aircrafts and satellites requires control signals to be sent over an imperfect channel.[/resinterests]
</font>

[publication]<font size=4>Following is the list of important publications</font>

A Framework for  Lyapunov Certificates  for Multi-Vehicle  Rendezvous  Problems.
A. Tiwari, J. Fung, J. M. Carson, R. Bhattacharya, R. M. Murray.
Proceedings of the American Control Conference (ACC). 2004.

Polyhedral Cone Invariance Applied to Rendezvous of Multiple Agents.
A. Tiwari, J. Fung, R. Bhattacharya, R. M. Murray.
Proceedings of the IEEE Conference on Decision and Control (CDC), 2004.

Ellipsoidal Cone and Rendezvous of Multiple Agents.
R. Bhattacharya, J. Fung, A. Tiwari, R. M. Murray.
Proceedings of the IEEE Conference on Decision and Control (CDC), 2004.

Analysis of Dynamic Sensor Coverage Problem using Kalman Filters for Estimation.
A. Tiwari, M. Jun, D. E. Jeffcoat, R. M. Murray
 Proceedings of the 16th IFAC World Congress, 2005

Estimation of Linear Stochastic Systems over a Queuing Network
M. Epstein, A. Tiwari, L. Shi, R. M. Murray
To appear in the Proceedings of the International Conference of Sensor Networks (SENET), 2005.

Estimation with Information Loss: Asymptotic Analysis and Error Bounds
L. Shi, M. Epstein, A. Tiwari, R.M. Murray
To appear in the Proceedings of the IEEE Conference on Decision and Control (CDC), 2005.

<font size=4></font>

<font size=4>AFOSR Meeting Poster
</font>

<font size=4>Candidacy Presentation (06/03/04) .ppt, .pdf
</font>

<font size=4>9th Southern California Nonlinear Control Workshop, Nov 19th 2004, UCLA. ppt
</font>

<font size=4>Testbed requirements for summer-05 ppt
</font>[/publication]

[resinterests]<b><font size=5>Research Interests</font></b>

<font size=4>[interests]Distributed Control[/interests], [interests]Cooperative Control[/interests], [interests]Sensor Fusion[/interests], [interests]Sensor Networks[/interests], [interests]Wireless Communication[/interests],.</font>[/resinterests]

<font size=4>Research Advisor: Dr. Richard Murray</font>

<b><font size=5>Awards and honors</font></b>

  <font size=4>"Notional Prize for Academic Excellence" for the year 1997-98 during my undergraduate studies at IIT Kanpur.</font>

  <font size=4> Killgore Fellowship award for the first year of my graduate studies at Caltech.</font>

<b><font size=5>Curriculum Vitae</font></b>
<b><font size=5>Pictures</font></b>

<b><font size=5>Friends on the web</font></b>

<font size=4>Abhijeet Neogy</font>
<font size=4>Amrit Pratap</font>
<font size=4>Chaitanya Rao</font>
<font size=4>M Phani Karthik</font>
Shankar Kalyanaraman
<font size=4>Swaminathan Krishnan</font>
<font size=4>Tejaswi N V</font>

[contactinfo]<b><font size=5>Contact Information</font></b>

<IMAGE src="mail.gif" alt=""/><font size=4>Abhishek Tiwari</font>
<font size=4> 135, [affiliation]Steele Laboratory[/affiliation]</font>
<font size=4> [affiliation]California Institute of Technology[/affiliation](107-81)</font>
<font size=4> [address]1200, East California Boulevard</font>
<font size=4> Pasadena, CA - 91125[/address]</font>

<font size=4>email : [email]atiwari@caltech.edu[/email]</font>
<font size=4> [email]atiwari@cds.caltech.edu[/email]</font>
<IMAGE src="telephone.gif" alt=""/><font size=4> [phone]++1-626-395-3369[/phone](Office)</font>

<IMAGE src="fax.gif" alt=""/><font size=4> [fax]++1-626-796-8914[/fax](Fax)</font>[/contactinfo]
