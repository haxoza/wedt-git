<IMAGE src="../images/site_nav/spacer.gif" alt="Skip Navigation"/> <IMAGE src="../images/site_nav/spacer.gif" alt=""/> <IMAGE src="../images/site_nav/spacer.gif" alt=""/> <IMAGE src="../images/site_nav/spacer.gif" alt=""/> <IMAGE src="../images/site_nav/spacer.gif" alt=""/> <IMAGE src="../images/site_nav/spacer.gif" alt=""/> <IMAGE src="../images/site_nav/spacer.gif" alt=""/> <IMAGE src="../images/site_nav/spacer.gif" alt=""/> <IMAGE src="../images/site_nav/spacer.gif" alt=""/> <IMAGE src="../images/site_nav/spacer.gif" alt=""/> <IMAGE src="../images/site_nav/spacer.gif" alt=""/>

<IMAGE src="../images/site_nav/ohiostate.gif" alt="Ohio State University Logo"/> <IMAGE src="../images/site_nav/m_title1.gif" alt="Mechanical Engineering Title"/> <IMAGE src="../images/site_nav/e_title1.gif" alt=""/> <IMAGE src="../images/site_nav/scottlab.gif" alt="Picture of Scott Laboratory"/> <IMAGE src="../images/site_nav/spacer.gif" alt=""/>

<IMAGE src="../images/site_nav/quickjumps.gif" alt="table of navigational jumps along the top and left sides"/> <IMAGE src="../images/site_nav/why_me.gif" alt="Why choose Mechanical Engineering link"/> <IMAGE src="../images/site_nav/pstuds.gif" alt="Prospective Students link"/> <IMAGE src="../images/site_nav/deptoverv.gif" alt="Department Overview link"/> <IMAGE src="../images/site_nav/focus_area.gif" alt="Focus Areas link"/> <IMAGE src="../images/site_nav/research.gif" alt="Research link"/> <IMAGE src="../images/site_nav/faculty.gif" alt="Faculty & Staff link"/> <IMAGE src="../images/site_nav/news.gif" alt="News link"/> <IMAGE src="../images/site_nav/spacer.gif" alt=""/>

<IMAGE src="../images/site_nav/below_mainnav.gif" alt=""/> <IMAGE src="../images/site_nav/spacer.gif" alt=""/>

<IMAGE src="../images/site_nav/current_studs.gif" alt="Link for Current Students"/>

<IMAGE src="../images/site_nav/student_orgs.gif" alt="Link to Student Organizations"/>

<IMAGE src="../images/site_nav/alumni.gif" alt="Link for Alumni & Friends"/>

<IMAGE src="../images/site_nav/employers.gif" alt="Link for Employers"/>

<IMAGE src="../images/site_nav/calendar.gif" alt="Link to the department calendar"/>

<IMAGE src="../images/site_nav/capcmpgn.gif" alt="Capital Campaign link"/>

<IMAGE src="../images/site_nav/COE.gif" alt="Link to the College of Engineering Web Site"/>

<IMAGE src="../images/site_nav/osu_link.gif" alt="Link to Ohio State Universities web site"/>

<IMAGE src="../images/site_nav/undrqjs.gif" alt=""/>

<IMAGE src="../images/site_nav/search.gif" alt="Link to our seach page"/>

<IMAGE src="../images/site_nav/homemod.gif" alt="Link to the Mechanical Engineering home page"/>

[pic]<IMAGE src="images/ghosh4.jpg" alt=""/>[/pic]

[contactinfo]Somnath Ghosh

<font size=3>[position]John B. Nordholt Professor[/position]
</font>PhD [phddate]1988[/phddate],   [phduniv]University of Michigan[/phduniv]

 CONTACT INFORMATION

<b>Address:</b> [address]234 Building 1
650 Ackerman Road[/address]

<b>E-Mail :</b> <b>[email]Ghosh.5@osu.edu[/email]</b>

<b>Phone:</b>

[phone](614) 292-2599[/phone]

<b>Web Site:</b> <font size=2><b>[affiliation]Computational Mechanics Research Laboratory[/affiliation]</b></font>[/contactinfo]

 [resinterests]RESEARCH

  Professor Somnath Ghosh is an international leader in the field of [interests]multiple-scale computational modeling of behavior[/interests] and [interests]failure of heterogeneous materials[/interests]. He has made pioneering contributions to the field of [interests]integrated experimental-computational modeling of composite materials and polycrystalline materials[/interests]. His early work on [interests]serial-sectioning[/interests], [interests]3-D microstructural representation and characterization[/interests] has prompted a number of current research programs. He has developed a powerful Voronoi Cell Finite Element Method (VCFEM) for efficient and accurate modeling of complex heterogeneous microstructures, which now has a worldwide following. Multi-scale modeling with VCFEM to predict the evolution of damage across scales has yielded unique modeling tools with tremendous potential in interdisciplinary research fields.

  Professor Ghosh was one of the first contributors in the development of the Arbitrary Lagrangian-Eulerian FEM, an important current simulation tool. His research has generated significant funding from various governmental and industrial agencies, which is rare in the field of computational mechanics. He was the only awardee of the (1994) National Young Investigator award from the U.S. National Science Foundation for Mechanics of Materials, and one of his student advisees received the prestigious Robert Melosh Medal for the best student paper in computational mechanics.

  Professor Ghosh is on the editorial board of six journals related to computational mechanics. He has delivered numerous plenary and keynote lectures at conferences, and he organized the highly successful NUMIFORM 2004 (8th International Conference on Numerical Methods of Industrial Forming Processes), which more than doubled the past participation of delegates. His excellence as an educator has been recognized by a number of honors, including the Harrison Faculty Award for Excellence in Engineering Education, which is the highest award in the College of Engineering at OSU. In addition to these honors, the OSU Lumley Research Award for outstanding research has been given to Professor Ghosh on four separate occasions. He is a [position]member of the executive council[/position] of the [affiliation]U.S. Association for Computational Mechanics[/affiliation], and he is a [position]Fellow[/position] of the [affiliation]American Society of Mechanical Engineers[/affiliation].[/resinterests]

 THRUST AREA

  <b>Applied Mechanics</b> <b>and </b><b>Design and Manufacturing </b>

 ASSOCIATED RESEARCH LABS


  <b> [affiliation]Computational Mechanics Research Laboratory[/affiliation]</b>

 COURSES TAUGHT


  Statics and Strength of Materials (ME 400)
  Strength of Materials (ME 440)
  Applied Finite Element Methods (ME 639)
  Introduction to Finite Element Methods (ME/CE 768)
  Finite Element Methods in Engineering Science (ME/CE 839)

 SELECTED PUBLICATIONS


  Please refer to the "Publications" area of Dr. Ghosh's web site: The Computational Mechanics Research Laboratory at http://ghomech1.eng.ohio-state.edu/.

<IMAGE src="../images/site_nav/spacer.gif" alt=""/>

<IMAGE src="../images/site_nav/undrhomemod.gif" alt=""/>

<IMAGE src="/images/site_nav/maps.gif" alt="Link to maps and directions for the department"/> <IMAGE src="../images/site_nav/bot_bar1.gif" alt="Mechanical Engineering phone (614) 292-2289 and Fax (614) 292-3163"/> <IMAGE src="../images/site_nav/bot_bar2.gif" alt="The department address at 650 Ackerman Road, Suite 255; Columbus, OH  43202"/> <IMAGE src="../images/site_nav/spacer.gif" alt=""/>
