[pic]<IMAGE src="images/manuelc-web.jpg" alt=""/>[/pic]

[contactinfo]<font size=5>[name]Manuel Costa[/name]</font>
<font size=3>[position]Researcher[/position]
[affiliation]Microsoft Research[/affiliation]
[address]7 J J Thomson Avenue
Cambridge, CB3 0FB, UK.[/address]
Tel: [phone]+44 1223 479740[/phone]
Fax: [fax]+44 1223 479999[/fax] </font>

<font size=3>mailto:[email]manuelc@microsoft[/email]</font>[/contactinfo]

[introduction]<font size=3>I'm a [position]member[/position] of the [affiliation]Cambridge Systems & Networking group[/affiliation]. I'm interested in [interests]scalable and reliable systems[/interests] and [interests]networks[/interests]. Lately, I've been working on [interests]automatic worm containment[/interests] and on [interests]peer-to-peer overlays[/interests].</font>[/introduction]

[publication]<font size=4><b>P</b></font><b>publications</b>

<font size=3>Miguel Castro, Manuel Costa, and Tim Harris, "<b>Securing software by enforcing data-flow integrity</b>", Proceedings of the 7th USENIX Symposium on Operating Systems Design and Implementation (OSDI'06), Seattle, USA, November 2006 [ pdf ] .</font>

<font size=3>Manuel Costa, Jon Crowcroft, Miguel Castro, Antony Rowstron, Lidong Zhou, Lintao Zhang, and Paul Barham, "<b>Stopping Internet Epidemics</b>", Proceedings of the International Zurich Seminar on Communications (IZS'06), Zurich, Switzerland, February 2006.</font>

<font size=3>Manuel Costa, Jon Crowcroft, Miguel Castro, Antony Rowstron, Lidong Zhou, Lintao Zhang, and Paul Barham, "<b>Vigilante: End-to-End Containment of Internet Worms</b>", Proceedings of the 20th ACM Symposium on Operating Systems Principles (SOSP'05), Brighton, UK, October 2005. [ ps | pdf ] (<b>Award paper</b>)</font>

<font size=3>Miguel Castro, Manuel Costa, and Antony Rowstron, "<b>Debunking some myths about structured and unstructured overlays</b>", Proceedings of the 2nd Symposium on Networked Systems Design and Implementation (NSDI'05), Boston, MA, USA, May 2005. [ ps | pdf ]</font>

<font size=3> </font>

<font size=3>Manuel Costa, Jon Crowcroft, Miguel Castro and Antony Rowstron, "<b>Can we contain Internet worms?</b>", Proceedings of the Third Workshop on Hot Topics in Networks (HotNets III), San Diego, California, USA, November 2004. [ ps | pdf ] </font>

<font size=3>Manuel Costa, Miguel Castro, Antony Rowstron, and Peter Key, "<b>PIC: Practical Internet Coordinates for Distance Estimation</b>", Proceedings of the 24th International Conference on Distributed Computing Systems (ICDCS'04), Tokyo, Japan , March 2004. [ ps | pdf ] </font>

<font size=3>Miguel Castro, Manuel Costa and Antony Rowstron, "<b>Performance and Dependability of structured peer-to-peer overlays</b>", Proceedings of the International Conference on Dependable Systems and Networks (DSN-2004), Florence, Italy, (June 2004) [ ps | PDF] </font>

<font size=3>Miguel Castro, Manuel Costa and Antony Rowstron, "<b>Should we build Gnutella on a structured overlay?</b>" Proceedings of the Second Workshop on Hot Topics in Networks (HotNets-II), Cambridge, MA, USA, November 2003. [ ps | pdf ]</font>

<font size=3>Manuel Costa, Paulo Guedes, Manuel Sequeira, Nuno Neves and Miguel Castro, "<b>Lightweight Logging for Lazy Release Consistent Distributed Shared Memory</b>" Proceedings of the 2nd USENIX Symposium on Operating Systems Design and Implementation (OSDI'96), Seattle, WA, USA, October 1996. </font>

<font size=3>Manuel Costa and Paulo Guedes, "<b>Two-Level Recovery For Distributed Shared Memory</b>" Proceedings of the European Research Seminar on Advances in Distributed Systems (ERSADS'97), Zinal, Switzerland, March 1997. </font>

<font size=3>Miguel Castro, Paulo Guedes, Manuel Sequeira and Manuel Costa, "<b>Efficient and Flexible Object Sharing</b>" Proceedings of the International Conference on Parallel Processing (ICPP'96), Chicago, USA, August 1996. </font>[/publication]
