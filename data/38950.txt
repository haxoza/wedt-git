<font size=6><b>Researcher</b></font> <IMAGE src="/images/shared/locked/clear.gif" alt="Skip to main content"/>
<font size=1><IMAGE src="/images/shared/locked/research_banner_p.gif" alt="IBM Research Homepage"/> </font>

<font size=1> </font><font size=2><b>Research Home</b> <font size=1>>></font></font> <IMAGE src="/images/shared/locked/clear.gif" alt=""/> <font size=2><b> Dr. John Linwood Griffin </b></font>
<IMAGE src="/images/shared/locked/clear.gif" alt=""/>

<IMAGE src="/images/shared/locked/clear.gif" alt=""/>
<font size=2>

<font size=2><b> </b></font>

[introduction]</font> <IMAGE src="/images/shared/locked/clear.gif" alt=""/> <font size=2><IMAGE src="/images/shared/locked/clear.gif" alt=""/>
[pic]<IMAGE src="images/john-maine-sep-2004.jpg" alt="Picture of John taken in Portland, Maine, in September, 2004."/>[/pic] I am a [position]Research Staff Member[/position] at [affiliation]IBM's T.J. Watson Research Center[/affiliation] in Hawthorne, New York, where I am affiliated with the excellent folks in [affiliation]the secure systems department[/affiliation]. In particular I am involved with the [interests]secure hypervisor project[/interests]. A hypervisor (also known as a virtual machine monitor) is an "operating system for operating systems"--a piece of software that allows multiple OSes to peacefully and simultaneously coexist on a single computer system. Our work advances the state of the art in security for modern hypervisors. (But what does that intentionally ambiguous statement mean? Check this space for future updates...)

In addition to my recent interests in [interests]security[/interests], I have long been interested in storage. My graduate research investigated issues involving OS-level access to secondary storage: disks, disk arrays, and new disk-like technologies. For details, see my publications on MEMS-based storage, track-aligned extents, and timing-accurate storage emulation; if you're really into this stuff, my dissertation advocates an emulation-based approach for storage component evaluations. In general, I am in the class of people who refer to themselves as "systems" folks. I enjoy finding small and efficient solutions to big problems involving the interactions between software and hardware.

I did my graduate work in [phdmajor]computer engineering[/phdmajor] at [phduniv]Carnegie Mellon[/phduniv] (Ph.D., [phddate]2004[/phddate]; M.S., [msdate]2000[/msdate]) and my undergraduate at [bsuniv]Auburn University[/bsuniv] (B.C.E., [bsdate]1998[/bsdate]). This raises the interesting coincidence that I both root for Big Blue (Auburn) and work for Big Blue (IBM). As a graduate student I studied under Dr. Greg Ganger with an unparalleled set of colleagues at the Parallel Data Laboratory. During the summer of 2001 I worked as an intern with Dr. Elizabeth Shriver at Bell Labs.

The best way to contact me is by email. My mailbox is [email]JLG, with the domain us.ibm.com[/email]. Alternatively, you are welcome to leave a message on my voice mail at [phone]914-784-6143[/phone]. For non-IBM related mail or messages, refer to my contact information at my CMU web page.[/introduction]

</font>

<font size=1> Privacy | Legal | Contact | IBM Home | Research Home | Project List | Research Sites | Page Contact </font>
