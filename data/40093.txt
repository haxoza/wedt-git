<IMAGE src="../Graphics/Header/technion-logo.jpg" alt=""/> <IMAGE src="../Graphics/Header/koteret.gif" alt=""/> <IMAGE src="../Graphics/Header/logo.gif" alt=""/> <IMAGE src="../Graphics/Header/right.gif" alt=""/>

<IMAGE src="../Graphics/Header/tat-koteret.gif" alt=""/>

<IMAGE src="../Graphics/Menu/p-menu-left.gif" alt=""/> <IMAGE src="../Graphics/All/spacer.gif" alt=""/> <IMAGE src="../Graphics/Menu/kaft-between-space.gif" alt=""/> <IMAGE src="../Graphics/All/spacer.gif" alt=""/> <IMAGE src="../Graphics/All/spacer.gif" alt=""/> <IMAGE src="../Graphics/All/spacer.gif" alt=""/> <IMAGE src="../Graphics/All/spacer.gif" alt=""/> <IMAGE src="../Graphics/All/spacer.gif" alt=""/> <IMAGE src="../Graphics/All/spacer.gif" alt=""/> <IMAGE src="../Graphics/All/spacer.gif" alt=""/> <IMAGE src="../Graphics/All/spacer.gif" alt=""/> <IMAGE src="../Graphics/All/spacer.gif" alt=""/> <IMAGE src="../Graphics/All/spacer.gif" alt=""/> <IMAGE src="../Graphics/All/spacer.gif" alt=""/> <IMAGE src="../Graphics/All/spacer.gif" alt=""/> <IMAGE src="../Graphics/Menu/siman-kaft-login.gif" alt=""/>Login <IMAGE src="../Graphics/Menu/menu-right.gif" alt=""/>

<IMAGE src="../Graphics/Title/kot-left.gif" alt=""/> People <b><font size=4>Israel Cidon</font></b>

<IMAGE src="../Graphics/All/spacer.gif" alt=""/><IMAGE src="../Graphics/All/spacer.gif" alt=""/>

<IMAGE src="../Graphics/All/sub-menu-kaft-left.gif" alt=""/> CCIT

<IMAGE src="../Graphics/All/spacer.gif" alt=""/>

<IMAGE src="../Graphics/All/sub-menu-kaft-left.gif" alt=""/> All publications

<IMAGE src="../Graphics/All/spacer.gif" alt=""/>

<IMAGE src="../Graphics/All/sub-menu-kaft-left.gif" alt=""/> [phddegree]Ph.D. Students[/phddegree]

<IMAGE src="../Graphics/All/spacer.gif" alt=""/>

<IMAGE src="../Graphics/All/sub-menu-kaft-left.gif" alt=""/> M.Sc. students

<IMAGE src="../Graphics/All/spacer.gif" alt=""/>

<IMAGE src="../Graphics/All/sub-menu-kaft-left.gif" alt=""/> More about me

<IMAGE src="../Graphics/All/spacer.gif" alt=""/>

<IMAGE src="../Graphics/All/sub-menu-kaft-left.gif" alt=""/> CV

<IMAGE src="../Graphics/All/spacer.gif" alt=""/>

<IMAGE src="../Graphics/All/spacer.gif" alt=""/>

[pic]<IMAGE src="tie-dark-center.jpg" alt=""/>[/pic]

<IMAGE src="../Graphics/People/staff-ac-kot-left.gif" alt=""/> <b>Israel Cidon</b>

[contactinfo]<b>[position]Tark Professor[/position], [position]Head[/position] of [affiliation]Irwin and Joan Jacobs Center for Communication and Information Technologies (CCIT)[/affiliation]</b>

<IMAGE src="../Graphics/All/siman-news-2.gif" alt=""/> <b>Office:</b> <b>[address]Room 907, A. Meyer Building[/address]</b>

<IMAGE src="../Graphics/All/siman-news-2.gif" alt=""/> <b>Telphone:</b> <b>[phone]+972-4-829-4647[/phone]</b>

<IMAGE src="../Graphics/All/siman-news-2.gif" alt=""/> <b>Fax:</b> <b>[fax]+972-4-829-5757[/fax]</b>

<IMAGE src="../Graphics/All/siman-news-2.gif" alt=""/> <b>Mobile:</b> <b>[phone]+972-54-863-330[/phone]</b>

<IMAGE src="../Graphics/All/siman-news-2.gif" alt=""/> <b>Home:</b> <b>[phone]+972-4-834-4539[/phone]</b>

<IMAGE src="../Graphics/All/siman-news-2.gif" alt=""/> <b>E-mail:</b> [email]cidon@ee.technion.ac.il[/email][/contactinfo]

<IMAGE src="../Graphics/All/spacer.gif" alt=""/>

<IMAGE src="../Graphics/All/spacer.gif" alt=""/>

<b><font size=5>Publication Selector</font></b>

<IMAGE src="auditorium3.jpg" alt=""/>

Past projects: The Kogan Auditorium

<IMAGE src="../Graphics/All/spacer.gif" alt=""/>

<IMAGE src="../Graphics/All/spacer.gif" alt=""/> EE Site <IMAGE src="../Graphics/Footer/kaft-down-space.gif" alt=""/> Technion Site <IMAGE src="../Graphics/Footer/kaft-down-space.gif" alt=""/> Webmaster

<IMAGE src="../Graphics/All/spacer.gif" alt=""/>
