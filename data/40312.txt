[resinterests]<b>Your contact person for projects and products in the research field: [interests]Cognitive Computing[/interests] & [interests]Medical Imaging[/interests]</b>[/resinterests]


[contactinfo]<b> [position]Prof.[/position] [phddegree]Dr.-Ing.[/phddegree]
Georgios Sakas</b>

[pic]<IMAGE src="http://a7www.igd.fhg.de/persons/gsakas/sakas-kl.jpg" alt=""/>[/pic]
(More images)

Fraunhofer IGD
Prof. Dr.-Ing. Georgios Sakas
[address]Fraunhoferstr. 5
D-64283 Darmstadt
Germany[/address]

Telephone [phone]+49-6151-155-153[/phone]
Fax [fax]+49-6151-155-480[/fax][/contactinfo]

<IMAGE src="../../imga7/email/georgios_sakas_e.gif" alt=""/>

<IMAGE src="../../images/pfeill.gif" alt=""/>Curiculum Vitae (PDF, 33KB)
<IMAGE src="../../images/pfeill.gif" alt=""/>Publications (PDF, 104KB)
<IMAGE src="../../images/pfeill.gif" alt=""/>Awards

<IMAGE src="/images/transp.gif" alt=""/>

<IMAGE src="/images/liniel.gif" alt=""/>

Top <IMAGE src="/images/pfeilt.gif" alt="Top"/> Home <IMAGE src="/images/home.gif" alt="Home"/> ?2006 Fraunhofer IGD - last modified: 16.03.2006
<IMAGE src="../../imga7/email/weba7.gif" alt=""/>
