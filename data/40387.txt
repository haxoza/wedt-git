[phddegree]Dr.[/phddegree] Yuri Alexeev

[contactinfo][affiliation]Pacific Northwest National Laboratory[/affiliation] [address]901 Battelle Blvd. MS-IN: K1-96 P.O. Box 999 Richland, WA 99352[/address] Phone: [phone]509-375-6433[/phone] Fax: [fax]509-375-6631[/fax][/contactinfo]

[introduction]Yuri graduated in [phddate]December 2002[/phddate]. He is presently a [position]post doctoral fellow[/position] with Dr. Theresa Windus at [affiliation]PNNL[/affiliation].[/introduction]

[contactinfo][email]Yuri.Alexeev@pnl.gov[/email][/contactinfo]

[pic]<IMAGE src="urihead.JPG" alt="Grad. student days in Ames"/>[/pic]

Back to the Gordon Group home page.
Back to the GAMESS home page.
