<IMAGE src="../../pics/COGAHead.jpg" alt="COGA Head"/>Home Contact Sitemap Glossary

Menu

  * People

  * Teaching

  * Research

  * Publications

  * Projects

  * Cooperations

  * Jobs

News

  * Nov 2006INFORMS TSL Diss Prize

  * Sep 2006GOR Diss Preise

  * Sep 2006GOR Diplom Preise

  * May 2006Deutschlandfunk

  * 2006Informatics Year

  * 2006Algorithmus der Woche

Intranet

[contactinfo]Login[pic]<IMAGE src="../../pics/nunkess.jpg" alt="photo of Marc Nunkesser"/>[/pic]<b>Marc Nunkesser</b> guest Fakultät II - [address]Mathematik und NaturwissenschaftenInstitut für Mathematik, Sekr. MA 6-1 Technische Universität BerlinStraße des 17. Juni 136 10623 BerlinGermany[/address] email: [email]mnunkess 'at' inf.ethz.ch[/email] phone: [phone]+49 +30 314-25728[/phone] fax: [fax]+49 +30 314-25191[/fax] office: [address]MA 601[/address] office hours: by appointment[/contactinfo]

Further informationHomepageWebmaster |  Statistics |  Built on 2007-01-10 09:41:21 CET  |  Last modified on 2006-01-04 16:30:34 CET
