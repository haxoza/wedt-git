Contact[pic]<IMAGE src="Me.jpg" alt="photo of me"/>[/pic]


[contactinfo]Laura Hollink

[affiliation]Vrije Universiteit Amsterdam VU
Department of Computer Science CS
Section Business Informatics BI[/affiliation]


Monday, Tuesday, Friday:

[address]De Boelelaan 1081a
1081 HV Amsterdam[/address]
T [phone]+31 (0)20 598 7740[/phone]
F [fax]+31 (0)20 598 7728[/fax]
E [email]hollink@cs.vu.nl[/email]


Wednessday, Thursday:

[address]Nederlands Instituut voor Beeld en Geluid
Media Park, Sumatralaan 45
1200 BB Hilversum[/address]
[email]E hollink@cs.vu.nl[/email][/contactinfo]
(Telephone no. will follow once I have a fixed room)

<IMAGE src="valid-html401.gif" alt="Valid HTML 4.01!"/>

























[resactivity]Research


NEWS ! On the 16th of November 2006 I defended my PhD thesis! [PDF]


[interests]MUNCH[/interests]

I'm working as a Postdoc in the MUNCH project (Multimedia Analysis of Cultural Heritage), funded by the NWO Catch program. MUNCH is a cooperation between the VU, the UvA and the Dutch Institute for Sound and Vision. We aim to improve search possibilities in the rich collection of Dutch television broadcasts that is archived at 'Sound and Vision', by combining three research directions: structured background knowledge in the form of ontologies or thesauri, language technologies and content-based image retrieval.

<IMAGE src="munchtTrio2.gif" alt="photo of me"/>


[interests]IMIK[/interests]

From January 2002 until April 2006 I worked in the context of the IOP-project Interactive Disclosure of Multimedia, Information and Knowledge, or I'mIk (Interactieve Ontsluiting van Multimedia Informatie en kennis).[/resactivity]


Participants

  * Bob Wielinga

  * Giang Nguyen

  * Guus Schreiber

  * Laura Hollink

  * Marcel Worring

























[publication]Publications

  * L. Hollink. Semantic annotation for retrieval of visual resources. PhD Thesis, defended November 2006 [PDF]

  * Guus Schreiber, Alia Amin, Mark van Assem, Victor de Boer, Lynda Hardman, Michiel Hildebrand, Laura Hollink, Zhisheng Huang, Janneke van Kersen, Marco de Niet, Borys Omelayenko, Jacco van Ossenbruggen, Ronny Siebes, Jos Taekema, Jan Wielemaker and Bob Wielinga. MultimediaN E-Culture Demonstrator. First prize in the Semantic Web Challenge at the Fifth International Semantic Web Conference, Athens, GA, USA, November 2006. [PDF]

  * L.Hollink, M. Worring and A.Th. Schreiber. Building a Visual Ontology for Video Retrieval. Accepted as a short paper in ACM MultiMedia 2005. [PDF]

  * L. Hollink, G.P. Nguyen, D. Koelma, A.Th. Schreiber, M. Worring. Assessing User Behaviour in News Video Retrieval. IEE proceedings on Vision, Image and Signal Processing 152/6, December 2005, p. 911-918. [PDF]

  * L.Hollink, S. Little, J. Hunter. Evaluating the Application of Semantic Inferencing Rules to Image Annotation. Third International Conference on Knowledge Capture (K-CAP05), Oct 2-5, Banff, Canada. [PDF]

  * L.Hollink, G. Nguyen, G. Schreiber, J. Wielemaker, B. Wielinga, and M. Worring. Adding Spatial Semantics to Image Annotations. 4th International Workshop on Knowledge Markup and Semantic Annotation at ISWC'04. [PDF]

  * L.Hollink, A.Th.Schreiber, B. Wielinga, M.Worring. Classification of User Image Descriptions. International Journal of Human Computer Studies 61/5 (2004) 601-626 [PDF]

  * L.Hollink, G.P.Nguyen, D.Koelma, A.Th.Schreiber, M.Worring. User Strategies in Video Retrieval: a Case Study. In Proceedings of the International Conference on Image and Video Retrieval (CIVR) 2004, July 21-23, Dublin, Ireland. [PDF]

  * M. Worring, G.P. Nguyen, L. Hollink, J.C. van Gemert, D.C. Koelma. Accessing Video Archives Using Interactive Search. In Proceedings of the International Conference on Multimedia and Expo (ICME) 2004, June 27-30, Taipei, Taiwan. [PDF]

  * M.Worring, G.P.Nguyen, L.Hollink, J.van Gemert, D.C.Koelma. Interactive search using indexing, filtering, browsing, and ranking. In Proceedings of the TREC Video Retrieval Evaluation Workshop, November 2003, Washington. [PDF]

  * L.Hollink, A.Th.Schreiber, J.Wielemaker, B.Wielinga. Semantic Annotation of Image Collections. In proceedings of the KCAP'03 Workshop on Knowledge Capture and Semantic Annotation, Florida, October 2003. [PDF] [BibTeX]


  * Master's Thesis. Ondersteuning van Gemeentelijke Frontoffices. Haarlem, 2001 (in Dutch). [ZIP][/publication]

























[introduction]Brief CV

After graduation from highschool in 1997 I started the beta-gamma propedeuse. This is a one year program in which the students study many (18) different topics, both beta and gamma. After that I studied [msmajor]Social Science Informatics[/msmajor] at the [msuniv]University of Amsterdam[/msuniv]. I did an internship at the local government of Haarlem and graduated on a masters thesis titled "Ondersteuning van Gemeentelijke Frontoffices" in [msdate]December 2001[/msdate]. In Januari 2002 I started a PhD on the project [phdmajor]Interactive Disclosure of Multimedia, Information and Knowledge[/phdmajor] (I'mIk) at the Social Science Informatics Department of the [phduniv]UvA[/phduniv]. In april 2003 I moved to the Business Informatics Section of the Vrije Universiteit Amsterdam where I continued my work on the I'mIk project. In November 2006 I defended my thesis titled [interests]semantic Annotation for Retrieval of Visual Resources[/interests]. Currently I work as a postdoc at the [affiliation]Vrije Universiteit[/affiliation] in the MUNCH project.[/introduction]
