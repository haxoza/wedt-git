<IMAGE src="mm_spacer.gif" alt=""/>

[Japanese]

Hideaki Takeda, Dr., Eng. <IMAGE src="banar1-4.gif" alt="NII Logo"/>

[contactinfo][affiliation]National Institute of Informatics[/affiliation]

Top Research Topics Publication Research Group Lecture CV Link

<IMAGE src="mm_spacer.gif" alt=""/>

[pic]<IMAGE src="takeda-kao-big.jpg" alt="Face photo"/>[/pic]



<IMAGE src="mm_spacer.gif" alt=""/>

Welcome to My Page

<b>Affiliation</b> <b>[affiliation]National Institute of Informatics[/affiliation]</b>
[affiliation]Research Center for Testbeds and Prototyping[/affiliation]
[affiliation]Office for Cooperative Research Program[/affiliation]

<b>Position</b> [position]Professor[/position]

<b>Address: </b> [address]2-1-2 Hitotsubashi, Chiyoda-ku, Tokyo 101-8430, Japan[/address]

<b>Room No.: </b> [address]1408 (Bldg. of National Center of Sciences, 14F)[/address]

<b>Phone: </b> [phone]+81-3-4212-2543[/phone]

<b>Phone: </b> [phone]+81-3-4212-2620[/phone] (Secretaries)

<b>Fax: </b> [fax]+81-3-3556-1916[/fax]

<b>E-mail: </b> [email]takeda@nii.ac.jp[/email][/contactinfo]

WWW Page [homepage]http://www-kasm.nii.ac.jp/~takeda/index.html[/homepage]
[homepage]http://research.nii.ac.jp/~takeda/official/content_e.html[/homepage]


<IMAGE src="mm_spacer.gif" alt=""/>

Hideaki Takeda

<IMAGE src="mm_spacer.gif" alt=""/>

<IMAGE src="mm_spacer.gif" alt=""/>

<IMAGE src="mm_spacer.gif" alt=""/>
