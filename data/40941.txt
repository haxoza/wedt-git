Krishnendu (Krish) Chakrabarty [pic]<IMAGE src="pictures/Chakrabarty.Potsdam.May2006.2.jpg" alt=""/>[/pic]


[introduction][position]Associate Professor[/position] [affiliation]Electrical and Computer Engineering Duke University[/affiliation]<IMAGE src="pictures/cline.gif" alt=""/>


BACKGROUND Krish Chakrabarty joined Duke University in September 1998. His current research is focused on [interests]design and test of system-on-chip integrated circuits[/interests], [interests]microfluidics-based biochips[/interests], [interests]distributed sensor networks[/interests], [interests]embedded real-time systems[/interests], and [interests]chip cooling using droplet-based microfluidics[/interests].

Dr. Chakrabarty is a recipient of the 1999 National Science Foundation Early Faculty (CAREER) Award, the 2001 Office of Naval Research Young Investigator Award, the Mercator Professor award from Deutsche Forschungsgemeinschaft, Germany, for 2000-2002, and best paper awards at the 2001 IEEE Design Automation and Test in Europe (DATE) Conference and the 2005 International Conference on Computer Design. He is a Distinguished Visitor of the IEEE Computer Society for 2005-2007, and a Distinguished Lecturer of the IEEE Circuits and Systems Society for 2006-2007. He is also a recipient of the Humboldt Research Fellowship, awarded by the Alexander von Humboldt Foundation, Germany, in 2003. He serves as [position]Vice Chair[/position] of Technical Activities in [affiliation]IEEE's Test Technology Technical Council[/affiliation], and holds a US patent on built-in self-test. He is an [position]Editor[/position] of the [affiliation]Journal of Electronic Testing: Theory and Applications (JETTA)[/affiliation], an [position]Associate Editor[/position] of [affiliation]IEEE Transactions on Computer-Aided Design of Integrated Circuits and Systems[/affiliation], [affiliation]IEEE Transactions on VLSI Systems[/affiliation], [affiliation]IEEE Transactions on Circuits and Systems I[/affiliation], and [affiliation]ACM Journal on Emerging Technologies in Computing Systems[/affiliation]. He serves on the [position]editorial board[/position] of [affiliation]IEEE Design & Test of Computers[/affiliation]. Until recently, he also served as an Associate Editor of IEEE Transactions on Circuits and Systems II: Analog and Digital Signal Processing.

Dr. Chakrabarty received the B. Tech. degree from the [bsuniv]Indian Institute of Technology[/bsuniv], Kharagpur, India in [bsdate]1990[/bsdate], and the M.S.E. and Ph.D. degrees from the [phduniv]University of Michigan[/phduniv], Ann Arbor in [msdate]1992[/msdate] and [phddate]1995[/phddate], respectively, all in [phdmajor]Computer Science and Engineering[/phdmajor] . During 1990-95, he was a research assistant at the Advanced Computer Architecture Laboratory of the Department of Electrical Engineering and Computer Science, University of Michigan. During 1995-1998, he was an Assistant Professor of Electrical and Computer Engineering at Boston University. Dr. Chakrabarty is a [position]Senior Member[/position] of [affiliation]IEEE[/affiliation], a [position]Senior Member[/position] of [affiliation]ACM[/affiliation], a [position]member[/position] of [affiliation]SIGDA[/affiliation], and a [position]member[/position] of [affiliation]Sigma Xi[/affiliation].[/introduction]

<IMAGE src="pictures/cline.gif" alt=""/>


Research Group


  * Lara Oliver (SRC Graduate Fellow), PhD student

  * Zhanglei Wang, PhD student

  * Tong Zhou, PhD student

  *  Sudarshan Bahukudumbi, PhD student

  *  Amit Kumar, PhD student

  *  Tao Xu, PhD student

  * Harshavardhan Sabbineni, PhD student (part-time)



Click here for group photograph (Holiday Party, 2000)

2001 Holiday Party Pictures

May 2003 Group Dinner


Visitors

  * Dr Makoto Sugihara (post-doctoral researcher), April 2002-March 2003.

  * Srinivas Chakravarthula, Visiting graduate student from Louisiana State University, June-July 2002.

  * Markus Seuring, University of Potsdam, Germany (June 1999, October 1999, May 2000)

  * Dr Hiroshi Date and Makoto Sugihara, Kyushu University, Japan (January 2000)

  * Alexej Dmitriev, University of Potsdam, Germany (February 2001)

  * Prof. Seiji Kajihara, Kyushu Institute of Technology, Japan, April 2003.

  * Qiang Xu, McMaster University, Canada (May-August 2005)


Graduated Students

  * Phil Paik (PhD, May 2006), First Position: Advanced Liquid Logic, Inc., Research Triangle Park, NC.
    PhD Thesis title: Adaptve Hot-Spot Cooling of Integrated Circuits Using Digital Microfluidics,

  * Fei Su (PhD, January 2006), First Position: Senior DFT Engineer, Intel Corporation, Folsom, CA. (Click here for graduation picture)
    PhD Thesis title: Synthesis, Testing, and Reconfiguration Techniques for Digital Microfluidic Biochips

  * Anuja Sehgal (PhD, May 2005), First Position: Senior Design Engineer with AMD Corporation, Sunnyvale, CA
    PhD Thesis title: Test Infrastructure Design for Digital, Mixed-Signal and Hierarchical SOCs

  * Yi Zou (PhD, December 2004), First Position: Unitrends Software Corporation Columbia , SC.
    PhD Thesis title: Coverage-Driven Sensor Deployment and Energy-Efficient Information Processing in Wireless Sensor Networks.

  * Lei Li (PhD, August 2004), First Position: Digital Design Engineer, Freescale Semiconductor, Austin, TX.
    PhD Thesis title: Reducing Test Data Volume for System-on-Chip Integrated Circuits using Test Data Compression and Built-In Self-Test,

  * Ying Zhang (PhD, May 2004)
    PhD Thesis title: Dynamic Adaptation for Fault Tolerance and Power Management in Real-Time Embedded Systems, May 2004, First Position: Senior Software Engineer, Guidant Corporation, St. Paul, MN.

  *  Vishnu Swaminathan (PhD, May 2003), First Position: Researcher at Siemens Corporate Research Center, Bangalore, India.
    PhD Thesis title: Dynamic Power Management in Hard Real-Time Systems

  *  Chunsheng Liu (PhD, May 2003), First Position: Assistant Professor at University of Nebraska (click here for graduation picture)
    PhD Thesis title: Fault Diagnosis in Scan-BIST with System-on-Chip Applications

  *  Anshuman Chandra (PhD, September 2002), First Position: Senior R & D Engineer at Synopsys, Mountain View, CA (click here for graduation picture)
    PhD Thesis title: Test Resource Partitioning and Test Data Compression for System-on-a-Chip

  *  Vikram Iyengar (PhD, May 2002), First position: Advisory Engineer at IBM Corp.(ASICs Test and Methodology Department), Burlington, VT
    PhD Thesis title: Test Planning and Plug-and-Play Test Automation for System-on-a-Chip
    IBM Graduate Fellowship recipient (2001-2002)
    Winner of European Design Automation Association (EDAA) Outstanding Dissertation Award in 2004 for the category "New Directions in System Test"
    Click here and here for Vikram's graduation pictures)

  *  Tianhao Zhang (PhD, May 2001; co-advised with Professor Richard Fair), First Position: Senior Member of the Technical Staff at Cadence Design Systems, Research Triangle Park, NC
    (Click here for a picture of Tianhao with the thesis committee after the defense)
    PhD Thesis title: An Integrated Hierarchical Modeling and Simulation Approach for Microelectrofluidic Systems

  * Usman Zaheer, M. S. Thesis, December 2005, Thesis Title: "Low-Power System-Level Fault Tolerance using Dynamic Voltage Scaling, Adaptive Body Biasing, and Checkpointing".

  * Harshavardhan Sabbineni, M.S. Thesis, February 2004 (Thesis title: "Location-Aware Protocols for Energy-Efficient Information Processing in Wireless Sensor Networks")

  * Charles Schweizer, M.S. Project, May 2002, First position: Microsoft Corp., Redmond, WA

  * Sharon Stewart Schweizer, M. S. Project, May 2002

  *  Selase Agbenoto, M.S. project, December 2002.

  * Amil Patel (undergraduate researcher, 2001-2002), currently a graduate student at MIT

  * Mark Krasniewski (undergraduate researcher, 2002-2003, Seagar Award winner for the best undergraduate research project in the Department of Electrical and Computer Engineering for 2002-2003), currently a graduate student at Purdue University

  *  Shivakumar Swaminathan (M.S. Thesis, Dec 1999), Current position: IBM Corp. Research Triangle Park, NC

  * Jie Ding (M.S. Thesis, May 2000, co-advised with Professor Richard Fair, currently with ASIC Alliance, North Carolina)


<IMAGE src="pictures/cline.gif" alt=""/>


PUBLICATIONS


Professional Activities


Invited Presentations<IMAGE src="pictures/cline.gif" alt=""/>


TeachingECE 156: Computer Network Architectures, 2006 (Fall)
ECE 261: CMOS VLSI Design Methodologies, 1998-2005 (Fall)
ECE 269: VLSI System Testing, Spring 1999, Spring 2002-2003, Spring 2005
ECE 266: Synthesis and Verification of VLSI Systems, Spring 2000, Spring 2006
ECE 151: Introduction to Switching Theory and Logic Design, Spring 2001
<IMAGE src="pictures/cline.gif" alt=""/>


[contactinfo]Contact Information:

Krishnendu Chakrabarty
[affiliation]Electrical and Computer Engineering
Duke University[/affiliation]
[address]Box 90291, 130 Hudson Hall
Durham, NC 27708[/address]
E-mail: [email]krish AT ee DOT duke DOT edu[/email]
<b> Tel </b>: [phone]+1 (919) 660-5244[/phone]
<b> Fax </b>: [fax]+1 (919) 660-5293[/fax][/contactinfo]
