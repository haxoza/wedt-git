[pic]<IMAGE src="gao.jpg" alt=""/>[/pic]

<b>Background</b>

[introduction]Haijun Gao earned his master’s in [msmajor]Reproductive and Developmental Science[/msmajor] from the [msuniv]University of British Columbia[/msuniv], Canada in [msdate]2004[/msdate], M.S. in [msmajor]Cell Biology[/msmajor] from [msuniv]Peking Union Medical College[/msuniv], China in [msdate]2002[/msdate] and B.S. in [bsmajor]Biology[/bsmajor] from [bsuniv]Sichuan University[/bsuniv], China in [bsdate]1997[/bsdate]. He worked as assistant Engineer in Shandong Lukang Pharmaceutical Group Company, China and managed techniques & quality control and clinical trial of bioproducts for anti-tumor immunization therapy from July 1997 to August 1999. His ongoing research interest is to [interests]investigate the role of mammalian target of rapamycin (mTOR) in conceptus development and survival and underlying mechanisms[/interests].[/introduction]

<b> </b>

<b>Honors and Awards</b>

Albert B and Mary Steiner Summer Research Award, the University of British Columbia, Canada 2003

International Graduate tuition Scholarship Faculty of Graduate Studies, University of British Columbia, Canada 09/2002-12/2004

Graduate Research Assistantship Department of Obstetrics & Gynecology, University of British Columbia Canada 09/2002-08/2004

Excellent Staff Award, Shandong Lukang Pharmaceutical Group Company, China 1998    

Excellent Student Scholarship Sichuan University, China 1995, 1996

<b> </b>

[publication]<b>Publications</b>

        Haijun Gao, Thomas E. Spencer, Guoyao Wu, Gregory A. Johnson1 and Fuller W. Mammalian target of rapamycin in ovine conceptus development Bazer 39th Annual meeting of Society for the Study of Reproduction Omaha Nebraska July 29- August 1 2006

        Fuller W. Bazer, Haijun Gao, Thomas E. Spencer, and Guoyao Wu Expression of uteroferrin in ovine uterus 37th Annual meeting of Society for the Study of Reproduction Omaha Nebraska July 29- August 1 2006

        Hatakeyama C, Gao H, Harmer K, Ma S (2006) Meiotic segregation patterns and ICSI pregnancy outcome of a rare (13;21) Robertsonian translocation carrier: a case report. Human Reproduction, 21(4):976-9

        Tang SS, Gao H, Robinson WP, Ho Yuen B, Ma S, (2004) An association between sex chromosomal aneuploidy in sperm and an abortus with 45,X of paternal origin: possible transmission of chromosomal abnormalities through ICSI, Human Reproduction, 19(1): 147-51.

        Ma S, Gao HJ, Tang SS, Ho Yuen B, Chow V, Nigro M. Clinical outcome of intracytoplasmic sperm injection (ICSI) with frozen-thawed epididymal and testicular sperm with an investigation of chromosomal abnormalities in the sperm and ICSI pregnancies. 60th Annual Meeting of the American Society for Reproductive Medicine, Philadelphia, Pennsylvania, October 16-20, 2004

Haijun Gao, Brasil Ho Yuen, Sai Ma (2003) FISH analysis of Chromosomal Aneuploidy in Epididymal, Testicular and Ejaculated Sperm from Obstructive Azoospermic, Non-obstructive azoospermic and Severely Oligoasthenoteratozoospermic Men Undergoing ICSI 49th Annual meeting of the Canadian Fertility and Andrology Society in Victoria, BC, November 5-8, 2003

        Tang SS, Gao HJ, Robinson WP, Ho Yuen B, Ma S. A correlation between sex chromosomal aneuploidy in sperm and an abortus with 45,X of paternal origin: evidence of transmission of chromosomal abnormalities through ICSI. 59th Annual Meeting of the American Society for Reproductive Medicine, San Antonio, Texas, October 11-16, 2003[/publication]

[pic]<IMAGE src="Haijungao.jpg" alt=""/>[/pic]
