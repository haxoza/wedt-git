<IMAGE src="/images/topban_internal_yale.gif" alt="Yale University."/> <IMAGE src="/images/topban_internal_2.gif" alt=""/>

<IMAGE src="/images/midban_internal_compsci.gif" alt="Computer Science."/> <IMAGE src="/images/midban_internal_2.jpg" alt=""/>

<IMAGE src="/images/spacer.gif" alt=""/>

<IMAGE src="../images/spacer.gif" alt=""/>

Home

About Us

People

Research

Publications

Graduate Program

Undergraduate Program

Calendars

Resources

Alumni Corner

Internal


Valery Trifonov


[introduction][position]Associate Research Scientist[/position] of Computer Science

M.S., [msuniv]Sofia University[/msuniv], Sofia, Bulgaria, [msdate]1985[/msdate]
Ph.D., [phduniv]Johns Hopkins University[/phduniv], [phddate]1997[/phddate]
Joined Yale Faculty 1997
Personal Homepage[/introduction]

[contactinfo][address]Office Location: AKW 304[/address]
Telephone: [phone]203.432.7606[/phone][/contactinfo]

[pic]<IMAGE src="../images/graphics/trifonov.gif" alt="Valery Trifonov."/>[/pic]<IMAGE src="../images/spacer.gif" alt=""/>

[resinterests]Valery Trifonov's interests are in the [interests]semantics and implementations of programming languages[/interests], including [interests]type systems and compilation techniques for object-oriented languages[/interests], and [interests]higher-order typed intermediate languages[/interests]. Currently he is working with Zhong Shao on the FLINT project, designing intermediate calculi for compiling higher-order typed languagesML, Haskell, Javato proof-carrying machine code. The reflexive properties of the developed languages are so expressive that they allow constructing machine-verifiable safety proofs for services typically included in the trusted computing base (the runtime or the operating system), such as storage reclamation and dynamic loading. His current research is focused on extending this design to the level of safe mobile machine code.[/resinterests]

[publication]Representative Publications:

<IMAGE src="../images/bullet.gif" alt="Bullet."/>

"A type system for certified binaries, with Zhong Shao, Bratin Saha, and Nikolaos Papaspyrou. In Proceedings of POPL 2002: The 29th
ACM SIGPLAN-SIGACT Symposium on Principles of Programming Languages, pp. 217-232.

<IMAGE src="../images/bullet.gif" alt="Bullet."/>

"Fully reflexive intensional type analysis, with Bratin Saha and Zhong Shao. In Proceedings of the 2000 ACM SIGPLAN International
Conference on Functional Programming, pp. 82-93.

<IMAGE src="../images/bullet.gif" alt="Bullet."/>

"Representing Java classes in a typed intermediate language, with Christopher League and Zhong Shao. In Proceedings of the 1999
ACM SIGPLAN International Conference on Functional Programming, pp. 183-196. Subtyping Constrained Types, with Scott F. Smith. In
Proceedings of the 3rd International Symposium on Static Analysis, Lecture Notes in Computer Science 1145, Springer-Verlag, 1996,
pp. 349-365.[/publication]

<IMAGE src="../images/top_arrow.gif" alt="Top of Page."/>

<IMAGE src="../images/spacer.gif" alt=""/>

<IMAGE src="../images/spacer.gif" alt=""/> <IMAGE src="../images/spacer.gif" alt=""/> <IMAGE src="../images/spacer.gif" alt=""/> <IMAGE src="../images/spacer.gif" alt=""/>

<IMAGE src="../images/yalelogo_home.gif" alt="Yale University."/>

Copyright ?2002 Department of Computer Science, Yale University.
All rights reserved. Comments or suggestions to the site editor.
Last modified: October 23, 2006 by Nancy
<IMAGE src="../images/spacer.gif" alt=""/>
Home URL: http://www.cs.yale.edu/
