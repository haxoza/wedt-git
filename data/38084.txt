[pic]<IMAGE src="XiaoHuang.jpg" alt=""/>[/pic]

<b><font size=5>Xiao Huang</font></b><font size=2>
</font><font size=3><b>[introduction][position]Research Associate[/position] </b></font>

<font size=2>[affiliation]Center for Human-Computer Communication (CHCC)
Department of Computer Science and Engineering
OGI School of Science & Engineering
Oregon Health & Science University[/affiliation]</font>[/introduction]

<font size=2>[contactinfo]Email: [email]huangx@cse.ogi.edu[/email]][/contactinfo]
</font>

  <font size=2>

  [education]<b>Ph.D. [phdmajor]Computer Science and Engineering[/phdmajor]</b>, [phduniv]Michigan State University[/phduniv], [phddate]May, 2005[/phddate].

  <b>M.E. [msmajor]Electrical Engineering[/msmajor]</b>, [msuniv]Institute of Automation, Chinese Academy of Sciences[/msuniv], Beijing, China, [msdate]2000[/msdate].

  <b>B.S. [bsmajor]Electrical Engineering[/bsmajor]</b>, [bsuniv]Northern Jiaotong University[/bsuniv], Beijing, China, [bsdate]1997[/bsdate][/education]

  </font>


  <b><font size=4>[resinterests]Research Interests:</font></b>

  <font size=2>[interests]Human Computer Interaction[/interests] 
  [interests]Computer Vision[/interests] 
  [interests]Machine Learning[/interests] 
  [interests]Pattern Recognition[/interests] 
  [interests]Speech Processing[/interests] 
  [interests]Multimedia Technologies[/interests][/resinterests] 
  </font>


  <font size=4>Work Experience</font>

    <font size=2> </font>*  Research Assistant, Embodied Intelligence (EI) Lab, Michigan State University, 2000-2004. 

    *  [publication]Paper Reviewer:
      IEEE Transaction on Pattern Analysis and Machine Intelligence
      IEEE Transaction on Multimedia
      International Joint Conference on Neural Network (2004)
      2001-present. 

    *  Research Assistant, Character Recognition Lab, Institution of Automation, Chinese Academy of Sciences, Beijing, China, 1998-2000. 

    *  Teaching Assistant, Computer Vision, Computer Science and Engineering Department, Michigan State University, 2002.[/publication]


  <font size=4>Projects:</font>

    * <font size=2>CALO (Cognitive Agent that Learns and Organizes) </font>


  <font size=4>Awards</font>

    <font size=2> </font>*  Graduate Office Fellowship, MSU, 2004

    *  International Travel Fellowship, MSU, 2004

    *  Third prize of annual research evaluation, Embodied Intelligence Lab, MSU, 2003

    *  Graduate: "All-Best Student Fellowship," Institute of Automation, 1998-1999

    *  Undergraduate: First Grade Fellowship 1994-1996


  [publication]<font size=4>Publications</font><font size=3><b>Thesis</b></font>

  <font size=2><b>X. Huang</b>
  "Machine-Printed Chinese Character Recognition", Master Thesis, Beijing, China, July 2000. </font>

  <font size=3><b>Invited Talks</b></font>

  "Novelty and Reinforcement Learning in the Value System of Developmental Robots," Microsoft Research, Redmond, Seattle, May 16th, 2002.

  <font size=3><b>Journal Papers</b></font>

  <font size=2><b>X. Huang</b> and J. Weng.
  "The Value system for a Developmental Robot" Submitted to the IEEE Transaction on Neural Network. </font>

  <font size=2>C. Zhang and <b>X. Huang</b>.
  "Chinese Business Cards Recognition System," Journal of Chinese Information Processing, Vol. 14 No.2, pp21-26, 1999. </font>

  <font size=3><b>Conference Papers (Published or accepted)</b></font>

  <font size=2><b>X. Huang</b> and J. Weng.
  "Value System Development for a Robot," International Joint Conference on Neural Network, Budapest, Hungary, July 25-29, 2004.</font>

  <font size=2>Y. Chen, J. Weng, and <b>X. Huang</b>.
  "'Object Permanence': Results From Developmental Robotics," International Joint Conference on Neural Network, Budapest, Hungary, July 25-29, 2004. </font>

  <font size=2><b>X. Huang</b> and J. Weng.
  "Motivational System for Human-Robot Interaction," ECCV Workshop on Human-Computer Interaction, Prague, Czech Republic, May 16, 2004. </font>

  <font size=2><b>X. Huang</b>, J. Weng, and Z. Zhang.
  "Office Presence Detection Using Multimodal Context Information,"
  in Proceedings of International Conference on Acoustics, Speech, and Signal Processing (ICASSP 2004), Montreal, Quebec, Canada, May 17-21, 2004. </font>

  <font size=2><b>X. Huang</b>, J. Weng, and R. Calantone.
  "Locally-Balanced Incremental Hierarchical Discriminant Regression," in Proceedings of the Fourth International Conference on Intelligent Data Engineering and Automated Learning (IDEAL), Hong Kong, March 21-23, 2003.</font>

  <font size=2><b>X. Huang</b> and J. Weng.
  "Novelty and Reinforcement Learning in the Value System of Developmental Robots," in Proceedings of Second International Workshop on Epigenetic Robotics: Modeling Cognitive Development in Robotic Systems, Edinburgh, Scotland, Aug 10 - 11, 2002. </font>

  <font size=2>N. Zhang, J. Weng, and <b>X. Huang</b>.
  "Visual-Based Navigation with a Stagger Hierarchical Mapping," SPIE Symposium on Intelligent Systems and Advanced Manufacturing. Boston, MA, USA, Oct 28 - Nov 2, 2001.</font>

  <font size=2>L. Huang and <b>X. Huang</b>.
  "Multiresolution Recognition of Offline Handwritten Chinese Characters with Wavelet Transform," in Proceedings of International Conference on Document Analysis and Recognition (ICDAR 2001), Seattle, WA, USA, Sep 10-13, 2001.</font>

  <font size=2><b>X. Huang</b> and T. Zhang.
  "A Knowledge-Based System for Business Cards Understanding," in Proceedings of 7th National Conference on Chinese Character Recognition, Kunming, China, 1999.</font>[/publication]


  [contactinfo]<font size=2>Contact Information:</font>

        <font size=2>[address]20000 N.W. Walker Road
        Beaverton, Oregon 97006, USA[/address]
        Phone: [phone](503) 748-1511[/phone]
        Fax: [fax](503) 748-1875[/fax]
        Email: [email]huangx@cse.ogi.edu[/email] </font>[/contactinfo]
