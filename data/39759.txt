<IMAGE src="../../title.gif" alt=""/>

[contactinfo][pic]<IMAGE src="R_Byrne.jpg" alt=""/>[/pic] <font size=5><b>Roger Byrne</b></font>

[position]Associate Professor[/position]
Ph.D. [phduniv]University of Wisconsin[/phduniv], [phddate]1972[/phddate]
[email]arbyrne@berkeley.edu[/email][/contactinfo]

<IMAGE src="../../geoglink.gif" alt=""/>

[resinterests]<font size=2><b>Interests:</b> [interests]historical biogeography[/interests], [interests]vegetation change[/interests], [interests]prehistoric agriculture[/interests], [interests]pollen analysis[/interests]. Mr. Byrne is currently conducting research on the [interests]history of late-Pleistocene/Holocene environmental change in California and Mexico[/interests]. He is also Curator of Fossil Pollen for the Museum of Paleontology at Berkeley.[/resinterests]

[publication]<b>Recent publications</b>:</font>

<font size=2>Mensing, S., Byrne, R.
1998 Pre-mission invasion of Erodium cicutarium in California. Journal of Biogeography, 24:757-762.

Goman, M., and Byrne, R.
1998 A 5000-year record of agriculture and tropical forest clearance in the Tuxtlas, Veracruz, Mexico. Holocene, 8:83-89.

Ingram, BL; De Deckker, P; Chivas, AR; Conrad, ME; and Byrne, R.
1998 Stable isotopes, Sr/Ca, and Mg/Ca in biogenic carbonates from Petaluma Marsh, northern California, USA. Geochimica et Cosmochimica Acta, 62 N19-20:3229-3237.

Byrne, R. and C. Turton
1998 Experimental Pollen Studies in the Crawford Lake Region, pp., 91-94 in Iroquoian Peoples of the Land of Rocks and Water, A.D. 1000-1650: A Study in Settlement Archaeology. Vol 1. William D. Finlayson (ed.). London Museum of Archaeology, London , Ontario, Canada.

Byrne, R., and W. Finlayson
1998 Iroquoian Agriculture and Forest Clearance at Crawford Lake, Ontario, pp., 94-107 in Iroquoian Peoples of the Land of Rocks and Water, A.D. 1000-1650: A Study in Settlement Archaeology. Vol 1. William D. Finlayson (ed.). London Museum of Archaeology, London , Ontario, Canada.

Byrne, R.
1998 Areal Variation in Woodland Food Plant Potential around Crawford Lake, pp., 121-138 in Iroquoian Peoples of the Land of Rocks and Water, A.D. 1000-1650: A Study in Settlement Archaeology. Vol 1. William D. Finlayson (ed.). London Museum of Archaeology, London, Ontario, Canada.

Mensing, S., J. Michaelsen, and R. Byrne
1999 A 560 Year Record of Santa Ana Fires Reconstructed from Charcoal Deposits in the Santa Barbara Basin California. Quaternary Research 51: 295-305

Mensing, S., and R. Byrne
2000 Invasion of Mediterranean Weeds into California before 1769. Fremontia 27: 6-9
</font>[/publication]

<font size=2><b>Recent grants: </b></font>

<font size=2>CAL-DWR. Long-term Variability of Fresh Water Flow into the San Francisco Estuary using Paleoclimatic Methods. (co-PI with Lisa Wells) $30,000. 1994-1995.

USNFS. Grazing Impacts on Monache Meadow, Inyo National Forest (with Robert Dull). $10,000. 1994.

NSF Doctoral Dissertation Research Grant (Eric Edlund). $10,000. July 1994-June 1995.

USGS/NEHRP. Geometry and Holocene Displacement of the Pittsburg Fault Analyses of Tectonic Landforms and Holocene Stratigraphy, (with Patrick Williams and James Wanket). $30,000. 1998-99.

USGS. Detailed Pollen Stratigraphy for Mira Vista Trench Site, (with Patrick Williams and James Wanket). $20,000. 1998-99.

DOE-Westgec Stratigraphic Evidence for Recent Changes in Salinity Regimes in the San Francisco Estuary (with Lynn Ingram) $280,000. 1997-1998.

Tetratek for USN Dating of Recent Marsh Sediments at Concord California by means of Pollen and 210 Pb (with David Wahl and Frankie Malamud Roam). $10,000. 1999.</font>

<font size=2><b>Web Pages</b></font>

<font size=2>Biogeography (Geography 148) Course Page</font>

<font size=2>Aztec Place Name Glyphs and the Extent of the Aztec Empire according to R.H. Barlow</font>

<font size=2>The Valley of Mexico in the middle of the 16th Century</font>

<font size=2>Return to Faculty List
</font><font size=2>Go to Climate and Atmospheric Sciences
</font><font size=2>
</font><IMAGE src="../../geoglink.gif" alt=""/>
