[pic]<IMAGE src="photo/Quanzhong.bmp" alt="My Photo"/>[/pic]



<b> Quanzhong Li </b>

[introduction]I am a [position]PhD candidate[/position] working with Professor Bongki Moon in the [affiliation]Computer Science Department of University of Arizona[/affiliation]. My research interests include [interests]XML indexing and query processing[/interests], [interests]time series and multi-dimensional databases[/interests], [interests]distributed Web server systems[/interests], and [interests]Web technologies[/interests]. My current research focus is on [interests]XML Indexing and Query processing[/interests].[/introduction]


[publication]Publications


Conference Publications

  * XVM: A Bridge between XML Data and Its Behavior,
    <b>Quanzhong Li</b>, Michelle Y. Kim, Edward So and Steve Wood,
    to appear in Proceedings of the 13th International World Wide Web Conference (WWW'2004), New York, NY, May 2004. [Acceptance rate 14.6%]

  *  Partition Based Path Join Algorithms for XML Data,
    <b>Quanzhong Li</b> and Bongki Moon,
    in Proceedings of the 14th International Conference on Database and Expert Systems Applications (DEXA 2003), Prague, Czech Republic, September 2003.

  *  Indexing and Querying XML Data for Regular Path Expressions,
    <b>Quanzhong Li</b> and Bongki Moon,
    in Proceedings of the 2001 International Conference on Very Large Data Bases (VLDB'2001), Rome, Italy, September, 2001. [Acceptance rate 17.4%]

  *  Distributed Cooperative Apache Web Server,
    <b>Quanzhong Li</b> and Bongki Moon,
    in Proceedings of the 10th International World Wide Web Conference (WWW10), Hong Kong, May, 2001. Best Student Paper Award. [Acceptance rate 19.9%]


Journal Publications

  * Skyline Index for Time Series Data,
    <b>Quanzhong Li</b>, Ines Fernando Vega Lopez and Bongki Moon,
    to appear in IEEE Transactions on Knowledge and Data Engineering (TKDE).


Demo/Poster Publications

  * XVM: XML Virtual Machine,
    <b>Quanzhong Li</b>, Michelle Y. Kim, Edward So and Steve Wood,
    to appear in Proceedings of ACM Symposium on Applied Computing 2004 (SAC'2004)(Poster Abstract), Nicosia, Cyprus, March 2004.

  *  XISS/R: XML Indexing and Storage System Using RDBMS,
    Philip J Harding, <b>Quanzhong Li</b> and Bongki Moon,
    in Proceedings of the 2003 International Conference on Very Large Data Bases (VLDB'2003) (Demo Paper), Berlin, Germany, September, 2003. [Acceptance rate 25%][/publication]


Research Projects

<IMAGE src="images/xiss.bmp" alt=""/> [interests]XISS: XML Indexing and Storage System[/interests]

<IMAGE src="images/dca.bmp" alt=""/> [interests]Distributed Cooperative Apache Web Server (DC-Apache)[/interests]

<IMAGE src="images/stagg.bmp" alt=""/> [interests]Spatial and Spatio-Temporal Aggregation[/interests]


My Current Resume

  *  PDF format

  *  MS Word format

  *  HTML format


[contactinfo]Contact Information

[affiliation]Department of Computer Science
University of Arizona[/affiliation]
[address]Gould-Simpson, #718
PO Box 210077
Tucson, AZ 85721-0077[/address]

E-mail address: [email]lqz<IMAGE src="images/blackat.gif" alt=""/>cs.arizona.edu[/email]
Office phone: [phone](520) 621-2759[/phone][/contactinfo]

Last revised: March 3, 2004
