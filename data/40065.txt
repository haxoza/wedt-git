<b> [pic]<IMAGE src="gifs/mateo.jpg" alt=""/>[/pic]</b>

[introduction]<b><font size=4>[position]Prof.[/position] MATEO VALERO</font></b>

<font size=2> <b>[position]CIRI Director[/position]</b></font>
<font size=2>[affiliation]European Center for Parallelism of Barcelona[/affiliation]
<font size=2>[affiliation]Technical University of Catalonia[/affiliation]</font></font>[/introduction]

[contactinfo]<font size=1>Contact:
[affiliation]CEPBA-UPC[/affiliation]
[address]Campus Nord, Modul D6 - 201
Jordi Girona, 1-3
08034 Barcelona
Spain[/address] </font>

<font size=1>Tel:  [phone]+34 - 93 401 6979[/phone]
Fax: [fax]+34 - 93 401 7055[/fax] </font>

<font size=2>email: </font>

<font size=2> </font><b><font size=2>MORE INFORMATION: </font></b> <font size=2> http://www.ac.upc.es/homes/mateo</font>[/contactinfo]

<font size=1>Last update: 30/Jan/2000</font>
