[pic]<IMAGE src="juell98.jpg" alt=""/>[/pic]


[contactinfo]Paul Juell Office: [address]285 A22 IACC[/address]

Mail address
Dr. Paul Juell
[address]Dept. of Computer Science
IACC 258, NDSU
Fargo, ND 58105-5164[/address]

Phone: [phone](701) 231-8196[/phone]
FAX: [fax](701) 231-8255[/fax]
email: [email]paul.juell@ndsu.edu[/email]
web: [homepage]http://www.cs.ndsu.nodak.edu/~juell[/homepage][/contactinfo]

Teaching interests include Artificial Intelligence, Software Engineering, Office Information Systems and Operating Systems.

Research interests include [interests]using technology to improve the way we teach[/interests], [interests]distance education[/interests] and [interests]Artificial Intelligence[/interests].

One of the architects of the Wang Laboratories VS Series computer. Earned his PhD in [phddate]1981[/phddate] from [phduniv]Ohio State[/phduniv] and has been at NDSU since 1982.

Professional Vita: (short version, Publications, Graduate Students completing MS or PhD) (full version)

Schedule for Spring 2007
CS724 , CS790 AI Seminar , for students working under Dr. Juell on MS thesis, MS Plan B, Ph.D

Research about teaching: The Visual Program project. (recent paper and presentation at SSGRR 2001 )

Class Web pages from previous terms.

We saw the Gand Canyon in the summer of 2005. We also saw Cairo the Summer of 2004 . Wow, the pictures from the Chandra X-ray Observatory are something.

Some items of interest.
A trip to Hawaii and to Banff during the summer of 2003. first snow of Fall 2002 , A trip to Italy.
Some of the flooding Spring 2001 (taken 4/14/01 about the crest). A fun question .
The Air Show at Grand Forks Air Force Base (Aug 30, 98). Sunrise 12/3/98
There are pictures from the Fargo flood of 1997. There are also some pictures from a trip to Viet Nam in 1996 and a trip to Germany in 1999 . In 1985 I was part of a People to People delegation to The Peoples' Republic of China .
An interesting place to visit, Mars. I just started reading a book that gives some of the future of our technology: The Design of Children's Technology by Allison Druin (published by Morgan Kaufmann 1999). A book that presents some future technology that is just starting to be adopted is: "When Things Start to Think" by Neil Gershenfeld.

a/html>
