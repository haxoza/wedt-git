[contactinfo]<b> <font size=6> Welcome to Jimmy Huang's Home Page </font> </b> [pic]<IMAGE src="http://www.cs.yorku.ca/~jhuang/images/huang.jpg" alt="(Xiangji's Email: [email]jhuang@cs.yorku.ca[/email])"/>[/pic][/contactinfo]

[introduction]I am an [position]associate professor[/position] at the [affiliation]School of Information Technology, York University[/affiliation]. From September 2001 to June 2003, I was a Post Doctoral Fellow working with Professor Nick Cercone at the School of Computer Science, University of Waterloo. I did my PhD in [phdmajor]Information Science[/phdmajor] at [phduniv]City University in London[/phduniv], England, with Professor Stephen Robertson.

My major research areas are [interests]information retrieval[/interests] and its [interests]application to the Web[/interests]. In particular, my research activities focus on: (1) [interests]statistical models and machine learning for information retrieval[/interests]; (2) [interests]contextual information retrieval and Web personalization[/interests]; (3) [interests]biomedical text retrieval and mining[/interests]; (4) [interests]high accuracy retrieval from terabyte scale data sets[/interests]; (5) [interests]Chinese information retrieval[/interests]; (6) [interests]Web search and mining[/interests].

Students who are interested in pursuing their PhD and Master degrees with me are encouraged to contact me. My graduate students should be intelligent, have a strong motivation to conduct high quality research, have an open and clear mind, and be able to work diligently and independently.[/introduction]

<IMAGE src="http://www.cs.yorku.ca/~jhuang/images/hline.gif" alt=""/>


<IMAGE src="http://www.cs.yorku.ca/~jhuang/images/ball_blue.gif" alt=""/> Short Bio


<IMAGE src="http://www.cs.yorku.ca/~jhuang/images/ball_blue.gif" alt=""/> Research

  *  [resinterests]Areas of interest: [interests]information retrieval[/interests], [interests]data mining[/interests], [interests]natural language processing[/interests], [interests]bioinformatics[/interests], [interests]computational linguistics and digital library[/interests].[/resinterests]

  * Selected Peer Reviewed Publications

  * Special Issue on Biomedical Text Retrieval and Mining


<IMAGE src="http://www.cs.yorku.ca/~jhuang/images/ball_blue.gif" alt=""/> Teaching


<IMAGE src="http://www.cs.yorku.ca/~jhuang/images/ball_blue.gif" alt=""/> Service


<IMAGE src="http://www.cs.yorku.ca/~jhuang/images/ball_blue.gif" alt=""/> Research Supervision





<IMAGE src="http://xyz.freeweblogger.com/counter/index.php?u=xiangjihuang&s=break3" alt="free web counter"/>
free web counter
