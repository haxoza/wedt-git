Welcome-Welcome-Welcome!

[pic]<IMAGE src="michael.jpg" alt=""/>[/pic]

<font size=5>My Position</font> <IMAGE src="PIX/bgu_logo_tiny.gif" alt=""/>

  [introduction]* <font size=4> I've finished [phddegree]Ph.D[/phddegree] in [phdmajor]Computer Science[/phdmajor] at the Dept. of Math & CS, [phduniv]Ben-Gurion University of the Negev[/phduniv], Israel, moved to the [affiliation]University of British Columbia, Canada[/affiliation] and now I'm at [affiliation]Communication Systems Engineering Department[/affiliation], serving as a [position]Chairman[/position]. For more information about me, please, go to here </font>[/introduction]

<font size=5>My family</font> The "sun" kiss,

<font size=5>...and together</font> ...around the world,

<font size=5>...and my beautiful</font> ...wife,

<font size=5>My hobbies</font> <IMAGE src="PIX/table_tennis.gif" alt=""/>

  * <font size=4>I just LOVE this game. I'm also playing in Israel competitions for championship. For more information, see Table Tennis.</font>

[contactinfo]<font size=5>My e-mail address:</font> <IMAGE src="MAILBOX.GIF" alt=""/> <font size=4>[email]segal@cs.bgu.ac.il[/email]</font>[/contactinfo]

<font size=5>My teaching</font>

<IMAGE src="cyanball.gif" alt=""/> <font size=4>Automata and formal languages</font>

<IMAGE src="cyanball.gif" alt=""/> <font size=4>Design of algorithms</font>

<IMAGE src="cyanball.gif" alt=""/> <font size=4>Complexity of computations</font>

<IMAGE src="cyanball.gif" alt=""/> Data structures

<IMAGE src="cyanball.gif" alt=""/> <font size=4>Introduction to Computer Science</font>

<IMAGE src="cyanball.gif" alt=""/> <font size=4>Data structures 2</font>

<IMAGE src="cyanball.gif" alt=""/> <font size=4>Algorithmica</font>

<IMAGE src="cyanball.gif" alt=""/> <font size=4>Architecture and Assembly language</font>

<font size=5>My awards</font> <IMAGE src="prizedomain-logo.gif" alt=""/>

  * <font size=4>Ministry of Justice Post-doctoral award, 1999, Israel</font>

  * <font size=4>Pacific Institute of the Mathematical Studies Post-doctoral Fellowship, 1999, Canada</font>

  * <font size=4>Kreitman Prize for Distinguished Ph.D studies, 1999</font>

  * <font size=4>New York Federation Award, 1998</font>

  * <font size=4> Otto Schwartz Prize "For Excellent Studies, Ph.D", 1997</font>

  * <font size=4> Intel Prize "For Outstanding M.Sc work", 1996</font>

  * <font size=4>Achievement Award "The Big Ten of the University", 1995 </font>

  * <font size=4> First place in Table Tennis Israelian competions, 1994

    </font>

  * <font size=4>Gold Medal "For Excellent Achievements in the Studies" ,1989 </font>

  * <font size=4>Prizes in the finals of Soviet Union and Moldavian National Contests for high school students in math, physics and computer science. </font>

[publication]<font size=5>Not so recent publications</font> <IMAGE src="write.gif" alt=""/>

  * Segal M. and Shimony, S. E. Genetic Algorithm for finding minimal explanations, 12th Israeli Symposium on AI, CV, NN (1995).

  * Segal M. and Kedem, K. Enclosing k points in the smallest axis parallel rectangle, Information Processing Letters 65(2): 95-99 (1998)

  * Segal M. and Kedem, K. Geometric applications of posets, Workshop on Algorithms and Data Structures'97, Springer-Verlag 1272. Also to appear in Computatational Geometry: Theory and Applications

  * Segal M. On piercing of axis-parallel rectangles and rings, European Symposium on Algorithms'97, Springer-Verlag 1284. Also to appear in International Journal of Computational Geometry and Applications

  *  Bespamyatnikh, S. ,Segal M. Covering a set of points by two axis-parallel boxes, To appear in Information Processing Letters.

  *  Katz, M. , Kedem, K. ,Segal M. Constrained square-center problems., Scandinavian Workshop on Algorithmic Theory'98. Also to appear in Computatational Geometry: Theory and Applications.

  *  Bespamyatnikh, S. , Kedem, K. , Segal M., Tamir, A. Optimal Facility Location under Various Distance Functions, accepted to WADS'99. Also to appear in International Journal of Computational Geometry and Applications.

  *  Katz, M , Benmoshe, B., Segal M. Obnoxious facility location: complete service with minimal harm, accepted to CCCG'99. Also to appear in International Journal of Computational Geometry and Applications.

  *  Bespamyatnikh, S. , Segal M. Rectilinear Static and Dynamic Discrete 2-center Problems, Accepted to WADS'99. Accepted to Int. Journal of Math. Algorithms.

  *  Katz, M. , Kedem, K. , Segal M. Improved Algorithms for Placing Undesirable Facilities, European Workshop on Computational Geometry'99. Submitted for journal publication.

  * Katz M.,Nielsen F., Segal M. Dynamic maintenance of piercing sets with applications, accepted to International Symposium on Algorithms And Computation (ISAAC'00)

  * Segal M. Covering Point Sets and Accompanying Problems, PhD thesis, Ben-Gurion University, 1999.

  * Segal M. Lower bounds for covering problems, Submitted for journal publication

  *  Bespamyatnikh, S. , Segal M. Longest increasing subsequence and patience sorting in less than O(nlogn) time, accepted to Information Processing Letters

  *  Bespamyatnikh, S. , Segal M. Fast algorithms for approximating distances, Submitted for journal publication

  *  Bespamyatnikh, S. , Bhattacharya, B., Kirkpatrick, D, Segal M. Efficient algorithms for centers and medians in interval and circular-arc graphs, accepted to ESA'00. Submitted for journal publication.

  *  Bespamyatnikh, S. , Bhattacharya, B., Kirkpatrick, D, Segal M. Mobile facility location, Accepted to ACM International Workshop on Discrete Algorithms and Methods for Mobile Computing and Communications.

  *  Bespamyatnikh, S. , Segal M. Selecting distances in arrangements, Submitted for journal publication.

  * Segal M. Placing Facilities in Geometric Networks, Submitted for journal publication

  *  Hadar, O. , Segal M. Models and Algorithms for Bandwidth Allocation of CBR Video Streams in a VoD, accepted to IEEE International conference on Information Technology: Coding and computing (ITCC'2001)

  *  Bepamyatnikh, S. , Segal M. Kth Order Voronoi Diagrams and Their Applications, manuscript in preparation.

  *  Bepamyatnikh, S. , Bhattacharya, B., Kirkpatrick, D, Segal M. Dynamic facility location, manuscript in preparation.

  * Benmoshe B., Katz M., Segal M. Finding medians and centers for the set of points, manuscript in preparation.[/publication]

    <font size=5>One of My Projects</font> <IMAGE src="drawing.gif" alt=""/>

    [resinterests]<IMAGE src="orangeball.gif" alt=""/> <font size=4>My research : [interests]Computational Geometry[/interests], [interests]Computational complexity[/interests]</font>

      * <font size=4>[interests]Computational Geometry resources[/interests] </font>

      * <font size=4>[interests]Another Computational Geometry[/interests] resources </font>[/resinterests]

    <font size=5>My credo</font> <IMAGE src="comp.mouse.gif" alt=""/>

    <IMAGE src="whiteball.gif" alt=""/> <font size=4>No pain - no gain!!!</font>

    <font size=5>The most popular Computer Science Departments in World</font>

    <IMAGE src="logo.csd.gif" alt=""/> <font size=4> Department of CS, Stanford University</font>

    <IMAGE src="banner.gif" alt=""/> <font size=4> Department of CS, Princeton University</font>

    <IMAGE src="littlebluemen.gif" alt=""/> <font size=4> Department of EE & CS, Massachusetts Institute of Technology</font>

    <IMAGE src="HTTPLogo.gif" alt=""/> <font size=4> Department of CS, Berkeley University</font>

    <IMAGE src="cornell.gif" alt=""/> <font size=4> Department of CS, Cornell University</font>

    <IMAGE src="front-small.gif" alt=""/> <font size=4> Department of CS, Yale University</font>
