[pic]<IMAGE src="../facphotos/bowers133.jpg" alt=""/>[/pic] <font size=6><b>John E. Bowers </b></font>

[contactinfo]<font size=6><b>[position]Professor[/position]</b></font>

<font size=5>[position]Director[/position]: </font><font size=5>[affiliation]Multidisciplinary Optical Switching Technology Center[/affiliation] (MOST</font><font size=5>)</font>

[address]Eng. Science Bldg., Room 2221C
Department of Electrical and Computer Engineering
University of California
Santa Barbara, CA 93106-9560[/address]
<b>e-mail</b>:[email]bowers@ece.ucsb.edu[/email]
<b>phone:</b> [phone](805) 893-8447[/phone] or [phone](805) 893-2149[/phone] (assistant)
<b>fax:</b> [fax](805) 893-7990[/fax][/contactinfo]

[introduction]<b>[phddegree]Ph.D.[/phddegree], [phduniv]Stanford University[/phduniv]</b>
Solid State: fiber optic networks, photonic integrated circuits, high speed photonic and electronic devices, femtosecond lasers and physics

Professor Bowers' research interests are in the development of [interests]novel optoelectronic devices for the next generation of optical networks[/interests]. His research interests include [interests]novel ways of growing quantum wires and dots using MBE and MOCVD[/interests], [interests]techniques to fuse dissimilar materials together for new devices and improved performance[/interests], the [interests]design of quantum well structures for high speed light generation and detection[/interests], and the [interests]design of high speed time division multiplexed systems and devices[/interests]. Prior to joining UCSB in 1987, Professor Bowers worked at AT&T Bell Laboratories on semiconductor lasers and photodetectors. As a Research Associate at Stanford University, he worked on fiber optic sensors. His Ph.D. thesis at Stanford University was on SAW signal processing devices. Professor Bowers is [position]Director[/position] of the [affiliation]UCSB Multidisciplinary Optical Switching Technology Center (MOST)[/affiliation] , and is a [position]member[/position] of the [affiliation]Heterogeneous Optoelectronics Technology Center (HOTC)[/affiliation] and the [affiliation]NSF Center on Quantized Electronic Structures (QUEST)[/affiliation] . He is also the [position]Executive Director[/position] of the newly created [affiliation]Center for Entrepreneurship & Engineering Management (CEEM)[/affiliation].[/introduction]

<b>Recognitions and Honors:</b>
* Fellow of the American Physical Society (1996)
* IEEE Leos William Streifer Award (1996)
* Board of Governors, IEEE Lasers and Electro-optics Society (1995)
* Fellow, IEEE (1992)
* Distinguished Lecturer, IEEE (1992)
* Presidential Young Investigator, National Science Foundation (1988)
* National Science Foundation Ph.D. Fellowship (1976)
* Sigma Xi's Thomas F. Andrews Price (1976)

<b>Research Group:</b>
Optoelectronics Research Group

<b>Research Projects:</b>
Nano-Photonic Integration of Ultra-Fast WDM Optical Communications Systems - NSF Initiative on Ultra-High Capacity optical Communications and Networking

<b><font size=2>College of Engineering</font></b><font size=2><b> ?University of California, Santa Barbara</b></font>

<font size=2>The Department ?Current Announcements ?Student Information ?Research
Program of Study ?Admissions Information ?Department Directory</font>

<font size=2>Questions or Comments: webadmin@ece.ucsb.edu</font>

<font size=2>Last Updated: August 13, 2004 </font>
