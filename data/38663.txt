[pic]<IMAGE src="images/JongPark2006.jpg" alt="Jong C. Park"/>[/pic]

<IMAGE src="images/welcome3.gif" alt="Welcome message"/>

<b><IMAGE src="images/board_a_home.gif" alt="board_a_home.gif"/>

<IMAGE src="images/mail09.gif" alt="mail09.gif"/>
</b>

[contactinfo]Address

Jong Cheol Park, Ph.D.
[position]Associate Professor[/position]
[affiliation]Computer Science Division
EECS Department, KAIST[/affiliation]
[address]373-1 Guseong-dong, Yuseong-gu
Daejon 305-701 South KOREA[/address]

<IMAGE src="images/jongpark-name.jpg" alt=""/>
Office: [phone]+82 42 869 3541[/phone]
Fax: [fax]+82 42 869 5581[/fax]
Lab: [phone]+82 42 869 3581[/phone]
e-mail: [email]park AT cs.kaist.ac.kr[/email]
URL: [homepage]http://nlp.kaist.ac.kr/~park[/homepage][/contactinfo]

[resinterests]<b>Academic
Interests</b>

[interests]User Customized Natural Language Information Service[/interests]: http://ilens.org
[interests]Computational Linguistics (grammar-based approaches to language modeling)[/interests]
[interests]Natural Language Processing (human-friendly interface for computers and artificial beings)[/interests]
[interests]Biomedical Informatics[/interests] (applications of NLP techniques to biomedicine): http://www.biopathway.org
[interests]Robotics (emotionally appropriate interface for computers)[/interests]
[interests]Artificial Intelligence[/interests], [interests]Cognitive Science[/interests][/resinterests]

[resactivity]<b>Academic
Affiliations</b>

[affiliation]Sigma Xi[/affiliation]: The Scientific Research Society ([position]member[/position], 1988~)
The [affiliation]Association for Computational Linguistics[/affiliation] ([position]member[/position], 1992~)
[affiliation]Korea Information Science Society[/affiliation] ([position]lifetime member[/position], 1998~; editor, 1999~2001; board member, 2007)
Advanced Information Technology Research Center (principal investigator, 1999~2008)
Brain Science Research Center (participating researcher, 2004~)
[affiliation]Korean Society for Language and Information[/affiliation] ([position]board member[/position], 2005~; [position]editor[/position], 2003~)
KAIST Interdisciplinary Program: Robotics Institute (steering committee)
KAIST CS Division (publicity committee; newsletter editorial board)

<b>Recent
Activities</b>

Dagstuhl Seminar: Ontologies and Text Mining for Life Sciences (Germany, 3/2008, link)
The Second International Symposium on Languages in Biology and Medicine (Singapore, 12/2007)
ISMB/ECCB, Program Committee (Austria, 7/2007)
BioCreative Phase II, Program Committee (Spain, 4/2007)
Advanced NLP and Text Mining Forum (Japan, 3/2007)
The 5th Korea Singapore Workshop on Bioinformatics and NLP (Korea, 11/2006, link)
Workshop on Text Mining, Ontologies, and NLP in Biomedicine (UK, 3/2006, link)
The First International Symposium on Languages in Biology and Medicine (Korea, 11/2005, link)[/resactivity]

[education]<b>Education</b>

[msuniv]Seoul National University[/msuniv], [msmajor]Computer Engineering[/msmajor]: BE & MSE
[phduniv]University of Pennsylvania[/phduniv], [phdmajor]Computer & Information Science[/phdmajor]: Ph.D[/education]

<IMAGE src="images/taddy01b.gif" alt="taddy01b.gif"/>

<IMAGE src="http://img.yahoo.co.kr/spm/linkbtn_dic.gif" alt=""/>
