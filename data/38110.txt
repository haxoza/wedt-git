[pic]<IMAGE src="me-upd.jpg" alt="mugshot"/>[/pic]

[introduction]Welcome to my home page. I am a [position]graduate student[/position] pursuing my M.S. in [msmajor]Computer Science[/msmajor] at the School of Computing in the [msuniv]University of Utah[/msuniv]. I am working with Prof. Ganesh Gopalakrishnan in the [affiliation]Utah Verifier (UV) group[/affiliation]. I did my undergraduate studies from the [bsuniv]Birla Institute of Technology and Science[/bsuniv], Pilani, India. I majored in [bsmajor]Mathematics and Computer Science[/bsmajor]. After my undergraduate studies I worked for one year at Hughes Software Systems in Bangalore developing software based on Voice over IP (VoIP) protocols H.323 and SIP.[/introduction]

<b>Research</b>

[publication]<b>Publications</b>

  * <b>Shared memory consistency protocol verification against weak memory models: refinement via model-checking</b>, [PS, PDF] Prosenjit Chatterjee, Hemanthkumar Sivaraj, Ganesh Gopalakrishnan, In 14th International Conference on Computer Aided Verification (CAV'02), Lecture Notes in Computer Science, Copenhagen, July 2002. Springer-Verlag.


  * <b>Random Walk Based Heuristic Algorithms for Distributed Memory Model Checking</b>, [PS] Hemanthkumar Sivaraj, Ganesh Gopalakrishnan, In 2nd International Workshop on Parallel and Distributed Model Checking (PDMC'03), Boulder, Colorado, USA, July 2003.
    More details like the psudocodes for the various heuristics can be found in the School of Computing Technical Report UUCS-03-001 [PS, PDF].


  * <b>Parallel Random Walk Based Heuristics for Semi-Formal Verification</b>, Hemanthkumar Sivaraj, Ganesh Gopalakrishnan, In TECHCON 2003, Dallas, USA, August 2003.[/publication]

Here's my <b>Resume</b> (doc) (pdf)

[contactinfo]<b>Contact Information</b>

You can mail me at <IMAGE src="namepic.jpg" alt=""/>

<b>Office Address</b> <b>Home Address</b>

[address]School of Computing, Room #3190 1263 E South Temple, Apt. 10

50 S. Central Campus Drive, University of Utah Salt Lake City, UT 84102

Salt Lake City, Utah - 84112-9205[/address]

Phone: [phone](801) 581-4183[/phone] Phone: [phone](801) 531-1756[/phone][/contactinfo]

Personal

<b>Fun Stuff</b>

Warp images.

<b>Friends</b>

  * Abhijeet Joglekar, scandwitty@times :-)

  * Parveen Patel, no no noo ...

  * Vijay, ... yeah right, that's what my uncle used to say...

  * Prashant Mohan, alias Pondy, Bastu, Pups, ..., eh ehh ehhh teach me DiffG

  * Sandeep Tare , the illustrious sandy

  * Vishal Sanghvi

  * Sameer Herlekar, Sam ra baboo

My Bookmarks (not updated for a long time)

"Be what you would seem to be" - or if you'd like it put more simply -
"Never imagine yourself not to be otherwise than what it might appear
to others that what you were or might have been was not otherwise than
what you had been would have appeared to them to be otherwise."
- The Duchess, from Lewis Caroll's Alice's Adventures in Wondeland
