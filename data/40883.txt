<IMAGE src="duke-shield.gif" alt=""/> <font size=4>[contactinfo]Badrish Chandramouli</font>
[position]Ph.D. candidate[/position]
[affiliation]Department of Computer Science
Duke University[/affiliation]
Office phone: [phone]919-660-6599[/phone]
[email]<IMAGE src="email.png" alt=""/>[/email] [pic]<IMAGE src="badrish.jpg" alt=""/>[/pic][/contactinfo]


<b>Main page</b>

<b>Publications </b>

<b>Projects</b>

<b>Resume</b>

<b>Coursework</b>


[introduction]I am a [position]Ph.D. candidate[/position] in the Department of [phdmajor]Computer Science[/phdmajor] at [phduniv]Duke University[/phduniv]. I have a Master's degree in [msmajor]Computer Science[/msmajor] from [msuniv]Duke university[/msuniv] and a Bachelor's degree in [bsmajor]computer technology[/bsmajor] from [bsuniv]VJTI[/bsuniv], Mumbai, India. Before coming to Duke, I worked for two years as a software design engineer. My current research interests are:

  * [interests]Databases and publish/subscribe systems[/interests]

  * [interests]Query processing and optiimization[/interests]

  * [interests]Distributed systems and distributed data management[/interests]

  * [interests]Pervasive access to data and applications[/interests]

  * [interests]Approximate replication[/interests]

I am currently working on publish/subscribe systems and some interesting query processing challenges. My main line of research focuses on [interests]supporting better scalability and richer subscription models in wide-area publish/subscribe[/interests]. My Ph.D. advisor is Dr. Jun Yang.[/introduction]

[publication]<b>Most recent publication:</b>

  * B. Chandramouli, C. Bond, S. Babu, and J. Yang. On Suspending and Resuming Dataflows. In Proceedings of the 23rd International Conference on Data Engineering (ICDE '07), Istanbul, Turkey, April 2007 (poster paper). To appear.[/publication]

Other publications are listed here.

<b>Brief biodata:</b>

[introduction]Prior to Duke, I was employed with the Texas Instruments Research and Development Center (India) for 2 years as a software design engineer. In spring 2003, I worked as a teaching assistant for the undergraduate course on Software Design and Implementation (CPS 108). In summer 2003, I worked as a research assistant for Dr. Amin Vahdat, on distributed resource querying. In fall 2003, I was employed as a teaching assistant for the undergraduate course in Operating Systems (CPS 110). In spring 2004, I was the teaching assistant for the graduate computer networks (CPS 214) course.

In summer 2004, I worked as a research intern at IBM T. J. Watson Research Center, Yorktown Heights, NY. There, under the mentorship of Dr. Hui Lei, I designed and implemented Puma. Puma is a generic framework for using ad hoc, peer communication tools (such as email, SMS, IM) for structured interactions between users and applications. Puma supports both user-initiated (pull) and application-initiated (push) interactions. It offers flexibility, convenience, and intimacy to end users and enables seamless pervasive access to applications. In summer 2005, I again interned at IBM Watson Research. This time, I worked with Dr. Hui Lei and several others on a Business Performance Management project. We designed and implemented a solution that offers a model-driven approach to business performance visibility. The project involved data warehousing, automated code generation, and view generation using Service Data Objects.

In fall 2002, my first semester at Duke, I took courses in advanced computer architecture (CPS 220), analysis/design of algorithms (CPS 230), algorithms in computational biology (CPS 296) and a research seminar (CPS 300). In spring 2003, I took courses in graduate operating systems (CPS 210) and parallel computer architecture (CPS 221). In fall 2003, I took courses in machine learning (CPS 271) and mathematics and methodologies of systems analysis (ECE 255). In spring 2004, I took courses in advanced computer networks (CPS 214), advanced database systems (CPS 216), and performance/reliability of computer networks (ECE 257). I also completed my second year research project titled <b>Distributed network querying: reducing costs by providing approximate answers</b>, under the supervision of Dr. Amin Vahdat and Dr. Jun Yang. This work was awarded the <b>best</b> second year research project in the Computer Science department of Duke university, and earned me an MS degree. In fall 2004, I took a course on Experimental Methods in Computer Systems (CPS 296.2). I was also working on extending my second year research project to cover various issues involved in distributed bounded approximate caching, including cache location and selection techniques and efficient distributed data structures for network querying.

Some of my recently completed projects include:
- On Suspending and Resuming Queries
- On the Database/Network Interface in Large-Scale Publish/Subscribe Systems
- A model-driven approach to business performance visibility (at IBM T. J. Watson Research Center)
- Distributed Network Querying with Bounded Approximate Caching
- Pervasive enablement of applications and human tasks (at IBM T. J. Watson Research Center)[/introduction]

Choose from the links to the left for more information.

<font size=1>Last updated: July 12, 2006</font>
