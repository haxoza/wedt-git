    <IMAGE src="index2003_files/en_255x60_ff_tr.gif" alt="HUT"/>


    [pic]<IMAGE src="index2003_files/ess.gif" alt="ess"/>[/pic]



    [introduction]Eljas Soisalon-Soininen[position]Professor[/position] of Computer Science

    [affiliation]Helsinki University of Technology

    Laboratory of Software Technology[/affiliation][/introduction]


    [contactinfo]Contact information

    <b>Postal address:</b>


    [address]Helsinki University of Technology
    Department of Computer Science and Engineering 
    P.O. Box 5400, FI-02015 HUT, Finland[/address]

    <b>Street address:</b>
    [address]Computer Science Building ,
    Konemiehentie 2, Espoo, Finland[/address]

    <b> Office:</b>
    [address]A204[/address]

    <b> Tel: </b>
    [phone]+358 9 451 3224[/phone]

    <b> Fax:</b>
    [fax]+358 9 451 3293[/fax]

    <b> Email:</b>
    [email]ess@cs.hut.fi[/email]

    <b>Office hours</b>:
    Call for appointment[/contactinfo]



    Courses


    T-106.5200 Tietokanta-algoritmit (T-106.510 Tietokanta-algoritmit)
    T-106.5800 Ohjelmistotekniikan seminaari ( T-106.850 Ohjelmistotekniikan seminaari)
    T-106.6100 Täydentävät opinnot (T-106.710 Täydentäv?opintojakso)
    T-106.6400 Yksilölliset opinnot (T-106.870 Yksilöllinen opintojakso)
    (T-106.910 Concurrency in Data Structures)



    Publications


    [resinterests]Areas of Interests


      * [interests]Algorithms and data structures[/interests]

      * [interests]Database systems[/interests]

      * [interests]Concurrency and recovery[/interests]

      * [interests]Logic databases[/interests]

      * [interests]Query processing and optimization[/interests]

      * [interests]Formal languages and parsing[/interests][/resinterests]





    This page is maintained by K. Toivonen.
