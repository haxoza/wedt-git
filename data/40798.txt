[pic]<IMAGE src="img/Butz342_small.jpg" alt=""/>[/pic]

Home

Research

Teaching

Publications

Professional Activities

Private Activities


Andreas Butz - Homepage

[introduction]Welcome Visitor! My name is Andreas Butz and I'm a [position]Professor[/position] of Computer Science in the [affiliation]Media Informatics Group of the CS Department at the University of Munich[/affiliation], Germany. In person, you can find me in room 502 of the media informatics building at the address below. These pages are meant to give you a brief overview of my professional and private activities. For colleagues, they contain a short research agenda with links to the relevant projects, a list of my publications and of other professional activities I'm involved in. For students there's a list of my teaching activities with links to course material, which is updated weekly during the semester. To round off the picture, a few links to web pages about my private activities are given. Have fun browsing through these pages, and let me know if you're missing some information which should be there![/introduction]

[contactinfo]Prof. Dr. Andreas Butz
[affiliation]Ludwig-Maximilians-Universität München
Institut für Informatik
LFE Medieninformatik[/affiliation]
[address]Amalienstrasse 17
80333 München
Germany[/address]
Tel.: [phone]+49-89-2180-4650[/phone] (secretariate)
Tel.: [phone]+49-89-2180-4665[/phone] (personal)
Fax: [fax]+49-89-2180-4652[/fax]
email: [email]butz@ifi.lmu.de[/email][/contactinfo]
office hour: wednesday 4-5pm
