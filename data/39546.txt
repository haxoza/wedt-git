<IMAGE src="/topnav_images/logo.gif" alt="Center Home"/> <IMAGE src="/topnav_images/photos.jpg" alt="Science Policy Photos"/> <IMAGE src="/topnav_images/cu.gif" alt="University of Colorado"/> <IMAGE src="/topnav_images/sponsors.gif" alt="spacer"/>

<IMAGE src="/topnav_images/noaadisclaimer.gif" alt="NOAA Disclaimer"/>

About Us Projects Publications For Students Outreach Search

<b>Links for Martyn Clark:</b>

  * Curriculum Vitae

  * Publications

<b>Location:</b> Center Home > About Us > Meet Us > Martyn Clark > Homepage


Homepage

[pic]<IMAGE src="/admin/resize_img.php?type=png&pic=/home/html/admin/affiliate_files/images/photo-1.jpg&width=150&height=169" alt=""/>[/pic]

[contactinfo]<b>Martyn P. Clark</b>

[affiliation]National Institute for Water and Atmospheric Research (NIWA)[/affiliation]
[address]P.O. Box 8602, 10 Kyle Street
Riccarton
Christchurch[/address],

Tel: [phone]64-3-343-7881[/phone]
Fax: [fax]64-3-343-7899[/fax]
[email]mp.clark@niwa.co.nz[/email][/contactinfo]

[introduction]Martyn Clark received a Ph.D. from the [phduniv]University of Colorado[/phduniv] in [phddate]1998[/phddate]. Martyn worked as a research scientist at the University of Colorado's Cooperative Institute for Research in Environmental Sciences (CIRES) on a variety of topics including [interests]large-scale climate dynamics[/interests], [interests]land-atmosphere interactions[/interests], and [interests]applied hydro-climatology[/interests]. Martyn has a number of projects on improving [interests]streamflow forecasts in snowmelt dominated river basins[/interests], and on incorporating [interests]scientific advances into operational streamflow forecasting systems[/interests].

Martyn worked for the Center for Science and Technology Policy Research until 2005 and now works for the [affiliation]National Institute of Water and Atmospheric Research[/affiliation] in New Zealand.[/introduction]

NOAA Disclaimer | Sitemap | Contact | Find us | Email webmaster
