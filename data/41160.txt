<IMAGE src="../../images/dept-economicsandfinance_main.gif" alt=""/>

<font size=2></font> <IMAGE src="../../images/busgroup2.jpg" alt=""/>

<font size=1></font>

<font size=2>Economics & Finance Department:</font>
<font size=2>-Overview</font>
<font size=2>-Undergraduate</font>
<font size=2>-Graduate</font>
<font size=2> </font><font size=2>-Faculty & Staff</font>
<font size=2>-Contact</font>



<font size=1></font> <font size=2>CBA LINKS: </font>
-<font size=2>-Graduate Programs </font>
-<font size=2>-College</font>

<font size=1></font>

<font size=1></font>

<font size=1></font>

<font size=1></font> <font size=2>
</font>

<font size=1></font> <font size=2>
</font>

<font size=1></font> <font size=2>
</font>

<font size=1></font> <font size=2>
</font>

<font size=1></font> <font size=2> </font>

<font size=1></font> <font size=2> </font>

<font size=1></font> <font size=2> </font>

<font size=1></font> <font size=2> </font>

<font size=1></font> <font size=2> </font>

<font size=2><b><font size=3></font></b></font>

  [introduction]<font size=2><b>[affiliation]Department of Economics and Finance[/affiliation]</b><b> Faculty & Staff</b></font>

  <font size=2>[pic]<IMAGE src="images/gzhang.jpg" alt=""/>[/pic]</font><font size=2>
  </font> <font size=2><b>[name]GE ZHANG[/name]</b></font>

  <font size=2> </font> <font size=2>[position]Assistant Professor[/position] of Finance
  Ph.D., [phduniv]Duke University[/phduniv], [phddate]2003[/phddate] </font>

  <font size=2>At UNO since 2003</font>

  <font size=2>Dr. Zhang is the latest addition to the finance faculty. She currently teaches corporate finance and her research interests include corporate finance and empirical finance. </font>[/introduction]

    [contactinfo]<font size=2>[address]Office: BA 327[/address]
    Phone: [phone](504) 280-6096[/phone]
    [email]gzhang@uno.edu[/email]</font>[/contactinfo]

  <font size=2>
  </font>


<font size=2>
</font>

<font size=1>Overview</font><font size=1> | Faculty & Staff | Undergraduate Program | Graduate Program
College of Business Administration | UNO </font>

<font size=1>Contact
</font>

  <font size=1><b> &COPY; Copyright, Disclaimer and Site Design
  </b> </font>

<font size=1> </font>

<font size=2> </font>
