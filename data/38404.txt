<b><IMAGE src="_derived/index.html_cmp_balance110_bnr.gif" alt="Carlos Guestrin's Home Page"/></b>

<IMAGE src="_derived/home_cmp_balance110_vbtn_p.gif" alt="Home"/>
<IMAGE src="_derived/group.html_cmp_balance110_vbtn.gif" alt="Group"/>
<IMAGE src="_derived/research.html_cmp_balance110_vbtn.gif" alt="Research"/>
<IMAGE src="_derived/publications.html_cmp_balance110_vbtn.gif" alt="Publications"/>
<IMAGE src="_derived/teaching.html_cmp_balance110_vbtn.gif" alt="Teaching"/>
<IMAGE src="_derived/bio.html_cmp_balance110_vbtn.gif" alt="Bio"/>
<IMAGE src="_derived/contact-info.html_cmp_balance110_vbtn.gif" alt="Contact Information"/>
<IMAGE src="_derived/personal.html_cmp_balance110_vbtn.gif" alt="Personal"/>



<b> </b>



<b>Welcome! </b>



<b><b>Carlos Guestrin</b></b>


[introduction][position]Assistant Professor[/position]
[affiliation]Machine Learning Department and
Computer Science Department
Carnegie Mellon University[/affiliation]

[pic]<IMAGE src="index_files/image002.gif" alt=""/>[/pic]<b>
</b>

This year, I am running the newly renamed Intelligence Seminar (formerly known as AI seminar).

This semester I teach: Machine Learning --- 10-701 and 15-781, Spring 2007
Last semester I taught: Probabilistic Graphical Models --- 10-708, Fall 2006


I co-direct the Sense, Learn, Act (SELECT) Lab with Geoff Gordon.

I co-organized a NIPS 2005 workshop called <b>Intelligence Beyond the Desktop</b> with Mark Paskin.[/introduction]

<IMAGE src="Images/cg16-5.jpg" alt=""/><IMAGE src="Images/cg16-3.jpg" alt=""/><IMAGE src="Images/cg16-5.jpg" alt=""/>

<IMAGE src="Images/cg16-2.jpg" alt=""/><IMAGE src="Images/cg16-4.jpg" alt=""/><IMAGE src="Images/cg16-2.jpg" alt=""/>

<IMAGE src="Images/cg16-5.jpg" alt=""/><IMAGE src="Images/cg16-3.jpg" alt=""/><IMAGE src="Images/cg16-5.jpg" alt=""/>

<IMAGE src="_themes/balance/abalrule.gif" alt="horizontal rule"/>

Home | Group | Research | Publications | Teaching | Bio | Contact Information | Personal

Comments, questions and suggestions? Please contact me.
