 Teaching Staff  |  Administrative/Technical Staff  |  Visiting Scholars  |  Students  <IMAGE src="../Images/home.gif" alt="PHY Home"/> <IMAGE src="../Images/up.gif" alt="Teaching Staff"/>



Xi-Tian Zhang's Home Page
[pic]<IMAGE src="xtzhang.jpg" alt="Xi-Tian Zhang"/>[/pic]


[education]Xi-Tian Zhang
BSc ([bsuniv]Jilin Univ.[/bsuniv]); MS, PhD (Changchun Inst. of Optics, [phduniv]CAS[/phduniv])<b>[/education]

  * Education

  * Position

  * Honour and Award

  * Research Interests

  * Selected Publications

  * Address/Telephone no./E-mail


[education]Education

  1. <b>BSc,</b> [bsuniv]Jilin University[/bsuniv], Changchun, China

  2. <b>MS,</b> [msuniv]Changchun Institute of Optics, Chinese Academy of Science[/msuniv], Changchun, China

  3. <b>PhD,</b> [phduniv]Changchun Institute of Optics, Fine Mechanism and Physics, Chinese Academy of Science[/phduniv], Changchun, China[/education]


[introduction]Position

  1. <b>2005-now</b>, [position]Research Assistant Professor[/position], The [affiliation]Chinese University of Hong Kong[/affiliation][/introduction]


Honour and Award

  1. <b>2002</b>, President Excellent Award of Chinese Academy of Sciences


Research Interests

  1. [interests]Physical Properties of One-dimensional Wide-band Gap Semiconductor nanostructures[/interests]

  2. [interests]Nanodevice Fabrication[/interests]

  3. [interests]Photoluminescence Properties of Rare-earth in One-dimensional Wide-band Gap Materials as a host[/interests]


[publication]Selected Publications

  1. <b>X. T. Zhang¡AZ. Liu, Q. Li, Y. P. Leung, K. M. Ip, and S. K. Hark</b>
    "Routes to grow well-aligned arrays of ZnSe nanowires and nanorods", Advanced Materials, 17, 1045 (2005)

  2. <b>X. T. Zhang, Z. Liu, Quan Li, and S. K. Hark</b>
    "Growth and luminescence of ternary semiconductor ZnCdSe nanowires by metalorganic chemical vapor deposition", J. Phys. Chem. B, 109, 17913 (2005)

  3. <b>X. T. Zhang, K. M. Ip, Quan Li and S.K. Hark</b>
    "Photoluminescence of Ag-doped ZnSe nanowires synthesized by metalorganic chemical vapor deposition", Appl. Phys. Lett. 86, 203114 (2005)

  4. <b>X. T. Zhang, K. M. Ip, Z. Liu, Y. P. Leung, Quan Li, and S. K. Hark</b>
    "Structure and photoluminescence of ZnSe nanoribbons grown by metal-organic chemical vapor deposition", Appl. Phys. Lett., 84, 2641-3 (2004)

  5. <b>X. T. Zhang, Z. Liu, K. M. Ip, Y. P. Leung, Quan Li, and S. K. Hark</b>
    "Luminescence of ZnSe nanowires grown by metalorganic vapor phase deposition", J. Appl. Phys., 95, 5752-5 (2004)

  6. <b>X. T. Zhang, Z. Liu, Y. P. Leung, Quan Li, and S. K. Hark</b>
    "Growth and luminescence of zinc-blende-structured ZnSe nanowires by metal-organic chemical vapor deposition", Appl. Phys. Lett., 83, 5533-5 (2003)[/publication]

<IMAGE src="../Images/mail.gif" alt=""/> <b>

[contactinfo][address]Room G6, Science Centre North Block,
The Chinese University of Hong Kong, Shatin, Hong Kong[/address].

</b>

<IMAGE src="../Images/phone.gif" alt=""/> <b>

Phone: [phone](852) 2609 6315[/phone]
Fax: [fax](852) 2603 5204[/fax]

</b>

<b>

E-mail: [email]xtzhang@phy.cuhk.edu.hk[/email] or [email]xtzhangzhang@hotmail.com[/email][/contactinfo]

</b>

</b>
