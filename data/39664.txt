<font size=5>Nilo Stolte Home Page</font>


[education]<b><font size=2> Titles </font> </b>

PhD in [phdmajor]Computer Science[/phdmajor] (Computer Graphics)- [phduniv]IRIT, UPS[/phduniv], [phddate]1996[/phddate]

Master's Degree ([msmajor]Computer Graphics[/msmajor])- [msuniv]UFRGS[/msuniv] [msdate]1989[/msdate]

Bachelor's Degree in [bsmajor]Computer Science[/bsmajor] - [bsuniv]UFRGS[/bsuniv] [bsdate]1983[/bsdate][/education]

[resinterests]<b><font size=2> Research </font> </b>

[interests]Voxelisation and Visualisation of Implicit Surfaces[/interests]

[interests]Voxel Fractal Clouds[/interests]

[interests]Discrete Ray Tracing[/interests]

[interests]Octree[/interests]

Publications

<b><font size=2> Other Interests </font></b>

[interests]SVG: Vectorial Graphics on the Web[/interests] (recommended by Adobe)[/resinterests]

Progressive Music

[pic]<IMAGE src="myphoto.jpg" alt=""/>[/pic]


[contactinfo]<font size=1> [email]nilo.stolte@online.fr[/email] </font>[/contactinfo]



11 January 2007 last updated 2 August 2005

.

<IMAGE src="http://perso0.free.fr/cgi-bin/wwwcount.cgi?df=nilo.stolte.dat&dd=C" alt=""/> <IMAGE src="logofreeA_86x29.gif" alt="FREE: Libert? Fraternit? Egalit?/>



Nilo Martinho Jacoby Stolte, NILO MARTINHO JACOBY STOLTE, nilo martinho jacoby stolte, Graphics, GRAPHICS, graphics, Computer Graphics, COMPUTER GRAPHICS, computer graphics, Implicit Surfaces, IMPLICIT SURFACES, implicit surfaces, Voxel, Voxels, VOXEL, VOXELS, voxel, voxels, Voxelization, VOXELIZATION, voxelization, Rasterization, RASTERIZATION, rasterization, Voxelisation, VOXELISATION, voxelisation, Rasterisation, RASTERISATION, rasterisation Octree, OCTREE, octree, Oct-tree, Oct-Tree, OCT-TREE, oct-tree, Oc-tree, OC-TREE, oc-tree, Implicit Surface, IMPLICIT SURFACE, implicit surface, implicit, IMPLICIT, Surface, Surfaces, surface,surfaces, SURFACE, SURFACES, Ray Tracing, RAY TRACING, ray tracing, Ray-Tracing, RAY-TRACING, ray-tracing, Discrete Ray Tracing, DISCRETE RAY TRACING, discrete ray tracing, Discrete Ray-Tracing, DISCRETE RAY-TRACING, discrete ray-tracing, Ray Tracing Acceleration, RAY TRACING ACCELERATION, ray tracing acceleration, Ray-Tracing Acceleration, RAY-TRACING ACCELERATION, ray-tracing acceleration, Interval, Intervals, INTERVAL, INTERVALS, interval, intervals, Interval Arithmetic, INTERVAL ARITHMETIC, interval arithmetic, Interval Arithmetics, INTERVAL ARITHMETICS, interval arithmetics, Cloud, Clouds, CLOUD, CLOUDS, cloud, clouds, Fractal Clouds, FRACTAL CLOUDS, fractal clouds, Fractal Cloud, FRACTAL CLOUD, fractal cloud, Fractal, Fractals, FRACTAL, FRACTALS, fractal, fractals, Rendering, RENDERING, rendering, Rendering Algorithms, RENDERING ALGORITHMS, rendering algorithms, Point Rendering, POINT RENDERING, point rendering, Grid, Grids, GRID, GRIDS, grid, grids, Voxel Space, VOXEL SPACE, voxel space, Voxel Spaces, VOXEL SPACES, voxel spaces, High Resolution, High-Resolution, HIGH RESOLUTION, HIGH-RESOLUTION, high resolution, high-resolution, High Resolutions, High-Resolutions, HIGH RESOLUTIONS, HIGH-RESOLUTIONS, high resolutions, high-resolutions, 3d, 3D, Three-Dimensional, Three-Dimensional, THREE-DIMENSIONAL, THREE-DIMENSIONAL, three-dimensional, three-dimensional, Space subdivision, SPACE SUBDIVISION, space subdivision, OpenGL, OPENGL, opengl, SVG, vectorial graphics, XML, PostScript, Java, JavaScript, Display PostScript Parallel Processing, PARALLEL PROCESSING, parallel processing, Parallel Algorithms, PARALLEL ALGORITHMS, parallel algorithms, Implicit Site, Brazil, brazil, Brasil, brasil, Nilo Normilo Stolte, Leocadia Jacoby Stolte, Porto Alegre, RS, 1960, born in 1960, 10/01/60, 01/10/60, 10.01.60, 01.10.60, Dinarte Ribeiro, Vila dos Comerciários, Grupo Escolar Uruguay, Ginásio Presidente Costa e Silva, Escola Técnica Parob? Universidade Federal do Rio Grande do Sul, UFRGS, Curso de Tecnólogos em Processamento de Dados, Tecnólogo, Edisa, C&P Telephone Company, Baltimore, Chronos, Curso de Pósgraduação em Ciência da Computação, Pósgraduação em Ciência da Computação, CPGCC, PGCC, Mestrado, Mestre, Amatistas, Buenos Aires, Argentina, Doutorado, Doutor, Docteur, Doctor, Alemanha, Manheim, Germany, Deutchland, Françoise Cabau, Cabau, France, FRANCE, france, Toulouse, TOULOUSE, toulouse, Universit?Paul Sabatier, IRIT, Ren?Caubet, Caubet, Rene Caubet, Durham, Duke, Duke University New York, Stony Brook, State University of New York at Stony Brook, London, Kingston University, Oktal, Singapore, NTU, Nanyang Technological University, Progressive Rock, Art Rock, Progressive Music PROGRESSIVE ROCK, ART ROCK, PROGRESSIVE MUSIC progressive rock, art rock, progressive music, Amon Duul 2, Andreas Vollenweider, Anekdoten, Anglagard, Arzachel, Il Balletto Di Bronzo, Banco Del Mutuo Soccorso, Camel, Consorzioacquapotabile, CAP, Caravan, Cream, Dixie Dregs, Dream Theater, East of Eden, Egg, ELP, Emerson Lake & Palmer, Emerson Lake and Palmer, Focus, Genesis, Gentle Giant, Gong, Gracious, Guru Guru, Happy the Man, Iron Butterfly, Jeronimo, Jethro Tull, Kansas, King Crimson, The Essential King Crimson: Frame by Frame, Frame by Frame, Frame by Frame CD covers, The Great Deceiver Live 1973-1974, The Great Deceiver, The Great Deceiver CD covers, Lehmejum, Magma, Marillion, Mike Oldfield, Nektar, The Nice, Nucleus, Le Orme, Ozric Tentacles, PFM, Premiata Forneria Marconi, Pink Floyd, Denise Pink Floyd, Denise Meddle, Renaissance, Rush, Soft Machine, Spock's Beard, Sweet Smoke, Tangerine Dream, Triunvirat, UK, Uno, Weather Report, Web, Yes, Frank Zappa, Zappa, Nilo Stolte, Nilo stolte, Nilo STOLTE, nilo Stolte, nilo stolte, nilo STOLTE, NILO Stolte, NILO stolte, NILO STOLTE, Stolte Nilo, Stolte nilo, Stolte NILO, stolte Nilo, stolte nilo, stolte NILO, STOLTE Nilo, STOLTE nilo, STOLTE NILO, Nilo Martinho Stolte, Nilo Martinho stolte, Nilo Martinho STOLTE, Nilo martinho Stolte, Nilo martinho stolte, Nilo martinho STOLTE, Nilo MARTINHO Stolte, Nilo MARTINHO stolte, Nilo MARTINHO STOLTE, nilo Martinho Stolte, nilo Martinho stolte, nilo Martinho STOLTE, nilo martinho Stolte, nilo martinho stolte, nilo martinho STOLTE, nilo MARTINHO Stolte, nilo MARTINHO stolte, nilo MARTINHO STOLTE, NILO Martinho Stolte, NILO Martinho stolte, NILO Martinho STOLTE, NILO martinho Stolte, NILO martinho stolte, NILO martinho STOLTE, NILO MARTINHO Stolte, NILO MARTINHO stolte, NILO MARTINHO STOLTE,
