<b> <font size=6>Xue    Wu</font></b> <IMAGE src="pics/computer-science.gif" alt=""/> <IMAGE src="pics/umlogol.gif" alt=""/>

[introduction]I am a [position]graduate student[/position] at [affiliation]Computer Science Department, University of Maryland[/affiliation]. My advisor is Professor Chau-Wen Tseng. My research focuses on [interests]applying high performance computing techniques on bioinformatics[/interests].

Before I came to here, I was studying at Computer Science Department of Tsinghua University, Beijing, China.[/introduction]

[contactinfo]Contact Information:

<b>Office:</b> [address]4132 A.V. Williams Building
Computer Science Department
University of Maryland
College Park, MD 20742[/address]
<b>Phone:</b> [phone](301) 405-2722[/phone]
<b>Email:</b> <b>[email]wu at cs.umd.edu[/email]</b>
[<font size=2>Personal Homepage]</font>[/contactinfo]

[pic]<IMAGE src="pics/me.gif" alt=""/>[/pic]

[Research | Education | Experience | Resume]

[publication]<font size=4><b>Publications</b></font>

  * 

    X. Wu, N. Edwards, and C.-W. Tseng, Peptide Identification via Tandem Mass Spectroscopy, in Computational Biology and Bioinformatics, Advances in Computers, Volume 68 (C.-W. Tseng, ed.), Elsevier, 2006.

  * 

    X. Wu, N. Edwards, and C.-W. Tseng, Experimental Comparison of Peptide Identification Algorithms, Poster at 9th Annual Conference on Computational Genomics, Baltimore, MD, October 2006.

  * 

    N. Edwards, X. Wu and C.-W. Tseng, Novel Peptide Identification Using ESTs and Genomic Sequence, Poster at 2nd Annual Conference of the US Human Proteome Organisation (US HUPO 2006), Boston, MA, March 2006.

  * 

    Book Chapter: X. Wu and C.-W. Tseng, Searching Sequence Databases Using High Performance BLASTs , Parallel Computing for Bioinformatics and Computational Biology (A. Zomaya, ed.), John Wiley & Sons, 2005.

  * 

    X. Wu, W.-J. Lee, and C.-W. Tseng, ESTmapper: Efficiently Aligning DNA Sequences to Genomes, Fourth IEEE International Workshop on High Performance Computational Biology (HiCOMB 2005), Denver, CO, April 2005.

  * 

    K. Albayraktaroglu, A. Jaleel, X. Wu, B. Jacob, M. Franklin, C.-W. Tseng, and D. Yeung, BioBench: A Benchmark Suite of Bioinformatics Applications, IEEE International Symposium on Performance Analysis of Systems and Software (ISPASS), Austin, TX, March 2005.

  * 

    Z. Yu, C.-Y. Seng, X. Wu, T. Jiang and W. A. Arbaugh, Robust Routing in Malicious Environment for Ad Hoc Networks, to appear at The First Information Security Practice and Experience Conference (ISPEC 2005) , Singapore, April 2005.

  * 

    X. Wu, W.-J. Lee, D. Gupta, and C.-W. Tseng, ESTmapper: Efficiently Clustering EST Sequences Using Genome Maps, poster at RECOMB'04 (Extended version as University of Maryland Technical Report CS-TR-4575 )

  * 

    X. Wu and C.-W. Tseng, Searching Bioinformatic Sequence Databases using UM-BLAST---A Wrapper for High-Performance BLASTs, poster at RECOMB'04 (Extended version as University of Maryland Technical Report CS-TR-4576 )

  * 

    Z. Yu, T. Jiang, X. Wu, and W. A. Arbaugh, Risk Based Probabilistic Routing for Ad Hoc Networks, Peer reviewed poster session at Wireless Security Workshop in conjunction with Mobicom, September 2002. [pdf]

  * 

    X. Wang, R. M. M. Chen, X. Wu, and X. An, A Class of Parallel Algorithm for Solving Large Sparse Linear Systems on Multiprocessors, HPC Asia 2000.

<b><font size=4>Talks</font></b>

  * <font size=3>Investigation of High Throughput BLASTs, CBCB Journal Club, November 2003.</font>

  * ESTmapper: Efficiently Clustering EST Sequences Using Genome Maps, CBCB Journal Club, April 2004[/publication]

<font size=3> (Please find the related projects at my <b>Bioinformatics</b> page!)</font>

[education]<font size=4><b>Education</b></font>

      <font size=3>August, 2000 - present              PhD in [phdmajor]Computer Science[/phdmajor], [phduniv]University of Maryland[/phduniv] at College Park</font>

      <font size=3>September, 1995 - [bsdate]July 2000[/bsdate]    B.E in [bsmajor]Computer Science[/bsmajor], [bsuniv]Tsinghua University[/bsuniv]</font>[/education]

<font size=4><b>Teaching Experience</b></font>

      <font size=3>Fall 2006 Teaching Assistant, CMSC 330  Organization of Programming Languages</font>

      <font size=3>Fall 2005 Teaching Assistant, CMSC 351  Algorithms </font>

      Spring 2001 - Spring 2002<font size=3> Teaching Assistant, CMSC 251  Algorithms </font>

      <font size=3>Fall 2000 Teaching Assistant, CMSC 330  Programming Languages</font>

<font size=4><b>Working Experience</b></font>

      June, 2005 - August, 2005        Calibrant Biosystems

      June, 2001 - August, 2001        Research Assistant, University of South California - Information Sciences Institute (ISI)

      October, 1999 - July, 2000       Research Assistant, Chinese Academy of Science
