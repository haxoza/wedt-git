[pic]<IMAGE src="/images/profhuang.gif" alt=""/>[/pic]

[contactinfo]<b>Jau-hsiung Huang , [position]Professor[/position]</b>

      ¡@

      [affiliation]Communication and Multimedia Lab

      Department of Computer Science and Information Engineering,

      National Taiwan University[/affiliation]
      [address]Room 333, CSIE Building, #1 Roosevelt Rd. Sec. 4, Taipei, Taiwan 106, ROC[/address]

      ¡@

      Phone: [phone]886-2-[363-0231 ext 3232 or 362-5336] ext 333[/phone]
      Email: [email]jau@csie.ntu.edu.tw[/email][/contactinfo]

<b>¡@</b>
      ¡@

      [resinterests]Areas of research interest

      <IMAGE src="note.gif" alt=""/> [interests]Computer Network[/interests]

      <IMAGE src="note.gif" alt=""/> [interests]Queueing Theory[/interests]

      <IMAGE src="note.gif" alt=""/> [interests]Multimedia System[/interests]

      <IMAGE src="note.gif" alt=""/> [interests]Network Security[/interests]

      <IMAGE src="note.gif" alt=""/> [interests]Vedio Conference[/interests][/resinterests]

      ¡@

      Publications

      <IMAGE src="note.gif" alt=""/> Publication List

      ¡@

      Courses

      <IMAGE src="note.gif" alt=""/> Computer Network

      <IMAGE src="note.gif" alt=""/> Queueing Theory

      <IMAGE src="note.gif" alt=""/> Advanced Computer Network

      ¡@

      [education]Education

      <IMAGE src="note.gif" alt=""/> BS:[bsmajor]EE[/bsmajor] in [bsuniv]NTU[/bsuniv].1977~[bsdate]1981[/bsdate],

      <IMAGE src="note.gif" alt=""/> MS,PhD:[phdmajor]CS[/phdmajor] in [phduniv]UCLA[/phduniv].1983~[phddate]1988[/phddate][/education]

      ¡@

      Last Modified :
      newman@cmlab.csie.ntu.edu.tw
      by Newman
