<IMAGE src="/hp/images/culogo.gif" alt="Cornell University Insignia"/><IMAGE src="/hp/images/nysaes.gif" alt="Cornell University New York State Agricultural Experiment Station"/>

Home / Departments, Labs & Units / Plant Pathology / Faculty / Fuchs

<b><IMAGE src="/hp/graphics/IndexImages/Search.gif" alt=""/></b>

<IMAGE src="/hp/graphics/IndexImages/Home.gif" alt=""/><IMAGE src="/hp/graphics/IndexImages/About.gif" alt=""/><IMAGE src="/hp/graphics/IndexImages/Academics.gif" alt=""/><IMAGE src="/hp/graphics/IndexImages/Comm&Diag.gif" alt=""/><IMAGE src="/hp/graphics/IndexImages/Dept.gif" alt=""/><IMAGE src="/hp/graphics/IndexImages/News.gif" alt=""/><IMAGE src="/hp/graphics/IndexImages/Outreach.gif" alt=""/><IMAGE src="/hp/graphics/IndexImages/Publications.gif" alt=""/><IMAGE src="/hp/graphics/IndexImages/StationLife.gif" alt=""/>

[pic]<IMAGE src="/pp/photos/04fuchs.jpg" alt="Fuchs Photograph"/>[/pic]

[contactinfo]Marc Fuchs
[position]Assistant Professor[/position]

Address, Phone, Fax

E-Mail: [email]mf13@cornell.edu[/email][/contactinfo]

[education]<b>[phddegree]Ph.D.[/phddegree]</b> [phddate]1989[/phddate] [phdmajor]Molecular Biology[/phdmajor], [phduniv]Universit?Louis Pasteur[/phduniv], Strasbourg, France
<b>[msdegree]M.S.[/msdegree] </b> [msdate]1986[/msdate] [msmajor]Molecular Biology[/msmajor], [msuniv]Universit?Louis Pasteur[/msuniv], Strasbourg, France
<b>[bsdegree]B.S.[/bsdegree] </b> [bsdate]1981[/bsdate] [bsmajor]Life Sciences[/bsmajor], [bsuniv]Universit?Louis Pasteur[/bsuniv], Strasbourg, France[/education]

<b>Division of Effort</b>
Research - 70%, Virus diseases of vegetable and fruit crops.

Extension - 30%, Management of virus diseases of fruit and vegetable crops.

Complete CV ?html | pdf

   Program Overview

Viruses can cause severe crop losses, reduce significantly the quality of products, and shorten the longevity of plants.  My program emphasizes research and extension on the biology and control of virus diseases of vegetable and fruit crops.  A primary research goal is to understand how viruses cause diseases by studying the molecular and genetic basis of virus-host and virus-vector interactions.  Increasing our knowledge of the mechanisms of virus infection will facilitate the design of more effective and environmentally friendly control strategies.  The ultimate goal of my research program is to develop control measures for virus diseases of vegetable and fruit crops through improved diagnostic methods, cultural practices, traditional breeding, and genetic engineering.  A related objective is to address environmental safety issues over virus-resistant transgenic plants in order to lessen potential risks while preserving the efficacy of this control strategy.  The goals of the extension component of my program include the identification of emerging virus diseases, the education of extension personnel and growers, and the implementation of management procedures of virus diseases of vegetable and fruit crops.

   Links to Recent and Current Projects

Leafroll Disease/Mealybug Study Announcement

   [introduction]Professional Experience

2004-present [position]Assistant Professor[/position],<b></b>[affiliation]Department of Plant Pathology, Cornell University, New York State Agricultural Experiment Station[/affiliation], Geneva, NY.

2000-2004 Charg?de Recherche, Unit?Mixte de Recherche Vigne et Vins d'Alsace, INRA- Universit?Louis Pasteur, Laboratoire de Virologie, Colmar, France.

1998-2000 Research Support Specialist, Unit?de Recherche Vigne et Vin, Laboratoire de Pathologie Végétale, INRA, Colmar, France.

1991-1997 Research Associate, Department of Plant Pathology, Cornell University, New York State Agricultural Experiment Station, Geneva, NY.

1986-1989 Graduate Research Assistant, Universit?Louis Pasteur, Département de Virologie, Institut de Biologie Moléculaire des Plantes, Strasbourg, France.

1983-1991 Research Support Specialist, Station de Recherches Vigne et Vin, Département de Pathologie Végétale, INRA, Colmar, France.[/introduction]

   Professional Activities

<b>Professional Societies
</b>American Phytopathology Society
International Council for the Study of Virus and Virus-like Diseases of the Grapevine
French Phytopathology Society

<b>Current and Former Graduate Students</b>

2006 Jonathan Oliver, PhD student, Department of Plant Pathology

2005 Clémentine Durantet, PhD student, Cell Signal and Plant Biotechnology, Institut Fédératif de Recherche, Toulouse, France.

[contactinfo][affiliation]New York State Agricultural Experiment Station[/affiliation], [address]630 West North Street, Geneva, New York 14456[/address]
Telephone: [phone]315.787.2011[/phone][/contactinfo]

<IMAGE src="/hp/images/helix.gif" alt="apple helix"/>

Last Modified: September 7, 2006
Comments to: webfeedback
