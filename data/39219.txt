[pic]<IMAGE src="eu.jpg" alt=""/>[/pic]



[introduction]Guilherme Dias da Fonseca

I'm a [position]Computer Science PhD candidate[/position] at [affiliation]University of Maryland[/affiliation], College Park. I started my studies in 2003, and advanced to candidacy in 2006. My advisor is David Mount. The topic of my proposal is [interests]approximate range searching in the absolute error model[/interests].

In [msdate]2002[/msdate], I obtained my Master degree in the department COPPE - Sistemas at [msuniv]UFRJ[/msuniv], Rio de Janeiro, Brazil. I'm specially interested in [interests]computational geometry[/interests], but I like anything related to [interests]algorithms (data structures[/interests], [interests]randomization[/interests], [interests]graphs[/interests], [interests]bioinformatics[/interests], [interests]complexity theory[/interests]...).[/introduction]

[contactinfo]<IMAGE src="mail.png" alt=""/> [email]email: fonseca@cs.umd.edu[/email][/contactinfo]


[publication]Publications

  *  "<b>Approximate Range Searching: The Absolute Model</b>"; Guilherme Fonseca, and David M. Mount; submitted to SoCG'07.
    [<IMAGE src="pdf.png" alt=""/> PDF file] [<IMAGE src="ps.png" alt=""/> PostScript file] [<IMAGE src="preview.png" alt=""/> QUICK ABSTRACT] <IMAGE src="ARS-SoCG.png" alt=""/>

  *  "<b>Algorithms for the homogeneous set sandwich problem</b>"; Celina Figueiredo, Guilherme Fonseca, Vinícius Gusmão, and Jeremy Spinrad; Algorithmica vol 46/2 pp 149-180, 2006.
    [ DOI ] [<IMAGE src="pdf.png" alt=""/> PDF file] [<IMAGE src="ps.png" alt=""/> PostScript file] [<IMAGE src="preview.png" alt=""/> QUICK ABSTRACT] <IMAGE src="hssp-algorithmica.png" alt=""/>

  *  "<b>Faster deterministic and randomized algorithms on the Homogeneous Set Sandwich Problem</b>"; Celina Figueiredo, Guilherme Fonseca, Vinícius Gusmão and Jeremy Spinrad; in III Workshop on Efficient and Experimental Algorithms (WEA2004), Lecture Notes in Computer Science vol 3058 pp 243 - 252, 2004.
    [ DOI ] [<IMAGE src="pdf.png" alt=""/> PDF file] [<IMAGE src="ps.png" alt=""/> PostScript file] [<IMAGE src="preview.png" alt=""/> QUICK ABSTRACT] <IMAGE src="hssp-mcd.png" alt=""/>

  *  "<b>Kinetic hanger</b>"; Guilherme Fonseca, Celina Figueiredo and Paulo Cezar Carvalho; Information Processing Letters vol 89/3 pp 105 - 157, 2004.
    [ DOI ] [<IMAGE src="pdf.png" alt=""/> PDF file] [<IMAGE src="ps.png" alt=""/> PostScript file] [<IMAGE src="preview.png" alt=""/> QUICK ABSTRACT] <IMAGE src="hanger.png" alt=""/>

  *  "<b>Kinetic heap-ordered trees: tight analysis and faster algorithms</b>"; Guilherme Fonseca and Celina Figueiredo; Information Processing Letters vol 85/3 pp 165 - 169, 2002.
    [ DOI ] [<IMAGE src="pdf.png" alt=""/> PDF file] [<IMAGE src="ps.png" alt=""/> PostScript file] [<IMAGE src="preview.png" alt=""/> QUICK ABSTRACT] <IMAGE src="kh.png" alt=""/>

  *  "<b>The stable marriage problem with restricted pairs</b>"; Vânia Dias, Guilherme Fonseca, Celina Figueiredo and Jayme Szwarcfiter; Theoretical Computer Science vol 306 pp 391 - 405, 2003.
    [ DOI ] [<IMAGE src="ps.png" alt=""/> PostScript file] [<IMAGE src="preview.png" alt=""/> QUICK ABSTRACT] <IMAGE src="smtcs.png" alt=""/>

  *  "<b>Stable marriages with restricted pairs (conference version)</b>"; Vânia Dias, Guilherme Fonseca, Celina Figueiredo and Jayme Szwarcfiter; in Brazilian Symposium on Graphs, Algorithms and Combinatorics, Electronic Notes in Discrete Mathematics vol 7, 2001.

  [ DOI ]


Other Documents

  * <b>Approximate Range Searching in the Absolute Error Model</b>. Slide presentation for my preliminary exam.
    [OpenDocument Presentation] [<IMAGE src="pdf.png" alt=""/> PDF file]

  * <b>Approximate Range Searching in the Absolute Error Model</b>. Proposal for my preliminary exam.
    [<IMAGE src="pdf.png" alt=""/> PDF file]


Documents in Portuguese

  * <b>Kinetic Priority Queues</b>. My Master thesis.
    [<IMAGE src="pdf.png" alt=""/> PDF file] [<IMAGE src="ps.png" alt=""/> PostScript file]

  * <b>Stable marriages with forbidden pairs</b>. Undergraduate final project (ComputerScience - UFRJ).
    [Word DOC file]

  * <b>Kinetic priority queues</b>. Master thesis presentation.
    [OpenOffice Impress file][/publication]

<IMAGE src="Linux.gif" alt="Linux"/> <IMAGE src="firefox.gif" alt="Firefox"/> <IMAGE src="kde.png" alt="KDE"/> <IMAGE src="gimp.gif" alt="Gimp"/> <IMAGE src="openoffice.png" alt="OpenOffice"/>
