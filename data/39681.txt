<IMAGE src="/GIF/drapeau_gb.gif" alt="English version"/> <IMAGE src="/images/logo-lsv-gauche.gif" alt="LSV"/>

<IMAGE src="/images/logo-lsv-droit.gif" alt="LSV"/>

<IMAGE src="/images/logo-ens.gif" alt="ENS de Cachan"/>

<IMAGE src="/images/logo-cnrs.gif" alt="CNRS"/>




Menu principal LSV

  * <b>Présentation</b>

  * <b>Axes de recherche</b>

  * <b>Membres</b>

  * <b>Séminaire</b>

  * <b>Offres de stages</b>

  * <b>Publications</b>

  * <b>Logiciels</b>

  * <b>Services</b>

  * <b>Accès</b>


  * <b>Département
    Informatique</b>



<font size=5>Nathalie BERTRAND</font>
<font size=4>Titulaire d'une Allocation Couplée</font>
<font size=4>Etudiante en thèse</font>

[contactinfo][affiliation]Laboratoire Spécification et Vérification[/affiliation]
[address]CNRS UMR 8643
École Normale Supérieure de Cachan
61, avenue du Président Wilson
94235 CACHAN Cedex - France[/address]

<b>Tél.</b> : [phone]+33 (0)1 47 40 75 40[/phone]

<b>Fax</b> : [fax]+33 (0)1 47 40 75 21[/fax]

<b>Secr.</b> : [phone]+33 (0)1 47 40 75 20[/phone]

<b>Mél</b> : [email]bertrand@lsv.ens-cachan.fr[/email]

<b>Bureau</b> : [address]RH-B-103[/address][/contactinfo]

[pic]<IMAGE src="nat_sf_200300.JPG" alt="Nathalie BERTRAND"/>[/pic]

  * Quelques publications

Je suis maintenant en post-doc ?Dresde. Voici mes nouvelles coordonnées.

  * Ma soutenance de thèse : le 6 octobre 2006 ?14h

  * ACI Persée

      *  Un peu de biblio

  *  Monitorat :

      mes enseignements en 2004-2005

      et cette année 2005-2006

<IMAGE src="/images/rond_BD.png" alt=""/>

Page maintenue par Nathalie BERTRAND.  Dernières modifications : le 23 janvier 2006
