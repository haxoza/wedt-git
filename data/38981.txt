[ research interests | projects | teaching | recent papers | publications | bio sketch | awards ] [ conferences | projects @ Bell Labs | internet privacy & anti spam | favorite music ]

[pic]<IMAGE src="yossi.JPG" alt=""/>[/pic]



[contactinfo]<font size=6> [position]Prof.[/position] Yossi Matias </font>

<font size=4> </font>

[affiliation]School of Computer Science

Tel Aviv University[/affiliation]

[email]matias+www@cs.tau.ac.il[/email]

Office: [address]Schreiber building, rm 319[/address][/contactinfo]

Currently on leave


Biographical information


The ACM-SIGACT / EATCS 2005 <b>Gödel Prize</b>


Publications


Patents


[resinterests]Research Interests

  * <b>[interests]Massive data sets - data synopses and data streams[/interests]:</b> [interests]algorithms[/interests], [interests]synopsis data structures[/interests], [interests]streaming algorithms[/interests], [interests]approximate query processing[/interests], [interests]data mining[/interests] [recent papers].

  * <b>[interests]Parallel computation[/interests]:</b> models of computation, fundamental algorithms, scheduling, load balancing, parallel systems [recent papers].

  * <b>[interests]Data compression[/interests], [interests]data structures and algorithms[/interests]:</b> [interests]data compression[/interests], [interests]hash tables[/interests], (approximate) [interests]priority queues[/interests] [recent papers].

  * <b>[interests]Internet privacy and spam control[/interests]:</b> [interests]personalization[/interests], [interests]privacy[/interests], [interests]spam control[/interests] [recent papers].[/resinterests]


Publications resources

  * Recent Papers

  * Publication list

  * Citeseer

  * Coauthors (from DBLP)

  * ACM DL

  * Google Scholar


Projects at TAU InfoLab The Informatics Laboratory (InfoLab) encapsulates projects dealing with various aspects of information processing. Most notably, my InfoLab projects deal with effective processing of massive data sets. They include the following:

  * [interests]Leonardo: sketches and synopses[/interests]

  * [interests]τ-synopses: a system for run-time management of remote synopses[/interests]

  * [interests]Profiler: on-the-fly run-time code profiling[/interests]

  * [interests]Memento: program reversals based on list traversal synopses[/interests]

  * [interests]IPCompress: effective compression in packet networks[/interests]

  * [interests]Sensible Sensing: adaptive probing and communication in sensor networks[/interests]

    <b>Hosted Projects</b>

  * [interests]Planet Lab @ TAU[/interests]

  * [interests]IAG - Israel Academic Grid[/interests]


[resactivity]Program Committees

  * SIGMOD 2006 - The 2006 ACM SIGMOD International Conference on Management of Data

  * SIGMOD 2006 (Industrial track) - The 2006 ACM SIGMOD International Conference on Management of Data - Industrial Track

  * KDD 2006 - The Twelth ACM SIGKDD International Conference on Knowledge Discovery and Data Mining

  * NGITS 2006 - The 6th workshop on Next Generation Information Technologies and Systems

  * KDDS 2005 - The Second International Workshop on Knowledge Discovery from Data Streams

  * FSTTCS 2005 - The 25th Foundations of Software Technology and Theoretical Computer Science Conference

  * KDD 2005 - The Eleventh ACM SIGKDD International Conference on Knowledge Discovery and Data Mining

  * WWW 2005 - The 14th International World Wide Web Conference

  * ICDT 2005 - Tenth international Conference on Database Theory

  * ICDE 2005 - Twenty First International Conference on Data Engineering

  * XSym 2004 - The Second XML Symposium

  * PODS 2004 - Twenty first ACM Symposium on Principles of Database Systems

  * JPDPS 2003 - JPDPS - Grid Computing

  * SIGMOD 2003 - The 2003 ACM SIGMOD International Conference on Management of Data

  * Previous conferences


Steering Committee

  * IAG - Israel Academic Grid
[/resactivity]

Other activities

  * Internet privacy and spam control


Projects @ Bell Labs (old)

  * LPWA: The Lucent Personalized Web Assistant - internet privacy and spam control

  * AQUA: Approximate Query Answering

  * DIGEST: Database Incorporating Guaranteed Estimation Techniques

  *  COMPAS: Computational Models for Parallel Systems


Courses & Seminars

  * Research seminar: advanced topics on
    algorithms for massive data sets - data synopses and streaming data (Spring 04)

  * Computer Structure (Fall 03)

  * Workshop on Data Synopses & Planet Lab (Fall 03)

  * Research seminar: advanced topics on
    algorithms for massive data sets - data synopses and streaming data (Fall 03) :

  * Data Structures (Spring 03)

  * Algorithms for Massive Data Sets (Spring 03)

  * Research seminar: advanced topics on
    algorithms for massive data sets - data synopses and streaming data (Spring 03)

  * Seminar: Efficient processing of Massive Data Sets (Spring 03)

  * Workshop on Data Synopses (Fall 02)

  * Research seminar: advanced topics on
    algorithms for massive data sets - data synopses and streaming data (Fall 02)

  * Seminar: Efficient processing of Massive Data Sets (Spring 02)

  * Data Structures (Spring 02)

  * Computer Structure (Spring

  * Workshop in Computer Science (Spring 00)

  * Data Structures (Fall 99/00)

  * Seminar: Data representation and retrieval (Fall 99/00)

  * Parallel Algorithms (Spring 99)

  * Workshop in Computer Science (Spring 99)

  * Seminar: Data representation and retrieval (Fall 98/99)

  * Data Structures (Fall 98/99)

  * Data Structures (Fall 97/98)

  * Seminar: Data structures and algorithms for massive data sets (Spring 98)


<b> Favorite music </b>

[contactinfo]<IMAGE src="http://citeseer.ist.psu.edu/icon/cssmall.gif" alt=""/> Search:

[email]matias+www@math.tau.ac.il[/email][/contactinfo]
April 1999
