<IMAGE src="/dai-arc/images/poli.gif" alt="Politecnico di Torino"/> <font size=5>Gruppo Architetture e Reti di Calcolatori </font>

<IMAGE src="/dai-arc/images/italy.gif" alt="Versione italiana"/><font size=2>Versione italiana</font>

<font size=6>Paolo Montuschi</font>


[contactinfo]<font size=4>[position]Professore Ordinario[/position]</font>

Tel.: [phone]+39 011 564 7014[/phone]

Fax: [fax]+39 011 564 7099[/fax]

E-mail: [email]montuschi@polito.it[/email]

[affiliation]Politecnico di Torino
Dipartimento di Automatica e Informatica[/affiliation]
[address]Corso Duca degli Abruzzi 24
10129 Torino - Italy[/address][/contactinfo]

<IMAGE src="/dai-arc/manual/people/montuschi/paoloM.jpg" alt=""/>

<IMAGE src="/dai-arc/images/ball.gif" alt=""/> <font size=4>Curriculum Vitae</font>

<IMAGE src="/dai-arc/images/ball.gif" alt=""/> <font size=4>Pubblicazioni</font>

[pic]<IMAGE src="/dai-arc/images/homepoli.gif" alt="Gruppo Architetture e Reti di Calcolatori"/>[/pic]
