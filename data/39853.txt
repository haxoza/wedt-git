[pic]<IMAGE src="rachid2.jpg" alt=""/>[/pic] <IMAGE src="Book-reliable_distributed_programming.jpg" alt=""/> <IMAGE src="Book-les_systemes_transactionnels.jpg" alt=""/>

Best prices Amazon Lavoisier



[introduction][position]Prof[/position] Rachid GUERRAOUI
[affiliation]<b><font size=5> School of Computer and Communication Sciences </font></b><b><font size=5>(LPD)</font></b><b><font size=5>, EPFL[/affiliation]

</font></b><b><font size=5></font></b>



<font size=5>Past</font>: Prior to becoming professor at EPFL, I worked at the centre de recherche de l'Ecole des Mines de Paris, the Commissariat ?l'Energie Atomique in Saclay, and at HP Labs in Palo Alto. More recently, I was visiting professor at MIT - Theory of Distributed Computing Group - from July 05 to July 06.
<font size=5>Research</font>: I worked first on distributed programming languages. The [interests]design and implementation of a distributed version of C++[/interests]  helped me get a PhD (1992) from the [phduniv]univ of Orsay[/phduniv].  Some later work on transactions helped me become a professor through a "habilitation" (1996) from the univ of Grenoble.  I currently work on [interests]distributed algorithms and  programming languages[/interests].[/introduction]
<font size=5>Papers and Software:</font> Here are some of my research papers. The full list is sorted by year: 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005. 2006. I was also involved in various software projects: [interests]DACE[/interests]<b><font size=4> </font></b><font size=4>(2000,2003), </font><font size=4>[interests]OGS[/interests] (1995-1998),  BAST(1995-1998)</font><font size=4> [interests]GARF[/interests] (1992-1995), </font><font size=4>and [interests]KAROS[/interests] (1989-1992)</font> .
<b><font size=5>Teaching</font>: </b>Distributed Algorithms and Systems;<font size=4> </font>Object Oriented Programming.<b><font size=5>
</font><font size=4></font></b>

  *  <font size=4>Interested in a position within LPD? Check out: </font>

    * <font size=4>These pictures of the LPD surf seminar</font>

    * <font size=4>The research activities of LPD members</font>

    * <font size=4>The doctoral program offered by our school </font>

  *  <font size=4>Curious about Rachid's best contributions? picture 1, picture 2 </font>


[contactinfo][address]<b>LPD (Station 14), I&C, EPFL CH 1015 Lausanne, Switzerland - Office INR 310[/address] - TÈl [phone]+41 21 693 5272[/phone] - Fax [fax]+41 21 693 75 70[/fax]</b>

<IMAGE src="mailbox.gif" alt=""/> <b>E-Mail: [email]firstName.lastName@epfl.ch[/email]</b>[/contactinfo]


``God does not often clap his hands; When he does, everybody should dance'' African proverb
