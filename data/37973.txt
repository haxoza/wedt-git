<IMAGE src="hampbar.gif" alt=""/>


[pic]<IMAGE src="lee.gif" alt=""/>[/pic]



Lee Spector

[contactinfo][affiliation]<font size=2>Cognitive Science</font><font size=2>
</font><font size=2>Hampshire College</font>[/affiliation]<font size=2>
[address]Amherst, MA 01002 USA[/address]
Phone: [phone](413) 559-5352[/phone], Fax: [fax](413) 559-5438[/fax]
E-mail: </font><font size=2>[email]lspector@hampshire.edu[/email]</font>[/contactinfo]

<font size=2>Teaching</font>

<font size=2>Publications</font>

<font size=2>Demos, Presentations,
and Projects</font>

<font size=2>Code</font>

<font size=2>Grants</font><font size=2>

Resume (</font><font size=2>PDF</font>)

<font size=2>Family</font><font size=2>

</font><font size=2>Audio</font>

[introduction]<font size=2>Artwork</font> <font size=2>Lee Spector is a [position]Professor[/position] of Computer Science in the [affiliation]School of </font><font size=2>Cognitive Science</font><font size=2> at </font><font size=2>Hampshire College</font>[/affiliation]<font size=2>. He received a [bsdegree]B.A.[/bsdegree] in [bsmajor]Philosophy[/bsmajor] from </font><font size=2>[bsuniv]Oberlin College[/bsuniv]</font><font size=2> in [bsdate]1984[/bsdate], and a [phddegree]Ph.D.[/phddegree] from the </font><font size=2>Department of [phdmajor]Computer Science[/phdmajor]</font><font size=2> at the </font><font size=2>[phduniv]University of Maryland[/phduniv]</font><font size=2> in [phddate]1992[/phddate]. At Hampshire he has held the MacArthur Chair, served as the elected faculty member of the Board of Trustees, and served as the Dean of the School of Cognitive Science. He supervises the Hampshire College </font><font size=2>Cluster Computing Facility</font><font size=2>.

Dr. Spector recently received the highest honor bestowed by the National Science Foundation for excellence in both teaching and research, the NSF Director's Award for Distinguished Teaching Scholars (press releases: </font><font size=2>NSF</font><font size=2>, </font><font size=2>Hampshire</font><font size=2>). He has won several other awards and honors, including a gold medal in the Human Competitive Results contest of the Genetic and Evolutionary Computation Conference (</font><font size=2>more information</font><font size=2>) and election as a fellow of the International Society for Genetic and Evolutionary Computation (</font><font size=2>press release</font><font size=2>).

Dr. Spector teaches and conducts research in [interests]computer science[/interests], [interests]artificial intelligence[/interests], and [interests]artificial life[/interests]. His areas of interest include [interests]genetic and evolutionary computation[/interests], [interests]quantum computation[/interests], [interests]planning in dynamic environments[/interests], [interests]artificial intelligence education[/interests], [interests]artificial intelligence and neuropsychology[/interests], and [interests]artificial intelligence in the arts[/interests].

Dr. Spector has produced over 50 professional scientific </font><font size=2>publications</font><font size=2>, including his recent book: </font><font size=2>Automatic Quantum Computer Programming: A Genetic Programming Approach</font><font size=2>, published by Kluwer Academic Publishers in 2004. He was Editor-in-Chief for the Proceedings of the </font><font size=2>2001 GECCO (Genetic and Evolutionary Computation) conference</font><font size=2> and has served as track chair, organizer, or reviewer for several other conferences. He was lead editor for Advances in Genetic Programming, Volume 3, published by MIT Press (</font><font size=2>MIT Press page</font><font size=2>, </font><font size=2>list of chapters and text from the Introduction</font><font size=2>). He also writes for general audiences, including a recent OpEd piece in The Boston Globe (</font><font size=2>The Globe's page</font><font size=2>, </font><font size=2>local copy</font><font size=2>).

Dr. Spector is a [position]member of the Executive Board[/position] of the </font><font size=2>[affiliation]International Society for Genetic and Evolutionary Computation[/affiliation]</font><font size=2>, an [position]Associate Editor[/position] for the journal </font><font size=2>[affiliation]Genetic Programming and Evolvable Machines[/affiliation]</font><font size=2>, and a [position]member of the editorial board[/position] of the journal </font><font size=2>[affiliation]Evolutionary Computation[/affiliation]</font><font size=2>.</font>[/introduction]
