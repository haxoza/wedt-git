[pic]<IMAGE src="me3.jpg" alt="@ Acadia National Park, ME, August 2002"/>[/pic] <b><font size=5>Yi Feng (Eric)</font></b>
<b><font size=3>(resume)</font></b>

[contactinfo]<font size=3>[position]Graduate Student & Research Assistant[/position]</font>
[affiliation]<font size=3>PLASMA Group</font>
<font size=3>Department of Computer Science</font>
<font size=3>University of Massachusetts Amherst</font>[/affiliation]

<b><font size=4>Contact</font></b>
I'm now working at [affiliation]Google Enterprise[/affiliation] and you can send email to [email]my_usual_username@google.com[/email].[/contactinfo]

[resinterests]<b><font size=4>Research Interests</font></b>

I am interested in [interests]software systems[/interests] related areas, with particular interests in [interests]memory management[/interests] and [interests]data optimization[/interests]. I study operating systems, runtime systems, programming languages and focus on the cooperation between these components, with a goal of improving the performance, scalability and robustness of software systems. My advisor is Prof. Emery Berger.[/resinterests]

<b><font size=4>Projects</font></b>

  Hierarchical Hashing

  Cooperative Memory Management (PhD synthesis project)

  Scheduler Aware Virtual Memory Management (SAVMM) (led by Emery Berger, Scott Kaplan and Prashant Shenoy)

  Cooperative Robust Automatic Memory Management (CRAMM) (led by Emery Berger, Eliot Moss and Scott Kaplan)

[publication]<b><font size=4>Publications</font></b>

  <b>A Locality-Improving Dynamic Memory Allocator</b> (pdf)
  Yi Feng and Emery Berger
  3rd Annual ACM SIGPLAN Workshop on Memory Systems Performance (MSP 2005)

  <b>Garbage Collection without Paging</b> (pdf)
  Matthew Hertz, Yi Feng and Emery Berger
  ACM SIGPLAN 2005 Conference on Programming Language Design and Implementation (PLDI 2005)[/publication]

<b><font size=4>Software</font></b>

  <b>Vam</b>
  I developed this dynamic memory allocator which transparently improves the application's data locality in both the cache level and the virtual memory level. It is a replacement for the widely used malloc/free functions in the C library and improves application performance over the state-of-the-art allocators with varied memory sizes. This memory manager can additionally communicate with the (VMSig-patched) Linux kernel under memory pressure and reduce the performance degradation caused by paging. Vam is available as part of the heaplayers package.

  <b>VMSig</b>
  I developed this virtual memory communication patch for the Linux kernel. It allows the user applications to register and receive signals from the OS kernel on VM paging events. With the extended functionality of the madvise system call, the user applications can help improve the efficiency of VM management in the kernel. It also fixes the problem of the mincore system call in the stock kernel where mincore does not work with anonymous pages. This patch was originally developed for Linux 2.4 (download the kernel patch for Linux 2.4.24) and later ported to Linux 2.6 (download the kernel patch for Linux 2.6.14 and the header file for the new system interface).

<b><font size=4>Courses</font></b>

  Current (Fall 2005)

    CmpSci 691B: Hot Topics in Programming Languages and Systems

  Previous

    CmpSci 691S: Topics in Runtime Systems (Fall 2004)
    CmpSci 677: Distributed Operating Systems (Spring 2004)
    CmpSci 701: Advanced Computer Science Topics (Fall 2003)
    CmpSci 635: Modern Computer Architecture (Spring 2003)
    CmpSci 710: Advanced Compiler Techniques (Spring 2003)
    CmpSci 673(691R): Performance Evaluation (Fall 2002)
    CmpSci 691P: Robust Software Systems (Fall 2002)
    CmpSci 601: Computation Theory (Spring 2002)
    CmpSci 620: Advanced Software Engineering: Synthesis and Development (Spring 2002)
    CmpSci 630(691F): Programming Languages (Spring 2002)
    CmpSci 653: Computer Networking (Fall 2001)
    CmpSci 683: Artificial Intelligence (Fall 2001)

  Previous (at Rutgers University)

    CS 505: Computer Structures (Spring 2001)
    CS 510: Numerical Analysis (Spring 2001)
    CS 513: Design and Analysis of Data Structures and Algorithms I (Fall 2000)
    CS 519: Operating System Theory (Fall 2000)

<font size=2>Yi Feng's Homepage / Email: my_usual_username@cs.umass.edu / Last updated in January 2006</font><IMAGE src="http://m1.nedstatbasic.net/n?id=ABzjbg+VDDgRScCWp16wt/E8xbww" alt="Nedstat Basic - Free web site statistics
Personal homepage website counter"/>
Free counter
