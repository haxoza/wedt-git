<IMAGE src="images2/header81.gif" alt="Department of Chemistry"/> <IMAGE src="images2/header8a1.jpg" alt="Images of the Department"/> <IMAGE src="images2/header8a2.jpg" alt="University of Illinois at Urbana-Champaign and Department of Chemistry"/>


<IMAGE src="faculty/gifs/tbut_overview.gif" alt="Research Overview"/><IMAGE src="faculty/gifs/bline.gif" alt="spacer line"/><IMAGE src="faculty/gifs/tgrouppage.gif" alt="Research Group"/><IMAGE src="faculty/gifs/bline.gif" alt="spacer line"/><IMAGE src="faculty/gifs/tbutinorg.gif" alt="Inorganic Chemistry"/><IMAGE src="faculty/gifs/bline.gif" alt="spacer line"/><IMAGE src="faculty/gifs/cbfaculty.gif" alt="Chemical Biology Faculty"/>


HOME


Graduate Program


Faculty
AlphabeticallyAnalytical FacultyInorganic FacultyOrganic FacultyPhysical Faculty Chemical Biology Faculty Materials Chemistry Faculty


Research AreasAnalytical ChemistryInorganic ChemistryOrganic ChemistryPhysical ChemistryChemical BiologyChemical PhysicsMaterials Chemistry

<IMAGE src="gifs2/scstext.gif" alt="School of Chemical Sciences"/>

[pic]<IMAGE src="faculty/gifs/yi-lu.jpg" alt="photo Yi Lu"/>[/pic]


[contactinfo]Contact Info[affiliation]Department of Chemistry
University of Illinois[/affiliation]
[address]A322 Chemical & Life Sciences Lab
600 South Mathews Avenue
Urbana, IL 61801[/address]
[phone](217) 333-2619[/phone]
[fax](217) 244-3186[/fax] Fax[/contactinfo] 
Email

<b>Lu Group Web Site</b>


Yi Lu
[introduction][position]Professor[/position] of Chemistry Yi Lu received his B.S. from [bsuniv]Beijing University[/bsuniv], P. R. China in [bsdate]1986[/bsdate] and his Ph.D. from [phduniv]University of California, Los Angeles[/phduniv] in [phddate]1992[/phddate]. After two years of postdoctoral research at California Institute of Technology, he joined the faculty at Illinois in 1994. Professor Yi Lu's research interests are in bioinorganic chemistry.[/introduction]


Honors and Awards

  *  Howard Hughes Medical Institute Professors Award, 2002

  *  University Scholar Award (UIUC), 2002

  * Award Runner-up for the Biosensors and Bioelectronics Award, 2002

  * University of Illinois SCS Excellence in Teaching Award, 2000

  * Camille Dreyfus Teacher-Scholar Award, 1999

  * National Science Foundation Special Creativity Award

  * Alfred P. Sloan Research Fellowship, 1998

  * Cottrell Scholars Award (Research Corporation), 1997

  * Beckman Young Investigator Award, 1996


[publication]Representative Publications

"Fast Colorimetric Sensing of Adenosine and Cocaine Based on a General Sensor Design Involving Aptamers and Nanoparticles," Juewen Liu and Yi Lu, Angew. Chem. Int. Ed, <b>45</b>, 90-94 (2006) [Full text] [News Link]

"Role of Heme Types in Heme-Copper Oxidases: Effects of Replacing a Heme b with a Heme o Mimic in an Engineered Heme-Copper Center in Myoglobin," N. Y. Wang, X. Zhao, and Y. Lu, J. Amer. Chem. Soc., <b>127</b>, 16541-16547 (2005). [Full Text]

"Blue Ferrocenium Azurin: An Organometalloprotein with Tunable Redox Properties," H. J. Hwang, J. R. Carey, E. T. Brower, A. J. Gengenbach, J. A. Abramite, and Y. Lu, J. Amer. Chem. Soc., <b>127</b>, 15356-15357 (2005). [Full Text]

"Stimuli-Responsive Disassembly of Nanoparticle Aggregates for Light-Up Colorimetric Sensing," J. Liu and Y. Lu, J. Amer. Chem. Soc., <b>127</b>, 12677-12683 (2005). [Full Text]

"Proofreading and Error Removal in a Nanomaterial Assembly," J. W. Liu, D. P. Wernette, and Y. Lu, Angew. Chem. Int. Ed., <b>44</b>, 7290-7293 (2005). [Full Text] [News Link] [Lu Lab Lab News]

"Axial Mehtionine Has Much Less Influence on Reduction Potentials in a CuA Center than in a Blue Copper Center," H. J. Hwang, S. M. Berry, M. J. Nilges and Y. Lu, J. Am. Chem. Soc., <b>127</b>, 7274-7275 (2005) [Full Text]

"Design and Engineering of Metalloproteins Containing Unnatural Amino Acids or Non-native Metal-containing Cofactors," Yi Lu, Curr. Opin. Chem. Biol, <b>9</b>, 118-126 (2005). [Full Text]

"Redox-Dependent Structural Changes in and Engineered Heme-Copper Center in Myoglobin: Insights into Chloride Binding to CuB in Heme Copper Oxidases," Xuan Zhao, Mark J. Nilges and Yi Lu, Biochemsitry <b> 44</b>, 6559-6564 (2005). [Full Text][/publication]


Most recent publications in PubMed

<IMAGE src="faculty/gifs/tbut_overview.gif" alt="Research Overview"/><IMAGE src="faculty/gifs/bline.gif" alt="spacer line"/><IMAGE src="faculty/gifs/tgrouppage.gif" alt="Research Group"/><IMAGE src="faculty/gifs/bline.gif" alt="spacer line"/><IMAGE src="faculty/gifs/tbutinorg.gif" alt="Inorganic Chemistry"/><IMAGE src="faculty/gifs/bline.gif" alt="spacer line"/><IMAGE src="faculty/gifs/cbfaculty.gif" alt="Chemical Biology Faculty"/>


Faculty <IMAGE src="images2/diamond.gif" alt="diamond"/>Graduate Program <IMAGE src="images2/diamond.gif" alt="diamond"/>Undergraduate Program
Courses<IMAGE src="images2/diamond.gif" alt="diamond"/>Events and Info<IMAGE src="images2/diamond.gif" alt="diamond"/>Department Info<IMAGE src="images2/diamond.gif" alt="diamond"/>Alumni

Send corrections or comments to
