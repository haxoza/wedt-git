<IMAGE src="images/menu-left.gif" alt=""/> <IMAGE src="images/links-left.gif" alt=""/>



Carlos Areces
<IMAGE src="images/drawinghands.gif" alt="[Drawing Hands]"/>
``Drawing Hands.'' M. C. Escher. 1948. (c) Cordon Art B.V.-Baarn-the Netherlands. All rights reserved.<IMAGE src="images/deco.gif" alt=""/><IMAGE src="images/top.gif" alt=""/>



Approaching Events

  * ESSLLI 2005 - The 17th European Summer School in Logic, Language and Information.

  *  ESSLLI 2006 - The 18th European Summer School in Logic, Language and Information. University of Málaga, Spain. 31 July - 11 August, 2006

<IMAGE src="images/deco.gif" alt=""/><IMAGE src="images/top.gif" alt=""/>

<IMAGE src="images/loria.gif" alt="[-LORIA-]"/>

[contactinfo][affiliation]INRIA Lorraine, Langue et Dialog (LED)[/affiliation],
[address]Langue et Dialogue, Batiment B
615, rue du Jardin Botanique
54600 Villers lès Nancy Cedex[/address]
Phone: [phone]+33 (0)3 54 95 84 90[/phone]
Fax: [fax]+33 (0)3 83 41 30 79[/fax]
mail: [email]firstname.lastname (at) loria.fr[/email]
[homepage]http://www.loria.fr/~areces[/homepage][/contactinfo]

<IMAGE src="images/deco.gif" alt=""/><IMAGE src="images/top.gif" alt=""/> <IMAGE src="images/xhtml10.png" alt="xhtml"/> <IMAGE src="images/powered.gif" alt="[Hosted by Loria]"/> <IMAGE src="images/css.png" alt="CSS"/>

?2004 Maintained by Carlos Areces. Last updated: 16-09-2005. [pic]<IMAGE src="images/carlos.jpg" alt="[ -Myself- ]"/>[/pic]Who I am Academic My Work
My Papers
My Talks

Things I am Involved with Langue et Dialogue
Hybrid Logics
FoLLI
The Student Session at ESSLLI
Methods for Modalities
GLyC
Grupo de Procesamiento de Lenguaje Natural - FaMAF

Past Events Conferences
Courses
