[pic]<IMAGE src="mor2.jpg" alt=" "/>[/pic]

      <b>


      [contactinfo]Mor Harchol-Balter, [position]Associate Professor[/position] </b>

      [affiliation]<b> Department of Computer Science</b>

      <b> Carnegie Mellon University</b>[/affiliation]

     [address] <b> Pittsburgh, PA 15213-3891</b>[/address]

      <b>Office: [address]Wean 8119</b>[/address]

      <b>Phone: [phone](412) 268-7893[/phone]</b>

      <b>Fax: [fax](412) 268-5576[/fax]</b>

      <b>Email: [email]harchol@cs.cmu.edu[/email]</b>[/contactinfo]

      <b>Exec. Assistant: Charlotte Yano </b>

<b>


SHORT BIO </b> <b>


CV (in postscript) </b> <b>


CV (in pdf) </b> <b>


RESEARCH PAPERS ONLINE </b>


[resinterests]<b> RESEARCH </b> I am interested in the [interests]performance analysis and design of computer systems[/interests], particularly [interests]distributed systems[/interests]. I work on finding [interests]analytical models[/interests] which capture the important characteristics of a computer system and allow me to redesign the system to improve its performance (response time).

I believe that many conventional wisdoms on which we base system designs are not well understood and sometimes false, leading to inferior designs. Many of our existing beliefs stem from queueing research in the 60's and 70's -- the great era for system performance modeling. Unfortunately, back then we did not have all the analytical and computational tools available today. Consequently, some questions were answered only under the approximation of Markovian workloads (exponentially distributed job sizes) which we now know to be non-representative of many real-world workloads (which show much greater variability and often heavy tails). Furthermore other questions could only be speculated upon.

My research revisits these very classic questions in system design, armed with today's new queueing and computational techniques, as well as a new perspective on real-world workloads, performance metrics, and implementation experience. I work on deriving new fundamental theorems in system design, many of which seem couterintuitive in light of age-old beliefs. I then incorporate these theorems into implementations of Web servers, database servers, and distributed server systems.

Here are a few examples of commonly-held beliefs that my research challenges:

  *  [interests]Thousands of "load balancing" heuristics do exactly that -- they aim to balance the load among the existing hosts. But is load balancing necessarily a good thing?[/interests]

  *  [interests]Ever notice that the oprimal scheduling policy, Shortest-Remaining-Processing-Time-First (SRPT), is rarely used in practice? There's a fear that the big jobs will "starve", or be treated unfiarly as compared with Processor-Sharing (PS). But is this a reality?
[/interests]
  *  [interests]To minimize mean waiting time in a server farm, research suggests that each job should be sent to the server at which it will experience the least wait. That seems good from the job's perspective, but is the greedy strategy best for the system as a whole?[/interests]

  *  [interests]Given a choice between a single machine with speed s, or n identical machines each with speed s/n, which would you choose?[/interests] Think again ...

  *  [interests]Migrating active jobs is generally considered too expensive. Killing jobs midway through execution and restarting them from scratch later is even worse! Says who?[/interests]

  *  [interests]Cycle stealing (using another machine's idle cycles to do your work) is a central theme in distributed systems and has been the topic of thousands of papers. But can we quantify when cycle stealing pays off, as a function of switching costs and threshold parameters?[/interests][/resinterests]

Some Research Projects:

  *  <b> SYNC -- Scheduling Your Network Connections (implementation and theory) </b>

  * <b> Towards optimal Scheduling: Classifying scheduling policies with respect to unfairness and performance (theory) </b>

  *  <b> Task Assignment and Cycle Stealing in Multiserver Systems, including New Dimensionality Reduction Technique for Markov Chains (theory and empirical) </b>

  * <b> QOS for Dynamic Web Requests by Scheduling the Database (implementation) </b>

  *  <b> Delay guarantees for Packet-Routing and Communication in Networks (theory -- OLDER) </b>

  *  <b> Measurements and Impact of Heavy-tailed Job Service Requirements (empirical -- OLDER) </b>

My theoretically-oriented students and some of our collaborators and I meet for a <b> SQUALL (Scheduling and QUeueing Around Lunchtime) </b> lunch most Tuesdays. The SQUALL page is maintained by Adam Wierman .


Current PhD STUDENTS <b> Varun Gupta </b> , <b> David McWherter </b> , <b> Adam Wierman </b> .


Graduated PhD STUDENTS <b> Takayuki Osogami </b> , Entered: 2001. Graduated 2005. Location: (IBM-TRL)

<b> Bianca Schroeder </b> , Entered: 1999. Graduated 2005. Location: CMU Postdoc with Garth Gibson.


[resactivity]PROFESSIONAL SERVICE

  *  CHAIR OF PROGRAM COMMITTEES:

      * <b> ACM Sigmetrics 2007 </b> .

      * <b> QEST 2007 </b> .

      * <b> First WORkshop on Multiserver Scheduling (WORMS04) <b> </b></b>

      * <b> CMU/Eindhoven Collaborative Workshop on Queueing (CMU/Eindhoven) <b> </b></b>

  *  PROGRAM COMMITTEES:

      * <b> Sigmetrics 2000 </b> , <b> Sigmetrics 2001 </b> , <b> Sigmetrics 2002 </b> , <b> Sigmetrics 2003 </b> , Sigmetrics 2004 , <b> Sigmetrics 2005 </b> , <b> Sigmetrics 2006 </b> .

      * <b> Performance 2002 </b> , <b> Performance 2005 </b> .

      * <b> WWW 2004 </b> , <b> WWW 2006 </b> .

      * <b> QEST 2006 </b> .

  *  TUTORIAL CHAIR:

      * <b> Sigmetrics 2001-Tutorials </b>

  * <b> Annual talk on Applying to Ph.D. Programs in Computer Science (Postscript)</b>[/resactivity] . Now also available in pdf


TEACHING

  * <b> THIS SEMESTER: (Fall 2006): 15-857A Performance Modeling (graduate) </b>

  * <b> (Spring 2000, Spring 2001, Fall 2002): 15-441 Computer Networks (undergraduate)</b>

  * <b> (Fall 1999, 2000, 2001, 2002, 2003, Spring 2005): 15-849 Performance Modeling (graduate) : NOW AVAILABLE ONLINE OUTSIDE CMU </b>

  * <b> (Fall 2004, Spring 2006): 15-359 Probability and Computing (undergraduate) </b>

<IMAGE src="downtownbig.jpg" alt=" "/>
