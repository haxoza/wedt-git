[pic]<IMAGE src="lchang.jpg" alt=""/>[/pic]
[contactinfo]LongWen Chang
Long-Wen Chang, [position]Professor[/position]
[affiliation]Department of Computer Science
National Tsing Hua University[/affiliation]
[address]Hsinchu, 300, Taiwan[/address]
FAX: [fax]886+3-5723694[/fax]
TEL: [phone]886+3-5731076[/phone]
email: [email]lchang@cs.nthu.edu.tw[/email][/contactinfo]
Research Areas
[interests]Video and Image Processing[/interests] Laboratory
 Personal Information
Teaching
