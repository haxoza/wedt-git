[pic]<IMAGE src="veronica.jpg" alt="photo"/>[/pic]


<b>Verónica Dahl</b>

[education]Logic Programming, Computational Linguistics, Deductive Knowledge Bases, Bioinformatics Computador Cientifico, Buenos Aires University, 1975
DEA en Intelligence Artificielle, Univ. Aix-Marseille II, 1976
[phddegree]Doctorat de Specialite[/phddegree] en [phdmajor]Intelligence Artificielle[/phdmajor], [phduniv]Univ. Aix-Marseille II[/phduniv], [phddate]1977[/phddate][/education]

[contactinfo]<b>email</b>: [email]veronica at cs.sfu.ca[/email]
<b>phone</b>: [phone]+1 604 291 3372[/phone]
<b>fax</b>: [fax]+1 604 291 3045[/fax] <IMAGE src="sfucrest-minor.gif" alt=""/>[/contactinfo]

[resactivity]<b>NEW: Don't miss ICLP'07! (see also </b><b>ICLP'06, </b><b>ICLP'05) </b>

  * <b>[position]Professor[/position] of Computing Science</b>, [affiliation]Simon Fraser University[/affiliation]

  * <b>[position]Associate Faculty[/position]</b> in Bioinformatics

  * <b>[position]Director[/position], [affiliation]Logic and Functional Programming Group[/affiliation]</b>

  * <b> [position]Founder[/position] of the [affiliation]Field of Logic Programming[/affiliation] (with 14 other international scientists)</b>

  * <b>Past President, Association for Logic Programming. (</b>See The Early Days of Logic Programming, <b>A Message from the former ALP President,</b> <b>A word with the President, part I, part 2,</b><b> A day in the life of a proof</b><b> (alternative lyrics for Antonio Carlos Jobim's song, ibidem); photo: </b>Addressing ICLP 2003, Mumbai-- with R.K. Shyamasundar and Catuscia Palamidessi))

  * <b>Co-Chair, ICLP'07
    </b>

  * <b>Co-Chair, PADL'03</b>

  * <b>Banquet Speaker, ICLP'02</b>

  * <b>Area Coordinator (Natural Language Processing), Compulog Americas</b>

  * <b>Coordinator of the SIG Computational Logic and Natural Language Processing (CL&NLP) (a joint initiative of Compulog Net and Compulog Americas)</b>

  * <b>Editorial Board Member: Theory and Practice of Logic Programming</b>

  * <b>Steering Committee Member</b>, CSS and Cognitive Sciences Pgm, SFU.[/resactivity]

<IMAGE src="red.gif" alt=""/> Current Research

<IMAGE src="red.gif" alt=""/> Recent Work 

<IMAGE src="red.gif" alt=""/> Publications

<IMAGE src="red.gif" alt=""/> Teaching

<IMAGE src="red.gif" alt=""/> Disclaimer/Advice for Students

<IMAGE src="blue.gif" alt=""/> Graduate Students

<IMAGE src="red.gif" alt=""/> Recent Funding

<IMAGE src="red.gif" alt=""/> Awards, Honours

<IMAGE src="blue.gif" alt=""/> CV

<IMAGE src="white.gif" alt=""/> Double Life of Veronica

<IMAGE src="white.gif" alt=""/> Miscellaneous

<IMAGE src="yellow.gif" alt=""/> Back to Faculty Home Page
