<IMAGE src="BD21315_.GIF" alt=""/>

[pic]<IMAGE src="capture.jpeg" alt=""/>[/pic]

<IMAGE src="BD21315_.GIF" alt=""/>

[contactinfo]<font size=4> [position]Professor[/position] of Computer Science and Electrical Engineering, and
[position]Chair[/position] of the [affiliation]Computer Science Department[/affiliation]
</font>

<font size=4> Rami Melhem</font>

<font size=4>[address]6137 Sennott Square[/address] </font>

<font size=4>Phone:([phone]412 624-8493[/phone])</font>

[email]X at cs dot pitt dot edu[/email] (where X = "melhem")[/contactinfo]

<IMAGE src="BD15155_.GIF" alt=""/>

[resinterests]<IMAGE src="bd19706_.wmf" alt=""/> <font size=4>Research Interests</font>

  * [interests]Fault-Tolerant Systems[/interests]

  * [interests]Optical Networks[/interests]

  * [interests]Real-time[/interests], [interests]Parallel and Distributed Systems[/interests]

  * [interests]Power Aware Computing[/interests][/resinterests]

<IMAGE src="j02237703.gif" alt=""/> <font size=4>Research Projects</font>

  * [interests]Power Management in Real-Time Systems[/interests]

  * [interests]Connection Control and QoS routing in WDM Networks[/interests]

  * [interests]Network Security[/interests]

  * [interests]Productive[/interests], [interests]Easy to use[/interests], [interests]Reliable Computer Systems[/interests]

<IMAGE src="pe02376_.wmf" alt=""/> <font size=4> Teaching</font>

  * CoE1502: Advanced Digital Design.

  * CS3410: Advanced Topics in Computer Architectures.

  * CS1645: Introduction to High Performance Computing Systems.

  * CS1541: Introduction to Computer Architectures.

  * CS2410: Computer Architectures.

  * CS2450: Parallel Computing

  * CS0447: Computer Organization and Assembly Language Programming.

  * CS0445: Introduction to Information Structures

  * CS1550: Introduction to Operating Systems

  * CS3420: Fault Tolerant Parallel and Distributed Systems

  * CS3530: Advanced Topics in Distributed and Real-Time Systems

  * CS2001: Research topics in Computer Science

<IMAGE src="j0336979.gif" alt=""/> <font size=4> Professional Activities</font>

<IMAGE src="j0336389.gif" alt=""/> <font size=4> CV</font>

<IMAGE src="pe02376_.wmf" alt=""/> <font size=4> Interesting Demonstrations</font>
