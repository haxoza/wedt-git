[pic]<IMAGE src="images.small/steven.cheung.gif" alt=""/>[/pic] <font size=5><b> Steven Cheung</b> </font>

[contactinfo][affiliation]SRI International[/affiliation]
[address]333 Ravenswood Avenue
Menlo Park, CA 94025[/address]
[email]cheung@sdl.sri.com[/email][/contactinfo]

<IMAGE src="images/separate.gif" alt=""/>


[introduction]<IMAGE src="images/sisyphus.gif" alt=""/> Steven Cheung has received his Ph.D. in [phdmajor]Computer Science[/phdmajor], [phduniv]U.C. Davis[/phduniv]. He has got a B.Sc. degree and an M.Phil. degree both in [msmajor]Computer Science[/msmajor] from the Department of Computer Science and Information Systems, [msuniv]University of Hong Kong[/msuniv]. His research interests include [interests]network security[/interests], [interests]intrusion detection[/interests], and [interests]parallel and distributed computing[/interests]. His doctoral research concerns [interests]intrusion detection for network infrastructures[/interests], which is part of the UC Davis's "Intrusion Detection for Large Network" project.[/introduction]

<IMAGE src="images/separate.gif" alt=""/>


Security Related Pages

      <IMAGE src="images/rd_tri.gif" alt="o"/> <b>Computer Security Research Lab., UC Davis</b>

      <IMAGE src="images/bl_tri.gif" alt="o"/> <b>Computer Security Group, University of Cambridge</b>

      <IMAGE src="images/bl_tri.gif" alt="o"/> <b>Salvatore Stolfo's Group, University of Columbia</b>

      <IMAGE src="images/bl_tri.gif" alt="o"/> <b>Center for Secure Information Systems, George Mason Univ.</b>

      <IMAGE src="images/bl_tri.gif" alt="o"/> <b>Global Operating Systems Technology Group, ISI</b>

      <IMAGE src="images/bl_tri.gif" alt="o"/> <b>Cryptography and Information Security Group, MIT</b>

      <IMAGE src="images/bl_tri.gif" alt="o"/> <b>Stephanie Forrest's Group, University of New Mexico</b>

      <IMAGE src="images/bl_tri.gif" alt="o"/> <b>Secure Internet Programming Lab., Princeton University</b>

      <IMAGE src="images/bl_tri.gif" alt="o"/> <b>CERIAS, Purdue Univ.</b>

      <IMAGE src="images/bl_tri.gif" alt="o"/> <b>SRI Computer Science Laboratory</b>

      <IMAGE src="images/pr_tri.gif" alt="o"/> <b>Cipher - Newsletter of the IEEE CS TC on Security and Privacy</b>

      <IMAGE src="images/pr_tri.gif" alt="o"/> <b>Ron Rivest's Cryptography and Security List</b>

      <IMAGE src="images/pr_tri.gif" alt="o"/> <b>Security Focus</b>

      <IMAGE src="images/pr_tri.gif" alt="o"/> <b>Yahoo - Security and Encryption</b>

<IMAGE src="images/birds.gif" alt=""/>
