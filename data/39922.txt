<IMAGE src="/ubc_clf/ubc_nav/ubc_home1.gif" alt="UBC Home Page"/> <IMAGE src="/ubc_clf/ubc_nav/ubc_pixel_transp.gif" alt="-"/>

<IMAGE src="/ubc_clf/ubc_nav/ubc_home2.gif" alt="UBC Home Page"/> <IMAGE src="/ubc_clf/ubc_nav/ubc_pixel_transp.gif" alt="-"/>
<IMAGE src="/ubc_clf/ubc_nav/ubc_univbc1.gif" alt="UBC Home Page"/> <IMAGE src="/ubc_clf/ubc_nav/ubc_univbc2.gif" alt="UBC Home Page"/> <IMAGE src="/ubc_clf/ubc_nav/ubc_pixel_transp.gif" alt="-"/>
<IMAGE src="/ubc_clf/ubc_nav/ubc_pixel_transp.gif" alt="-"/> <IMAGE src="/ubc_clf/ubc_nav/ubc_pixel_transp.gif" alt="-"/>
<IMAGE src="/ubc_clf/ubc_nav/ubc_news.gif" alt="News"/> <IMAGE src="/ubc_clf/ubc_nav/ubc_events.gif" alt="Events"/> <IMAGE src="/ubc_clf/ubc_nav/ubc_directories.gif" alt="Directories"/> <IMAGE src="/ubc_clf/ubc_nav/ubc_search.gif" alt="Search UBC"/> <IMAGE src="/ubc_clf/ubc_nav/ubc_myubc.gif" alt="myUBC Login"/>
<IMAGE src="/ubc_clf/ubc_nav/ubc_pixel_transp.gif" alt="-"/>

<IMAGE src="/ubc_clf/ubc_nav/ubc_pixel_transp.gif" alt="-"/> <IMAGE src="/ubc_clf/ubc_nav/ubc_pixel_transp.gif" alt="-"/>

<IMAGE src="/images/computerscience.gif" alt="Computer Science"/> UBC >> Computer Science



[contactinfo]Listings

[pic]<IMAGE src="http://www.cs.ubc.ca/people/photos/wagner.jpg" alt="My Photo"/>[/pic]
Alan Wagner
[position]Associate Professor[/position], Computer Science

<b>E-mail:</b> [email]wagner<IMAGE src="/images/atcs.gif" alt="at cs domain"/>[/email]
<b>Office Phone: </b>[phone]604-822-6450[/phone]
<b>Office Fax: </b>[fax]604-822-5485[/fax]
<b>Office:</b> [address]ICICS/CS 321[/address][/contactinfo]

[education]B.Sc. (Honors), [bsuniv]Dalhousie University[/bsuniv] ([bsdate]1977[/bsdate]); M.Sc. [msuniv]University of Alberta[/msuniv] ([msdate]1983[/msdate]); Ph.D. [phduniv]University of Toronto[/phduniv] ([phddate]1987[/phddate]); Assistant Professor, University of British Columbia (1987-1997); [position]Associate Professor[/position], [affiliation]University of British Columbia[/affiliation] (1997-).[/education]


[resinterests]Research Interests

  * [interests]Parallel and distributed computation[/interests]

  * [interests]Cluster computing[/interests]

  * [interests]Parallel programming environments[/interests]

  * [interests]Interconnection networks[/interests]

The use of parallel computing is increasing with the growing demand for compute cycles and availability of high networks and inexpensive powerful commodity processors. As the use of parallel computing increases there is a growing need for system software that can deliver the hardware performance to the application and tools to help programmers design, construct and maintain parallel applications.

The focus of my research is [interests]parallel computation on machines and systems[/interests] where processes communicate by passing messages. I am interested in the [interests]use of message-passing libraries[/interests] like MPI (Message Passing Interface) and PVM (Parallel Virtual Machine). Interests include the [interests]performance of libraries[/interests], [interests]tools to aid programmers in developing programs[/interests], and [interests]design of message-passing applications to effectively use parallelism[/interests]. Specific details about my research and current projects can be found in my homepage.[/resinterests]


[publication]Selected Publications

  1. Ng R., Wagner A. and Yin Y., "Iceberg-cube computation with PC clusters", ACM SIGMOD/PODS, May, 2001.

  2. Keppitiyahama C. and Wagner A., "Asynchronous MPI messageing on Myrinet", International Parallel and Distributed Processing Symposium (IPDPS), San Francisco 2001.

  3. Wagner A., Sreekantaswamy H. V. and Chanson S., "Performance models for the processor farm paradigm", IEEE

    Transactions on Parallel and Distributed Systems, Vol. 8, No. 5, May 1997

  4. Gil Y., and Wagner A., "An Alternative Mapping of 3-D Space onto Processor Arrays", Journal of Parallel and Distributed Computing, April 1999.

  5. Wagner, A., "Embedding All Binary Trees in the Hypercube". Journal of Parallel and Distributed Computing, Vol. 18, pp. 33-43, May 1993.[/publication]


Affiliated Web Pages

  * My Personal Web Page

CS Home | About | Research | Prospective Students | Grad | Undergrad | People | Events & Seminars | Employment | Local Resources

This site works best with JavaScript enabled.
