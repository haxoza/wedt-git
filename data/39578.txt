<IMAGE src="/mm/images/spacer.gif" alt=""/> <IMAGE src="/mm/images/spacer.gif" alt=""/> <IMAGE src="/mm/images/spacer.gif" alt=""/>

<IMAGE src="/mm/images/head-dcs-h55.jpg" alt="UIUC Computer Science Department"/>

<IMAGE src="/mm/images/spacer.gif" alt=""/>

<IMAGE src="/mm/images/uofioff.gif" alt="University of Illinois at Urbana-Champaign"/>

<IMAGE src="/mm/images/spacer.gif" alt=""/>

  * Undergraduate

  * Graduate

  * Online Programs

  * Research

  * Directory

    * Faculty by Alphabet

    * Faculty by Research

    * Dept. Staff

    * Research Staff

    * Graduating PhDs

    * Dept. Offices

    * Student Groups

  * About Us

  * Awards

  * Alumni

  * Industrial Relations

  * Outreach/Diversity

  * Calendar

  * In the News

  * Search

<IMAGE src="/mm/images/spacer.gif" alt=""/>



[education]Gerald DeJong<b>[position]Professor[/position]</b>
<b>PhD, [phduniv]Yale University[/phduniv], [phddate]1979[/phddate]</b>[/education]

[resinterests]Research Area: [interests]Automated Reasoning[/interests], [interests]Machine Learning and Natural Language Processing[/interests]

Research Interests: [interests]Artificial intelligence[/interests][/resinterests]

Research Group: Explanation-Based Learning Group

Research Statement

Explanation-based learning is a branch of machine learning in which prior knowledge of the world is integrated into the process of automatically forming new concepts. Many interesting machine learning problems with real world applications are hard. With no additional restrictions learning to select an appropriate behavior or course of action to carry out in the real world is in the worst case computationally intractable. Furthermore, experience with real-world systems indicates that many tasks that seem to require intelligence fall at the difficult end of the spectrum.

In explanation-based learning, the interaction among background knowledge axioms provides an ordering on the space of conceivable hypotheses. Plausible rather than logical inferencing is under study as defining relevant knowledge interactions. This points to a new semantics for declarative knowledge representations. Explanation-based learning has been applied to robotics, planning, experimentation, and control of dynamical systems.

[publication]Representative Publications

  * G. DeJong. AI can Rival Control Theory for Goal Achievement in a Challenging Dynamical System. Computational Intelligence, Vol. 15, No. 4, pp. 333-366, November 1999.

  * M. Cibulskis and G. DeJong. Interfaces that Learn: Path Planning through Mine Fields. Third Annual Fedlab Symposium, College Park, MD, pp. 143-144, February 1999.

  * M. Brodie and G. DeJong. Learning to Ride a Bicycle using Iterated Phantom Induction. Proceedings of the Sixteenth International Conference on Machine Learning, Bled, Slovenia, pp. 57-66, June 1999.[/publication]


Honors and Awards

Arnold O. Beckman Research Award (1984); Alcoa Foundation Faculty Recognition Grant (1989); Fellow, American Association for Artificial Intelligence (1992); and Marquis Who's Who (2000).

[contactinfo]Email: Gerald DeJong

<IMAGE src="/mm/images/spacer.gif" alt=""/>

[pic]<IMAGE src="/mm/images/directory/big/mrebl.jpg" alt=""/>[/pic]

[position]Professor[/position]

Office: [address]3320 SC[/address]

Phone: [phone]333-0491[/phone]

Fax: [fax]265-6591[/fax]

Secretary:
Ronda Pellegrini

<IMAGE src="/mm/images/spacer.gif" alt=""/>

<IMAGE src="/mm/images/spacer.gif" alt=""/>

[affiliation]Department of Computer Science, Thomas M. Siebel Center for Computer Science[/affiliation], [address]201 N Goodwin Ave,
Urbana, IL 61801-2302[/address]. The Department is part of the [affiliation]College of Engineering at the University of Illinois[/affiliation] at Urbana-Champaign. Contact <b>[email]academic@cs.uiuc.edu[/email]</b> with academic questions
or <b>webmaster@cs.uiuc.edu</b> with questions or comments on this page.[/contactinfo]
