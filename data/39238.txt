<IMAGE src="icons/mastlogo.gif" alt="Image:  Department of Planetary Sciences Logo"/> Department of Planetary Sciences
Lunar and Planetary Laboratory

<IMAGE src="icons/blueline.gif" alt="Image:  a blue line"/>


[education]Graduate Program

[pic]<IMAGE src="images/jim&ivy2001.jpeg" alt="Image: Jim Richardson and guide dog Ivy, 2001"/>[/pic]



James E. Richardson Jr.
(Jim)


B.S. degree in [bsmajor]Physics[/bsmajor]: [bsdate]Spring, 2000[/bsdate],
from [bsuniv]Florida State University[/bsuniv]


Ph.D. in [phdmajor]Planetary Sciences[/phdmajor]: [phddate]Spring, 2005[/phddate],
from the [phduniv]University of Arizona[/phduniv][/education]


[left] Jim and his Seeing Eye dog, Ivy


[contactinfo]Contact Information:

[affiliation]Department of Planetary Sciences
Lunar and Planetary Laboratory
University of Arizona[/affiliation]
[address]P.O. Box 210092
Tucson, AZ 85721[/address]
Office: [address]Space Sciences, Rm. 338[/address]
Office Phone: [phone](520) 621-6960[/phone]
Home Phone: [phone](520) 877-2555[/phone]
Cell Phone: [phone](520) 401-9095[/phone]
Email: [email]jrich@lpl.arizona.edu[/email][/contactinfo]


Download a Curiculum Vitae (PDF, 96KB)


[introduction]Overview:

  I recently finished my fifth year as a graduate student at the [affiliation]Lunar and Planetary Laboratory (LPL)[/affiliation], successfully graduating with a Ph.D. in [phdmajor]Planetary Science[/phdmajor]. My primary area of interest involves small [interests]solar system bodies[/interests] -- [interests]asteroids[/interests], [interests]comets[/interests], and [interests]planetary moons[/interests] -- which led to research in two areas: (1) I worked with H. Jay Melosh on the [interests]computer modeling of impact ejecta plume behavior[/interests], as part of the mission planning for the Deep Impact comet flyby and probe impact mission; and (2) I performed my dissertation research with H. Jay Melosh & Richard Greenberg, in which an analytical and numerical modeling approach was used to investigate the effects of impact-induced seismic activity and impact ejecta emplacement on the [interests]geomorphology of fractured asteroids[/interests] in the 5-100 km size range.

LPL Information:<IMAGE src="icons/balls/redball.gif" alt="red bullet"/> Joined Department: Fall, 2000
<IMAGE src="icons/balls/redball.gif" alt="red bullet"/> Graduated: Spring, 2005
<IMAGE src="icons/balls/redball.gif" alt="red bullet"/> Ph. D. Advisors: H. Jay Melosh & Richard Greenberg
<IMAGE src="icons/balls/redball.gif" alt="red bullet"/> Minor Area: Geosciences (geophysics)[/introduction]

Research Project Web Pages:<IMAGE src="icons/balls/orangeball.gif" alt="orange bullet"/> [interests]Titan's Surface and Rotation from Voyager 1[/interests]
<IMAGE src="icons/balls/orangeball.gif" alt="orange bullet"/> [interests]What will the Deep Impact Mission See?[/interests]
<IMAGE src="icons/balls/orangeball.gif" alt="orange bullet"/> [interests]Deep Impact Ejecta Plume Simulations[/interests]
<IMAGE src="icons/balls/orangeball.gif" alt="orange bullet"/> [interests]433 Eros Surface Gravitational Properties[/interests]

Spacecraft Involvement:<IMAGE src="icons/balls/yellowball.gif" alt="yellow bullet"/> Deep Impact: an upcoming comet Discovery mission
<IMAGE src="icons/balls/yellowball.gif" alt="yellow bullet"/> Cassini-Huygens: a mission to Saturn's moon, Titan

Department Involvement & Awards:<IMAGE src="icons/balls/greenball.gif" alt="green bullet"/> Teaching Assistant: NATS 102 (Fall 2001)
<IMAGE src="icons/balls/greenball.gif" alt="green bullet"/> Teaching Assistant: PTYS 206 (Spring 2001)
<IMAGE src="icons/balls/greenball.gif" alt="green bullet"/> UA Graduate College Fellowship, 2000-2001
<IMAGE src="icons/balls/greenball.gif" alt="green bullet"/> Galileo Circle Scholarship, 2005-2006
<IMAGE src="icons/balls/greenball.gif" alt="green bullet"/> Gerard P.Kuiper Memorial Award, 2005

Professional Organization Memberships:<IMAGE src="icons/balls/blueball.gif" alt="blue bullet"/> AAS Division of Planetary Sciences (DPS)
<IMAGE src="icons/balls/blueball.gif" alt="blue bullet"/> American Geophysical Union (AGU)
<IMAGE src="icons/balls/blueball.gif" alt="blue bullet"/> The Meteoritical Society
<IMAGE src="icons/balls/blueball.gif" alt="blue bullet"/> American Meteor Society (AMS)

Gravitational slopes on asteroid 433 Eros<IMAGE src="Deep/himeros2_slope.gif" alt=""/>

Himeros and Shoemaker Regio<IMAGE src="Deep/crater90W10Ns.gif" alt=""/>

Psyche Crater

<IMAGE src="images/seeingeye.gif" alt="Image:  The Seeing Eye Logo"/>Go to Jim's Background Page <IMAGE src="images/et-rate.gif" alt="Image: USN Electronics Technician Rating, 1981-1989"/> <IMAGE src="images/dolfin.gif" alt="Image: USN Submarine Service Dolphins, 1986-1989"/>


[publication]Publications

Reviewed Papers

  J.E. Richardson, (2005)
  The seismic effect of impacts on asteroid surface morphology
  Ph. D. Thesis, Department of Planetary Sciences, Unversity of Arizona.
  [PDF document, 9.3MB],

  J.E. Richardson, H.J. Melosh, and R. Greenberg (2005)
  The global effects of impact-induced seismic shaking on fractured asteroid surface morphology
  Icarus, [In Press].
  [PDF document, 5.0MB],

  J.E. Richardson, H.J. Melosh, N.A. Artemeiva, and E. Pierazzo (2005)
  Impact cratering theory and modeling for the Deep Impact mission: from mission planning to data analysis
  Space Science Reviews, [In Press].
  [PDF document, 1.7MB],

  J.E. Richardson, H.J. Melosh, and R. Greenberg (2004)
  Impact-induced seismic shaking on asteroid 433 Eros: a surface modification process
  Science, <b>306 (5701)</b>, 1526-1529
  [PDF article, 302KB], [Supporting Material, 1957KB]

  J.E. Richardson, R.A. Lorenz, and A.S. McEwen (2004)
  Titan's surface and rotation: new results from Voyager 1 images
  Icarus, <b>170/1</b>, 113-124
  [Web-based summary], [PDF article, 670KB]

  P.S. Gural, P. Jenniskens, M. Koop, M. Jones, J. Houston-Jones, D. Holman, and J.E. Richardson (2004)
  The relative activity of the 2001 Leonid storm peaks and implications for the 2002 return
  Advances in Space Research, <b>33-9</b>, 1501-1506

  D.D. Meisel and J.E. Richardson (1999)
  Statistical properties of meteors from a simple, passive forward-scatter system
  Planetary and Space Science, <b>47/1-2</b>, 107-124
  [PDF article, 837KB]

Non-reviewed Papers

  J.E. Richardson (1999)
  Detailed analysis of the geometric shower radiant altitude correction factor
  WGN, Journal of the International Meteor Organization, <b>27:6</b>, 308-317
  [NASA ADS abstract]

  J.E. Richardson and W. Kuneth, (1998)
  Revisiting the radio doppler effect from forward-scatter meteor head echoes
  WGN, Journal of the International Meteor Organization, <b>26:3</b>, 117-130
  [PDF article, 2461KB]

  J.E. Richardson (1997)
  The Poplar Springs radiometeor station
  Radio Astronomy, Journal of the Society of Amateur Radio Astronomers, <b>December</b>, 1-10
  [Web-based article]

Presentation and Poster Abstracts

  J.E. Richardson, H.J. Melosh, and R. Greenberg, (2005)
  A stochastic cratering model for asteroids surfaces
  36th annual Lunar and Planetary Science Conference (LPSC), No. 2032
  [PDF abstract, 260KB]

  J.E. Richardson, H.J. Melosh, and R. Greenberg, (2004)
  Impact-induced seismic shaking on asteroid 433 Eros: the mechanics of a surface modification process
  American Astronomical Society, DPS meeting No. 36, No. 48.09
  [NASA ADS absract]

  J.E. Richardson, H.J. Melosh, and R. Greenberg, (2004)
  The seismic effect of impacts on asteroid surface morphology: early modeling results
  35th annual Lunar and Planetary Science Conference (LPSC), No. 1864
  [PDF abstract, 122KB]

  J.E. Richardson, H.J. Melosh, and R. Greenberg (2003)
  The seismic effect of impacts on asteroids: early modeling results
  American Geophysical Union (AGU), Fall Meeting 2003, No. P52A-0476
  [NASA ADS absract]

  J.E. Richardson, H.J. Melosh, and R. Greenberg (2003)
  An impact ejecta behavior model for small irregular bodies
  34th annual Lunar and Planetary Science Conference (LPSC), No. 1241
  [PDF abstract, 208KB]

  O. Abramov, J.E. Richardson, and A.S. McEwen (2002)
  Altimetry-based analysis of valley systems on Mars
  American Geophysical Union (AGU), Fall Meeting 2002, No. P51B-0361
  [NASA ADS abstract]

  J.E. Richardson and H.J. Melosh (2002)
  A numerical impact ejecta model for the Deep Impact mission
  Bulletin of the American Astronomical Society, <b>34</b>, 886
  [NASA ADS abstract]

  J.E. Richardson, R.A. Lorenz, and A.S. McEwen (2001)
  Titan's surface and rotation: new results from Voyager 1 images
  Bulletin of the American Astronomical Society, <b>33</b>, 1110
  [PDF poster, 1043KB]

  J.E. Richardson, J. Bedient, R. Lunsford, N. McLeod, and P. Martin (1999)
  Refining visual meteor perception models: shower radiant altitude effect, probability function, and limiting magnitude effect
  The 1999 Asteroids, Comets, Meteors conference, IAU Commission 22 professional-amateur working group meeting
  [PDF slides, 86KB]

  J.E. Richardson, D.D. Meisel, D.E. Binns, and A. Mallama (1999)
  Analysis of radiometeor rates using Fourier and Wavelet techniques
  The 1999 Asteroids, Comets and Meteors conference, No. ACM99 20.02

"To consider the Earth as the only populated world in infinite space is as absurd as to assert that on a vast plain only one stalk of grain will grow" -- Metrodoros of Chios, 4th center B.C.

"There is a theory which states that if ever anyone discovers exactly what the Universe is for and why it is here, it will instantly disappear and be replaced by something even more bizarre and inexplicable."

" There is another theory which states that this has already happened."

-- Douglas Adams, 1980[/publication]

<IMAGE src="icons/back.gif" alt="Graduate Program page button"/> Back to the Graduate Program Page <IMAGE src="icons/lpl-button.jpeg" alt="LPL Home page button"/>Back to the LPL Home Page

Last Modified: May 20, 2005

For questions or comments on this page contact:

James Richardson / jrich@lpl.arizona.edu

otherwise contact:
LPL Webmaster / webmaster@lpl.arizona.edu
