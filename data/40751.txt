Skip main navigation

  * ECE Information Portals

  * Prospective Students

  * Current Students

  * Faculty & Staff

  * Alumni

  * Research



Electrical and Computer Engineering

  * Home

  * Visitor Information

  * Directory

  * News & Events

  * Search

  * Carnegie Mellon

Skip secondary navigation


Directory

  * Other directories

      * Faculty

      * Staff

      * Faculty and staff

      * Class of 2007

      * Class of 2008

      * Class of 2009

      * Graduate students


Elaine Lawrence

Graduate Program Associate â?ECE

<b>Contact Information</b>

[contactinfo]<b>Department</b>

[affiliation]Electrical and Computer Engineering
[/affiliation]
<b>Office</b>

[address]1115 Hamerschlag Hall[/address]

<b>Telephone</b>

[phone](412)-268-3200[/phone]

<b>Fax</b>

[fax](412)-268-2860[/fax]

<b>Email</b>

[email]elaine@ece.cmu.edu[/email][/contactinfo]

[pic]<IMAGE src="/images/directory/fac-staff/elaine.jpg" alt="Headshot of Elaine Lawrence"/>[/pic]

  * Home

  * Search

  * Directory

  * Facilities

  * Web Team

  * College of Engineering

  * Â© 2007, ECE

  * ECE

  * Carnegie Mellon

5000 Forbes Avenue / Pittsburgh, PA 15213-3890 / Phone: 412-268-7400 / Fax: 412-268-2860
