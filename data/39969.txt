[pic]<IMAGE src="berezney.jpg" alt=""/>[/pic]


[introduction]Ronald Berezney
<font size=4><b>Molecular and Cell Biology of the Nucleus</b></font>
<font size=3>[position]Professor[/position]</font>

Ph.D [phddate]1971[/phddate] [phduniv]Purdue University[/phduniv]
NIH International Fellow1971-72 University of Freiburg, Germany
Postdoctoral Trainee 1972-75 Johns Hopkins School of Medicine
Assistant Professor 1975;
Associate Professor 1981;
Professor 1986, University at Buffalo[/introduction]


[contactinfo]Address Information

<b>Ronald Berezney
[address]Department of Biological Sciences
637 Cooke Hall
State University of New York at Buffalo
Buffalo, NY 14260[/address] </b>

[phone]<b>(716) 645-2363 </b><b>ext: 154</b>[/phone]

To send e-mail: <b>[email]berezney@buffalo.edu[/email]</b><b> </b>[/contactinfo]


RESEARCH SUMMARY:

<font size=5>T</font>he identification of a skeletal structure in the [interests]cell nucleus[/interests], [interests]termed the nuclear matrix[/interests], has provided a new stimulus for investigating the relationships of nuclear form and function in the eukaryotic cell. Using combined structural, molecular, and computer imaging approaches, we are studying the functional organization, assembly, and cell cycle regulation of higher order units of replication, transcription and RNA splicing in the cell nucleus. Work is in progress on three-dimensional visualization of the replication and transcription of specific gene sequences at spatially defined, individual sites. In another series of experiments we are using cloning and DNA transfection approaches in combination with real time laser scanning confocal microscopy and multidimensional image analysis to visualize and track in 3D space and time, replication and transcription sites in living cells. In a related project, we are investigating the proteins which constitute the nuclear matrix structure. Studies involving [interests]DNA cloning[/interests], [interests]sequencing of the genes and their expression[/interests], [interests]cell cycle relations[/interests], and [interests]3D computer imaging in relation to functions domains inside the nucleus[/interests] (e.g., DNA replication, transcription and RNA splicing sites), are in progress for several of the nuclear matrix proteins.


SELECTED PROJECTS:

  * <b>[interests]Multidimensional image analysis of genomic organization and function in the interphase nucleus of mammalian cells[/interests]. </b>
    We are investigating the 3-D organization and dynamics of chromosome territories, subchromosomal domains and individual genes in the interphase cell nucleus in relationship to replication, transcription and splicing factor domains. Recent results indicate that a small subclass of nuclear matrix associated proteins mediate the organization of the chromatin into discrete territories and subchromosomal domains.

  * <b>[interests]Molecular cloning of nuclear matrix proteins and genomic function[/interests]</b>
    Multidisciplinary studies spanning the fields of molecular and cell biology are in progress on nuclear matrix proteins. These include the genomic organization and regulation of nuclear matrin 3 and the analysis, nuclear assembly, cell cycle regulation, and functional properties of a recently discovered cyclophilin-type nuclear matrix protein, termed matrin cyp (cyclophilin), which co-localizes in three-dimensions with splicing component domains inside the cell nucleus.


[publication]PUBLICATIONS:

  * Berezney R, Mortillaro, M., Ma, H., Wei, X., & Samarabandu, J.(1995)
    The nuclear matrix: A structural milieu for genomic function.
    In: <b>International Reviews of Cytology</b>, Vol 162A, eds. R Berezney & K.W. Jeon, Academic Press, New York, 1-65. (Review Chapter)

  * Mortillaro, MJ, Blencowe, BJ., Wei, X., Nakayasu, H., Du, L, Warren, SL, Sharp, P & Berezney, R (1996)
    A hyperphosphorylated form of the large subunit of  RNA polymerase II is associated  with splicing complexes and the nuclear matrix.
    <b>Proc. Natl. Acad. Sci.</b> (USA) 93, 8253-8257.

  * Patturajan, M, Wei, X, Berezney, R & Cordon, JL (1998)
    A nuclear matrix protein interacts with the phosphorylated C-terminal domain of RNA polymerase II
    <b>Molec. Cell. Biol.</b>, 18, 2406-2415

  * Mortillaro, M & Berezney, R (1998)
    Matrin CYP, an SR-rich cyclophilin that associates with the nuclear matrix and splicing factors
    <b>J. Biol. Chem.</b>, 273, 8183-8192

  * Ma, H, Samarabandu, J, Devdhar, RS, Acharya, R, Cheng, P-C, Meng, C & Berezney, R (1998)
    Spatial and temporal dynamics of DNA replication sites in mammalian cells<b>
    J. Cell Biol</b>., 143, 1415-1425.

  * Wei, X, Samarabandu, J, Devdhar, RS, Siegel, AJ, Acharya, R & Berezney, R (1998)
    Segregation of transcription and replication sites into higher order domains<b>
    Science </b>281, 1502-1505.

  * Berezney, R. and Wei, X. (1998)
    The new paradigm: Integrating genomic function and nuclear architecture.
    <b>J. Cell Biochem.</b> Suppl. 30/31 238-242

  * Ma, H, Siegel, AJ & Berezney, R (1999)
    Association of Chromosome territories with the nuclear matrix: Disruption of human chromosome territories correlates with the release of a subset of nuclear matrix proteins<b>
    J. Cell Biol</b>., 146, 531-541.

  * Wei, X, Sumanathan, S, Samarabandu, J & Berezney, R (1999)
    Three dimensional visualization of transcription sites and their association with splicing factor-rich nuclear speckles.<b>
    J. Cell Biol.</b>, 146, 543-558.

  * Somanathan, S, Suchyna, TM, Siegel, AJ & Berezney, R (2001)
    Targeting of PCNA to sites of DNA replication in the mammalian cell nucleus.
    <b>J. Cell. Biochem</b>. 81, 56-67.

<IMAGE src="berezneyart.GIF" alt=""/>

Computer model simulation of DNA replication sites visualized inside the cell nucleus following laser scanning confocal microscopy, computer analysis, and 3-D reconstruction.
In this model, the individual sites are depicted as solid spheres, which occupy precisely the same volumes and x, y, z coordinates as the 3-D computer-generated images of these sites.
Sites of the same color indicate higher order domains of individual replication sites using a "nearest neighbor" approach.[/publication]

<IMAGE src="../gifs/arrow1.gif" alt=""/> Return to the directory

<IMAGE src="../gifs/arrow1.gif" alt=""/> Return to the Main Page
