[pic]<IMAGE src="images/orest1_web.gif" alt="Orest Pilskalns"/>[/pic]

[contactinfo]<b>Orest Jacob Pilskalns, Ph.D.</b>
[position]Assistant Professor[/position]
[affiliation]Washington State University - Vancouver
School of Engineering and Computer Science[/affiliation]
email: [email]orest AT wsu DOT edu[/email]
office: [phone](360) 546-9110[/phone]
fax: [fax](360) 546-9438[/fax][/contactinfo]

<IMAGE src="images/logo.jpg" alt=""/>

<b><font size=3>Courses:
</font></b><font size=3>
</font><font size=2>Computer Security CS427 (Spring 2007)
Software Engineering in Practice - CS420 (Spring 2007)
Database Systems - CS 451 (Fall 2006)
Fundamentals of Software Engineering - CS 320 (Fall 2006)
Software Engineering in Practice - CS420 (Spring 2006)</font><font size=3><b>
</b></font><font size=2>Fundamentals of Software Engineering - CS 320 (Fall 2005)
Software Engineering Analysis - CS521 (Fall 2005) </font><font size=3><b>
</b></font><font size=2>Software Engineering in Practice - CS420 (Spring 2005)
Software Engineering I - CptS 322 (Fall 2004)

</font><b><font size=3>Research Interests:
</font></b>
<font size=2>[interests]Software Security[/interests]
[interests]Software Architecture and Design Analysis[/interests]
[interests]Empirical Studies in Software Engineering[/interests]</font><font size=2></font><font size=2> </font><font size=2></font><font size=2> </font><font size=2>
[interests]Visual Modeling and Analysis Tools for Software Engineering[/interests] </font><font size=2>
[interests]Distributed Systems[/interests]</font><font size=2>
</font><font size=2>
[education]</font><b><font size=3>Education:

</font></b><font size=2><b>[phduniv]Washington State University[/phduniv]</b><b>:</b>
[phddate]2004[/phddate] Ph.D. - [phdmajor]Computer Science[/phdmajor]</font><font size=2>
<IMAGE src="index1_files/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/>Advisor - Anneliese Andrews
</font><font size=2><IMAGE src="index1_files/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/>Dissertation - UML Design Testing and Analysis

<b>[msuniv]University of Montana[/msuniv]</b><b>:</b>
[msdate]1998[/msdate] Master's - [msmajor]Computer Science[/msmajor]
<IMAGE src="index1_files/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/>Advisor - David Opitz
<IMAGE src="index1_files/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/>Thesis - </font><font size=2>An Evolutionary, Hill-Climbing Approach to </font><font size=2><IMAGE src="index1_files/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="index1_files/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="index1_files/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="index1_files/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="index1_files/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/></font><font size=2>Symbolic Theory Refinement</font><font size=2>

[bsdate]1996[/bsdate] Bachelor's - [bsmajor]Mathematics[/bsmajor]
<IMAGE src="index1_files/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/>Advisor - Keith Yale

[bsdate]1996[/bsdate] Bachelor's - [bsmajor]Physics[/bsmajor][/education]
<IMAGE src="index1_files/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/><IMAGE src="images/spacer.gif" alt=""/>Advisor - Jim Jacobs


[introduction]</font><b><font size=3>Work:
</font></b><font size=2>
2000-2002 Gemstone Systems (formerly Brokat)
1998-2000 Lockheed Martin Astronautics
</font><font size=3><b>
Bio:</b></font>

<font size=2>Although my name sounds European, I was born in the United States. My father emigrated from Latvia and my mother from the Ukraine shortly after WWII. I grew up in Missoula, Montana and love outdoor activities. I enjoy skiing, biking, backpacking, hunting, golf, and ice-hockey. In 1995 I married my lovely wife, Sasha, and we now have three wonderful children, Ariyana, Ethan, and Aleksandra.</font>[/introduction]

<font size=2><b><font size=3>Awards:</font></b></font>

<font size=2>Second Century Graduate Fellowship 2002-2004
Washington State University</font>

<font size=2>(2) Software Engineering Travel Grants</font><font size=2>
EECS, Washington State University

Research Assistantship 2002-2005
EECS, Washington State University</font>

<font size=2>Teaching Assistantship 1996-1998
CS, University of Montana</font>

<b><font size=3>Links:

</font></b><font size=3><font size=2>IEEE at Washington State University
ACM at Washington State University</font><b>
</b></font><font size=2>Sigma Xi</font>

<font size=3><b>Startup Companies :</b></font>

  * <font size=2>GEOMONKEY, LLC </font>

[publication]<font size=3><b>Refereed Conference Publications:</b></font>

  * <font size=2><b>Pilskalns</b>, Andrews, Uyan "Regression Testing UML Designs ", <b>IEEE Interational Conference on Software Maintenance (ICSM)</b> 2006 (pdf)</font>

  * <font size=2><b>Pilskalns</b>, Andrews "Using UML Designs to Generate OCL for Security Testing ", <b>Software Engineering and Knowledge Engineering Conference (SEKE)</b> 2006 (pdf)</font>

  * <font size=2><b>Pilskalns</b>, Williams, Aracic, Andrews "Security Consistency in UML Designs", <b>30th International Computer Software and Applications Conference (COMPSAC)</b> 2006 (pdf) </font>

  * <font size=2><b>Pilskalns</b>, Williams, Andrews "Defining Maintainable Components in the Design Phase ",<b> IEEE </b></font><font size=2><b>International Conference on Software Maintenance </b></font><font size=2><b> (ICSM) </b>2005 (pdf) </font>

  * <font size=2>O'Fallon,<b> Pilskalns</b>, Knight, Andrews "Defining and Qualifying Components in the Design Phase", <b>Software Engineering and Knowledge Engineering Conference (SEKE) </b>2004 (pdf) </font>

  * <b><font size=2>Pilskalns</font></b><font size=2>, Andrews, France, Ghosh "Rigorous Testing by Merging Structural and Behavioral UML Representations" <b>Sixth International Conference on the Unified Modeling Language (UML) </b><font size=2>2003</font> (pdf) </font>

  * <font size=2>Ghosh, France, Braganza, Kawane, Andrews, <b>Pilskalns </b>"Test Adequacy Assessment for UML Design Model Testing" <b>IEEE 14th. International Symposium on Software Reliability Engineering (ISSRE) </b>2003 (pdf) </font>

  * <font size=2>Sheldon, Jerath, <b>Pilskalns</b>, Kwon, Kim, Chung "Case Study: B2B E-Commerce System Specification and Implementation Employing Use-Case Diagrams, Digital Signatures and XML," <b>IEEE Fourth International Symposium on Multimedia Software Engineering</b> 2002 (pdf)</font>

<b><font size=3>Refereed Journal Publications:</font></b>

  * <font size=2><b>Pilskalns</b>, Andrews, Knight, Ghosh, France. "UML Design Testing", <b>Journal of Information and Software Technology </b>(in press) </font>

  * <font size=2>Dekker, Kromminga, Baak, <b>Pilskalns</b>,<b> </b>Jacobs. "Quantitative Investigation of Fresnel Reflection in the Laboratory", <b>American Journal of Physics</b>, 67, July 1999 (pdf)</font>

<b><font size=3>Invited</font></b><font size=3><b> Publications:</b></font>

  * <font size=2>Sheldon, Xie, <b>Pilskalns</b>, Zhou, "A Review of Some Rigorous Software Design and Analysis Tools", <b>Software Focus Journal</b> (John Wiley & Sons), 2(4) pp. 140-150, Winter 2001 (pdf)</font>

<font size=3><b>2006 Software Engineering Conferences:</b></font>

  * <font size=2>ICSE - International Conference on Software Engineering
    Location: <b>Shanghai, China</b>
    Deadline: <b>September 9, 2006</b> </font>

  * <font size=2>ICEIS - International Conference on Enterprise Information Systems
    Location: <b>Paphos, Cyprus </b>
    Deadline: <b>October 18, 2006</b></font>

  * <font size=2>SEKE - International Conference on Software Engineering and Knowledge Engineering
    Location: <b>San Francisco, CA </b>
    Deadline: <b>March 1, 2006</b></font>

  * <font size=2><font size=2>ICSM - International Conference on Software Maintenance
    Location: <b>Philadelphia, PA </b>
    Deadline: <b>March 31 , 2006</b> </font></font>[/publication]


<font size=2></font>
