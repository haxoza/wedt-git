<IMAGE src="http://www.umassmed.edu//graphics/main_page/umass_logo.jpg" alt="GRAPHIC: UMass Medical School logo (6kb)"/> <IMAGE src="http://www.umassmed.edu/neurobiology/graphics/banners/banner.gif" alt="Header Graphic"/> directory|contacts|index this section only <IMAGE src="http://www.google.com/logos/Logo_25gry.gif" alt="Powered By Google"/>

Home Page

About Neurobiology

Faculty

Graduate Program

Faculty Openings

Seminars

Student Publications

Monarch Project

See also:

Program in Neuroscience

<IMAGE src="/graphics/blank.gif" alt="spacer graphic"/>
neurobiology : faculty

<b>Section: </b>Research


[introduction]Patrick Emery, Ph.D.

<b>Academic Role:</b> [position]Assistant Professor[/position]

<b>Faculty Appointment(s) In:</b>
<b>Neurobiology</b>

<b>Other Affiliation(s):</b>
[affiliation]Program in Neuroscience[/affiliation][/introduction]



Circadian Rhythms and Photoreception in Drosophila

[pic]<IMAGE src="/faculty/photo/emery.jpg" alt="Photo: Patrick Emery"/>[/pic]Drosophila melanogaster is a powerful model organism for understanding the genetic and molecular basis of animal behaviors. Circadian rhythms are a prime example of behaviors whose molecular foundation has been greatly increased by studies in Drosophila. A biological clock dictates that animals sleep and wake with a 24-hour period, and this is true even when they are kept under constant conditions, without any information from the environment. Using genetic screens, several essential clock proteins (PER, TIM, CLK, CYC, DBT, SGG and CK-II) were identified in Drosophila. It has been shown that homologs of most of these proteins are also involved in generating mammalian circadian rhythms. Amazingly, a human homolog of Drosophila PER, hPER2, is mutated in a family of patients with an advanced sleep-phase syndrome. This demonstrates that the discoveries made in Drosophila are playing a crucial role in understanding human circadian behavior.

Light is the most important environmental input for synchronizing the circadian clock to the 24-hr day. Interestingly, in Drosophila, the eyes and opsin-based photoreceptors are not required for the light entrainment of the circadian clock. The unusual blue-light photoreceptor cryptochrome (CRY) is the dedicated circadian photoreceptor. CRY is expressed directly within the brain cells that control circadian behavior and is therefore a deep-brain photoreceptor (see figure).

The primary focus of our lab is to understand the molecular mechanisms underlying CRY circadian photoreception.  With a novel genetic screen, we have isolated new mutants with abnormal response to light and identified several candidate light input genes.  Their function is currently being investigated.  We are also using biochemical approaches to determine which molecules are targeted by CRY in vivo, and how light affects these interactions.  Finally, as CRY responses to light can be recapitulated in cell culture, we use embryonic Drosophila cell lines as a model to understand in more structural details how CRY functions. 

Our work should thus help understanding the molecular interface that connects the circadian clock and the behaviors under its control with the environment.


[contactinfo]<b>Office</b>: [address]LRB 770 N-O Rm726[/address]
<b>Phone</b>: [phone](508) 856-6599[/phone]
<b>E-mail</b>: [email]Patrick.Emery-Le@umassmed.edu[/email][/contactinfo]
<b>Keywords</b>: Genetic Systems, Signal Transduction, Neurobiology

<b>More on Patrick Emery's Research</b>

Research | Figures | Publications | Rotations | Biography
View All Sections on One Page

<IMAGE src="/graphics/blank.gif" alt="spacer graphic"/>

INTRANET <IMAGE src="/graphics/blank.gif" alt="spacer graphic"/> top print <IMAGE src="/graphics/blank.gif" alt="spacer graphic"/>

This is an official Page/Publication of the University of Massachusetts Worcester Campus
<b>Neurobiology</b> <IMAGE src="/graphics/blackbullet.gif" alt=""/> Lazare Research Bldg., 364 Plantation Street Worcester, MA 01605-2324
Questions or Comments? Email: kristin.bradley@umassmed.edu Phone: 508-856-6148


