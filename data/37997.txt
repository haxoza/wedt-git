<IMAGE src="/library/toolbar/3.0/gradient.aspx?a=4B92D9&b=1E77D3&w=250&h=22&d=ltr&c=eFLbOmyDquZL7nbgTLBSftxozLo%3d" alt="*"/>

Quick Links | Home | Worldwide

<IMAGE src="/library/toolbar/3.0/images/banners/ms_masthead_ltr.gif" alt="Microsoft"/> <IMAGE src="/library/toolbar/3.0/gradient.aspx?a=0A6CCE&b=FFFFFF&w=250&h=42&d=ltr&c=NAbKOqnrunJiULJyhm6k3VulBy4%3d" alt="*"/>


Search:

<IMAGE src="/library/mnp/2/gif/arrowLTR.gif" alt="*"/>Microsoft Research Home<IMAGE src="/library/mnp/2/gif/arrowLTR.gif" alt="*"/>About Microsoft Research<IMAGE src="/library/mnp/2/gif/arrowLTR.gif" alt="*"/>Research Areas<IMAGE src="/library/mnp/2/gif/arrowLTR.gif" alt="*"/>People<IMAGE src="/library/mnp/2/gif/arrowLTR.gif" alt="*"/>Worldwide Labs<IMAGE src="/library/mnp/2/gif/arrowLTR.gif" alt="*"/>University Relations<IMAGE src="/library/mnp/2/gif/arrowLTR.gif" alt="*"/>Publications<IMAGE src="/library/mnp/2/gif/arrowLTR.gif" alt="*"/>Downloads<IMAGE src="/library/mnp/2/gif/arrowLTR.gif" alt="*"/>Conferences and Events<IMAGE src="/library/mnp/2/gif/arrowLTR.gif" alt="*"/>Lectures Online<IMAGE src="/library/mnp/2/gif/arrowLTR.gif" alt="*"/>Careers<IMAGE src="/library/mnp/2/gif/arrowLTR.gif" alt="*"/>Visiting Microsoft Research<IMAGE src="/library/mnp/2/gif/arrowLTR.gif" alt="*"/>Press Resources <IMAGE src="/images/rss.gif" alt=""/>

[pic]<IMAGE src="byzhang.jpg" alt=""/>[/pic]<b> </b>

<b></b>

[contactinfo]<font size=4><b>Benyu Zhang</b></font>

[position]Researcher[/position]
[affiliation]Microsoft Research Asia[/affiliation]
[address]5F, Beijing Sigma Center, No. 49, Zhichun Road Haidian District, Beijing 100080, P.R.China[/address]
E-mail: [email]byzhang at microsoft dot com[/email][/contactinfo]
my resume DBLP


[publication]Articles

  1.  Yan, Jun, Zhang, Benyu, Yan, Shuicheng, Liu, Ning, Yang, Qiang, Cheng, Qiansheng, Li, Hua, Chen, Zheng, Ma, Wei-Ying. <b>A Scalable Supervised Algorithm for Dimensionality Reduction on Streaming Data</b> 2006 Information Sciences 14 2042-2065 176

  2.  Yan, Jun, Zhang, Benyu, Liu, Ning, Yan, Shuicheng, Cheng, Qiansheng, Fan, Weiguo, Yang, Qiang, Xi, Wensi, Chen, Zheng. <b>Effective and Efficient Dimensionality Reduction for Large-Scale and Streaming Data Preprocessing</b> 2006 IEEE Transactions on Knowledge and Data Engineering 2 320-333 18

  3.  Yan, Jun, Liu, Ning, Yang, Qiang, Zhang, Benyu, Cheng, Qiansheng, Chen, Zheng. <b>Mining Adaptive Ratio Rules from Distributed Data Sources</b> 2006 Data Mining and Knowledge Discovery 2-3 249-273 12


Conference Papers

  1.  Yan, Jun, Liu, Ning, Zhang, Benyu, Yang, Qiang, Chen, Zheng. <b>A Novel Scalable Algorithm for Supervised Subspace Learning</b> 2006 HongKong, China 6th IEEE International Conference on Data Mining

  2.  Li, Hua, Shen, Dou, Zhang, Benyu, Chen, Zheng, Yang, Qiang. <b>Adding Semantics to Email Clustering</b> 2006 HongKong, China 6th IEEE International Conference on Data Mining

  3.  Chen, Jilin, Zhang, Benyu, Yan, Jun, Yang, Qiang. <b>Diverse Topic Phrase Extraction through Latent Semantic Analysis</b> 2006 HongKong, China 6th IEEE International Conference on Data Mining

  4.  Liu, Ning, Yan, Jun, Zhang, Benyu, Fan, Weiguo, Chen, Zheng. <b>Semantic Overall and Partial Similarity of Temporal Query Logs for Similar Query Suggestion</b> 2006 HongKong, China 6th IEEE International Conference on Data Mining

  5.  Liu, Ning, Yan, Jun, Bai, Fengshan, Zhang, Benyu, Xi, Wensi, Fan, Weiguo, Chen, Zheng, Ji, Lei, Hu, Chenyong, Ma, Wei-Ying. <b>A Similarity Reinforcement Algorithm for Heterogeneous Web Pages</b> 2005 Shanghai, China 7th Asia-Pacific Web Conference

  6.  Yan, Jun, Cheng, Qiansheng, Yang, Qiang, Zhang, Benyu. <b>An Incremental Subspace Learning Algorithm to Categorize Large Scale Text Data</b> 2005 Shanghai, China 7th Asia-Pacific Web Conference

  7.  Yan, Shuicheng, Xu, Dong, Zhang, Lei, Zhang, Benyu, Zhang, Hong-Jiang. <b>Coupled Kernel Discriminant Analysis</b> 2005 San Diego, CA IEEE Computer Society International Conference on Computer Vision and Pattern Recognition 2005

  8.  Zhuang, Dong, Zhang, Benyu, Yang, Qiang, Yan, Jun, Chen, Zheng, Chen, Ying. <b>Efficient Text Classification by Weighted Proximal SVM</b> 2005 Houston, Texas 5th IEEE International Conference on Data Mining 538-545

  9.  Yan, Shuicheng, Xu, Dong, Zhang, Benyu, Zhang, Hong-Jiang. <b>Graph Embedding: A General Framework for Dimensionality Reduction</b> 2005 San Diego, CA IEEE Computer Society International Conference on Computer Vision and Pattern Recognition 2005

  10.  Zhang, Benyu, Li, Hua, Liu, Yi, Ji, Lei, Xi, Wensi, Fan, Weiguo, Chen, Zheng, Ma, Wei-Ying. <b>Improving Web Search Results Using Affinity Graph</b> 2005 Salvador, Brazil 28th annual international ACM SIGIR conference on Research and development in informaion retrieval

  11.  Hu, Chenyong, Wang, Yongji, Zhang, Benyu, Yang, Qiang, Wang, Qing, Zhou, Jinhui, He, Ran, Yan, Jun. <b>Mining Quantitative Associations in Large Database</b> 2005 Shanghai, China 7th Asia-Pacific Web Conference

  12.  Yan, Jun, Liu, Ning, Zhang, Benyu, Yan, Shuicheng, Chen, Zheng, Cheng, Qiansheng, Fan, Weiguo, Ma, Wei-Ying. <b>OCFS: optimal orthogonal centroid feature selection for text categorization</b> 2005 Salvador, Brazil 28th annual international ACM SIGIR conference on Research and development in information retrieval 122-129

  13.  Xi, Wensi, Fox, Edward Allan, Fan, Weiguo, Zhang, Benyu, Chen, Zheng, Yan, Jun, Zhuang, Dong. <b>SimFusion: Measuring Similarity using Unified Relationship Matrix</b> 2005 Salvador, Brazil 28th annual international ACM SIGIR conference on Research and development in informaion retrieval

  14.  Liu, Ning, Bai, Fengshan, Yan, Jun, Zhang, Benyu, Chen, Zheng, Ma, Wei-Ying. <b>Supervised Semi-definite Embedding for Email Data Cleaning and Visualization</b> 2005 Shanghai, China 7th Asia-Pacific Web Conference

  15.  Liu, Ning, Zhang, Benyu, Yan, Jun, Chen, Zheng. <b>Supervised Semi-Definite Embedding for Image Manifolds</b> 2005 Amsterdam, The Netherlands IEEE International Conference on Multimedia & Expo 2005

  16.  Liu, Ning, Zhang, Benyu, Yan, Jun, Chen, Zheng, Liu, Wenyin, Bai, Fengshan, Chien, Leefeng. <b>Text Representation: from Vector to Tensor</b> 2005 Houston, Texas 5th IEEE International Conference on Data Mining 725-728

  17.  Liu, Yi, Zhang, Benyu, Chen, Zheng, Lyu, Michael R., Ma, Wei-Ying. <b>Affinity Rank: A New Scheme for Efficient Web Search</b> 2004 New York, NY 13th international conference on World Wide Web

  18.  Yan, Shuicheng, Zhang, Hong-Jiang, Hu, Yuxiao, Zhang, Benyu, Cheng, Qiansheng. <b>Discriminant Analysis on Embedded Manifold</b> 2004 Prague, Czech Republic 8th European Conference on Computer Vision

  19.  Lu, Yizhou, Liu, Xuezheng, Xi, Wensi, Zhang, Benyu, Li, Hua, Chen, Zheng, Yan, Shuicheng, Ma, Wei-Ying. <b>Efficient PageRank with Same Out-link Groups</b> 2004 Beijing, China 2004 Asia Information Retrieval Symposium

  20.  Sun, Jiantao, Zhang, Benyu, Chen, Zheng, Lu, Yuchang, Shi, Cuiyi, Ma, Wei-Ying. <b>GE-CKO: A Method to Optimize Composite Kernels for Web Page Classification</b> 2004 Beijing, China IEEE/WIC International Conference on Web Intelligence 2004

  21.  Yan, Jun, Zhang, Benyu, Yan, Shuicheng, Yang, Qiang, Li, Hua, Chen, Zheng, Xi, Wensi, Fan, Weiguo, Ma, Wei-Ying, Cheng, Qiansheng. <b>IMMC: Incremental Maximum Margin Criterion</b> 2004 Seattle, WA 10th ACM SIGKDD international conference on Knowledge discovery and data mining

  22.  Liu, Tao, Chen, Zheng, Zhang, Benyu, Ma, Wei-Ying, Wu, Gongyi. <b>Improving Text Classification using Local Latent Semantic Indexing</b> 2004 Brighton, UK 2004 IEEE International Conference on Data Mining

  23.  Liu, Ning, Zhang, Benyu, Yan, Jun, Yang, Qiang, Yan, Shuicheng, Chen, Zheng, Ma, Wei-Ying. <b>Learning Similarity Measures in the Non-orthogonal Space</b> 2004 Washington, DC 13th Conference on Information and Knowledge Management

  24.  Xi, Wensi, Zhang, Benyu, Chen, Zheng, Lu, Yizhou, Yan, Shuicheng, Ma, Wei-Ying, Fox, Edward Allan. <b>Link fusion: a unified link analysis framework for multi-type interrelated data objects</b> 2004 New York, NY 13th international conference on World Wide Web 319-327

  25.  Hu, Chenyong, Zhang, Benyu, Yan, Shuicheng, Yang, Qiang, Chen, Zheng, Ma, Wei-Ying. <b>Mining Ratio Rules Via Principal Sparse Non-Negative Matrix Factorization</b> 2004 Brighton, UK 2004 IEEE International Conference on Data Mining

  26.  Huang, Shen, Xue, Gui-Rong, Zhang, Benyu, Chen, Zheng, Ma, Wei-Ying, Yu, Yong. <b>Multi-Type Features based Web Document Clustering</b> 2004 Brisbane, Australia 5th International Conference on Web Information Systems Engineering

  27.  Liu, Ning, Zhang, Benyu, Yan, Jun, Xi, Wensi, Yan, Shuicheng, Chen, Zheng, Bai, Fengshan, Ma, Wei-Ying. <b>Online Supervised Learning for Digital Library</b> 2004 7th Internaltion Conference of Asian Digital Libraries

  28.  Lu, Yizhou, Zhang, Benyu, Xi, Wensi, Chen, Zheng, Liu, Yi, Lyu, Michael R., Ma, Wei-Ying. <b>The PowerRank Web Link Analysis Algorithm</b> 2004 New York, NJ 13th international conference on World Wide Web

  29.  Huang, Shen, Xue, Gui-Rong, Zhang, Benyu, Chen, Zheng, Yu, Yong, Ma, Wei-Ying. <b>TSSP: A Reinforcement Algorithm to Find Related Papers</b> 2004 Beijing, China IEEE/WIC International Conference on Web Intelligence 2004

  30.  Shen, Dou, Chen, Zheng, Zeng, Hua-Jun, Zhang, Benyu, Yang, Qiang, Ma, Wei-Ying, Lu, Yuchang. <b>Web-page Classification through Summarization</b> 2004 Sheffield, UK 27th annual international ACM SIGIR conference on Research and development in informaion retrieval

  31.  Zhang, Benyu, Li, Wenxin, Xu, Zhuoqun. <b>Personalized Tour Planning System Based on User Interest Analysis</b> 2002 PoznaÃâ? Poland 5th International Conference on Business Information Systems 5th International Conference on Business Information Systems


Public Patent Applications

  1.  Zeng, Hua-Jun, Ma, Wei-Ying, Zhang, Benyu, Yan, Jun, Chen, Zheng. <b>Method and system for incrementally learning an adaptive subspace by optimizing the maximum margin criterion</b> 2005 20060204081

  2.  Chen, Zheng, Hinrichs, Randy, Zhang, Benyu, Gao, Hongbin, Xu, Gu, Ma, Wei-Ying, Zeng, Hua-Jun. <b>Method and system for mining information based on relationships</b> 2005 20060184481

  3.  Zhang, Benyu, Chen, Zheng, Xi, Wensi, Zeng, Hua-Jun, Ma, Wei-Ying. <b>Method and system for ranking messages of discussion threads</b> 2005 20060112392

  4.  Wang, Xianfang, Zhang, Benyu, Yu, Roger, Gao, Hongbin, Seide, Frank, Zeng, Hua-Jun, Li, Li, Li, Ying, Najm, Tarek, Zhou, Jian-Lai, Chen, Zheng. <b>System and method for utilizing the content of audio/video files to select advertising content for display</b> 2005 20060212897

  5.  Chen, Zheng, Wang, Xuanhui, Zeng, Hua-Jun, Ma, Wei-Ying, Zhang, Benyu. <b>Clustering based text classification</b> 2004 20050234955

  6.  Zhang, Benyu, Zeng, Hua-Jun, Ma, Wei-Ying, Chen, Zheng, Hirschler, Gabor, Samuelson, Kurt, Cook, Daniel, Hon, Hsiao-Wuen, Fries, Karen. <b>Content propagation for enhanced document retrieval</b> 2004 20050234952

  7.  Zhang, Benyu, Zeng, Hua-Jun, Ma, Wei-Ying, Chen, Zheng, Hirschler, Gabor, Samuelson, Kurt, Cook, Daniel, Hon, Hsiao-Wuen, Fries, Karen. <b>Enhanced document retrieval</b> 2004 20050234880

  8.  Chen, Zheng, Zhang, Benyu, Ma, Wei-Ying, Zeng, Hua-Jun. <b>Method and system for calculating document importance using document classifications</b> 2004 20060004809

  9.  Chen, Zheng, Zhang, Benyu, Ma, Wei-Ying, Zeng, Hua-Jun. <b>Method and system for classifying display pages using summaries</b> 2004 20050246410

  10.  Chen, Zheng, Ma, Wei-Ying, Zeng, Hua-Jun, Zhang, Benyu. <b>Method and system for clustering using generalized sentence patterns</b> 2004 20060004561

  11.  Chen, Zheng, Ma, Wei-Ying, Zeng, Hua-Jun, Zhang, Benyu. <b>Method and system for detecting when an outgoing communication contains certain content</b> 2004 20060005247

  12.  Chen, Zheng, Liu, Ning, Yan, Jun, Zhang, Benyu, Ma, Wei-Ying, Zeng, Hua-Jun. <b>Method and system for determining similarity of items based on similarity objects and their features</b> 2004 20060112068

  13.  Chen, Zheng, Zhang, Benyu, Zeng, Hua-Jun, Ma, Wei-Ying. <b>Method and system for identifying an author of a paper</b> 2004 20060059121

  14.  Ma, Wei-Ying, Zeng, Hua-Jun, Zhang, Benyu, Chen, Zheng. <b>Method and system for identifying questions within a discussion thread</b> 2004 20060112036

  15.  Zhang, Benyu, Zeng, Hua-Jun, Ma, Wei-Ying, Chen, Zheng. <b>Method and system for prioritizing communications based on interpersonal relationships</b> 2004 20060026298

  16.  Zhang, Benyu, Zeng, Hua-Jun, Ma, Wei-Ying, Chen, Zheng. <b>Method and system for prioritizing communications based on sentence classifications</b> 2004 20060047497

  17.  Chen, Zheng, Zhang, Benyu, Zeng, Hua-Jun, Ma, Wei-Ying. <b>Method and system for ranking documents of a search result to improve diversity and information richness</b> 2004 20050246328

  18.  Chen, Zheng, Zhang, Benyu, Zeng, Hua-Jun, Ma, Wei-Ying, Xi, Wensi. <b>Method and system for ranking objects based on intra-type and inter-type relationships</b> 2004 20050256832

  19.  Zhang, Benyu, Zeng, Hua-Jun, Ma, Wei-Ying, Shen, Dou, Chen, Zheng. <b>Method and system for summarizing a document</b> 2004 20060036596

  20.  Samuelson, Kurt, Chen, Zheng, Hirschler, Gabor, Wen, Ji-Rong, Ma, Wei-Ying, Zeng, Hua-Jun, Li, Hang, Zhang, Benyu. <b>Mining service requests for product support</b> 2004 20050234973

  21.  Zhang, Benyu, Zeng, Hua-Jun, Ma, Wei-Ying, Liu, Guimei, He, Qicai, Chen, Zheng. <b>Query-based snippet clustering for search result grouping</b> 2004 20060026152

  22.  Ma, Wei-Ying, Zeng, Hua-Jun, Li, Li, Zhang, Benyu, Najm, Tarek, Chen, Zheng, Li, Ying. <b>Reinforced clustering of multi-type data objects for search term suggestion</b> 2004 20050234972

  23.  Mah, Teresa, Li, Ying, Zhang, Benyu. <b>System and method for deriving and visualizing business intelligence data</b> 2004 20060085434

  24.  Li, Ying, Chen, Zheng, Najm, Tarek, Ma, Wei-Ying, Zeng, Hua-Jun, Li, Li, Zhang, Benyu. <b>Term suggestion for multi-sense query</b> 2004 20050234879

  25.  Zhang, Benyu, Li, Li, Zeng, Hua-Jun, Ma, Wei-Ying, Najm, Tarek, Chen, Zheng, Li, Ying. <b>Verifying relevance between keywords and Web site contents</b> 2004 20050234953

  26.  Zeng, Hua-Jun, Ma, Wei-Ying, Zhang, Benyu, Xue, Gui-Rong, Chen, Zheng. <b>Web page ranking with hierarchical considerations</b> 2004 20060095430[/publication]


Manage Your Profile |Contact Us?007 Microsoft Corporation. All rights reserved. Terms of Use |Trademarks |Privacy Statement

<IMAGE src="http://m.webtrends.com/dcsslvlng89k7may281d5dedm_8n8e/njs.gif?dcsurl=/nojavascript&WT.js=No" alt=""/><IMAGE src="http://c.microsoft.com/trans_pixel.asp?source=research&TYPE=PV&p=users_byzhang&URI=%2fusers%2fbyzhang%2fdefault.aspx&GUID=1F4FC18C-F71E-47FB-8FC9-612F8EE59C61&lc=en-us" alt=""/>
