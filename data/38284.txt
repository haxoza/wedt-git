<IMAGE src="fileadmin/tmpl/biobase/img/logo.gif" alt=""/>

  * Home

  * Contact Form

  * Sitemap

  * Imprint

  * Login

<b>You are here : </b>Company / Organization / Management

  * Company

      * Vision & Mission

      * Organization

          * Management

          * Scientific Board

      * History

      * Memberships

      * Partners

      * Grants

      * Publications

      * Careers

  * Databases and Tools

  * Subscriptions

  * Discounts

  * Free Trial

  * Support

  * News

  * Contact


Latest NewsDecember 20, 06

Laboratoires Fournier Licenses TRANSFAC database

December 06, 06

Bayer HealthCare Licences BRENDA-AMENDA for Target Research

November 02, 06

NIH licenses TRANSFAC and PROTEOME databases


Upcoming Events


Dec 14 - 15, 06

BIOBASE Tutorial Tokyo, Japan

[pic]<IMAGE src="typo3temp/pics/4bd10ac311.jpg" alt=""/>[/pic]


Dr. Frank Schacherer

[introduction]Dr. Frank G. Schacherer holds the position of [position]Vice President[/position] Indian Operations at [affiliation]Biobase[/affiliation]. He joined BIOBASE in 2005 as director of BIOBASE Corporation, the US subsidiary of BIOBASE GmbH.

<IMAGE src="clear.gif" alt=""/>

Dr. Schacherer is responsible for operations management of the Indian subsidiary of BIOBASE GmbH. Before joining BIOBASE, Dr. Schacherer conceptualized and developed the microarray expression and pathway analysis business of Miltenyi Biotech. He also worked as a consultant for [interests]database integration projects[/interests] at SHS information systems, and lead the development of the TRANSPATH(R) database at Biobase GmbH. Dr. Schacherer obtained his M.S., in [msmajor]biochemistry[/msmajor] from the [msuniv]Free University of Berlin[/msuniv] in [msdate]1997[/msdate], and his Ph.D. in [phdmajor]Bioinformatics[/phdmajor] from the [phduniv]Technical University of Braunschweig and the German Research Centre for Biotechnology[/phduniv] in [phddate]2001[/phddate]. His thesis investigated the development of an object oriented database for signal transduction pathways, and simulation of such pathways. Dr. Schacherer is a [position]member[/position] of the [affiliation]international Society for Computational Biology (ISCB)[/affiliation], of [affiliation]Bioinformation Systems e.V.[/affiliation], of the [affiliation]BioPathways Consortium[/affiliation] and of the [affiliation]BioPAX Consortium[/affiliation].[/introduction]

<IMAGE src="clear.gif" alt=""/>

<IMAGE src="uploads/RTEmagicC_arrow-left_17.gif.gif" alt=""/> back

<IMAGE src="fileadmin/tmpl/biobase/img/arrow-top.gif" alt="Top"/> TopComments and Suggestions to webmaster@biobase-international.com ?1999-2006 BIOBASE Biological Databases. All rights reserved.
