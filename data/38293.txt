<b>Dr. Kinshuk</b>

I have now moved to:

[contactinfo][position]Professor and Director[/position]
[affiliation]School of Computing and Information Systems
Athabasca University[/affiliation]
[address]1 University Drive
Athabasca, Alberta T9S 3A3
Canada[/address]
Tel: [phone]+1 780 675-6812[/phone]
Email: [email]kinshuk@ieee.org[/email]
URL: [homepage]http://scis.athabascau.ca/scis/staff[/homepage][/contactinfo]

[pic]<IMAGE src="images/kinshuk.jpg" alt="Kinshuk's virtual image"/>[/pic]



<b><font size=5>CV and List of Publications</font></b>


[introduction]Short biography<font size=2></font><font size=2>Kinshuk joined [affiliation]Athabasca University[/affiliation] in August 2006 as the [position]Professor and Director[/position] of [affiliation]School of Computing and Information Systems[/affiliation].  Before moving to Canada, Kinshuk worked at German National Research Centre for Information Technology as Postgraduate Fellow, and at Massey University, New Zealand as Associate Professor of Information Systems and Director of Advanced Learning Technology Research Centre. He also holds Honorary Senior E-Learning Consultant position with Online Learning Systems Ltd., New Zealand, and Docent position with University of Joensuu, Finland. He has been involved in large-scale research projects for exploration based adaptive educational environments and has published over 185 research papers in international refereed journals, conferences and book chapters. He is [position]Chair[/position] of [affiliation]IEEE Technical Committee on Learning Technology and International Forum of Educational Technology & Society[/affiliation]. He is also [position]editor[/position] of the [affiliation]SSCI indexed Journal of Educational Technology & Society[/affiliation] (ISSN 1436-4522).[/introduction]
</font>

.

[resinterests]<font size=4><b>Current Research</b></font>

<font size=4>[interests]Advanced Learning Technologies Research[/interests] Centre</font>[/resinterests]

<font size=4><b>Current Involvements</b></font>

<b>International Forum of Educational Technology & Society</b>

<b>IEEE Technical Committee on Learning Technology</b>

<b>Distance Education Association of New Zealand</b>

<b>New Zealand Chapter of ACM SIGCHI</b>

<font size=4><b>Past Research</b></font>

Till June 1999, I was a research fellow at Human Computer Interaction Institute of German National Research Centre for Information Technology, Germany. My research at GMD was based on interactive simulations for cognitive skills acquisition in medical domain. This invloves the design and development of skills enabling learning systems with intelligent assistance.

Before that, I finished my PhD in [phddate]1996[/phddate] at [phduniv]De Montfort University[/phduniv], Leicester, England. You are invited to see the postscript version of my thesis here. <IMAGE src="images/open_book.gif" alt=""/>

The abstract of my PhD work is available here<IMAGE src="images/papirus_3.gif" alt=""/>

My PhD research work was based on intelligent tutoring modules developed in the Byzantium Project funded under Teaching & Learning Technology Programme (TLTP). As a part of a team, I was involved in the developement of four packages: Marginal Cosing, Capital Investment Appraisal, Absorption Costing and Standard Costing.

Screenshots of all the packages are available here <IMAGE src="byz/byz_gifs/hw1.gif" alt=""/>

<font size=4><b>Favorite Links</b></font>

<font size=4>Sahaja Yoga</font>

<font size=4> Alumni - Engineering College Kota</font>
