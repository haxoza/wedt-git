[introduction][affiliation]Department of Education in Technology and Science

Technion - Israel Institute of Technology[/affiliation]

Updated: December 2006

Dr. ORIT HAZZAN, [position]Associate Professor[/position][/introduction]

Academic Background

[resinterests]Research Interests

- [interests]Teaching human aspects of software engineering[/interests]

- [interests]eXtreme Programming and Agile Software Development (new window)[/interests]

- [interests]Computer science education[/interests]

- [interests]ICT in cultural & educational changes[/interests]

- [interests]Collegiate mathematics education[/interests][/resinterests]

[pic]<IMAGE src="index_files/image005.jpg" alt=""/>[/pic]

Publications

- Computer science and software engineering education

- Collegiate mathematics education

- ICT as agents of cultural and educational changes

Professional Experience

Human Aspects of Software Engineering

<IMAGE src="index_files/image006.jpg" alt=""/>

Columns

System Design Frontier

àñéîåï Asimon (=Token): Diversity in Science and Technology

Presentations

Contact Information
