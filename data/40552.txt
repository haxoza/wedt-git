<font size=6>FATOS XHAFA</font><font size=6> <IMAGE src="logoupc.gif" alt=""/>
</font>Departament de LSI, Universitat Politècnica de Catalunya


[pic]<IMAGE src="FatosXhafa.jpg" alt=""/>[/pic]

[introduction]<b>FATOS XHAFA,</b> [phddegree]PhD[/phddegree] in [phdmajor]Computer Science[/phdmajor], [position]Associate Professor[/position] at the [affiliation]Department of Languages and Informatics Systems, Polytechnic University of Catalonia[/affiliation], Barcelona, Spain.


Fatos is native from Albania where he was graduated in [bsmajor]Mathematics[/bsmajor] from the [bsuniv]Faculty of Natural Sciencies (FNS), University of Tirana (UT)[/bsuniv], Albania, in [bsdate]1988[/bsdate]. During 1989-1993 he was an Assistant Pedagog at the Department of Applied Mathematics of the FNS, UT, Albania.


Fatos joined the [phduniv]Polytechnic University of Catalonia[/phduniv] as a Visiting Professor at the Applied Mathematics Department I, and later he entered the Department of Languages and Informatic Systems as a PhD student and in 1996 as an Assistant Professor. Fatos got his [phddegree]PhD[/phddegree] in [phdmajor]Computer Science[/phdmajor] from the Department of LSI in [phddate]1998[/phddate].

Currently, Fatos is [position]Associate Professor (Professor Agregat)[/position] at the [affiliation]Department of Languages and Informatic Systems and member of the ALBCOM Research Group (ALGORITHMS, BIOINFORMATICS, COMPLEXITY AND FORMAL METHODS)[/affiliation]. His current research interest include [interests]parallel algorithms[/interests], [interests]approximation and meta-heuristics[/interests], [interests]distributed programming[/interests], [interests]Grid and P2P computing[/interests].[/introduction]


RESEARCH PUBLICATIONS TEACHING CV LINKS CONTACT PERSONAL INFO
