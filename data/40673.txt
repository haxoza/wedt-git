<font size=6> </font>

[contactinfo]<font size=7><b>Raya Fidel, [position]Professor[/position]</b></font>


        Email:[email]fidelr@u.washington.edu[/email]
        Office: [address]330R Mary Gates Hall[/address]
        Phone: [phone](206) 543-1888[/phone]
        Fax: [fax](206) 616-3152[/fax]


        Mailing address:
        Professor Raya Fidel
        [address]The Information School
       Box 352840
        Suite 370 Mary Gates Hall
        Seattle, WA 98195-2840[/address]<font size=5> </font>[/contactinfo]

          ?       

[pic]<IMAGE src="default_files/fidelr2.jpg" alt=""/>[/pic]

            [introduction]<font size=5>Dr. Raya Fidel brings expertise in the fields of information seeking behavior, information storage and retrieval, and knowledge organization. She currently teaches in the areas of information behavior, information systems, knowledge representation and thesaurus construction. Her research focuses on [interests]information seeking and searching behavior[/interests]. </font>

            <font size=5>Dr. Fidel is the [position]Head[/position] of the [affiliation]Center for Human-Information Interaction[/affiliation]. Her work appears in various journals and books. Professor Fidel holds an [msdegree]M.L.S.[/msdegree] from the [msuniv]Hebrew University of Jerusalem[/msuniv] ([msdate]1972[/msdate]) and a [phddegree]Ph.D.[/phddegree] in [phdmajor]Library and Information Science[/phdmajor] from the [phduniv]University of Maryland[/phduniv] ([phddate]1982[/phddate]). </font>[/introduction]

            <font size=4> Link to Publications Page</font>
