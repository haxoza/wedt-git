Dr. Fan Zhang


[contactinfo][affiliation]Department of Computer Science
Hong Kong University of Science and Technology[/affiliation]
[address]Clear Water Bay
Kowloon, Hong Kong
Office: Rm. 3536[/address]
Phone: [phone]2358-6992[/phone] Fax: [fax]2358-1477[/fax]
E-mail: [email]zhangfan(at)cs(dot)ust(dot)hk[/email]
 or [email]fanzhangfd(at)gmail(dot)com[/email]
Web: [homepage]http://www.cs.ust.hk/~zhangfan[/homepage][/contactinfo]

[pic]<IMAGE src="photos/me.png" alt="A picture of me"/>[/pic]


<b>Brief Bio</b> (CV in HTML)

[introduction]Fan ZHANG was a Visiting Assistant Professor at the Department of Computer Science, HKUST from August 2005 to July 2006. He received his [bsdegree]BS degree[/bsdegree] in [bsmajor]Computer Science[/bsmajor] from [bsuniv]Fudan University[/bsuniv], Shanghai, in [bsdate]1999[/bsdate], and [phddegree]PhD[/phddegree] in [phdmajor]Computer Science[/phdmajor] from [phduniv]HKUST[/phduniv] in [phddate]July 2005[/phddate]. His thesis advisors were Professor Samuel T. Chanson and Professor Lionel Ni.


....I am now working with the [affiliation]Windows Server Solution Group (WSSG), Microsoft Advanced Technology Center[/affiliation] (Shanghai). While I still check my CSD email account every day, this account will expire as soon as this page is taken down :).[/introduction]


[resinterests]Research Interests

My research interests include (but are not limited to :):

  <IMAGE src="photos/ball2.gif" alt="bullet"/> [interests]Power-Aware Computing[/interests]
  <IMAGE src="photos/ball2.gif" alt="bullet"/> [interests]Real-Time Systems[/interests]
  <IMAGE src="photos/ball2.gif" alt="bullet"/> [interests]Embedded Systems[/interests]
  <IMAGE src="photos/ball2.gif" alt="bullet"/> [interests]Resource Management[/interests]
  <IMAGE src="photos/ball2.gif" alt="bullet"/> [interests]Wireless Networks[/interests][/resinterests]

Visit our Embedded Systems and Software Group.


[publication]Publications


Journal

1. <b>Fan Zhang</b> and S. T. Chanson, "Blocking-Aware Processor Voltage Scheduling for Real-Time Tasks", ACM Transactions on Embedded Computing Systems, pages 307-335, May 2004. PDF

2. <b>Fan Zhang</b> and S. T. Chanson, "Improving Communication Energy Efficiency in Wireless Networks Powered by Renewable Energy Sources", IEEE Transactions on Vehicular Technology, pages 2125-2136, Nov. 2005. PDF


Conference

1. M. Tao, Y.C. Liang, and <b>Fan Zhang</b>, "Adaptive Resource Allocation for Delay Differentiated Traffics in Multiuser OFDM Systems", Accepted to ICC 2006.

2.<b> Fan Zhang</b> and S. T. Chanson, "Proxy-Assisted Scheduling for Energy-Efficient Multimedia Streaming over Wireless LAN", In Proceedings of IFIP Networking'05, (Acceptance Ratio: 24.7%). Also in Springer-Verlag LNCS 3462.

3. <b>Fan Zhang</b> and S. T. Chanson, "Power-Aware Processor Scheduling under Average Delay Constraints", In Proceedings of the 11th IEEE Real-Time and Embedded Technology and Applications Symposium (RTAS'05), pages 202-212, March 2005. PS PDF
Presentation Slides: PPT

4. <b>Fan Zhang</b> and S. T. Chanson, "Value and Throughput Maximization in Wireless Packet Scheduling under Energy and Time Constraints". In Proceedings of the 24th IEEE International Real-Time Systems Symposium (RTSS'03), pages 324-334, Cancun, Mexico. Dec. 2003. PS PDF Errata  (Acceptance Ratio: 16.5%)
Presentation Slides: PPT

5. <b>Fan Zhang</b> and S. T. Chanson, "Rate-Controlled Scheduling for Networked Multimedia Applications", In Proceedings of the 22nd IEEE International Performance, Computing ,and Communications Conference (IPCCC 2003), pages 395-403, Phoenix, Arizona. USA, Apr. 2003. PS PDF
Presentation Slides: PPT

6. <b>Fan Zhang</b> and S. T. Chanson, "Processor Voltage Scheduling for Real-Time Tasks with Non-Preemptable Sections", In Proceedings of the 23rd IEEE International Real-Time Systems Symposium (RTSS'02), pages 235-245, Austin, Texas. USA, Dec. 2002. PS PDF (Acceptance Ratio: 25%)
Presentation Slides: PPT

7. X. Tang, <b>Fan Zhang</b> and S. T. Chanson, "Streaming Media Caching Algorithms for Transcoding Proxies", International Conference on Parallel Processing (ICPP), Vancouver, Canada, Aug. 2002. PDF[/publication]


[resactivity]Service

Program Committee member of The 3rd International Workshop on Embedded Computing (in conjunction with ICPP'06), Columbus, Ohio, USA, August 14, 2006.

Program Committee member of International Workshop on Embedded Software Optimization (in conjunction with IFIP International Conference on Embedded And Ubiquitous Computing), Seoul, Korea, August 01-04, 2006.

Program Committee member of IEEE Real-Time and Embedded Technology and Applications Symposium (RTAS'06), San Jose, April 2006.

Program Co-Chair of 2005 Intl. Workshop on Scheduling Techniques for Real-Time Systems (in conjunction with ICESS'05), Xi’an, P.R.China, December, 2005. (CFP)

Program Committee member of 2nd Intl. Workshop on Power-Aware Real-Time Computing (PARC), Jersey City, NJ, September 2005.

Reviewer for IEEE Transactions on Computers, IEEE Transactions on Mobile Computing, IEEE Transactions on CAD of Integrated Circuits and Systems, IEEE Transactions on Vehicular Technology, ACM Transactions on Embedded Computing Systems, ACM/Kluwer MONET.

Reviewer for various conferences including RTSS, INFOCOM, DATE, QShine.[/resactivity]


Teaching


2006 Spring

COMP 151 Object Oriented Programming.
COMP 111 Software Tools.


2005 Fall

COMP 102 Programming Fundamentals I.
COMP 111 Software Tools.


<IMAGE src="daisy.gif" alt="daisy"/><IMAGE src="daisy.gif" alt="daisy"/>


<IMAGE src="http://home-cgi.ust.hk/cgi-bin/Count.cgi?md=5&dd=C&df=zhangfan.dat" alt="counter"/>
