[contactinfo][affiliation]<b><font size=5>D</font>IPARTIMENTO DI <font size=5>I</font>NFORMATICA
Universit?di Torino[/affiliation] </b>


Guido Boella's Home Page[affiliation]Dipartimento di Informatica
Universita' degli Studi di Torino[/affiliation]
[address]Corso Svizzera, 185
I-10149 Torino ITALY[/address]

<b>e-mail</b>: [email]guido@di.unito.it[/email]
<b>Tel</b>.:      [phone]+39 011 670 68 20[/phone]     
<b>Fax</b>.:     [fax]+39 011 75 16 03[/fax][/contactinfo]

Teaching (Italian only)


Scienze cognitive 2005/2006

Laboratorio di programmazione 2005/2006

Bioinformatica 2004

Interazione uomo-macchina 2003/2004: sistemi multiagente
<b>
Orario di Ricevimento</b>: su appuntamento via email

Research

PUBLICATIONS (ALL) (DBLP)

Events

  * October, 2006, Special issue of Computational & Mathematical Organization Theory on normative multi-agent systems.

Upcoming events:

  * March 18-23, 2007, Dagstuhl Workshop on Normative Multi-Agent Systems


CV

[introduction]<font size=4>Guido Boella  graduated in Philosophy at the [phduniv]University of Torino[/phduniv] in 1995 and he's a PhD at the Dept. of [phdmajor]Computer Science[/phdmajor]. The title of his PhD Thesis is ``</font><font size=4>[interests]Cooperation among economically rational agents[/interests]</font><font size=4>".</font>

<font size=4>He is currently a [position]researcher[/position] at the <b>[affiliation]Department of Computer Science</b> in the <b>Universita' degli studi di Torino[/affiliation]</b> where he is [position]member[/position] of the [affiliation]Natural Language Processing Group[/affiliation]. His major interests are multiagent systems.
</font>

<font size=4>He is in the [position]Program Committee[/position] of the following events:
AAMAS'05, IAT'06, ESAW'06, ESAW'07, ACL'07, COIN@AAMAS'06, COORG'05 and COORG'06, OOOP'05, LOAIT'05 , MASTA'05, IBERAMIA'06, ARGMAS'07.
He is one of the organizers of:
NORMAS'05 and NORMAS'07, COIN@ECAI'06, AAAI Fall Symposium Roles'05, </font>

<font size=4> He is in the [position]Steering Committee[/position] of [affiliation]COIN[/affiliation]</font>

Guido Boella's Curriculum Vitae 2005 (Italian only)

Normative Multiagent Systems (NORMAS)[/introduction]


[pic]<IMAGE src="berkeley.jpg" alt=""/>[/pic]
