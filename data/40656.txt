[contactinfo]Mr. Sujit Kuthirummal

[position]PhD Student[/position]

Contact

[address]Room 7LW1 CEPSR/618 CEPSR[/address] [pic]<IMAGE src="getPhoto.php?personID=3336" alt=""/>[/pic]

Phone [phone]212 939 7146[/phone]/[phone]212 939 7055[/phone][/contactinfo]

[resinterests]Research Interests

[interests]Computer Vision[/interests], [interests]Computer Graphics[/interests]

My current work involves developing [interests]novel cameras[/interests] that capture new kinds of visual information that would be useful for both computer vision and computer graphics.[/resinterests]

Links

Personal web page
