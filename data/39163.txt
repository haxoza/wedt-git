<IMAGE src="IMAGES/bannersb.gif" alt=""/>


<IMAGE src="IMAGES/purple.gif" alt=""/> <b>Home</b>

<IMAGE src="IMAGES/purple.gif" alt=""/> <b>Simon's CV</b>

<IMAGE src="IMAGES/purple.gif" alt=""/> <b>Foam Research</b>

[contactinfo]Mail me at:
[email]foams AT aber.ac.uk[/email]

[address]Institute of Mathematical and
Physical Sciences,
University of Wales,
Aberystwyth,
SY23 3BZ,
UK.[/address]

Phone:
[phone]+44-(0)1970-622764[/phone]
Fax:
[fax]+44-(0)1970-622826[/fax][/contactinfo]

[introduction]Welcome to Simon Cox's webpages.

I do research on [interests]foams and lecture in Applied Mathematics[/interests] in the [affiliation]Institute of Mathematical and Physical Sciences at the University of Wales[/affiliation], Aberystwyth.
Here I am with a stone representation of the third incarnation of Vishnu in [affiliation]Bandhavgarh National Park[/affiliation], India:

[pic]<IMAGE src="IMAGES/boar046.jpg" alt="boar"/>[/pic]

(picture courtesy of P. Hammerton)

<IMAGE src="IMAGES/colorbar.gif" alt=""/>


<IMAGE src="IMAGES/albatross.jpg" alt="save the albatross"/>

The information provided on this and other pages by me, Simon Cox (foams AT aber.ac.uk), is under my own personal responsibility and not that of the University of Wales Aberystwyth. Similarly, any opinions expressed are my own and are in no way to be taken as those of U.W.A.[/introduction]
