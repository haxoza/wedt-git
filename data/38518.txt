<IMAGE src="Images/berkeley.gif" alt=""/><IMAGE src="Images/intel_research_design.jpg" alt=""/><IMAGE src="Images/intel_research_design_end.jpg" alt=""/>

<IMAGE src="Images/intel_research_people.gif" alt=""/>


[pic]<IMAGE src="Images/minos.jpg" alt="mugshot"/>[/pic]


[contactinfo]Minos Garofalakis[affiliation]Intel Research Berkeley[/affiliation]
[address]2150 Shattuck Avenue
Penthouse Suite
Berkeley, CA 94704
USA[/address]

Tel: [phone]+ 1-510-495-3058[/phone],  Fax: [fax]+ 1-510-495-3049[/fax][/contactinfo]

<IMAGE src="Images/e2.jpg" alt=""/>


[introduction]About me I am a [position]Senior Research Scientist[/position] with [affiliation]Intel Research Berkeley[/affiliation], and an [position]Adjunct Associate Professor[/position] of Computer Science at the [affiliation]University of California, Berkeley[/affiliation] where I am also part of the [affiliation]UC-Berkeley Database Group[/affiliation]. My current research interests lie in the areas of [interests]probabilistic data management[/interests], [interests]approximate query processing[/interests], [interests]data streaming[/interests], [interests]network management[/interests], [interests]data mining[/interests], and [interests]XML[/interests] and [interests]text databases[/interests].

Some personal history: I am originally from the beautiful city of Chania on the island of Crete, Greece. (I guess that explains my first name.) I received my BSc (Valedictorian, School of Engineering) in [bsdate]1992[/bsdate] from the [bsuniv]University of Patras[/bsuniv], [bsmajor]Computer Engineering and Informatics[/bsmajor] Dept. (UOPCEID). I also spent the following year at UOPCEID as a post-graduate fellow. In the Fall of 1993, I joined the graduate program in [phdmajor]Computer Sciences[/phdmajor] at the [phduniv]University of Wisconsin[/phduniv] - Madison, where I received my MSc ([msdate]1994[/msdate]) and PhD ([phddate]1998[/phddate]) under the supervision of Prof. Yannis Ioannidis. I joined the Database Principles Research Department at Bell Labs as a Member of Technical Staff in September 1998, and moved to Intel Research in July 2005.[/introduction]




Publications | Tutorials | Recent Talks | Professional Service | Patents | Vita (pdf)


<IMAGE src="Images/vldb07-logo.png" alt="VLDB'2007"/>

Submit your best work to VLDB 2007!


Research-related links

<IMAGE src="Images/dblp-logo.gif" alt="dblp-logo"/>

DBLP Bibliography Server
[ My entry ] <IMAGE src="Images/sigmod.gif" alt="sigmod-logo"/>

ACM SIGMOD

<IMAGE src="Images/vldblogo.gif" alt="vldb-logo"/>

The VLDB Endowment <IMAGE src="Images/sigkdd.gif" alt="sigkdd-logo"/>

ACM SIGKDD


Web sightings

  <IMAGE src="Images/don.jpg" alt="don-pic"/>

    I'm a proud member of the infamous "Greek Database Mafia". :-)



  <IMAGE src="Images/vldb98-small.jpg" alt="vldb-pic"/>

    Preparing for a presentation at VLDB'98 -- you can't see my face, but it really is me!! :-)





<IMAGE src="Images/work-anim.gif" alt="Workin'"/>

  Page under perpetual construction...
