<font size=1>






[introduction]</font> Bill Tomlinson<font size=2>
[position]Assistant Professor[/position] of Informatics in the [affiliation]Donald Bren School of Information and Computer Sciences at the University of California[/affiliation], Irvine and [position]researcher[/position] in the [affiliation]California Institute for Telecommunication and Information Technology (Calit2)[/affiliation]. Also [position]member of the faculty[/position] in the [affiliation]Drama Department in the Claire Trevor School of the Arts and the ACE (Arts Computation Engineering) graduate program[/affiliation].[/introduction]
</font> [pic]<IMAGE src="images/billPic.jpg" alt=""/>[/pic]

<font size=2> Curriculum Vitae (pdf)
Current Research Projects
Previous Projects
Publications
Office Hours
Biography
Students
Courses</font>

<IMAGE src="images/pointing.gif" alt=""/> Research/Creative Areas<font size=2>
Computer Graphics, Human-Computer Interaction, Autonomous Agents & Multi-Agent Systems, Computer Games, Interactive Animation, Computational Social Relationships, Environmental Education, Mobile/Ubiquitous Computing, Interactive Narrative, Multi-Device Graphics

</font>


[contactinfo]Email<font size=2>
[email]wmt then the at-sign then uci dot edu[/email].[/contactinfo]
Why the funny format?
</font>
