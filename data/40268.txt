<IMAGE src="images/netgroup_logo.jpg" alt=""/>

<font size=7>University of California, Irvine</font>

<font size=5>School of Information and Computer Science</font>

<b> <font size=4> Home</font></b>

<b> <font size=4> Research</font></b>

<b> <font size=4> People</font></b>

<b> <font size=4> Downloads</font></b>

<b> <font size=4> Opportunities<IMAGE src="images/new.gif" alt=""/></font></b>

<b> <font size=2>Networked Systems Graduate Degree Program</font></b>

[contactinfo]<b><font size=5>Computer Network Research</font></b>

<b><font size=4>[position]Professor[/position] Tatsuya Suda</font></b>
[affiliation]<b>School</b><b> of Information and Computer Science
University of California, Irvine[/affiliation]
[address]Irvine, CA 92697-3425[/address]
Phone:    [phone]949-824-5474[/phone]
Fax:        [fax]949-824-2886[/fax]
Email:</b><b> [email]suda@ics.uci.edu[/email]</b>[/contactinfo]

[pic]<IMAGE src="images/suda_nsf_picture.jpg" alt=""/>[/pic]

[resinterests]Dr. Suda's research interests are in [interests]computer communication networks[/interests] and [interests]distributed computing systems[/interests] and span the entire spectrum from the design and performance evaluation of these systems to their actual implementation.  His current research focuses on [interests]application of biological principles[/interests] and large [interests]complex system principles onto networks, high speed networks, next generation Internet, ATM (Asynchronous Transfer Mode) networks, object-oriented distributed systems, and multimedia applications[/interests].[/resinterests]

<b> <font size=4> <IMAGE src="images/new.gif" alt=""/></font></b>We are looking for some undergraduate students interested in wireless communication technology. Background in or willingness to learn Linux, TCP/IP, 802.11b and C++ programming is preferred. (02/27/05) 


<b>Search</b>
<font size=2> WWW </font> <font size=2> NETGROUP </font>
