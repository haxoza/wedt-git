<font size=5><b><font size=4></font></b></font>

<font size=5><b><font size=4><font size=5></font></font></b></font><IMAGE src="../images/logo.gif" alt="home"/>

<font size=5><b>Chiou-Ting Hsu</b></font>
<IMAGE src="../images/people_bar.gif" alt="bar"/>

<IMAGE src="../images/newbar.gif" alt=""/>

<IMAGE src="../images/siteSearch.gif" alt=""/> <font size=2> </font>

<font size=2><IMAGE src="../images/powered_by_google.gif" alt=""/>
</font>

[pic]<IMAGE src="cthsu_s.jpg" alt=""/>[/pic]

[introduction]Chiou-Ting Hsu received the B.S. degree in [bsmajor]computer and information science[/bsmajor] from [bsuniv]National Chiao Tung University[/bsuniv], Hsinchu, Taiwan in [bsdate]1991[/bsdate], and the Ph.D. degree in [phdmajor]computer science and information engineering[/phdmajor] from [phduniv]National Taiwan University (NTU)[/phduniv], Taipei, Taiwan, in [phddate]1997[/phddate].

She was a post-doctoral researcher with the Communication and Multimedia Laboratory, NTU, from 1997 to 1998. From 1998 to 1999, she was with Philips Innovation Center Taipei, Philips Research, as a senior research engineer. She joined the [affiliation]Department of Computer Science, National Tsing Hua University[/affiliation], Hsinchu, Taiwan, as an Assistant Professor in 1999 and is currently an [position]Associate Professor[/position]. Her research interests include [interests]multimedia signal processing[/interests], [interests]image/video analysis and retrieval[/interests], [interests]digital watermarking[/interests], and [interests]data compression[/interests].

Prof. Hsu received the Citation Classic Award from Thomson ISI in 2001 for her paper "Hidden digital watermarks in images." She served as the Proceedings Chair in the third IEEE Pacific-Rim Conference on Multimedia (PCM 2002) and in the third International Conference on Information Technology: Research and Education (ITRE 2005). She also served on the program committees of several international and local conferences.[/introduction]

<IMAGE src="../images/people_bar.gif" alt="bar"/>

<font size=2>For problems or questions regarding this web site contact The Web Master.
Last updated: June 6, 2003.</font>
