[pic]<IMAGE src="me.png" alt="That's Me"/>[/pic]



[contactinfo]Dmitry Zinoviev

<b>[position]Associate Professor[/position] of Computer Science</b>

[affiliation]Department of Mathematics and Computer Science

Suffolk University[/affiliation]

[address]32 Derne St., Boston MA 02114, USA[/address]

<b>E-mail:</b> [email]dmitry@mcs.suffolk.edu[/email] (If I do not answer in 24 hrs, either your message or my reply has been lost in mail)

<b>Tel.:</b> [phone]617-305-1985[/phone] (leave a message)[/contactinfo]


Contents

  * Teaching

  * Research

  * Projects

  * Books

  * Photographs


Teaching The courses that I teach in the Fall 2006 are shown in <b>brown bold</b>.

<b>Undergraduate Courses</b>

<b>Graduate Courses </b>

  * CMPSC-F122 - Introduction into Computer Programming

  * CMPSC-F123 - Social and Technical Aspects of Computer Systems

  * SF-124 - <b>History of Computing & Computers</b>

  * CMPCS-F131 - Introduction into Computer Science

  * CMPCS-F132 - Intermediate Computer Science

  * CMPSC-F355 - Operating Systems

  * CMPCS-F365 - Introduction to Modeling and Simulation

  * CMPCS-F633 - Software Engineering

  * CMPSC-F635 - <b>Operating Systems/Distributed Systems </b>

  * CMPCS-F640 - User Interface Design

  * CMPSC-F643 - <b>Computer Graphics</b>


[resinterests]Research Interests My reasearch interests include:

  * [interests]Computer simulation and modelling[/interests]

      * [interests]Simulation of large computer networks[/interests]

      * [interests]Discrete Event System Specification (DEVS)[/interests]

  * [interests]Operating systems[/interests]

  * [interests]Software engineering[/interests]

      * [interests]Software metrics[/interests][/resinterests]

A list of my publications.


Projects

  * [interests]CLOWN -- computer system simulator for operating systems' studies[/interests]

  * [interests]Simulation of distributed mutual exclusion algorithms[/interests] (with A. Al-Shibli)

  * [interests]Representation of DEVS[/interests] (Discrete Event System Specifications) using UML (Unified Modeling Language) diagrams

  * [interests]Estimation of Web Site Complexity Using Pupil Dilation[/interests] (with A. Al-Shibli, M. Pomplun, D. Stefanescu, and Z. Xu)


[publication]Books

  1. ``Annie's CS101. A Charting Approach to Computer Programming,'' Lulu.com, 2006

  2. ``Data. Networks. Programs. Social and technical Aspects,'' Lulu.com, 2005

  3. ``A Gentle Introduction into Computer Programming using Python Language,'' Thomson Custom Publishing, 2002[/publication]


Photographs

  * University Commencement - 2006

  * University Commencement - 2005

  * Science Banquet - 2005

  * Presentation by Dr. Marc Pomplun - 2005

  * University Commencement - 2004


Useful Links

  * Python.org

  * RPMFind

  * PHP Manual

  * How to write a Makefile

  * Computer languages history

  * The Programmer's File Format Collection
