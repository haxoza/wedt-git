[pic]<IMAGE src="chile.jpg" alt=""/>[/pic]

<font size=5>Xiaofeng Ren</font>

[contactinfo][affiliation]Computer Science Division[/affiliation]
[address]545 Soda Hall
Berkeley, CA 94720-1776[/address]

Phone: [phone](510) 642-9940[/phone]
Fax: [fax](510) 642-5775[/fax]
Email: [email]xren@cs.berkeley.edu[/email][/contactinfo]



research / publications / teaching / personal

[introduction]I have been a [position]Ph.D. student[/position] in the [affiliation]Computer Vision Group at U.C. Berkeley[/affiliation]. My advisor is <font size=5>J</font>itendra Malik.

I have finished at Berkeley and moved to the Toyota Technological Institute at Chicago (TTI-C) as a research assistant professor. My thesis is on [interests]Probabilistic Models for Mid-level Vision[/interests].

My research is in the areas of [interests]computer vision[/interests] and [interests]artificial intelligence[/interests]. My current interests are mainly in [interests]Mid-level Vision[/interests] and its [interests]integration with both low-level image signal and high-level object knowledge[/interests]. I have worked on [interests]contour completion[/interests], [interests]image segmentation[/interests], [interests]figure/ground labeling[/interests] and [interests]human body pose recovery[/interests].[/introduction]


[resinterests]<font size=5>Research</font>

<IMAGE src="research/images/fgsegment.jpg" alt=""/> [interests]Cue Integration in Figure/Ground Labeling[/interests]

<IMAGE src="research/images/shapemes.jpg" alt=""/> [interests]Using Shapemes for Mid-level Vision[/interests]

<IMAGE src="research/images/crfcompletion.jpg" alt=""/> [interests]Scale-Invariant Contour Completion using Conditional Random Fields[/interests]

<IMAGE src="research/images/pairwise_im.jpg" alt=""/> <IMAGE src="research/images/pairwise_skeleton.jpg" alt=""/> [interests]Pairwise Constraints between Human Body Parts[/interests]

<IMAGE src="research/images/cdtgraph.jpg" alt=""/> A [interests]Scale-Invariant Image Representation: the CDT Graph[/interests]

<IMAGE src="research/images/segmentpeople_img.jpg" alt=""/> <IMAGE src="research/images/segmentpeople_seg.jpg" alt=""/> [interests]Human Body Configuration from Bottom-Up: a Segmentation-based Approach[/interests]

<IMAGE src="research/images/discrimsegment.jpg" alt=""/> [interests]Learning Discriminative Models for Image Segmentation[/interests]

<IMAGE src="research/images/superpixels.jpg" alt=""/> [interests]Superpixel: Empirical Studies and Applications[/interests]

<IMAGE src="research/images/naturalcontours.jpg" alt=""/> [interests]Contours in Natural Images and Scale Invariance[/interests][/resinterests]

[publication]<font size=5>Publications</font>

  * <b>Figure/Ground Assignment in Natural Images</b>. [abstract] [pdf] [ps] [bibtex]
    Xiaofeng Ren, Charless Fowlkes and Jitendra Malik, in ECCV '06, Graz 2006.
    <font size=1>
    </font>

  * <b>Learning Scale-Invariant Contour Completion</b>.
    Xiaofeng Ren, Charless Fowlkes and Jitendra Malik, submitted to IJCV Special Issue in Learning for Vision.
    <font size=1>
    </font>

  * <b>Cue Integration in Figure/Ground Labeling</b>. [abstract] [pdf] [bibtex]
    Xiaofeng Ren, Charless Fowlkes and Jitendra Malik, in NIPS '05, Vancouver 2005.
    <font size=1>
    </font>

  * <b>Recovering Human Body Configurations using Pairwise Constraints between Parts</b>. [abstract] [pdf] [bibtex]
    Xiaofeng Ren, Alex Berg and Jitendra Malik, in ICCV '05, volume 1, pages 824-831, Beijing 2005.
    <font size=1>
    </font>

  * <b>Scale-Invariant Contour Completion using Conditional Random Fields</b>. [abstract] [pdf] [ps] [bibtex]
    Xiaofeng Ren, Charless Fowlkes and Jitendra Malik, in ICCV '05, volume 2, pages 1214-1221, Beijing 2005.
    <font size=1>
    </font>

  * <b>Familiar Configuration Enables Figure/Ground Assignment in Natural Scenes</b>. [abstract] [poster] [bibtex]
    Xiaofeng Ren, Charless Fowlkes and Jitendra Malik, in VSS 05, Sarasota, FL 2005.
    <font size=1>
    </font>

  * <b>Mid-level Cues Improve Boundary Detection</b>. [abstract] [pdf] [ps] [bibtex]
    Xiaofeng Ren, Charless Fowlkes and Jitendra Malik, Berkeley Technical Report 05-1382, CSD 2005.
    <font size=1>
    </font>

  * <b>Recovering Human Body Configurations: Combining Segmentation and Recognition</b>. [abstract] [pdf] [ps] [bibtex]
    Greg Mori, Xiaofeng Ren, Alyosha Efros and Jitendra Malik, in CVPR '04, volume 2, pages 326-333, Washington, DC 2004.
    <font size=1>
    </font>

  * <b>Learning a Classification Model for Segmentation</b>. [abstract] [pdf] [ps] [bibtex]
    Xiaofeng Ren and Jitendra Malik, in ICCV '03, volume 1, pages 10-17, Nice 2003.
    <font size=1>
    </font>

  * <b>The Ecological Statistics of Good Continuation: Multi-scale Markov Models for Contours</b>. [abstract] [talk] [bibtex]
    Xiaofeng Ren and Jitendra Malik, in VSS 02, Sarasota, FL 2002.
    <font size=1>
    </font>

  * <b>A Probabilistic Multi-scale Model for Contour Completion Based on Image Statistics</b>. [abstract] [pdf] [ps] [bibtex]
    Xiaofeng Ren and Jitendra Malik, in ECCV '02, volume 1, pages 312-327, Copenhagen 2002.[/publication]
    <font size=1>
    </font>

<font size=5>Teaching</font>

I enjoy teaching and throughout the years I have TAed for the following courses:

Fall 2000 CS188 Introduction to Artificial Intelligence, U.C. Berkeley

Spring 1999 CS205 Mathematical Methods in Robotics and Vision, Stanford University

Winter 1999 CS249 Object-Oriented Programming, Stanford University

Fall 1996 Introduction to Mathematical Analysis, Zhejiang University


<font size=5>Personal</font>

I was born in the heavenly city of Hangzhou, China, shortly before the end of the Cultural Revolution. As I loved my hometown so much, I went to college there, in Zhejiang University, one of the oldest in the country. Then I decided to leave and see the world; and I have been living in California for quite a few years for my Ph.D. degree. Just like most of my friends I love history, poetry, and travel. I am happily married to a beautiful girl from Qingdao.







<IMAGE src="http://c8.statcounter.com/counter.php?sc_project=888817&java=0&security=847824cb&invisible=1" alt=""/>
