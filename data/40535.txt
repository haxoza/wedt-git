<font size=2> Albrecht Schmidt's Homepage</font> <font size=2> Publications & Presentations</font> <font size=2> Projects</font> <font size=2> Events</font> <font size=2>Ubicomp@Lancaster</font>

[contactinfo]<font size=7>Albrecht Schmidt
</font><b> [email]albrecht@comp.lancs.ac.uk[/email]</b>



<font size=5> [affiliation]Computing Department
Lancaster University[/affiliation], </font> <font size=5>UK</font>[/contactinfo]

<font size=1> Short Biography - Research Interest - Teaching - Publications - Events - Projects - Technical Issues & Examples - Private - Contact Details</font>

<IMAGE src="blue-1.gif" alt=""/> <IMAGE src="green-1.gif" alt=""/>

<IMAGE src="red-1.gif" alt=""/>

[pic]<IMAGE src="images/albrecht.jpg" alt=""/>[/pic]

[introduction]I am organizing<b> PERVASIVE 2005 - 3rd International Conference on Pervasive Computing</b> in Munich, Germany. It will take place in May 2005 - Submission deadline for full papers is October 6 2004.

Please visit <b> my new page at the Embedded Interaction Group (www.hcilab.org)</b> at the University of Munich - Ludwig-Maximilians-Universität München, LMU - in Germany. I moved there in Mai 2003.

Albrecht Schmidt received an [msdegree]MSc[/msdegree] in [msmajor]Computing[/msmajor] from [msuniv]Manchester Metropolitan University (UK)[/msuniv] in [msdate]1996[/msdate]. His master thesis was on modual neural networks. In [msdate]1997[/msdate] he finished his [msdegree]Master[/msdegree] in [msmajor]computer science[/msmajor] at [msuniv]the University of Ulm (Germany)[/msuniv]. As a research assistant he was working towards a PhD at TecO, University of Karlsruhe (Germany) from 1998 to 2001. He changed to Lancaster University (UK) where he is in now the final year of his PhD. Albrecht Schmidt is working in the area of situated interaction and context-awareness in the research field of [interests]ubiquitous computing[/interests].

More information is available on my old page at TecO.

My new page at the University of Munich


Short Biography

<b> Interacting with the Ubiquitous Computer
slides from my keynote @ Mobile HCI 03 in Udine, Italy. (Sept 2003)</b>

<b> I moved to the Media Informatics Group at the University of Munich in Germany (Mai 2003)</b>

<b> "Ubiquitous Computing - Computing in Context" - my PhD thesis is online available - the thesis is about context-awarness, context aware computing, and ubiquitous computing</b>[/introduction]

<b> Check out the preliminary call for Ubicomp 2003</b>

<font size=4> Lancaster Smart-its Hardware Tutorial (Smart-Its Atelier 07/02)</font>

<font size=4> Some new ubicomp development pages are avialble now (Hardware, PICs, ...)</font>

<b><font size=5>News</font></b>

Albrecht Schmidt is interested in the use of sensors to gain context, models and mechanisms to distribute and provide context, and the resulting implications for human computer interaction.

Albrecht Schmidt had a leading role in hard- and software development for the European project 'Technology for Enabling Awareness (TEA)'. In the project a component that can supply context information to mobile devices, such as mobile phones, PDAs, and wearable computers was developed. The context is calculated from a number of different sensors (e.g. light, acceleration, temperature, microphones, touch) using a microcontroller. http://www.teco.edu/tea/

He was also working on further projects such as 'Aware Goods' and 'Implicit Human Computer Interaction in Wearable Computing'. These projects that were conducted in cooperation with SAP research explored the uses of context in business applications.

Currently Albrecht Schmidt is involved in the project 'Smart-Its' that envision small-scale smart devices that can be attached to mundane everyday artefacts to augment these with a "digital self". http://www.smart-its.org/


[resinterests]Research Interest

At the University of Karlsruhe Albrecht Schmidt was teaching <b>a class on [interests]Web-Technology[/interests] and [interests]Web-Engineering[/interests]</b>. He also offered a <b> practical course on [interests]Web-Technology[/interests]</b>. The courses covered various different topics, e.g. [interests]HTTP details[/interests], [interests]writing a web-server and a client[/interests], [interests]client-server programming[/interests], [interests]web development[/interests], [interests]site management[/interests], [interests]ubiquitous web[/interests], [interests]web-based control systems[/interests], etc.[/resinterests]


Teaching

Here is a selection of papers I like most out of about 40 publications I was co-authoring.
For a full list go to the publications page...

  * 

    A Schmidt, K Van Laerhoven, M Strohbach, A Friday and HW Gellersen
    <b> Context Acquistion based on Load Sensing
    </b>In Proceedings of Ubicomp 2002, G. Boriello and L.E. Holmquist (Eds). Lecture Notes in Computer Science, Vol 2498, ISBN 3-540-44267-7; Springer Verlag, Göteborg, Sweden. September 2002, pp. 333 - 351.  See the slides from the talk at ubicomp 2002

  * 

    A. Schmidt, M. Strohbach, K. Van Laerhoven, and H.W. Gellersen
    <b> Ubiquitous Interaction - Using Surfaces in Everyday Environments as Pointing Devices
    </b>7th ERCIM Workshop "User Interfaces For All", 23 - 25 October, 2002,  LNCS

  * 

    H.W. Gellersen, A. Schmidt and M. Beigl
    <b> Multi-Sensor Context-Awareness in Mobile Devices and Smart Artifacts
    </b>in Mobile Networks and Applications (MONET), Oct 2002

  * 

    A. Schmidt, and K. Van Laerhoven
    <b>How to Build Smart Appliances?
    </b>IEEE Personal Communications 8(4), August 2001. pp. 66-71.

  * 

    A. Schmidt, A. Takaluoma and J. Mäntyjärvi
    <b>Context-Aware Telephony over WAP
    </b>Personal Technologies 4(4), December 2000. pp. 225-229.

  * 

    A. Schmidt, M. Beigl and H.W. Gellersen
    <b> There is more to context than location
    </b>Computer & Graphics 23(6), December 1999, pp. 893-901.

  * 

    A. Schmidt, K.A. Aidoo, A. Takaluoma, U. Tuomela, K. Van Laerhoven and W. Van de Velde
    <b>Advanced Interaction in Context
    </b>Proc. of First International Symposium on Handheld and Ubiquitous Computing (HUC99), Karlsruhe, Germany, September 1999, LNCS 1707, Springer-Verlag, pp. 89-101.

more publications ....


Publications

[resactivity]Some event that are related to the research areas were I was involved in:

  * Ubicomp 2002, September 30 - October 2, 2001, Atlanta, USA

  *  MobileHCI  2002,  - 18-20 September 2002, Pisa, Italy

  * Ubicomp 2001, September 30 - October 2, 2001, Atlanta, USA

  * MobileHCI Workshop 2001,  - 10 September 2001, Lille, France

  *  CHI2001 Workshop - Distributed and Disappearing User Interfaces in Ubiquitous Computing

  *  M&C 2001 Workshop - WAP - Interaktionsdesign und Benutzbarkeit

  * WWW10. 10th World Wide Web conference.

  *  CHI2000 Workshop - Situated Interaction in Ubiquitous Computing

  * HUC 99 - Int. Symposium on Handheld and Ubiquitous Computing[/resactivity]


Events

This is a selection of projects that I was concerned with and students that I supervised:

  * European Project Smart-Its (technical details on the vision smart-It)

  *  European Project TEA - Technology for Enabling Awareness

  * Project Aware Good in cooperation with SAP corporate research, Master thesis of Anke Thede

  * Context Phone. Master student of Tanjev Stuhr

  *  Student project Traffics Lights - Ambient Displays. Students: Christian Decker, Götz Mayser

  * ...


Projects

In various courses and projects different examples have been developed. Here are some of them:

  *  Some software and hardware examples to download

  *  Simple Web Server in C

  *  Simple Web Browser in VisualBasic

  *  Simple Web Client in Perl using LWP

  * Basic programs in C for the PIC-microcontroller (CCS)

  *  Examples for network programming in C

  *  Programming the Beck IPC WebChip


Technical Issues & Examples

Still to do - Have a look at my page at TecO


[contactinfo]Private

<b>Albrecht Schmidt</b>, MSc, Dipl. Inf.

[address]Computing Department
Engineering Building, Room A13
Lancaster University
Lancaster, UK
LA1 4YR[/address]

Tel: [phone]+44 (0) 1524 593675[/phone]
Fax: [phone]+44 (0) 1524 593608[/phone]

E-Mail: [email]albrecht@comp.lancs.ac.uk[/email]
Web: [homepage]http://www.comp.lancs.ac.uk/~albrecht/[/homepage][/contactinfo]


Contact Details<IMAGE src="http://148.88.250.76/sercgi?b" alt=""/>

>
