[pic]<IMAGE src="face.jpg" alt="picture"/>[/pic]



[contactinfo]Subhash Suri

[position]Professor[/position]
[affiliation]Department of Computer Science[/affiliation]
[address]Engineering I, Room 2111
University of California, Santa Barbara, 93106[/address]

<b>TEL:</b> [phone](805) 893-8856[/phone]
<b>FAX:</b> [fax](805) 893-8553[/fax]
<b>EMAIL:</b> [email]suri at cs.ucsb.edu[/email][/contactinfo]

<IMAGE src="picdir/brgld.gif" alt="o"/>


[education]Education.

  *  [phduniv]Johns Hopkins University[/phduniv], [phddegree]Ph.D.[/phddegree] in [phdmajor]Computer Science[/phdmajor], [phddate]1987[/phddate].

  *  [bsuniv]University Of Roorkee, India[/bsuniv], [bsdegree]B.S.[/bsdegree] in [bsmajor]Electronics Engineering[/bsmajor], [bsdate]1981[/bsdate].[/education]


Research and Publications.

  *  [resinterests]Research Interests: [interests]Algorithms[/interests], [interests]Networked Sensing[/interests], [interests]Data Streams[/interests], [interests]Computational Geometry[/interests], [interests]Game Theory[/interests].[/resinterests]

  *  Message for Prospective Students.

  *  Publications available online.

  *  Research funding.

  *  Slides of some recent talks.


News and Upcoming Events.

  *  I will be spending the academic year '06-'07 at the Swiss Federal Institute of Technology (ETH, Zurich)
    as a Gastprofessor (Visiting Scholar) within the Institute of Theoretical Computer Science.

  *  With Leo Guibas and Alon Efrat, I am organizing the
    <b> NSF Workshop on Geometric Approaches to Ad Hoc and Sensor Networks </b>, at UCSB, June 12-13, 2006.

  *  2nd Research Workshop on Flexible Network Design, Oct 2-6, 2006, Bertinoro, Italy.

  *  IIT Kanpur Workshop on Data Streams, Dec. 18-20, 2006, Kanpur, India.

  *  With Roger Wattenhofer and Peter Widmayer, I am organizing the
    <b> Schloss Dagstuhl Workshop on Geometry in Sensor Networks </b>, April 9-13, 2007.


Recent Courses.

  *  <b>CS 130b: Data Structures and Algorithms II,</b> <b> Spring '06 quarter</b>.

  *  <b>CS 130a: Data Structures and Algorithms I,</b> <b> Winter '06 quarter</b>.

  *  <b>CS 231: Algorithms for the Internet Age,</b> <b> Fall '05 </b>.

  *  <b>Euclid Bytes the Dust: Geometry in Sensor Networks,</b> <b> Seminar, CS 595 Spring '05</b>.


[resactivity]Selected Program Committees.

  *  Member, Program Committee for ACM SenSys '07, Sydney, Australia, Nov. 2007.

  *  Member, Program Committee for DCOSS 2007, Distributed Computing in Sensor Systems, Santa Fe, New Mexico, 2007.

  *  Member, Program Committee for IPSN 2007, Information Processing in Sensor Networks, MIT, Cambridge, MA, 2007.

  *  Member, Program Committee for ALENEX 2007, 9th Annual Workshop on Algorithm Engineering and Experiments, New Orleans, 2007.

  *  Member, Program Committee for MFI 2006, Int. Conference on Multisensor Fusion and Integration, Heidelberg, 2006.

  *  Member, Program Committee for ICPADS, 12th International Conference on Parallel and Distributed Systems, Minneapolis, 2006.

  *  Member, Program Committee for ESA 2005 , 13th Annual European Symposium on Algorithms, Ibiza, Oct. 3-6, 2005.

  *  Member, Program Committee for WWW 2005 , 14th International World Wide Web Conference, Chiba, Japan, May 10-14, 2005.

  *  Member, Program Committee for TCS 2004, Toulouse, France, August 23-26, 2004.

  *  Member, Program Committee for ACM Conference on Electronic Commerce '04 , New York, May 17-20, 2004.

  *  Member, Program Committee for ACM-SIAM SODA '04 , New Orleans, Jan 11-13, 2004.

  *  Member, Program Committee for AAAI-2002, Edmonton, Canada, July 28 - August 1, 2002.

  *  Chair, Program Committee (Theory) for ACM SoCG '02, Barcelona, June 4-7, 2002.

  *  Member, Program Committee for ACM-SIAM SODA '02 , San Francisco, Jan 6-8, 2002.[/resactivity]


Visitors and Post-docs.

  * Csaba Toth. (Postdoc: 2002-2004. Now at MIT.)


Current and Past Students.

  * Sorabh Gandhi (Ph.D.)

  * Nisheeth Shrivastava (Bell Labs, India. Ph.D. 2006)

  * Chiranjeeb Buragohain (Amazon. Ph.D. 2006)

  *  Anshul Kothari. (Google. Ph.D. 2005)

  *  Amit Bhosle. (Amazon.com. MS 2003.)

  *  Matthew Maxel. (US Navy. MS 2003.)

  *  Yunhong Zhou. (HP Labs. Ph.D. 2000.)

  *  Priyank Ramesh Warkhede (Cisco. MS 2000.)

  *  Adam Smith (Microsoft. MS 1999)

  *  Mingquan Xue (Microsoft, MS 1998.)

<IMAGE src="picdir/brgld.gif" alt="o"/>
