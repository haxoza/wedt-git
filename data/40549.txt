<IMAGE src="/common/images2003/row1.gif" alt=""/>

<IMAGE src="/common/images2003/btn_wolfram.gif" alt="Wolfram Research"/><IMAGE src="/common/images2003/mid-backangle.gif" alt=""/><IMAGE src="/common/images2003/btn_products.gif" alt="Products"/><IMAGE src="/common/images2003/btn_purchasing.gif" alt="Purchasing"/><IMAGE src="/common/images2003/btn_services_on.gif" alt="Services & Resources"/><IMAGE src="/common/images2003/btn_new.gif" alt="News & Events"/><IMAGE src="/common/images2003/btn_company.gif" alt="Company"/><IMAGE src="/common/images2003/row2_backangle.gif" alt=""/><IMAGE src="/common/images2003/btn_webresource.gif" alt="Other Wolfram Sites"/>

<IMAGE src="/common/images2003/btm_blackbar.gif" alt=""/><IMAGE src="/common/images2003/btm_blackbar2.gif" alt=""/> <IMAGE src="/common/images2003/row3_frontangle.gif" alt=""/>

<IMAGE src="/common/images2003/btn_search.gif" alt="Search Site"/> <IMAGE src="/common/images2003/spacer.gif" alt=""/>

<IMAGE src="/common/images2003/row3_backangle.gif" alt=""/>

<IMAGE src="/common/images/spacer.gif" alt=""/>

<IMAGE src="/services/education/images/weg-header.gif" alt=""/><IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images/spacer.gif" alt=""/> Services & Resources

<IMAGE src="/common/images2003/spacer.gif" alt=""/>

<IMAGE src="/common/images2003/spacer.gif" alt="-----"/>

<IMAGE src="/common/images2003/linkangle.gif" alt=" / "/> <IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/sidebar-topdownslash.gif" alt=""/>

<IMAGE src="/common/images/spacer.gif" alt=""/> <b>Wolfram Education Group</b>

<IMAGE src="/common/images/spacer.gif" alt=""/>

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images/spacer.gif" alt=""/>

<IMAGE src="/common/images/spacer.gif" alt=""/>

<IMAGE src="/common/images2003/lnk_bullet1.gif" alt="*"/> Training Calendar and Registration

<IMAGE src="/common/images2003/lnk_bullet1.gif" alt="*"/> Course Descriptions

<IMAGE src="/common/images2003/lnk_bullet1.gif" alt="*"/> Course Q&A Sessions

<IMAGE src="/common/images2003/lnk_bullet1.gif" alt="*"/> Mini-Course Descriptions

<IMAGE src="/common/images2003/lnk_bullet1.gif" alt="*"/> Free Seminars

<IMAGE src="/common/images2003/lnk_bullet1.gif" alt="*"/> Online Training Information

<IMAGE src="/common/images2003/lnk_bullet1.gif" alt="*"/> Scheduling Policy Information

<IMAGE src="/common/images2003/lnk_bullet1.gif" alt="*"/> About Wolfram Education Group

<IMAGE src="/common/images2003/lnk_bullet1.gif" alt="*"/> Q&A

<IMAGE src="/common/images2003/lnk_bullet1.gif" alt="*"/> Education Benefits Program

<IMAGE src="/common/images2003/lnk_bullet1.gif" alt="*"/> What Attendees Are Saying

<IMAGE src="/common/images2003/lnk_bullet1.gif" alt="*"/> Courseware Developer Program

<IMAGE src="/common/images2003/lnk_bullet1.gif" alt="*"/> Certified Instructor Program

<IMAGE src="/common/images2003/lnk_bullet1.gif" alt="*"/> <b>Meet the Instructors</b>

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Paul Abbott

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Emmanuel Amiot

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Rémi Barrère

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Adam Berry

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Kurt Bøge

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Richard Bréhéret

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> John Browne

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Harry Calkins

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Ian Collier

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Nabil Fares

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Enrique Garcia Moreno Esteva

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Darren Glosemeyer

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Allan Hayes

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Carsten Herrmann

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Dale Horton

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Éric Jacopin

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Mariusz Jankowski

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Rasmus Kamper

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Akiko Kanamitsu

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Maryam Karbalai

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Qutaibeh Katatbeh

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Rob Knapp

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Roman Maeder

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Rolf Mertig

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Ed Packel

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_arrow.gif" alt="<"/> <b>Yves Papegay</b>

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Schoeller Porter

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Medhat Rakha

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Philip Ramsden

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Ralf Rosenberger

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Anton Rowe

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Onkar Singh

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Jonas Sjöberg

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Paul Wellin

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Chris Williamson

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/lnk_bullet2.gif" alt="*"/> Jacqueline Zizi

<IMAGE src="/common/images2003/lnk_bullet1.gif" alt="*"/> On-Site Training at Your Location

<IMAGE src="/common/images2003/lnk_bullet1.gif" alt="*"/> Contact Wolfram Education Group

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images/spacer.gif" alt=""/>

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/sidebar-upslash.gif" alt=""/>

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images/spacer.gif" alt=""/>

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/sidebar-downslash.gif" alt=""/>

<IMAGE src="/common/images/spacer.gif" alt=""/>

<IMAGE src="/common/images2003/lnk_bullet1.gif" alt="*"/> Wolfram Information Center

<IMAGE src="/common/images2003/lnk_bullet1.gif" alt="*"/> Wolfram Research Calendar of Events

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images/spacer.gif" alt=""/>

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/sidebar-upslash.gif" alt=""/>

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images/spacer.gif" alt=""/>

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/sidebar-downslash.gif" alt=""/>

<IMAGE src="/common/images/spacer.gif" alt=""/>

<IMAGE src="/common/images2003/lnk_bullet1.gif" alt="*"/> Ask about this page

<IMAGE src="/common/images2003/lnk_bullet1.gif" alt="*"/> Print this page

<IMAGE src="/common/images2003/lnk_bullet1.gif" alt="*"/> Email this page

<IMAGE src="/common/images2003/lnk_bullet1.gif" alt="*"/> Give us feedback

<IMAGE src="/common/images2003/lnk_bullet1.gif" alt="*"/>

Sign up for our newsletter:

<IMAGE src="/common/images2003/spacer.gif" alt=""/>

<IMAGE src="/common/images2003/spacer.gif" alt=""/>

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images/spacer.gif" alt=""/>

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images2003/sidebar-upslash.gif" alt=""/>

<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images/spacer.gif" alt=""/>

<IMAGE src="/common/images/spacer.gif" alt=""/>


[introduction]Meet the Instructors: Yves Papegay

[pic]<IMAGE src="images/papegay.jpg" alt="Yves Papegay"/>[/pic]

<b>Organization/Location</b>
[affiliation]INRIA Sophia-Antipolis, France[/affiliation]

<b>Degrees</b>
[phddegree]Ph.D.[/phddegree] in [phdmajor]Computer Sciences[/phdmajor], [phduniv]University of Nice[/phduniv]

<b>Wolfram Education Group Courses Certified to Teach</b>
M101: Mathematica, premier cours[/introduction]

<b>Teaching Experience</b>
Yves Papegay has had 15 years of experience teaching mathematics and computer sciences to post-high school, undergraduate, and graduate students at the University of Nice in France and the University of French Polynesia in Tahiti. He has also been involved in several computer algebra courses for industry and education in which he lectured on Mathematica.

<b>Mathematica Experience</b>
Since 1994 Papegay has done research at the French National Institute for Research in Computer Science and Control. He works in a team that specializes in computer algebra. Most of his work is concerned with the application of symbolic computation tools and methods of modeling and simulation and includes the development of industrial prototypes in Mathematica.

<b>Language Fluency</b>
French, English, German, and some Japanese

[resinterests]<b>Interests</b>
[interests]Architecture[/interests], [interests]music and photography[/interests], [interests]digital processing of sound and images[/interests], and [interests]2D geometry software[/interests][/resinterests]

<IMAGE src="/common/images/spacer.gif" alt=""/>



<IMAGE src="/common/images/spacer.gif" alt=""/> <IMAGE src="/common/images/spacer.gif" alt=""/>

<IMAGE src="/common/images2003/spacer.gif" alt=""/> ?2007 Wolfram Research, Inc. <IMAGE src="/common/images2003/footer-separator.gif" alt=" | "/> <IMAGE src="/common/images2003/lang_bottom_fr.gif" alt="[fr]"/>
