<IMAGE src="../img/main/logo.gif" alt=""/> <IMAGE src="../img/main/tagline_earth.jpg" alt=""/>

<IMAGE src="../img/main/nav_about_off.gif" alt=""/> <IMAGE src="../img/main/nav_diseases_off.gif" alt=""/> <IMAGE src="../img/main/nav_research_on.gif" alt=""/> <IMAGE src="../img/main/nav_training_off.gif" alt=""/> <IMAGE src="../img/main/nav_sci-ed_off.gif" alt=""/> <IMAGE src="../img/main/nav_howhelp_off.gif" alt=""/> <IMAGE src="../img/main/nav_news_off.gif" alt=""/> <IMAGE src="../img/main/earth_bottom.gif" alt=""/>

<IMAGE src="../img/main/fill_dadefo.gif" alt=""/>

<IMAGE src="../img/main/spacer.gif" alt=""/><IMAGE src="../img/main/spacer.gif" alt=""/>

<IMAGE src="../img/main/spacer.gif" alt=""/>

<IMAGE src="../img/main/leftnav_box_top_research.gif" alt=""/>

<IMAGE src="../img/main/spacer.gif" alt=""/> Objectives <IMAGE src="../img/main/spacer.gif" alt=""/>

<IMAGE src="../img/main/leftnav_box_sub-box_top.gif" alt=""/>

<IMAGE src="../img/main/leftnav_box_sub-box_left.gif" alt=""/>

Scientists

<IMAGE src="../img/main/spacer.gif" alt=""/> <IMAGE src="../img/main/spacer.gif" alt=""/>

 . Gerard Cangelosi

<IMAGE src="../img/main/spacer.gif" alt=""/> <IMAGE src="../img/main/spacer.gif" alt=""/>

 . Patrick Duffy

<IMAGE src="../img/main/spacer.gif" alt=""/>

 . Jean Feagin

<IMAGE src="../img/main/spacer.gif" alt=""/>

 . Nancy Freitag

<IMAGE src="../img/main/spacer.gif" alt=""/>

 . Malcolm Gardner

<IMAGE src="../img/main/spacer.gif" alt=""/>

 . Nancy Haigwood

<IMAGE src="../img/main/spacer.gif" alt=""/>

 . Stefan Kappe

<IMAGE src="../img/main/spacer.gif" alt=""/>

 . Peter Myler

<IMAGE src="../img/main/spacer.gif" alt=""/>

 . Marilyn Parsons

<IMAGE src="../img/main/spacer.gif" alt=""/>

 . Arnold Smith

<IMAGE src="../img/main/spacer.gif" alt=""/>

 . Joseph Smith

<IMAGE src="../img/main/spacer.gif" alt=""/>

 . Leonidas Stamatatos

<IMAGE src="../img/main/spacer.gif" alt=""/>

 . Ken Stuart

<IMAGE src="../img/main/spacer.gif" alt=""/>

 . Ruobing Wang

<IMAGE src="../img/main/spacer.gif" alt=""/>

 . Theodore White

<IMAGE src="../img/main/lefnav_box_sub-box_bot.gif" alt=""/>

<IMAGE src="../img/main/spacer.gif" alt=""/> Associate Scientists <IMAGE src="../img/main/spacer.gif" alt=""/>

<IMAGE src="../img/main/spacer.gif" alt=""/>

<IMAGE src="../img/main/spacer.gif" alt=""/> Collaborations <IMAGE src="../img/main/spacer.gif" alt=""/>

<IMAGE src="../img/main/spacer.gif" alt=""/>

<IMAGE src="../img/main/spacer.gif" alt=""/> Core Technologies <IMAGE src="../img/main/spacer.gif" alt=""/>

<IMAGE src="../img/main/spacer.gif" alt=""/> <IMAGE src="../img/main/spacer.gif" alt=""/> <IMAGE src="../img/main/spacer.gif" alt=""/>

<IMAGE src="../img/main/leftnav_box_bot.gif" alt=""/>

<IMAGE src="../img/main/spacer.gif" alt=""/>


<IMAGE src="../img/main/spacer.gif" alt=""/>

<IMAGE src="../img/main/spacer.gif" alt=""/><IMAGE src="../img/main/spacer.gif" alt=""/>

<IMAGE src="../img/main/corner_curve.gif" alt=""/> <IMAGE src="../img/main/spacer.gif" alt=""/> <IMAGE src="../img/main/spacer.gif" alt=""/>

<IMAGE src="../img/titles/ttl_research_scientists.gif" alt=""/>

<IMAGE src="../img/main/spacer.gif" alt=""/>

<IMAGE src="../img/main/spacer.gif" alt=""/> <IMAGE src="../img/main/spacer.gif" alt=""/> <IMAGE src="../img/main/spacer.gif" alt=""/>

<IMAGE src="../img/research/rtnav_box_top_new.gif" alt=""/>

. Biography

<IMAGE src="../img/main/spacer.gif" alt=""/>

. Publications

<IMAGE src="../img/main/spacer.gif" alt=""/>

. Staff

<IMAGE src="../img/main/spacer.gif" alt=""/>

. Links

<IMAGE src="../img/research/rtnav_box_bot.gif" alt=""/>

<IMAGE src="../img/main/spacer.gif" alt=""/>

<IMAGE src="../img/main/spacer.gif" alt=""/>
[introduction]Arnold Smith, M.D.

[pic]<IMAGE src="../img/research/arnold.smith.gif" alt=""/>[/pic] <IMAGE src="../img/main/spacer.gif" alt=""/>

<b> [position]Member[/position], [affiliation]Seattle Biomedical Research Institute[/affiliation]</b>
[position]Professor[/position], [affiliation]Department of Pathobiology,  University of Washington[/affiliation][/introduction]
[contactinfo]Email: [email]arnold.smith@sbri.org[/email][/contactinfo]

Organism under study: Haemophilus influenzae

<IMAGE src="../img/main/spacer.gif" alt=""/>

<b>Mission</b>
Dr. Smith's research on [interests]Haemophilus influenzae[/interests] seeks to understand how this bacterium causes disease.  Such understanding will lead to improved treatment and prevention.

<b>Research</b>
The laboratory of Dr. Smith seeks to understand the molecular mechanisms by which H. influenzae colonizes the human respiratory tract and after colonization how it causes local or systemic disease.  Local infections of the respiratory tract under investigation include otitis media, particularly the recalcitrant chronic serous sequelae of infants, acute otitis media and chronic bronchitis. 

Certain strains of H. influenzae have the ability to invade across the respiratory tract of normal children immunized against type b strains and after invasion replicate in the blood stream.  Once in the blood they can cause sepsis and meningitis.  These strains lack the genes for the synthesis of the type b (and other) capsule and cause disease by a novel mechanism, which is under study.

The Smith lab investigates virulence mechanisms of nontypeable (unencapsulated) Haemophilus influenzae (NTHi).  A human-restricted Gram-negative bacterium, H. influenzae causes mucosal infections resulting in sinusitis, pneumonia, and bronchitis as well as invasive diseases such as bacteremia and meningitis.  One significant public health problem in children caused by NTHi is otitis media with effusion (OME).  This disease is often recurrent, even after antibiotic treatment, and is characterized by biofilm formation in the middle ear.  Dr. Smith discovered that a gene which synthesizes autoinducer-2 (AI-s) is present in all nontypeable H. influenzae strains studied and its inactivation results in a number of pleiotropic effects.  The role of AI-2 in H. influenzae biofilm development is being investigated in both an in vitro and an in vivo model.

Although a polysaccharide conjugate vaccine is currently available against the capsule type b H. influenzae (HiB), it is not protective against NTHi.  Another ongoing project in the Smith lab is the further characterization of an invasive nontypeable serum-resistant strain of H. influenzae, R2866.  The phenotype of serum resistance appears to be due to the bacterium's evasion of the complement system.  Understanding the mechanism by which the strain interferes with the deposition of complement without the benefit of encapsulation will be useful in engineering a conjugate H. influenzae vaccine that is efficacious against invasive nontypeable strains.

The complete genome sequence of an unencapsulated H. influenzae laboratory strain, Rd KW20, was published in 1995. However, NTHi have been shown to be highly genetically heterogeneous, with invasive strains carrying approximately 250 kb more DNA in their chromosomes than Rd KW20. Sequencing of this "extra" DNA will identify genes required for human colonization and disease. In collaboration with the University of Washington Genome Center, the Smith lab is determining the complete genomic sequence of two pathogenic NTHi and investigating the role of putative new virulence genes in two models of infection. The incomplete genome sequences are available at NCBI.

[resinterests]<b>Themes</b>
*  [interests]evasion of innate and acquired immunity by invasive nontypeable H. influenzae[/interests]
*  [interests]role of quorum sensing in H. influenzae causing otitis media[/interests]
*  [interests]mechanisms used by H. influenzae to cause disease[/interests][/resinterests]

<b>Program</b> <b>Accomplishments</b>
Dr. Smith spent the first 10 years of his academic career at Harvard Medical School as part of a team that developed a vaccine to prevent life-threatening Haemophilus influenzae infections.  Following the success of that vaccine, he spent 15 years researching better antibiotic treatment for children with cystic fibrosis at Children's Hospital and Regional Medical Center in Seattle.  At the University of Missouri, he again studied H. influenzae since antibiotic resistance and vaccine-resistant strains which began emerging.

<b>Support for Dr. Smith’s current research is provided by the National Institutes of Health (NIH).</b>

<IMAGE src="../img/main/spacer.gif" alt=""/> <IMAGE src="../img/main/spacer.gif" alt=""/>

<IMAGE src="../img/main/spacer.gif" alt=""/>

HOME | CONTACT US | SITEMAP | DONATE

<IMAGE src="../img/main/spacer.gif" alt=""/>

©2006 Seattle Biomedical Research Institute. All rights reserved. info@sbri.org

<IMAGE src="../img/main/spacer.gif" alt=""/>
