[pic]<IMAGE src="296.jpg" alt=""/>[/pic]



Gregory Cooper


Research

I am the developer of FrTime, an extension of PLT Scheme with first-class events and behaviors. With these features, programmers write event-driven programs as declarative models, without using callbacks or imperative operations. A program expresses dataflow relationships between events and behaviors (known collectively as signals), and the language maintains the relationships. FrTime is inspired and informed by work on functional reactive programming, which originated in the Haskell community.

A key design goal in FrTime is to make it feel like Scheme, rather than a completely new language. FrTime has been distributed with DrScheme since version 208 (released late 2003). More recently, we have adapted its evaluation model to JavaScript, creating a new language called Flapjax, which is designed for writing modern Web applications.


[publication]Publications

FLOPS 2006 â?Ignatoff, Cooper, and Krishnamurthi.
Adapting Object-Oriented Frameworks to Functional Reactive Languages

ASE Journal 2006 â?Marceau, Cooper, Spiro, Krishnamurthi, and Reiss.
The Design and Implementation of a Dataflow Language for Scriptable Debugging

ESOP 2006 â?Cooper and Krishnamurthi.
Embedding Dynamic Dataflow in a Call-by-Value Language

ASE 2004 â?Marceau, Cooper, Krishnamurthi, and Reiss.
A Dataflow Language for Scriptable Debugging

Real-Time Systems vol. 20, no. 2 (March 2001) â?DiPippo, Fay-Wolfe, Esibov, Cooper, Bethmangalkar, Johnston, Thuraisingham, and Mauer.
Scheduling and Priority-Mapping for Static Real-Time Middleware

TPDS vol. 11, no. 10 (October 2000) â?Fay-Wolfe, DiPippo, Cooper, Johnston, Kortmann, and Thuraisingham.
Real-Time CORBA

ISORC 1998 â?Squadrito, Esibov, DiPippo, Fay-Wolfe, Cooper, Thuraisingham, Krupp, Milligan, and Johnston.
Concurrency Control in Real-Time Object-Oriented Systems: the Affected Set Priority Ceiling Protocols[/publication]


[contactinfo]Contact

[address]Box 1910, Computer Science Department
Brown University
Providence, RI 02912[/address]
[phone](401) 863-7668[/phone] (voice)
[fax](401) 863-7657[/fax] (fax)[/contactinfo]

<IMAGE src="brown-email.png" alt=""/>

<IMAGE src="/icons/home.gif" alt="Home"/> <IMAGE src="/icons/people.gif" alt="People"/>
