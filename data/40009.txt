Home Page of Dott. Giovanni Pezzulo

<b>Home</b> | <b>About ISTC</b> | <b>Spotlights</b> | <b>Research</b> | <b>Downloads</b> | <b>Administration</b> | <b>Resources</b>


[pic]<IMAGE src="/img/pictures/1gio.jpg" alt=""/>[/pic]

[contactinfo]<b>Address: </b>[address]ISTC-CNR, Via S. Martino della Battaglia 44, I-00185 Rome, Italy[/address]

<b>Telephone: </b>[phone]+39 6 44595206[/phone]

<b>Mobile: </b>

<b>Email: </b>[email]giovanni.pezzulo@istc.cnr.it[/email][/contactinfo]

<b>Curriculum Vitae: </b>View it or download it (~62Kb)

<b>Personal Home Page: </b>


[resinterests]Research interests

[interests]cognitive science[/interests], [interests]philosophy of science[/interests], [interests]artificial intelligence[/interests][/resinterests]


[publication]Publications

Journals

  1. <b>Giovanni Pezzulo and Gianguglielmo Calvi</b> (2006). A Schema Based Model of the Praying Mantis. From animals to animats 9: Proceedings of the Ninth International Conference on Simulation of Adaptive Behaviour, Springer Verlag LNAI 4095, , 211-223.
    <b>Abstract:</b> We present a schema-based agent archite...
      (Related website: http://www.springerlink.com/content/v144835602k04l52/?p=07964f0d334d4470955cffdcab855b4dπ=2)
    Download ~324Kb

  2. <b>Giovanni Pezzulo and Gianguglielmo Calvi</b> (2005). Dynamic Computation and Context Effects in the Hybrid Architecture AKIRA . Modeling and Using Context: 5th International and Interdisciplinary Conference CONTEXT 2005. , Springer LNAI 3554, .
    <b>Abstract:</b> We present AKIRA, an agent-based hybrid...
      (Related website: http://springerlink.metapress.com/link.asp?id=48v8hlhpphpbye9f)
    Download ~150Kb

  3. <b>Giovanni Pezzulo and Gianguglielmo Calvi</b> (2005). Designing and Implementing MABS in AKIRA . Multi-Agent and Multi-Agent-Based Simulation: Joint Workshop MABS 2004, Springer LNCS 3415, , .
    <b>Abstract:</b> Here we present AKIRA, a framework for ...
      (Related website: http://www.springerlink.com/link.asp?id=dkuwj4hv5v010lvv)
    Download ~1300Kb

  4. <b>Giovanni Pezzulo, Gianguglielmo Calvi, Daniela Lalia, Dimitri Ognibene</b> (2005). Fuzzy-based Schema Mechanisms in AKIRA. CIMCA '05: Proceedings of the International Conference on Computational Intelligence for Modelling, Control and Automation and International Conference on Intelligent Agents, Web Technologies and Internet Commerce Vol-2 (CIMCA-IAWTIC'06), IEEE Computer Society, , 146-152.
    <b>Abstract:</b> We compare Action Selection and Schema ...
    Download ~281Kb

  5. <b>Bernardo Magnini, Carlo Strapparava, Giovanni Pezzulo and Alfio Gliozzo</b> (2002). The Role of Domain Information in Word Sense Disambiguation. Journal of Natural Language Engineering, 8 no. 4, 359-373.

  6. <b>Giovanni Pezzulo</b> (1999). Il continuum percezione - rappresentazione - spiegazione. Nuova Civiltà delle Macchine, 1, .
    Download ~193Kb

top

Chapters

  1. <b>Falcone R., Pezzulo G., Castelfranchi C. and Calvi G.</b> (2005). Contract Nets for Evaluating Agent Trustworthiness. In Rino Falcone, Suzanne Barber, Jordi Sabater-Mir, et al. , Trusting Agents for Trusting Electronic Societies: Theory and Applications in HCI and E-Commerce (p. 43). Springer LNCS 3577.
    <b>Abstract:</b> In this paper we use a contract net pro...
      (Related website: http://www.springerlink.com/link.asp?id=33t1avqjf88t6j74 )

  2. <b>Falcone R., Castelfranchi C., Pezzulo G.</b> (2003). Integrating Trustfulness and Decision Using Fuzzy Cognitive Maps. In Nixon P., Terzis S., Trust Management, Proceedings of the first International Conference (pp. 195-210). Springer LNCS 2692.
      (Related website: http://www.springerlink.com/link.asp?id=3hlhnuf4j28utwev)

  3. <b>Falcone,R., Pezzulo,G., Castelfranchi,C.</b> (2003). A fuzzy approach to a belief-based trust computation. In Falcone, R., Barber,S., Korba,L., Singh,M., Special Issue on "Trust, Reputation and Security: Theories and Practice" (pp. 73-86). Lecture Notes on Artificial Intelligence, Springer 2631.

  4. <b>D. Petrelli, G. Pezzulo, D. Baggio</b> (2002). Adaptive Hypertext Design Environments: Putting Principles into Practice. In P. Brusilovsky, O. Stock, C. Strapparava (Eds.): , (pp. 202-213). Springer-Verlag .
      (Related website: http://www.springerlink.com/link.asp?id=7kyt1m8xfn3qf6hv)
    Download ~184Kb

top

Proceedings

  1. <b>Giovanni Pezzulo, Gianguglielmo Calvi, Cristiano Castelfranchi</b> (2007). DiPRA: Distributed Practical Reasoning Architecture. In , Proceedings of IJCAI 2007 (p. ). .
    <b>Abstract:</b> DiPRA (Distributed Practical Reasoning ...
    Download ~100Kb

  2. <b>Dimitri Ognibene, Francesco Mannella, Giovanni Pezzulo, Gianluca Baldassarre</b> (2006). Integrating Reinforcement-Learning, Accumulator Models, and Motor-Primitives to Study Action Selection and Reaching in Monkeys. In , Proceedings of ICCM 2006 (p. ). .
    <b>Abstract:</b> This paper presents a model of brain sy...
    Download ~293Kb

  3. <b>Giovanni Pezzulo</b> (2006). How Can a Massively Modular Mind Be Context-Sensitive? A Computational Approach. In , Proceedings of ICCM 2006 (p. ). .
    <b>Abstract:</b> Starting from a question by Dan Sperber...
    Download ~126Kb

  4. <b>Giovanni Pezzulo and Alessandro Couyoumdjian</b> (2006). Ambiguity-Reduction: a Satisficing Criterion for Decision Making. In , Proceedings of Cogsci 2006 (p. ). .
    Download ~60Kb

  5. <b>Giovanni Pezzulo and Gianguglielmo Calvi</b> (2006). Toward a Perceptual Symbol System. In , Proceedings of EpiRob 2006 (p. ). .
    <b>Abstract:</b> We explore the possibility for a situat...
    Download ~327Kb

  6. <b>Giovanni Pezzulo, Gianluca Baldassarre, Martin V. Butz, Cristiano Castelfranchi, and Joachim Hoffmann</b> (2006). An Analysis of the Ideomotor Principle and TOTE. In Butz M.V., Sigaud O., Pezzulo G., Baldassarre G., Proceedings of the Third Workshop on Anticipatory Behavior in Adaptive Learning Systems (ABiALS 2006). (p. ). .
    <b>Abstract:</b> What does it mean for a system to be go...
    Download ~332Kb

  7. <b>Giovanni Pezzulo, Gianluca Baldassarre, Rino Falcone, Cristiano Castelfranchi</b> (2006). The Anticipatory Nature of Representations. In , Proceedings of the 50th Anniversary Summit of Artificial Intelligence (ASAI50) (p. ). .
    Download ~174Kb

  8. <b>Giovanni Pezzulo and Gianguglielmo Calvi</b> (2005). Distributed Representations and Flexible Modularity in Hybrid Architectures. In , Proceedings of COGSCI 2005 (p. ). .
    <b>Abstract:</b> Here we discuss the role of modules and...
    Download ~198Kb

  9. <b>Pezzulo G., Calvi G. and Falcone R.</b> (2005). Integrating a MAS and a Pandemonium: the Open-Source Framework AKIRA . In , Proceedings of AAMAS 2005 (p. ). .
    <b>Abstract:</b> The open-source framework AKIRA integra...
    Download ~197Kb

  10. <b>Falcone R., Pezzulo G., Castelfranchi C., Calvi G.</b> (2004). Why a cognitive trustier performs better: Simulating trust-based Contract Nets. In , proceedings of the 3rd International Conference on Autonomous Agents and Multi-Agent Systems (AAMAS-04) (pp. 1392-1393). New York, ACM.

  11. <b>Falcone R., Pezzulo G., Castelfranchi C., Calvi G.</b> (2004). Trusting the Agents and the Environment leads to successful Delegation: a Contract Net Simulation. In Rino Falcone, Suzanne Barber, Jordi Sabater and Munindar Singh, proceedings of the 7th International Workshop on Trust in Agent Societies c/o the Autonomous Agents and Multi-Agent Systems Conference (pp. 33-39). New York.
    <b>Abstract:</b> In this paper we use a contract net pro...

  12. <b>Giovanni Pezzulo, Emiliano Lorini, Gianguglielmo Calvi</b> (2004). How do I Know how much I dont Know? A cognitive approach about Uncertainty and Ignorance. In , Proceedings of COGSCI 2004 (p. ). Chicago.
    Download ~216Kb

  13. <b>Giovanni Pezzulo, Gianguglielmo Calvi</b> (2004). AKIRA: a Framework for MABS. In , proceedings of MAMABS 2004 (p. ). New York.
    Download ~724Kb

  14. <b>Giovanni Pezzulo, Gianguglielmo Calvi</b> (2004). A Pandemonium Can Have Goals. In , proceedings of ICCM 2004 (p. ). Pittsburgh.
    <b>Abstract:</b> AKIRA is an open-source framework for a...
    Download ~354Kb

  15. <b>Giovanni Pezzulo, Gianguglielmo Calvi</b> (2004). AKIRA: an Open-Source Framework for Building Game Actors and Worlds. In , proceedings of ISAGA 2004 (p. ). Berlin.

  16. <b>Castelfranchi, C., Falcone R., Pezzulo G.</b> (2003). Belief Sources for Trust: Some Learning Mechanisms. In , Proceedings of the Sixth International Workshop on Trust, Privacy, Deception and Fraud in Agent Systems c/o AAMAS-03, Melbourne Melburne (Australia), July 14 (pp. 101-106). Melbourne.

  17. <b>Falcone R., Castelfranchi C., Pezzulo G.</b> (2003). Trust in Information Sources as Source of Trust: A Fuzzy Approach. In , Proceedings of the Second International Joint Conference on Autonomous Agents and Multiagent Systems (AAMAS-03) Melburne (Australia), 14-18 July (pp. 89-96). Melburne, ACM.
    <b>Abstract:</b> The aim of this paper is to show how re...

  18. <b>Bernardo Magnini, Carlo Strapparava, Giovanni Pezzulo and Alfio Gliozzo</b> (2002). Using Domain Information for Word Sense Disambiguation. In , (p. ). .
    Download ~116Kb

  19. <b>Bernardo Magnini, Carlo Strapparava, Giovanni Pezzulo and Alfio Gliozzo</b> (2002). Comparing ontology-Based and Corpus-Based Domain Annotations in WordNet. In , proceedings of GWN2002 (p. ). .
    Download ~135Kb

  20. <b>Falcone R., Pezzulo G., Castelfranchi C.</b> (2002). Quantifying Belief Credibility for Trust-based Decision. In Falcone, R., Barber,S., Korba,L. & Singh,M., Proceedings of the AAMAS-02 Workshop on "Deception, Fraud and Trust in Agent Societies" (pp. 41-48). .

top

Reports, Magazines, etc.

  1. <b>Giovanni Pezzulo</b> (2006). How can a Massively Modular Mind Be Context-Sensitive? A Computational Approach . Poster Presentation. ISTC Technical Report (Presented at: ICCM 2006, Trieste). .
    Download ~284Kb

  2. <b>Giovanni Pezzulo</b> (2006). Into the Gamblers Frame of Mind: Decision Making under Uncertainty in the Two Cards Gambling Game. Doctoral Dissertation, University of Rome "La Sapienza".
    <b>Abstract:</b> This thesis has three main objectives: ...
    Download ~1248Kb

  3. <b>Giovanni Pezzulo, Gianguglielmo Calvi</b> (2005). Multi Agent Systems meet Pandemonium. In: ISTC Technical Report,
    <b>Abstract:</b> AKIRA is an open-source framework integ...
    Download ~528Kb

  4. <b>Giovanni Pezzulo, Gianguglielmo Calvi</b> (2005). Poster of AKIRA. ISTC Technical Report (Presented at: CONTEXT 2005, Paris).
    <b>Abstract:</b> AKIRA integrates features from Multi Ag...
    Download ~390Kb

  5. <b>Michele Piunti, Giovanni Pezzulo, Gianguglielmo Calvi</b> (2005). Akira Start Guide. ISTC Technical Report.
    <b>Abstract:</b> Quick Start Tutorial for A.k.i.r.a. v. ...
    Download ~432Kb[/publication]


top

<IMAGE src="/img/logo.gif" alt=" [ Logo ISTC, gentile dono di Francesco Pernice -- http://www.francesco-pernice.com/ ] "/>
This Site   All the Web
