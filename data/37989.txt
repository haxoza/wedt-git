[pic]<IMAGE src="eli.jpg" alt=""/>[/pic]

<b><font size=2>Elaheh (Eli) Bozorgzadeh</font></b>

[contactinfo]<font size=2>[position]Assistant Professor[/position]</font>

<font size=2>[affiliation]Computer Science Department</font>

<font size=2>University of California, Irvine[/affiliation] </font>

<font size=2>phone: [phone]949-824-8860[/phone]</font>

[email]<IMAGE src="email.jpg" alt=""/>[/email][/contactinfo]

[resinterests]<b><font size=2>Research Interests</font></b>

  * <font size=2>[interests]Design Automation and Synthesis for Reconfigurable and Embedded Systems[/interests]</font>

  * <font size=2>[interests]Architectural Synthesis  and Design of  Hybrid Programmable Platforms[/interests]</font>

  * <font size=2>[interests]VLSI CAD and Synthesis for FPGAs and ASICs[/interests]</font>[/resinterests]

[education]<b><font size=2>Education</font></b>

  * <font size=2>PhD , [phdmajor]Computer Science[/phdmajor] Department, [phduniv]University of California[/phduniv], Los Angeles (UCLA),[phddate]2003[/phddate]</font>

  * <font size=2>M.S. in [msmajor]Electrical and Computer Engineering[/msmajor] , [msuniv]Northwestern University[/msuniv],[msdate]2000[/msdate]</font>

  * <font size=2>B.S. Degree in [bsmajor]Electrical Engineering[/bsmajor] , [bsuniv]Sharif University of Technology[/bsuniv],[bsdate]1998[/bsdate]</font>[/education]

<b><font size=2>Incoming Events:</font></b>

  * <font size=2>IEEE  International Conference on Field Programmable Logic and Applications (FPL 2006)</font>

  * <font size=2>ACM/IEEE International Conference on Computer-Aided Design, 2006. (ICCAD 2006)</font>

<IMAGE src="http://t0.extreme-dm.com/0.gif?tag=elaheh&j=n" alt=""/>

Last updated on August 15, 2003
