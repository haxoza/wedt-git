<b>[affiliation]CTI Research & Development Unit 3 (RD3)[/affiliation]</b>

<b><font size=2>Home</font></b>

<b><font size=2>People</font></b>

<font size=2><b>Projects</b></font>

<font size=2><b>Publications</b></font>

[pic]<IMAGE src="photos/delis_vasilis.jpg" alt="kalles_dimitris.jpg (5159 bytes)"/>[/pic]

[contactinfo]<b><IMAGE src="../images/letter.gif" alt=""/><font size=2>[address]Kolokotroni 3, GR 262 21, Patras, Hellas[/address]</font></b>

<b><font size=2>Phone</font><font size=2>:[phone]+30-61-221834[/phone]</font></b>

<b><font size=2>Fax</font><font size=2>: [fax]+30-61-221826[/fax] </font></b>

<b><font size=2>E-mail:</font><font size=1> </font><font size=1>[email]delis@cti.gr[/email]</font></b>[/contactinfo]

<b><IMAGE src="../images/go.gif" alt=""/> Personal page</b>

<b><IMAGE src="../images/dld.gif" alt=""/> Download short CV</b>

<IMAGE src="../images/goldline.gif" alt="goldline.gif (151 bytes)"/>

<b><font size=6>V</font><font size=5>asilis</font><font size=6> D</font><font size=5>elis</font></b>

<IMAGE src="../images/goldline.gif" alt="goldline.gif (151 bytes)"/><font size=6>
</font>

[education]<font size=3><b>Education:</b></font> [bsdate]1993[/bsdate]: [bsdegree]Diploma[/bsdegree] in [bsmajor]Computer Eng. & Informatics[/bsmajor] - [bsuniv]Patras Univ.[/bsuniv],[msdate]1995[/msdate]: [msdegree]MSc[/msdegree] (distinction) in [msmajor]GIS[/msmajor] - [msuniv]Edinburgh University[/msuniv], [phddate]1999[/phddate]: [phddegree]PhD[/phddegree] - [phduniv]Patras University[/phduniv][/education]

[resinterests]<font size=3><b>Major Interests:</b></font><font size=2> [interests]Spatiotemporal databases[/interests], [interests]Spatiotemporal Reasoning[/interests], [interests]GIS[/interests], [interests]Location-Allocation Models[/interests].</font>[/resinterests]

<font size=3><b>Division:</b></font><font size=2> </font>[affiliation]Decision-support Applied Information Systems[/affiliation]<b> </b><IMAGE src="../images/gis.gif" alt="gis.gif (1293 bytes)"/>

[publication]<font size=3><b>Selected Publications</b></font>

    * 

      <font size=2>Delis V., Makris C., Sioutas S., A provably efficient computational model for approximate spatiotemporal retrieval? 7th ACM Symposium on Advances in Geographic Information Systems (ACM-GIS), Kansas City, MO, USA, 1999. </font>

    * 

      <font size=2>Delis V., Hadzilacos Th., Binary String Relations ?A Foundation for Spatiotemporal Knowledge Representation? ACM International Conference on Information and Knowledge Management (CIKM ?9), Kansas, USA, 1999.</font>

    * 

      <font size=2>Papadias, D., Mamoulis, N., Delis, V., Approximate Spatiotemporal Retrieval? ACM Transactions on Information Systems, (to appear). </font>[/publication]

Last updated: December 1, 1999
