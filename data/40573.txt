[pic]<IMAGE src="derek.jpg" alt=""/>[/pic]



[contactinfo]Derek Long

<font size=2>[position]Reader[/position] in Computer Science
[affiliation]Department of Computer and Information Sciences
University of Strathclyde[/affiliation]</font>
Phone [phone]+44 (0)141 548 3172[/phone][/contactinfo]


[resinterests]Research Interests

Derek is a [position]member[/position] of the [affiliation]Strathclyde Planning Group[/affiliation]. His research interests are in all aspects of [interests]AI planning[/interests]. He has particular interests in:

  * [interests]planning domain analysis[/interests]

  * [interests]classical planning[/interests]

  * [interests]extension of planning formalisms to temporal and metric planning[/interests]

  * [interests]planning with continuous effects[/interests]

  * [interests]exploiting symmetry in planning (and more generally)[/interests]

  * [interests]applications of planning technology (particularly in space)[/interests][/resinterests]


Publications

A list of publications can be found here.

A further list is available through DBLP: here.

Google Scholar contains these entries (amongst which some of my papers appear!).


Research Funding

Derek holds funds from EPSRC and PPARC and has also recently held funds from ESA, in collaboration with SciSys Ltd.


[resactivity]Activities

Derek is [position]Chairman[/position] of [affiliation]UK Planning and Scheduling Special Interest Group[/affiliation]. This organisation has annual technical workshop meetings. It attracts an attendance of 50 or more members, both from the UK and across the rest of Europe. It held recent meetings in Cork in 2004, London in 2005 and will next meet in Nottingham for 2006.

Derek has played a prominent tole in the series of International Planning Competitions. In 1998 and 2000 he entered the competitions, with Maria Fox, using the planner STAN. In 2002 Derek was co-chair, with Maria Fox, of the 3rd International Planning Competition. He is now ICAPS Competition Chair and member of the ICAPS Council.

Derek was a Program Co-Chair for ICAPS-06.

Derek is an [position]associate editor[/position] of [affiliation]JAIR[/affiliation].


Research History

Derek was appointed Reader in Computer Science at the University of Strathclyde in 2003. Before that he was lecturer in Computer Science at Durham, and, back in the almost forgotten past, at University College London.[/resactivity]


Bits and Pieces

Occasionally, Derek works on stuff that has nothing to do with planning (obviously we're talking about exceptional situations here). Here is one of the bits he did that could even turn out to be useful. It is a Linux application that will work with the Seiko InkLink device.
