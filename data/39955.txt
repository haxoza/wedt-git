<IMAGE src="/images/lib/headings/subpagetitle.jpg" alt="Department of Computer Sciences @ Purdue University"/>
<IMAGE src="/images/lib/nav/search.jpg" alt="Search"/> <IMAGE src="/images/lib/nav/div.jpg" alt="|"/> <IMAGE src="/images/lib/nav/general_information.jpg" alt="General Information"/> <IMAGE src="/images/lib/nav/div.jpg" alt="|"/> <IMAGE src="/images/lib/nav/academics.jpg" alt="Academics"/> <IMAGE src="/images/lib/nav/div.jpg" alt="|"/> <IMAGE src="/images/lib/nav/research.jpg" alt="Research"/> <IMAGE src="/images/lib/nav/div.jpg" alt="|"/> <IMAGE src="/images/lib/nav/people.jpg" alt="People"/> <IMAGE src="/images/lib/nav/div.jpg" alt="|"/> <IMAGE src="/images/lib/nav/external_relations.jpg" alt="External Relations"/>

[pic]<IMAGE src="/images/people-small/ninghui.jpg" alt=""/>[/pic]



[contactinfo]Ninghui Li


[position]Assistant Professor/Computer Science[/position]


[email]ninghui@cs.purdue.edu[/email]

Biographical Information


Address

    [address]Purdue University
    Department of Computer Sciences
    305 N. University Street
    West Lafayette, Indiana, 47907-2107[/address]
    Office Phone: [phone]+1 765-496-6756[/phone]
    FAX:          [fax]+1 765-494-0739[/fax][/contactinfo]

<IMAGE src="/homes/ninghui/images/bin4.jpg" alt="Patrick Ruibin Li"/>
My son Patrick


Teaching

  * <b>CS 655: Advanced Cryptology</b>

      * Spring 2007


  * <b>CS 426: Computer Security</b> (Undergraduate)

      * Fall 2006


  * <b>CS 590U: Access Control: Theory and Practice</b>

      * Spring 2006

      * Spring 2005

      * Fall 2003


  * <b>CS 555: Cryptography</b> (Graduate)

      * Fall 2004

      * Spring 2004


  * <b>CS 355: Cryptography</b> (Undergraduate)

      * Fall 2005


  * <b>CS 591C: Research Seminar for Beginning Graduate Students</b>

      * Spring 2006


  * <b>A reading list in security</b>


Research

  * 

    The TruSe Lab

  * 

    Publications


Current Students

  * Ji-Won Byun (Co-advise with Prof. Elisa Bertino)

  * Hong Chen

  * Tiancheng Li

  * Ziqing Mao

  * Ian Molloy

  * Qihua Wang


Information for Prospective Graduate Students


Former Students & Visitors

  * Ziad El Bizri (M.S. May 2004, joined Google Inc.)

  * Jiangtao Li  (Ph.D. May 2006, co-advised with Professor Mike Atallah, joined Intel)

  * Xinming Ou (PostDoc September 2005 to May 2006, joined Kansas State University)

  * Mahesh V. Tripunitara  (Ph.D.  Dec 2005, joined Motorola Labs)

  * Rui Xue  (Visitor September 2005 to May 2006)


[resactivity]Current and Recent Professional Activities

  * 2007 IEEE Symposium on Security and Privacy, PC member

  * 2007 IEEE Computer Security Foundations Workshop (CSFW), PC member

  * 2007 The World Wide Web Conference (WWW), the Security, Privacy, Reliability and Ethics (SPRE) track, PC member

  * 2007 ACM Symposium on Information, Computer and Communications Security (AsiaCCS), PC member

  * 2007 International Symposium on Data, Privacy, and E-Commerce (ISDPE), PC member

  * 2006 International Conference on Information and Communications Security (ICICS), Publication chair

  * 2006 ACM Conference on Computer and Communications Security (CCS), PC member

  * 2006 International Conference on Applied Cryptography and Network Security (ACNS), PC member

  * 2006 Conference on Security and Privacy for Emerging Areas in Communication Networks (SecureComm), PC member

  * 2006 ACM Symposium on Access Control Models and Technologies (SACMAT), PC member

  * 2006 International Confernce on Data Engineering (ICDE), PC member

  * 2006 IEEE International Conference on Policies for Distributed Systems and Networks (POLICY), PC member

  * 2006 AusCERT Asia Pacific Information Technology Security Conference: Academic Refereed Stream, PC member

  * 2006 International Conference on Trust Management, PC member

  * 2006 Information Security Practice and Experience Conference (ISPEC), PC member


Funded Projects

  * [interests]CAREER[/interests]: Access Control Policy Verification Through Security Analysis And Insider Threat Assessment.  National Science Foundation.  June 2005 to May 2010. (PI)

  * [interests]Collaborative Research[/interests]: A Comprehensive Policy-Drive Framework For Online Privacy Protection: Integrating IT, Human, Legal and Economic Perspectives. National Science Foundation. October 2004 to September 2007. (Co-PI)

  * [interests]ITR[/interests]: Automated Trust Negotiation in Open Systems. National Science Foundation. September 2003 to August 2008. (Co-PI)

  * [interests]Purdue Research Foundation[/interests]: Administration of Role-Based Access Control.  June 2004 to May 2006.  (PI)[/resactivity]


Visitor Information

  * Directions to Purdue's Union Club Hotel from Indy Airport
