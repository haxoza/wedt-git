<font size=5><b>Bruno Bouyssounouse</b></font> <font size=5><b><font size=3><IMAGE src="verimag.jpg" alt=""/></font></b></font> <font size=5><b><font size=2>[introduction][affiliation]VERIMAG Laboratory[/affiliation]</font></b></font>

<IMAGE src="title.gif" alt=""/> [pic]<IMAGE src="Bouyssounouse.gif" alt=""/>[/pic]

Bruno Bouyssounouse heads the [affiliation]Technical Coordination of the Artist FP5 Accompanying Measure and the ARTIST2 Network of Excellence on Embedded Systems Design[/affiliation].

He has played a leading role in setting up the Artist2 NoE consortium and proposal, which was rated highest amongst those submitted under FP6 embedded systems.

He is instrumental in setting up all of the Artist past and future events and publications.

He earned his B.A. in [bsmajor]Computer and Information Sciences[/bsmajor] at the [bsuniv]University of California[/bsuniv] at Santa Cruz in [bsdate]1985[/bsdate], and has interests in both traditional computer science (language theory, operating systems, algorithms), and in embedded systems.

Previous positions include large companies (Thomson CSF, Hewlett-Packard) and high-tech startups (ITMI, PolySpace).

He grew up in Palo Alto, California.[/introduction]

[contactinfo]<font size=2><b>Contact information: </b></font>

<font size=2><b>[email]Bruno.Bouyssounouse@imag.fr[/email]</b></font>

<font size=2>Phone:</font> <font size=2>[phone]+33 4 56 52 03 68[/phone]</font>

<font size=2>Secretary:</font> <font size=2>[fax]+33 4 56 52 03 41[/fax]</font>

<font size=2><b></b></font><font size=2><b>VERIMAG</b></font><font size=2><b> </b>
[address]Centre Ãquation - 2, avenue de Vignate
38610 GIÃRES, FRANCE[/address][/contactinfo]
</font>

[publication]<b>Major Publications</b>

"The ARTIST Roadmap for Research
and Development";
Springer-Verlag LNCS vol 3436;
Bouyssounouse, Bruno; Sifakis, Joseph (Eds.)

<font size=1>see preface and contributors </font> <IMAGE src="RM-cover.jpg" alt=""/>

"SystÃšmes embarquÃ©s : dÃ©fis et directions de travail", Bruno Bouyssounouse, Joseph Sifakis;
chapter 4 in "Paradigmes et enjeux de lâinformatique"
(TraitÃ© IC2, SÃ©rie Informatique et systÃšmes d'information) / 2-7462-1035-5. <IMAGE src="couverture.paradigmes.jpg" alt=""/>

<b>Other Publications</b>

La Revue des IngÃ©nieurs INPG;
"LÂInjection de lÂinformatique dans tous les appareils"; pages 22-23, paru en octobre 2005.
<IMAGE src="INPG-Dossier05-3-pages22-23.jpg" alt=""/>

La Revue des IngÃ©nieurs INPG;
"Un enjeu stratÃ©gique pour les Ã©conomies modernes"; page 24, paru en octobre 2005.
<IMAGE src="INPG-Dossier05-3-page24.gif" alt=""/>

La Revue des IngÃ©nieurs INPG;
"Des DÃ©rives Potentielles "; page 32, paru en octobre 2005.
<IMAGE src="INPG-Dossier05-3-page32.gif" alt=""/>

"ARTIST2 : Un programme embarquÃ© pour l'Europe";
La lettre de l'IMAG NÂ° 16,
paru en septembre 2004.

<IMAGE src="img_LettreIMAG.gif" alt=""/>

La Revue des IngÃ©nieurs INPG;
"Embarquement ImmÃ©diat "; pages 37â?8,
paru en juin 2004.

<IMAGE src="RevueIngÃ©nieursINPG-juin2004.jpg" alt=""/>

<b>Interviews</b>

<b>01Net : "L'Europe prÃªte Ã  embarquer dans Artist2"
</b> Electronique International Hebdo,
October 26th, 2004

<b>Major Workshops, Meetings,
Presentations
Organized </b>

<b>Date'05: "<b>Embedded Systems Design: An Emerging Unified Discipline</b>" workshop
</b>Date'05: Design, Automation and Test in Europe
March 11th, 2005

<b>Workshop: "ARTEMIS - European Technology Platform on Embedded Systems"
</b>(including a presentation by European Commissioner Erkki Liikanen)
June 28th and 29th, 2004 - Rome

<b>Date'04: "Embedded Systems Research in Europe" workshop
</b>Date'04: Design, Automation and Test in Europe
February 20th, 2004

<b>ARTIST presentation at ERTS</b>
Conference Programme January 21st-23rd, 2004 - Toulouse (France)

<b>OMG Special Event: Model Integrated Computing for Embedded, Real-Time Systems</b>
Within the OMG Technical Meeting<b>; </b>November 17th, 2003 - London

<b>Trends in Embedded Systems Design
</b>ARTIST International Collaboration Day - Trends
University of Pennsylvania - Philadelphia, USA
October 12th, 2003

<b>Education in Embedded Systems Design
</b>ARTIST International Collaboration Day - Education
University of Pennsylvania - Philadelphia, USA
October 11th, 2003

IST 2002 Conference : Partnerships for the Future
"<b>Embedded Software and Systems : Directions for Europe</b>"
ARTIST session on Embedded Software and Systems<b>
</b>Copenhagen - November 6th 2002[/publication]

<b>International Collaboration Day</b>
Grenoble - October 6th 2002

.
