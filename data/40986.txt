Jin-Yi Cai, Professor[pic]<IMAGE src="jinyi.jpg" alt=""/>[/pic]

[contactinfo]Jin-Yi Cai (²ÌœøÒ»)
[affiliation]Computer Sciences Department[/affiliation]
[address]Room 4393
University of Wisconsin - Madison
1210 West Dayton Street
Madison, WI 53706
U.S.A.[/address]
PHONE: [phone](608) 262-3158[/phone]
FAX: [fax](608) 262-9777[/fax]
EMAIL: [email]jyc AT cs DOT wisc DOT edu[/email]
Dept Mail Server[/contactinfo]

[introduction]I am one of the [position]organizers[/position] of the [affiliation]conference series Theory and Applications of Models of Computation[/affiliation]

(Submission deadline has been EXTENDED to Dec. 28, 2006.) Call for Papers (pdf file)
Go to Conference Site

Jin-Yi Cai studied at [bsuniv]Fudan University[/bsuniv] (class of 77). He continued his study at [phduniv]Temple University[/phduniv] and at [phduniv]Cornell University[/phduniv], where he received his Ph. D. in [phddate]1986[/phddate]. He held faculty positions at Yale University (1986-1989), Princeton University (1989-1993), and SUNY Buffalo (1993-2000), rising from Assistant Professor to Professor. He is currently a [position]Professor of Computer Science[/position] at the [affiliation]University of Wisconsin--Madison[/affiliation].

He received a Presidential Young Investigator Award in 1990, an Alfred P. Sloan Fellowship in Computer Science in 1994, a John Simon Guggenheim Fellowship in 1998, and a Morningside Silver Medal of Mathematics in 2004. He also received the Humboldt Research Award for Senior U.S. Scientists. He has been elected a [position]Fellow[/position] of [affiliation]ACM[/affiliation].

He is an [position]Associate Editor[/position] of [affiliation]Journal of Complexity, Journal of Computer and Systems Sciences (JCSS)[/affiliation], an [position]Editor[/position] of [affiliation]International Journal of Foundations of Computer Science[/affiliation], an [position]Editor[/position] of [affiliation]The Chicago Journal of Theoretical Computer Science[/affiliation] and a [position]member[/position] of the [affiliation]Scientific Board for the Electronic Colloquium on Computational Complexity[/affiliation]. He works in computational complexity theory. He has written and published over 70 research papers. (List of publications according to this web site.)

Here is an interview with Ubiquity Magazine Ubiquity interview[/introduction]

Curriculum Vitae ps pdf

[publication]Recent Talk at ICALP 06 on Matchgate Computations. pdf

Some representative publications:

The Resolution of a Hartmanis Conjecture ps pdf
Resolution of Hartmanis' Conjecture for NL-hard sparse sets ps pdf
An Improved Worst-Case to Average-Case Connection for Lattice Problems ps pdf
A relation of primal-dual lattices and the complexity of shortest lattice vector problem ps pdf
A new transference theorem and applications to Ajtai's connection factor ps pdf
Hardness and Hierarchy Theorems for Probabilistic Quasi-polynomial Time ps pdf
On the Hardness of Permanent ps pdf
S_2^p \subseteq ZPP^{NP} (updated) ps pdf
On Proving Circuit Lower Bounds Against the Polynomial-time Hierarchy: Positive and Negative Results ps pdf
Essentially Every Unimodular Matrix Defines an Expander (Revised) ps pdf
Progress in Computational Compexity Theory ps pdf
Valiant's Holant Theorem and Matchgate Tensors ps pdf
On the Theory of Matchgate Computations ps pdf
Some Results on Matchgates and Holographic Algorithms ps pdf
A Novel Information Transmission Problem and its Optimal Solution ps pdf
On Symmetric Signatures in Holographic Algorithms ps pdf
Holographic Algorithms: From Art to Science pdf
Bases Collapse in Holographic Algorithms pdf

In Fall 2006 I will teach
MATH/CS 240: Introduction to Discrete Mathematics
240 Syllabus

Position Papers on TCS:

Aho et. al. Report: Theory of Computing: Goals and Directions

Goldreich-Wigderson: Theory of Computing: A Scientific Prespective

Johnson Report: Challenges for Theoretical Computer Science[/publication]

Lake view in Madison (webcam)

"Cosmologists are often in error, but never in doubt." ---Lev Landau

WebCounter says that you are visitor number <IMAGE src="http://counter.digits.com/wc/-d/4/jinyicai" alt=""/> since June 9, 2004.
