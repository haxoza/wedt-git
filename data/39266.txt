<IMAGE src="images/uow.gif" alt="The University of Wollongong"/>



[contactinfo]Nicholas Paul Sheppard


[position]Research Fellow[/position]

[pic]<IMAGE src="images/snug.jpg" alt="Photo taken 30 June 2006"/>[/pic]

Nicholas Sheppard
[affiliation]School of Information Technology and Computer Science
The University of Wollongong[/affiliation] [address]NSW 2522
Australia[/address]

Room: 
      [address]Bldg 3 (Informatics) Rm 101[/address]

Phone:
      [phone]+61 2 4221 3788[/phone]

Fax:  
      [fax]+61 2 4221 4170[/fax]

E-mail:
      [email]nps@uow.edu.au[/email][/contactinfo]


[resinterests]Research Interests

As an undergraduate, I studied [bsmajor]computer systems engineering and pure mathematics[/bsmajor], and have degrees in both. My postgraduate and postdoctoral studies have been in [phdmajor]computer science[/phdmajor]. I am currently with the [affiliation]Secure Multimedia Information Communication Labs in the School of Information Technology and Computer Science at the University of Wollongong[/affiliation], Australia.

My principal research interest is [interests]digital rights management[/interests]. I've previously worked in [interests]combinatorial optimisation[/interests] and [interests]digital watermarking[/interests].[/resinterests]

People I have worked with include:

  * my PhD supervisor, Jeff Kingston

  * my postdoctoral supervisor, Rei Safavi-Naini

  * students, Rickard Lönneborg, Qiong Liu, and Sid Stamm.

Many of my publications can be downloaded from this web site.


The Rest of the NPS Universe

This isn't the only thing I do with my life. Though I do seem to spend a lot of it with computers, as evidenced by the fact that I have another web site at http://www.zeta.org.au/~nps, covering Cynicism and Negativity, software I have written and general self-promotion.

[contactinfo]Nicholas Paul Sheppard: [email]nps@uow.edu.au[/email][/contactinfo]
