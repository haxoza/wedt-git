[pic]<IMAGE src="./images/james.gif" alt=""/>[/pic] [contactinfo]James M. Moscola
[position]Research Assistant[/position]
[affiliation]Washington University[/affiliation]
[address]One Brookings Drive
Dept. of Computer Science and Engineering
Campus Box 1045
St. Louis, Missouri 63130[/address]
[phone]314-935-4163[/phone] (office)
Office: [address]Bryan Hall, Room 410[/address][/contactinfo]



[resinterests]My Research Interests Include:

  * [interests]FPGA Hardware[/interests]

  *  [interests]String and Pattern Matching[/interests]

  *  [interests]Hardware Parsers[/interests]

  *  [interests]Content-based Routing[/interests]

  *  [interests]Semantic Networking[/interests]

  *  [interests]Network Intrusion Detection and Prevention Systems[/interests][/resinterests]


[education]Education

  *  Currently pursuing D.Sc. in [phdmajor]Computer Engineering[/phdmajor] at [phduniv]Washington University[/phduniv] in St. Louis.
    Advisor: John Lockwood.

  *  M.S. in [msmajor]Computer Science[/msmajor], [msuniv]Washington University[/msuniv], [msdate]August 2003[/msdate].

  *  B.S. in [bsmajor]Computer Engineering[/bsmajor], [bsuniv]Washington University[/bsuniv], [bsdate]May 2001[/bsdate].

  *  B.S. in [bsmajor]Physical Science[/bsmajor], Liberal Arts Core, [bsuniv]Muhlenberg College[/bsuniv], [bsdate]May 2000[/bsdate].[/education]


[publication]Journal and Conference Papers

  *  Hardware-Accelerated Parser for Extraction of Metadata in Semantic Network Content,
    by James Moscola, Young H. Cho, John W. Lockwood;
    Proceedings of IEEE Aerospace Conference, Big Sky, MT, March 3-10, 2007.
    (Abstract) (Click for Bibtex)

    @inproceedings{moscola_aero07,
      author = {James Moscola and Young H. Cho and John W. Lockwood},
      title = {{Hardware-Accelerated Parser for Extraction of Metadata in Semantic Network Content}},
      booktitle = {{Proceedings of IEEE Aerospace Conference}},
      address  = {Big Sky, MT, USA},
      month = Mar,
      year  = 2007
    }

    (Abstract) (Close Bibtex)

    <font size=1> Coming soon! </font>

  * A Reconfigurable Architecture for Multi-Gigabit Speed Content-Based Routing,
    by James Moscola, Young H. Cho, John W. Lockwood;
    Proceedings of Hot Interconnects 14 (HotI), Stanford, CA, August 23-25, 2006.
    (Abstract) (Click for Bibtex)

    @inproceedings{moscola_hoti06,
      author = {James Moscola and Young H. Cho and John W. Lockwood},
      title = {{A Reconfigurable Architecture for Multi-Gigabit Speed Content-Based Routing}},
      booktitle = {{Proceedings of Hot Interconnects 14 (HotI)}},
      address  = {Stanford, CA, USA},
      month = Aug,
      year  = 2006
    }

    (Abstract) (Close Bibtex)

    <font size=1> This paper presents a reconfigurable architecture for high-speed content-based routing. Our architecture goes beyond simple pattern matching by implementing a parsing engine that defines the semantics of patterns that are parsed within the data stream. Defining the semantics of patterns allows for more accurate processing and routing of packets using any fields that appear within the payload of the packet. The architecture consists of several components, including a pattern matcher, a parsing structure, and a routing module. Both the pattern matcher and parsing structure are automatically generated using an application-specific compiler that is described in this paper. The compiler accepts a grammar specification as input and outputs a data parser in VHDL. The routing module receives control signals from both the pattern matcher and the parsing structure that aid in the routing of packets. We illustrate how a content-based router can be implemented with our technique using an XML parser as an example. The XML parser presented was designed, implemented, and tested in a Xilinx Virtex XCV2000E FPGA on the FPX platform. It is capable of processing 32-bits of data per clock cycle and runs at 100 MHz. This allows the system to process and route XML messages at 3.2 Gbps. </font>

  * Context-Free Grammar based Token Tagger in Reconfigurable Devices,
    by Young H. Cho, James Moscola, John W. Lockwood;
    Proceedings of International Workshop on Data Engineering (ICDE/SeNS), Atlanta, GA, April 3-7, 2006.
    (Abstract) (Click for Bibtex)

    @inproceedings{cho_sens06,
      author = {Young H. Cho and James Moscola and John W. Lockwood},
      title = {{Context-Free Grammar based Token Tagger in Reconfigurable Devices}},
      booktitle = {{Proceedings of International Conference of Data Engineering (ICDE/SeNS)}},
      address  = {Atlanta, GA, USA},
      month = Apr,
      year  = 2005
    }

    (Abstract) (Close Bibtex)

    <font size=1> In this paper, we present reconfigurable hardware architecture for detecting semantics of streaming data on 1+ Gbps networks. The design leverages on the characteristics of context-free-grammar (CFG) that allows the computers to understand the semantics of data. Although our parser is not a true CFG parser, we use the linguistic structure defined in the grammars to explore a new way of parsing data using Field Programmable Gate Array (FPGA) hardware. Our system consists of pattern matchers and a syntax detector. The pattern matchers are automatically generated using the grammar token list while the syntax detector is generated based on the aspects of the grammar that define the order of all possible token sequences. Since all the rules are mapped onto the hardware as parallel processing engines, the meaning of each token can be determined by monitoring where it is being processed. Our highly parallel and fine grain pipelined engines can operate at a frequency above 500 MHz. Our initial implementation is XML content-based router for XML remote procedure calls (RPC). The implementation can process the data at 1.57 Gbps on Xilinx VirtexE FPGA and 4.26 Gbps on the Virtex 4 FPGA. </font>

  * Transformation Algorithms for Data Streams,
    by Stephen G. Eick, John W. Lockwood, James Moscola, Chip Kastner, Andrew Levine, Mike Attig, Ron Loui, Doyle J. Weishar;
    Proceedings of IEEE Aerospace Conference, Big Sky, MT, March 5-12, 2005.
    (Abstract) (Click for Bibtex)

    @inproceedings{eick_aero05,
      author = {Stephen G. Eick and John W. Lockwood and James Moscola and Chip Kastner and Andrew Levine and Mike Attig and Ron Loui and Doyle J. Weishar},
      title = {{Transformation Algorithms for Data Streams}},
      booktitle = {{Proceedings of IEEE Aerospace Conference}},
      address  = {Big Sky, MT, USA},
      month = Mar,
      year  = 2005
    }

    (Abstract) (Close Bibtex)

    <font size=1> Next generation data processing systems must deal with very high data ingest rates and massive volumes of data. Such conditions are typically encountered in the Intelligence Community (IC) where analysts must search through huge volumes of data in order to gather evidence to support or refute their hypotheses. Their effort is made all the more difficult give that the data appears as unstructured text that is written in multiple languages using characters that have different encodings. Human analysts have not been able to keep pace with reading the data and a large amount of data is discarded even though it might contain key information. The goal of our project is to assess the feasibility of incrementally replacing humans with automation in key areas of information processing. These areas include document ingest, content categorization, language translation, and context-and-temporally-based information retrieval.

    Mathematical transformation algorithms, when implemented in rapidly reconfigurable hardware, offer the potential to continuously (re)process and (re)interpret extremely high volumes of multi-lingual, unstructured text data. These technologies can automatically elicit the semantics of streaming input data, organize the data by concept (regardless of language), and associate related concepts in order to parameterize models. To test that hypothesis, we are building an experimentation testbed that enables the rapid implementation of semantic processing algorithms in hardware. The system includes a high-performance infrastructure that includes a hardware-accelerated content processing platform; mass storage to hold training data, test data, and experiment scenarios; and tools for analysis and visualization of the data.

    In our first use of the testbed, we performed an experiment where we implemented three transformation algorithms using FPX hardware platforms to perform semantic processing on document streams. Our platform uses Field-programmable Port Extender (FPX) modules developed at Washington University in Saint Louis.

    This paper describes our approach to building the experimental hardware platform components, discusses the major features of the circuit designs, overviews our first experiment, and offers a detailed description of the results, which are promising. </font>

  * Architecture for a Hardware-Based, TCP/IP Content-Processing System,
    by David V. Schuehler, James Moscola, and John W. Lockwood;
    IEEE Micro, Volume 24, Number 1, January/February 2004, pp. 62-69.
    (Abstract) (Click for Bibtex)

    @article{schuehler_ieeemicro04,
      author = {David V. Schuehler and James Moscola and John W. Lockwood},
      title = {{Architecture for a Hardware-Based, TCP/IP Content-Processing System}},
      journal = {{IEEE Micro}},
      issn = {0272-1732},
      volume = 24,
      number = 1,
      pages = {62-69},
      month = Jan,
      year = 2004
    }

    (Abstract) (Close Bibtex)

    <font size=1> The Transmission Control Protocol is the workhorse protocol of the Internet. Most of the data passing through the Internet transits the network using TCP layered atop the Internet Protocol (IP). Monitoring, capturing, filtering, and blocking traffic on highspeed Internet links requires the ability to directly process TCP packets in hardware. Because TCP is a stream-oriented protocol that operates above an unreliable datagram network, there are complexities in reconstructing the underlying data flow.

    High-speed network intrusion detection and prevention systems guard against several types of threats. When used in backbone networks, these content- scanning systems must not inhibit network throughput. Gilders law predicts that the need for bandwidth will grow at least three times as fast as computing power.1 As the gap between network bandwidth and computing power widens, improved microelectronic architectures are needed to monitor and filter network traffic without limiting throughput. To address these issues, weve designed a hardwarebased TCP/IP content processing system that supports content scanning and flow blocking for millions of flows at gigabit line rates. </font>

  * Application of Hardware Accelerated Extensible Network Nodes for Internet Worm and Virus Protection,
    by John W. Lockwood, James Moscola, David Reddick, Matthew Kulig, and Tim Brooks;
    Proceedings of International Working Conference on Active Networks (IWAN), Kyoto, Japan, December, 2003.
    (Abstract) (Click for Bibtex)

    @inproceedings{lockwood_iwan03,
      author = {John W. Lockwood and James Moscola and David Reddick and Matthew Kulig and Tim Brooks},
      title = {{Application of Hardware Accelerated Extensible Network Nodes for Internet Worm and Virus Protection}},
      booktitle = {{Proceedings of International Working Conference on Active Networks (IWAN)}},
      address  = {Kyoto, Japan},
      month = Dec,
      year  = 2003
    }

    (Abstract) (Close Bibtex)

    <font size=1> Today's crucial information networks are vulnerable to fast moving attacks by Internet worms and computer viruses. These attacks have the potential to cripple the Internet and compromise the integrity of the data on the end-user machines. Without new types of protection, the Internet remains susceptible to the assault of increasingly aggressive attacks. A platform has been implemented that actively detects and blocks worms and viruses at multi-Gigabit/second rates. It uses the Field-programmable Port Extender (FPX) to scan for signatures of malicious software (malware) carried in packet payloads. Dynamically reconfigurable Field Programmable Gate Array (FPGA) logic tracks the state of Internet flows and searches for regular expressions and fixedstrings that appear in the content of packets. Protection is achieved by the incremental deployment of systems throughout the Internet. </font>

  *  Internet Worm and Virus Protection in Dynamically Reconfigurable Hardware;
    by John W. Lockwood, James Moscola, Matthew Kulig, David Reddick, and Tim Brooks;
    Proceedings of Military and Aerospace Programmable Logic Devices (MAPLD), Paper E10, Washington D.C., September 9-11, 2003.
    (Abstract) (Click for Bibtex)

    @inproceedings{lockwood_mapld03,
      author = {John W. Lockwood and James Moscola and Matthew Kulig and David Reddick and Tim Brooks},
      title = {{Internet Worm and Virus Protection in Dynamically Reconfigurable Hardware}},
      booktitle = {{Proceedings of Military and Aerospace Programmable Logic Device (MAPLD)}},
      address  = {Washington D.C., USA},
      month = Sep,
      year  = 2003
    }

    (Abstract) (Close Bibtex)

    <font size=1> The security of the Internet can be improved using Programmable Logic Devices (PLDs). A platform has been implemented that actively scans and filters Internet traffic for Internet worms and viruses at multi-Gigabit/second rates using the Field-programmable Port Extender (FPX). Modular components implemented with Field Programmable Gate Array (FPGA) logic on the FPX process packet headers and scan for signatures of malicious software (malware) carried in packet payloads. FPGA logic is used to implement circuits that track the state of Internet flows and search for regular expressions and fixed-strings that appear in the content of packets. The FPX contains logic that allows modules to be dynamically reconfigured to scan for new signatures. Network-wide protection is achieved by the deployment of multiple systems throughout the Internet. </font>

  * An Extensible, System-On-Programmable-Chip, Content-Aware Internet Firewall,
    by John W. Lockwood, Christopher Neely, Christopher Zuver, James Moscola, Sarang Dharmapurikar, and David Lim;
    Proceedings of International Conference on Field Programmable Logic and Applications (FPL), Lisbon, Portugal, Paper 14B, September 1-3, 2003.
    (Abstract) (Click for Bibtex)

    @inproceedings{lockwood_fpl03,
      author = {John W. Lockwood and Christopher Neely and Christopher Zuver and James Moscola and Sarang Dharmapurikar and David Lim},
      title = {{An Extensible, System-On-Programmable-Chip, Content-Aware Internet Firewall}},
      booktitle = {{Proceedings of Field Programmable Logic and Applications (FPL)}},
      address  = {Lisbon, Portugal},
      month = Sep,
      year  = 2003
    }

    (Abstract) (Close Bibtex)

    <font size=1> An extensible firewall has been implemented that performs packet filtering, content scanning, and per-.ow queuing of Internet packets at Gigabit/second rates. The firewall uses layered protocol wrappers to parse the content of Internet data. Packet payloads are scanned for keywords using parallel regular expression matching circuits. Packet headers are compared to rules specified in Ternary Content Addressable Memories (TCAMs). Per-flow queuing is performed to mitigate the effect of Denial of Service attacks. All packet processing operations were implemented with reconfigurable hardware and fit within a single Xilinx Virtex XCV2000E Field Programmable Gate Array (FPGA). The singlechip firewall has been used to filter Internet SPAM and to guard against several types of network intrusion. Additional features were implemented in extensible hardware modules deployed using run-time reconfiguration. </font>

  * Architecture for a Hardware Based, TCP/IP Content Scanning System,
    by David V. Schuehler, James Moscola, and John W. Lockwood;
    Proceedings of Hot Interconnects 11 (HotI), pp. 89-94, Stanford, CA, August 2003.
    (Abstract) (Click for Bibtex)

    @inproceedings{schuehler_hoti03,
      author = {David V. Schuehler and James Moscola and John W. Lockwood},
      title = {{Architecture for a Hardware Based, TCP/IP Content Scanning System}},
      booktitle = {{Proceedings of Hot Interconnects 11 (HotI)}},
      pages = {89-94},
      address  = {Stanford, CA, USA},
      month = Aug,
      year  = 2003
    }

    (Abstract) (Close Bibtex)

    <font size=1> Hardware assisted intrusion detection systems and content scanning engines are needed to process data at multigigabit line rates. These systems, when placed within the core of the Internet, are subject to millions of simultaneous flows, with each flow potentially containing data of interest. Existing IDS systems are not capable of processing millions of flows at gigabit-per-second data rates. This paper describes an architecture which is capable of performing complete, stateful, payload inspections on 8 million TCP flows at 2.5 gigabits-per-second. To accomplish this task, a hardware circuit is used to combine a TCP protocol processing engine, a per flow state store, and a content scanning engine. </font>

  * FPsed: A Streaming Content Search-and-Replace Module for an Internet Firewall,
    by James Moscola, Michael Pachos, John W. Lockwood, and Ron P. Loui;
    Proceedings of Hot Interconnects 11 (HotI), pp. 122-129, Stanford, CA, August 2003.
    (Abstract) (Click for Bibtex)

    @inproceedings{moscola_hoti03,
      author = {James Moscola and Michael Pachos and John Lockwood and Ronald Loui},
      title = {{FPsed: A Streaming Content Search-and-Replace Module for an Internet Firewall}},
      booktitle = {{Proceedings of Hot Interconnects 11 (HotI)}},
      pages = {122-129},
      address  = {Stanford, CA, USA},
      month = Aug,
      year  = 2003
    }

    (Abstract) (Close Bibtex)

    <font size=1> A module has been implemented in Field Programmable Gate Array (FPGA) hardware that is able to perform regular expression search-and-replace operations on the content of Internet packets at Gigabit/ second rates. All of the packet processing operations are performed using reconfigurable hardware within a single Xilinx Virtex XCV2000E FPGA. A set of layered protocol wrappers is used to parse the headers and payloads of packets for Internet protocol data. A content matching server automatically generates, compiles, synthesizes, and programs the module into the Field-programmable Port Extender (FPX) platform. </font>

  * Implementation of a Content-Scanning Module for an Internet Firewall,
    by James Moscola, John Lockwood, Ronald P. Loui, and Michael Pachos;
    Proceedings of IEEE Symposium on Field-Programmable Custom Computing Machines (FCCM), pp. 31-38, Napa, CA, April 9-11, 2003.
    (Abstract) (Click for Bibtex)

    @inproceedings{moscola_fccm03,
      author = {James Moscola and John Lockwood and Ronald Loui and Michael Pachos},
      title = {{Implementation of a Content-Scanning Module for an Internet Firewall}},
      booktitle = {{Proceedings of IEEE Symposium on Field-Programmable Custom Computing Machines (FCCM)}},
      pages = {31-38},
      address  = {Napa, CA, USA},
      month = Apr,
      year  = 2003
    }

    (Abstract) (Close Bibtex)

    <font size=1> A module has been implemented in Field Programmable Gate Array (FPGA) hardware that scans the content of Internet packets at Gigabit/second rates. All of the packet processing operations are performed using reconfigurable hardware within a single Xilinx Virtex XCV2000E FPGA. A set of layered protocol wrappers is used to parse the headers and payloads of packets for Internet protocol data. A content matching server automatically generates the Finite State Machines (FSMs) to search for regular expressions. The complete system is operated on the Field-programmable Port Extender (FPX) platform. </font>


Technical Reports

  * FPgrep and FPsed: Packet Payload Processors for Managing the Flow of Digital Content on Local Area Networks and the Internet,
    by James Moscola;
    Master's Thesis, Washington University, August 2003.
    (Abstract) (Click for Bibtex)

    @mastersthesis{moscola_ms03,
      author = {James Moscola},
      title = {{FPgrep and FPsed: Packet Payload Processors for Managing the Flow of Digital Content on Local Area Networks and the Internet}},
      school = {Washington University},
      address = {St. Louis, MO, USA},
      month = Aug,
      year = 2003
    }

    (Abstract) (Close Bibtex)

    <font size=1> As computer networks increase in speed, it becomes difficult to monitor and manage the transmitted digital content. To alleviate these problems, hardware-based search (FPgrep) and search-and-replace (FPsed) modules have been developed. FPgrep has the ability to scan packet payloads for a given set of regular expressions and pass or drop packets based on the payload contents. FPsed also scans packet payloads for a set of regular expressions and adds the ability to modify the payload if desired. The hardware circuits that implement the FPgrep and FPsed modules can be generated, compiled, and synthesized using a simple web interface. Once a module is created it is programmed into logic on a Field Programmable Gate Array (FPGA). The FPgrep and FPsed modules use FPGAs to process packets at the full rate of Gigabit-speed networks. Both modules, along with several supporting applications were developed and tested using the Field Programmable Port Extender (FPX) platform. Applications developed for the modules currently include a spam filter, virus protection, an information security filter, as well as a copyright enforcement function. </font>


Short Papers and Posters

  * Implementation of Network Application Layer Parser for Multiple TCP/IP Flows in Reconfigurable Devices,
    by James Moscola, Young H. Cho, John W. Lockwood;
    Proceedings of International Conference on Field Programmable Logic and Applications (FPL), Madrid, Spain, August 28-30, 2006.
    (Abstract) (Click for Bibtex)

    @inproceedings{moscola_fpl06,
      author = {James Moscola and Young H. Cho and John W. Lockwood},
      title = {{Implementation of Network Application Layer Parser for Multiple TCP/IP Flows in Reconfigurable Devices}},
      booktitle = {{International Conference on Field Programmable Logic and Applications (FPL)}},
      address  = {Madrid, Spain},
      month = Aug,
      year  = 2006
    }

    (Abstract) (Close Bibtex)

    <font size=1> This paper presents an implementation of a high-performance network application layer parser in FPGAs. At the core of the architecture resides a pattern matcher and a parser. The pattern matcher scans for patterns in high-speed streaming TCP data streams. The parser core augments each pattern found with semantic information determined from the patterns location within the data stream. The packet payload parser can provide a higher level of understanding of a data stream for many network applications. Such applications include high performance XML parsers, content-based/aware routers, and others. Additionally, a TCP processor allows stateful packet payload parsing of up to 8 million simultaneous TCP flows. The payload parser has been implemented in a Xilinx Virtex E 2000 FPGA on the Field-Programmable Port Extender platform. The parsing module runs at 200 MHz and parse raw data at 6.4 Gbps. The payload parser, integrated with the TCP processor, runs at 100 MHz for a throughput of 3.2 Gbps. </font>

  * Content-Free Grammar Parsing for High-Speed Network Applications in Reconfigurable Hardware,
    by James Moscola, Young H. Cho, John W. Lockwood;
    9th SIGDA Ph.D. Forum at DAC 2006, San Francisco, CA, July 25, 2006.
    (Abstract) (Click for Bibtex)

    
     

    (Abstract) (Close Bibtex)

    <font size=1> Currently, many network applications are designed to provide network security. Such technologies include spam filters, virus scanners, and network intrusion detection and prevention systems. Other applications include packet filters, content-based routing, and natural language processing. At the core of each of these systems resides a rule-based pattern matcher, capable of detecting strings and/or regular expressions.
    In recent years, many researchers have developed pattern matching hardware architectures capable of keeping pace with increasing network speeds and rule sets. However, naive pattern matchers do not consider the context of a match in the data. Therefore, they are susceptible to false positive identification. On a high-speed network, even a small number of false positives can surmount to an unmanageable amount of data.
    This work intends to illustrate how context-free grammars (CFG) can be utilized to increase the accuracy of pattern recognition. CFGs provide a higher level of expressiveness than both strings and regular expressions by defining the semantics of a pattern within the structure of its language. This semantic information can then be used to reduce the number of false positive pattern identifications. </font>

  * A Scalable Hybrid Regular Expression Pattern Matcher,
    by James Moscola, Young H. Cho, John W. Lockwood;
    Proceedings of IEEE Symposium on Field-Programmable Custom Computing Machines (FCCM), Napa, CA, April 24-26, 2006.
    (Abstract) (Click for Bibtex)

    @inproceedings{moscola_fccm06,
      author = {James Moscola and Young H. Cho and John W. Lockwood},
      title = {{A Scalable Hybrid Regular Expression Pattern Matcher}},
      booktitle = {{Proceedings of IEEE Symposium on Field-Programmable Custom Computing Machines (FCCM)}},
      address  = {Napa, CA, USA},
      month = Apr,
      year  = 2006
    }

    (Abstract) (Close Bibtex)

    <font size=1> We present a reconfigurable hardware architecture for searching for regular expression patterns in streaming data. This new architecture is created by combining two popular pattern matching techniques: a pipelined character grid architecture, and a regular expression NFA architecture. The resulting hybrid architecture can scale the number of input characters while still maintaining the ability to scan for regular expression patterns. </font>

  * Reconfigurable Context-Free Grammar based Data Processing Hardware with Error Recovery,
    by James Moscola, Young H. Cho, John W. Lockwood;
    Proceedings of International Parallel & Distributed Processing Symposium (IPDPS/RAW), Rhodes Island, Greece, April 25-26, 2006.
    (Abstract) (Click for Bibtex)

    @inproceedings{moscola_raw06,
      author = {James Moscola and Young H. Cho and John W. Lockwood},
      title = {{Reconfigurable Context-Free Grammar based Data Processing Hardware with Error Recovery}},
      booktitle = {{Proceedings of International Parallel \& Distributed Processing Symposium (IPDPS/RAW)}},
      address  = {Rhodes Island, Greece},
      month = Apr,
      year  = 2006
    }

    (Abstract) (Close Bibtex)

    <font size=1> This paper presents an architecture for context-free grammar (CFG) based data processing hardware for reconfigurable devices. Our system leverages on CFGs to tokenize and parse data streams into a sequence of words with corresponding semantics. Such a tokenizing and parsing engine is sufficient for processing grammatically correct input data. However, most pattern recognition applications must consider data sets that do not always conform to the predefined grammar. Therefore, we augment our system to detect and recover from grammatical errors while extracting useful information. Unlike the table look up method used in traditional CFG parsers, we map the structure of the grammar rules directly onto the Field Programmable Gate Array (FPGA). Since every part of the grammar is mapped onto independent logic, the resulting design is an efficient parallel data processing engine. To evaluate our design, we implement several XML parsers in an FPGA. Our XML parsers are able to process the full content of the packets up to 3.59 Gbps on Xilinx Virtex 4 devices. </font>

  * Context-Free Grammar based Token Tagger in Reconfigurable Devices,
    by Young H. Cho, James Moscola, John W. Lockwood;
    Proceedings of International Symposium on Field-Programmable Gate Arrays (FPGA), Monterey, CA, February 22-24, 2006.
    (Abstract) (Click for Bibtex)

    @inproceedings{cho_fpga06,
      author = {Young H. Cho and James Moscola and John W. Lockwood},
      title = {{Context-Free Grammar based Token Tagger in Reconfigurable Devices}},
      booktitle = {{Proceedings of International Symposium on Field-Programmable Gate Arrays (FPGA)}},
      address  = {Monterey, CA, USA},
      month = Feb,
      year  = 2006
    }

    (Abstract) (Close Bibtex)

    <font size=1> We present a high performance reconfigurable hardware architecture for detecting patterns as well as their contextual meaning. By analyzing for both semantic content and structure, the accuracy of content-level processing systems can be improved. Our system is built using semantics defined by context-free-grammar (CFG) to tag the streaming data. Unlike the traditional table look up and stack based engines used in CFG parsers, we explore a new method that maps the grammar structure on to the Field Programmable Gate Arrays (FPGA) hardware. The structure is a direct translation of the grammar which enables the meaning of the patterns to be determined based on the location of its detection. The parallel pattern detection engines are instantiated using FPGA resources. Our implementation scans for the regular expression patterns and determines their semantics as defined by a grammar. This highly parallel and fine grained pipelined engine with 8 bit input bus can operate in bandwidth above 2 Gbps. For a simple XML grammar example, our engine can detect and tag the patterns at 1.57 Gbps on Xilinx VirtexE FPGA and 4.26 Gbps on the new Virtex 4 devices. </font>

  * Secure Remote Control of Field-programmable Network Devices,
    by Haoyu Song, Jing Lu, John Lockwood, and James Moscola;
    Proceedings of IEEE Symposium on Field-Programmable Custom Computing Machines (FCCM), Napa, CA, April 20-23, 2004.
    (Abstract) (Click for Bibtex)

    @inproceedings{song_fccm04,
      author = {Haoyu Song and Jing Lu and John Lockwood and James Moscola},
      title = {{Secure Remote Control of Field-programmable Network Devices}},
      booktitle = {{Proceedings of IEEE Symposium on Field-Programmable Custom Computing Machines (FCCM)}},
      address  = {Napa, CA, USA},
      month = Apr,
      year  = 2004
    }

    (Abstract) (Close Bibtex)

    <font size=1> A circuit and an associated lightweight protocol have been developed to secure communication between a control console and remote programmable network devices. The circuit provides encryption, data integrity checking and sequence number verification to ensure confidentiality, integrity and authentication of control messages sent over the public Internet. All of these functions are performed directly in FPGA hardware to provide high throughput and near-zero latency. The circuit has been used to control and configure remote firewalls and intrusion detection systems. The circuit could also be used to control and configure other distributed network applications. </font>


Miscellaneous Papers

  * Control Packet Security for CAM based Firewall,
    by James Moscola, Jing Lu, Haoyu Song;
    unpublished course work, Washington University, April 2002.
    (Abstract) (Click for Bibtex)

    @unpublished{moscola_cs536,
      author = {James Moscola and Jing Lu and Haoyu Song},
      title = {{Control Packet Security for CAM based Firewall}},
      note = {unpublished course work, Washington University, (St. Louis, MO, USA)},
      month = Apr,
      year  = 2002
    }

    (Abstract) (Close Bibtex)

    <font size=1> This paper describes a encryption/authentication module for an FPGA-based system-on-chip (SOC) CAM-based firewall. The module implements several IPSec standards including the Advanced Encryption Standard (AES), Triple Data Encryption Standard (3DES), and keyed-hash message authentication code (HMAC) using both MD5 and SHA-1. Using these standards the firewall can be securely configured remotely via the Internet. The firewall has been implemented on the Field Programmable Port Extender (FPX) for use in theWashington University Gigabit Switch (WUGS). </font>

  * IPv6 Tunneling Over an IPv4 Network,
    by James Moscola, David Lim, and Alan Tetley;
    unpublished course work, Washington University, December 2001.
    (Abstract) (Click for Bibtex)

    @unpublished{moscola_cs535,
      author = {James Moscola and David Lim and Alan Tetley},
      title = {{IPv6 Tunneling Over an IPv4 Network}},
      note = {unpublished course work, Washington University, (St. Louis, MO, USA)},
      month = Dec,
      year  = 2001
    }

    (Abstract) (Close Bibtex)

    <font size=1> Due to the growth of the internet, the current address space provided provided by IPv4, with only 4,294,967,296 addresses, has proven to be inadequate. Because of IPv4's shortcomings, a new protocol, IPv6, has been created to take its place. This new protocol, using its 128-bit address scheme (thats 7x10^23 addresses per square meter of earth!), should provide enough addresses for everyone's computer, refrigerator and their toaster to have a connection to the internet. To help facilitate the movement from an IPv6 internet to an IPv4 internet we have created a module for the the Field Programmable Port Extender (FPX) in accordance with RFC1933. This module allows IPv6 packets coming from an IPv6 network to be packed into IPv4 packets, tunneled through an IPv4 network and then unpacked at the other end of the tunnel before reentering an IPv6 network. This approach to incorporating the new IPv6 specification allows a progressive changeover of networks from IPv4 to the newer IPv6. The current implementation runs at 80 MHz. </font>


Workshops

  * June 2002 FPX Workshop, Gigabit Kits Workshop, Saint Louis, MO, June 19-20, 2002.

  * January 2002 FPX Workshop, Gigabit Kits Workshop, Saint Louis, MO, January 3-4, 2002.


Patents

  * John Lockwood, Ronald Loui, James Moscola, Michael Pachos:
    Methods, Systems, and Devices Using Reprogrammable Hardware for High-Speed Processing of Streaming Data to Find a Redefinable Pattern and Respond Thereto
    Issued August 2006.[/publication]


[resactivity]Professional Service

  *  Session Chair : Globecom 2005

  *  Reviewer : International Symposium on Field-Programmable Gate Arrays (FPGA) (2006, 2007)

  *  Reviewer : International Conference on Field Programmable Logic and Applications (FPL) (2005)

  *  Reviewer : Hot Interconnects Symposium on High Performance Interconnects (HotI) (2004, 2005)[/resactivity]


Professional and Honor Societies

  *  IEEE

  *  Eta Kappa Nu

  *  Golden Key International Honor Society


























<IMAGE src="http://stat.onestat.com/stat.aspx?tagver=2&sid=341758&js=No&" alt="fraud click"/>

fraud click
