[pic]<IMAGE src="riko.jpg" alt=""/>[/pic]



Riko Jacob

[introduction]since May 2003 I am [position]member[/position] of the [affiliation]Algorithms, Data Structures, and Applications Group at the ETH Zürich[/affiliation]

Together with Matthias Müller-Hannemann I am PC co-chair of ATMOS 06.[/introduction]

Publications

Curriculum Vitae vita.ps vita.pdf

[contactinfo]Postal Adress:
      [address]ETH Zurich
      Institute of Theoretical Computer Science
      ETH Zentrum CAB J21.2
      CH - 8092 Zurich
      Switzerland[/address]

Office:
      [address]CAB J21.2, Universitätstrasse 6[/address]

Phone:
      [phone]+41-44-632 74 03[/phone]

Fax:  
      [fax]+41-44-632 13 99[/fax]

Mobile:
      [phone]+49 179 7924566[/phone]

Email:
      [email]rjacob ät inf.ethz.ch[/email]

      [email]rjacob at brics.dk[/email]
      (read ät as @)

Privat:
      [address]Uetlibergstr. 123
      8045 Zürich
      Switzerland[/address]
      Tel: [phone]+41 43 333 14 76[/phone]

or    
      [address]Keplerstr. 7

      81679 München

      Germany[/address]

      Tel: [phone]+49 89 74500033[/phone]

      Mobile: [phone]+49 179 7924566[/phone][/contactinfo]
