[pic]<IMAGE src="ghosh.jpg" alt=""/>[/pic] Sukumar Ghosh
[contactinfo][position]Professor[/position]
[affiliation]Department of Computer Science[/affiliation]
[address]201P Maclean Hall
[affiliation]The University of Iowa[/affiliation]
Iowa City, IA 52242-1419, USA[/address]
[email]ghosh at cs dot uiowa dot edu[/email]
Phone: [phone](319)335-0738[/phone], Fax: [fax](319) 335-3624[/fax][/contactinfo]


[resinterests]Research Areas of interest

The deployment of distributed systems in real applications has significantly increased. With the growth of embedded systems, the man-to-processor ratio is decreasing at an alarming rate. Large distributed systems view failures and perturbations as events and not catastrophic exceptions. Due to the dwindling man-to-processor ratio, it is not always feasible to ask for external intervention every time a failure or a perturbation occurs: future systems should be able to recover on their own. There are different paradigms addressing such issues: These are known as [interests]self-stabilization[/interests], [interests]self-healing[/interests], [interests]self-reconfiguration[/interests], [interests]autonomic computing[/interests], [interests]recovery-oriented computing[/interests], [interests]adaptive distributed systems[/interests] etc. These topics define the primary focus of my research.

Spontaneous recovery and adaptation to changing environments are sometimes accompanied by harmful side effects. For example, in self-stabilizing systems, even a single transient failure can corrupt the entire network before recovery begins. Paths to recovery can also compromise with the safety requirements. Examples are abundant in sensor networks and P2P networks. My research deals with various techniques for handling failures and recovery. The current topics of investigation are:

  *  [interests]Self-stabilizaton and games[/interests]

  *  [interests]Adaptive and self-optimizing P2P networks[/interests]

  *  [interests]Algorithms for sensor networks[/interests][/resinterests]

Graduate students

  *  Amlan Bhattacharya

  *  Anurag Dasgupta

  *  Ransom Briggs

Selected publications


Teaching Current courses
Computer Organization (22C:60) (Spring 2007)
Peer-to-peer Networks (22C:196:003)) (Spring 2007)
Distributed Systems and Algorithms (22C:166) (Fall 2006)

Past courses
Computer Organization (22C:060) (Summer 2006)
High-Performance Computer Architecture (22C:160/55:132) (Spring 2006)
Seminar on Systems and Networks (22C:294) (Spring 2006)


Additional Information

Biographical data
Eighth Symposium on Self-stabilizing Systems (SSS 2006)
Eighth International Conference on Distributed Computing and Networking (ICDCN 2006)
Self-stabilization bibliography.
