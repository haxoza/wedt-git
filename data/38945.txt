<IMAGE src="/img/czech.png" alt="(Czech)"/> <IMAGE src="/img/english.png" alt="(English)"/>

[pic]<IMAGE src="photo.jpg" alt=""/>[/pic]



[contactinfo]Ludek Kucera [position]Full professor[/position]
[affiliation]Department of Applied Mathematics
Faculty of Mathematics and Physics
Charles University[/affiliation], Prague

Tel. [phone](+420) 221 914 233[/phone]
Fax. [fax](+420) 257 531 014[/fax]
Secr.[phone](+420) 221 914 233[/phone]
[email]ludek@kam.mff.cuni.cz[/email]
Office: [address]room nr. 225
Building Malostranske nam. 25[/address][/contactinfo]


[resinterests]Research Areas [interests]combinatorial algorithms[/interests], [interests]probabilistic analysis[/interests], [interests]parallel computing[/interests][/resinterests]


Algovision

<IMAGE src="../gifs/mail.xbm" alt="Address:"/> Department of Applied Mathematics, Charles University,
Malostransk?nám. 25, 118 00 Praha 1, Czech Republic
