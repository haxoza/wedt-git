[pic]<IMAGE src="gautam.jpg" alt=""/>[/pic] <font size=7>Gautam Biswas</font>

  *  [introduction][position]Professor[/position], [affiliation]Electrical Engineering and Computer Science, Computer Engineering[/affiliation]

  *  [position]Senior Research Scientist[/position], [affiliation]Institute for Software Integrated Systems (ISIS)[/affiliation]

  * [position]Associate Director[/position], [affiliation]Center for Intelligent Systems[/affiliation][/introduction]



      <IMAGE src="hp.alphabetical1.gif" alt="[Education]"/> [education]Education

        * B. Tech. in [bsmajor]Electrical Engineering[/bsmajor], [bsuniv]Indian Institute of Technology[/bsuniv], Bombay, [bsdate]1977[/bsdate].

        * M.S. in [msmajor]Computer Science[/msmajor], [msuniv]Michigan State University[/msuniv], E. Lansing, [msdate]1980[/msdate].

        * Ph.D. in [phdmajor]Computer Science[/phdmajor], [phduniv]Michigan State University[/phduniv], E. Lansing, [phddate]1983[/phddate].[/education]


      <IMAGE src="note01.gif" alt="[Research]"/> [resinterests]Research

        * [interests]Modeling and Analysis of Complex Systems[/interests]

        * [interests]Intelligent Learning Environments and Cognitive Psychology[/interests]

        * Knowledge Discovery from Databases

        * [interests]Intelligent Manufacturing[/interests][/resinterests]


      <IMAGE src="button.faculty1.gif" alt="[Teaching]"/> Teaching (1998-2001)

        * Intelligent Learning Environments -- CS 364 (Fall 2004)

        *  Modeling and Simulation -- CS 274/ EECE 292 (Spring 2003,Spring 2004)

        * Model-Based Reasoning -- CS 367 (Spring 2002 -- New)

        * Model-Based Reasoning -- CS 367 (Spring 2001)

        * Intelligent Agents -- CS 367 (Spring 1998)

        * Artificial Intelligence -- CS 260 (Fall 2002, 2003) and CS 360

        * Programming Languages -- CS 270 (Fall 2001)


      <IMAGE src="note01.gif" alt="[Resume]"/> Resume

        * Complete Vita (.pdf file)

        * Complete Vita (postscript file)


      <IMAGE src="msbob.gif" alt="[Affiliations]"/> Affiliations

        * Center for Intelligent Systems (Associate Director)

        * Institute for Software Integrated Systems (ISIS)

        * Learning Sciences Institute at Vanderbilt

        * Learning Technology Center

        * Center for Innovation in Learning Technologies


      <font size=3>[publication]Special Issue of
      IEEE Trans. SMC Part B </font> <b> Diagnosis of Complex Systems:
      Bridging the methodologies of the FDI and DX Communities</b>
      (Guest Editors: Biswas, Cordier, Lunze, Trave-Massuyes, and Staroswecki)


      <font size=3> 18th Intl. Workshop on Principles of Diagnosis (DX 2007)</font> DX 2007 will be held at the Gaylord Opryland Center in Nashville, TN. It will be hosted by the Institute for Software Integrated Systems (ISIS) at Vanderbilt University[/publication].


      <font size=5>Personal</font> He is married to Sujata Biswas, a special education teacher, and they have two children, Pallavi and Nishant.


      [contactinfo]<font size=5>Office</font>

      <font size=3> </font>

      [affiliation]EECS Dept.[/affiliation] [address]Jacobs Hall, Room 366

      Box 1824 Sta B 400 24th Avenue South

      Vanderbilt University Vanderbilt University

      Nashville, TN 37325 Nashville, TN 37212[/address]

      Voice: [phone](615) 343-6204[/phone]

      Fax: [fax](615) 343-7440[/fax]

      Email: [email]gautam.biswas@vanderbilt.edu[/email][/contactinfo]

      <IMAGE src="students-button.gif" alt="[Students]"/> <IMAGE src="fundedpr-button.gif" alt="[Funded Projects]"/> <IMAGE src="other-button.gif" alt="[Other Links and Information]"/>

<IMAGE src="dept.gif" alt=""/> <IMAGE src="vu.gif" alt=""/>
