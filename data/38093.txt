<font size=13>E</font>RICA <font size=13>M</font>ELIS

[pic]<IMAGE src="ericaKopf06.jpg" alt=""/>[/pic]

[contactinfo]Privatdozentin Dr. Erica Melis
[position]Principal Researcher[/position]
[affiliation]German Research Institute for Artificial Intelligence (DFKI)[/affiliation]
&
[position]associate[/position] at
[affiliation]Universität des Saarlandes[/affiliation]

[address]D-66123 Saarbrücken
Germany[/address]
Phone: [phone]+49-681-302-4629[/phone]
Fax: [fax]+49-681-302-5076[/fax]

<IMAGE src="melis.jpg" alt=""/>

[address]Building D31 (DFKI Altbau), Room: -1.03[/address], campus map map Saarbrücken[/contactinfo]


<font size=5>Welcome! Hope you find something of interest here.
</font>

[ Research ][ Group ][ Projects ] [ Organization ][ Courses ] [ Publications ]

selected paintings 2000
selected paintings 2001
selected paintings 2002
selected paintings 2003
selected paintings 2005
selected paintings 2006


>[resinterests]Research Interests

  * 


    [interests]Intelligent Learning Environments and Tools Web-based education environment[/interests] <b>ActiveMath</b> for Mathematics. This includes AI-techniques such as user-adaptivity, course generation, student modeling, cognitive tools, exercise generation, feedback generation, diagnosis, tutorial strategies, integration of service systems, etc.
    Find the official demo here.

  * 


    [interests]Proof Planning[/interests] ... [interests]knowledge-based proof planning[/interests], [interests]constraint solving for proof planning[/interests], [interests]meta-reasoning[/interests], [interests]productive use of failure[/interests]. My favoured case study on epsilon-delta-proofs...
    Mixed-initiative proof planning for education, <b>MIPPA</b> , including user-interface for proof planning, feedback generation, communication with learning system, scenarios, etc.

  * 

    [interests]Case-Based Reasoning[/interests], [interests]Analogy[/interests]

  *  [interests]Deduction[/interests]


Leading the ActiveMath GroupGroup ... at University of Saarland and German Research Center for Artificial Intelligence[/resinterests]. This is us <IMAGE src="AMteam2004.jpeg" alt=""/> .


>Projects


Current Projects for Technology-Enhanced Learning

  * <b> EU-project LeActiveMath </b>
    I am coordinating the <font size=3.5>FP6</font> European project <font size=3.5>Language-enhanced, user-adaptive, interactive eLearning in Mathematics</font>, <b> LeActiveMath</b>. This is a project in the 6th framework of the EU, key action Technology-Enhanced eLearning. The project runs for three years, started January 2004 and has 9 European partners. See the web site for events and news.

  *  EU-project <b>iClass</b>

  *  EU-project <b>Mowgli</b>

  *  DFG project <b>MIPPA</b>

Previous projects include

  *  SFB 378

  *  SFB 314


> [resactivity]Recent Organizational Activities member of program committee of recent conferences, among others,

  *  WWW 2006

  *  User Interfaces for Theorem Provers 2003, 2005

  *  International Conference Cognition and Exploratory Learning in the Digital Age, 2004, 2005

  *  World Conference on E-Learning in Corporate, Government, Healthcare, and Higher Education, 2002, 2003, 2004, 2005

  *  IADIS International Conference on WWW/Internet 2004, 2005

  *  7th International Conference on Artificial Intelligence and Symbolic Computation, 2004

  *  International Conference on Case-Based Reasoning 2001, 2003, 2005.[/resactivity]


>Courses and other Education

  *  Master theses and doctoral theses
    in Arbeit befindliche und zu vergebende Bachelor- und
    Master- und Diplomarbeiten

  *  WS 2006/2007 Intelligent Educational Technologies

  *  SS 2006: Seminar Intelligent Tutoring Systems

  *  WS 2005/2006: Seminar Hands-on Mathematics for Computer Scientists

  *  SS 2004/2005 Educational Technologies

  *  WS 2004/2005 Seminar: Hands-on Mathematik für Informatiker

  *  WS 2004/2005 Interaktive Übungen zur Vorlesung Mathematik für Informatiker

  *  WS 2003/2004 Seminar: Hands-on Mathematics for Computer Scientists

  *  SS 2003 educational technologies

  *  SS 2003 AI-Planning/ automatische Planverfahren

  *  SS 2002 e-learning

  *  SS 2001

  *  WS 2001 Proseminar Benutzermodellierung und -adaptivität

  * SS 2000 Course on AI-planning and proof planning

  *  WS 1999 seminar on AI-planning

  * SS 1998 Machine Learning

  * SS 1997 Introduction to Artificial Intelligence

  * SS 1996 Analogy and Case-Based Reasoning


> [publication]Selected Publications

  2006 *  Reductio ad Absurdum: Planning Proofs by Contradiction. Essays in Honor of Gigina Aiello, O. Stock (ed), Springer-Verlag, LNAI 4155, 2006. with M. Pollet and J. Siekmann. preprint

  *  LeActiveMath, First European Conference on Technology Enhanced Learning 2006, with J. Haywood and T. Smith preprint

  *  Querying Heterogeneous and Distributed Learning Object Repositories via Ontology-Based Mediation. First European Conference on Technology Enhanced Learning 2006, with P. Kärger and C. Ullrich.

  *  Combining ITS and eLearning Technologies: Opportunities and Challenges. Intelligent Tutoring Systems (ITS-06), pages: 278-287, with C. Brooks and J. Greer, LNCS 4053, 2006. preprint

  *  Challenges in Search and Usage of Multi-Media Learning Objects. Journal of Computer Science and Technology with R. Shen, J. Siekmann, C. Ullrich, F. Yang, P. Han

  *  Semantic-Aware Components and Services in <b> ActiveMath</b>, British Journal of Educational Technology. Special Issue: Semantic Web for E-learning, Special Issue, 37(3): 405-423, 2006.

  * <b>ActiveMath!</b> In: OMDoc: An Open Markup Format for Mathematical Documents (M. Kohlhase), LNAI, Springer-Verlag

  *  Semantic Search in LeActiveMath. WebALT Conference 2006, pdf with P.Libbrecht

  *  Impasse-Driven Reasoning in Proof Planning. Proceedings of the Fourth International Conference on Mathematical Knowledge Management, LNAI 3863, pages:143-158, 2006. at SpringerLink preliminary pdf with A. Meier

  *  Authoring Presentation for OpenMath Proceedings of the Fourth International Conference on Mathematical Knowledge Management (MKM2005), LNAI 3863, pages:33-48, 2006. at SpringerLink preliminary pdf

    2005

  *  Interactivity of Exercises in <b> ActiveMath</b>. International Conference of Computer in Education and Learning (ICCEL-05), pdf with Goguadse and Gonzales-Palomo

  *  ePortfolios in <b>ActiveMath</b>. ePortfolio 2005, 213-222, Cambridge, 2005. pdf with M. Homik

  *  e-Learning Logic and Mathematics: What we Have and What we Still Need. In: Essays in Honor of Dov Gabbay. pdf with J. Siekmann

  *  Failure Reasoning in Multiple-Strategy Proof Planning. Electronic Notes in Theoretical Computer Science, vol 125 M.P. Bonacina and T. Boy de la Tour (eds), pages 67-90, 2005. pdf with A. Meier

  *  MULTI: A Multi-Strategy Proof Planner. 20th International Conference on Automated Deduction (CADE-20), 250-254, 2005. pdf, with A. Meier.

  *  Interactive Concept Mapping in <b> ActiveMath</b>, 3. Deutsche e-Learning Fachtagung der Gesellschaft fuer Informatik, DeLFI pdf. with P.Kaerger and M. Homik

  *  Design of Erroneous Examples for <b> ActiveMath</b>. International Conference on AI in Education. IOS Press, 2005. pdf

  *  Global Feedback in <b> ActiveMath</b>. In: International Journal of Computers in Mathematics and Science Teaching, AACE, 2005 with E.Andres pre version

  *  Why Proof Planning for Maths Education and How? In: Festschrift in Honor of Jörg Siekmann, D. Hutter and W.Stephan (eds), LNCS 2605, pages 364-376 2005. pdf

    2004

  *  Erroneous Examples as a Source of Learning in Mathematics. International Conference: Cognition and Exploratory Learning in the Digital Age (CELDA), Kinshuk, D.G. Sampson, P. Isaias (eds), pages 311-318, 2004. doc

  *  Constraint Solving for Proof Planning. In: Journal of Automated Reasoning,33(1): 51-88, 2004. with J.Zimmer pdf

  *  Adaptable Mixed-Initiative Proof Planning for Educational Interaction. Electronic Notes in Theoretical Computer Science, 2004 with A.Meier and M. Pollet pdf

  *  Towards Adaptive Generation of Faded Examples. International Conference on Intelligent Tutoring Systems, ITS-2004, J.C. Lester and R.M. Vicari and F. Paraguacu (eds), pages 762-771, with G. Goguadze pdf

  * <b> ActiveMath</b>: An Intelligent Tutoring System for Mathematics. Seventh International Conference 'Artificial Intelligence and Soft Computing (ICAISC). L. Rutkowski and J. Siekmann and R. Tadeusiewicz and L.A. Zadeh (eds), Springer-Verlag, pages 91-101, 2004. with J. Siekmann pdf

  *  Gender-Biased Adaptations in Educational Adaptive Hypermedia. Proceedings of the Third International Conference on Adaptive Hypermedia and Adaptive Web-Based Systems. pages {425-428, Springer-Verlag, 2004. with C. Ullrich pdf

  *  Adaptive Access to a Proof Planner. Third International Conference on Mathematical Knowledge Management (MKM-2004), A. Asperti and G. Bancerek and A. Trybule (eds), pages 251-264, Springer-Verlag, 2004. with A.Meier and M. Pollet pdf

  *  They Call It Learning Style But It's so Much More. World Conference on E-Learning in Corporate, Government, Healthcare, and Higher Education (eLearn-2004), pages 1383-1390, 20004. with R. Monthienvichienchai pdf

    2003

  *  Education-Relevant Features in <b>ActInMath</b>. In: Online Educa, 9th International Conference on Technology Supported Learning and Training, 2003.

  *  The Poor Man's Eyetracker DFKeye. In: International Conference on Human Computer Interaction, 2003 with C.Ullrich and D.Wallach. doc

  *  User Interface for Adaptive Suggestions for Interactive Proof. In: User Interfaces for Theorem Provers, 2003. with M.Pollet and A.Meier

  *  Local and Global Feedback In: International Conference on AI in Education, 2003. E. Melis. pdf

  *  Polya-Scenarios in<b> ActiveMath</b>. In: International Conference on AI in Education, 2003, with C. Ullrich. pdf

  *  Global Feedback in <b>ActiveMath</b>. In: Proceedings of the World Conference on E-Learning in Corporate, Government, Healthcare, and Higher Education with E. Andres. (eLearn-2003), 2003. pdf

  *  Lessons for (Pedagogic) Usability Design of eLearning Systems. In: Proceedings of the World Conference on E-Learning in Corporate, Government, Healthcare, and Higher Education (eLearn-2003), 2003. with M. Weber. pdf

  *  Problems and Solutions for Markup for Mathematical Examples and Exercises. In: International Conference on Mathematical Knowledge Management, MKM03, 2003. with G. Goguadse, C. Ullrich and P. Cairns pdf

    2002

  *  About the Global Suggestion Mechanisms in <b> ActiveMath</b>. ITS-02 Workshop on Creating Diagnostic Assessments, 2002, with E. Andres

  *  The Poor Man's Eyetracker in <b> ActiveMath</b>. InternationalWorld Conference on E-Learning in Corporate, Gevernment, Healthcare, and Higher Education 2002. with C. Ullrich

  *  Call for a Common Web-Repository of Interactive Exercises. Workshop on Internet-Accessible Mathenatical Computation 2002.

  *  Using Computer Algebra Systems as Cognitive Tools. 6th International Conference on Intelligent Tutor Systems, 2002, with the ActiveMath Group

  *  Knowledge Representation and Management in <b> ActiveMath</b>. Journal of Artificial Intelligence and Mathematics. 2002, with the ActiveMath Group

  *  Proof Development with OMEGA. 19th Conference on Automated Deduction, 2002, with the Omega Group

    2001

  * <b> ActiveMath</b>: A Generic and Adaptive Web-Based Learning Environment. Journal of Artificial Intelligence and Education 12(4), pages 385-407, 2001. pdf

  *  Randomization and Restarts in Proof Planning. European Conference on AI-Planning 2001, pages 403-408 with A. Meier and C. Gomes pdf

  * Automated Proof Planning for Instructional Design . Annual Conference of the Cognitive Science Society 2001, with Ch. Glasmacher, C. Ullrich, and P. Gerjets.

  *  <b> ActiveMath</b>: System Description . International Conference on AI and Education (AIED-2001)

  *  Generating Personalized Educational Documents Using a Presentation Planner, ED-Media 2001, with P.Libbrecht and C. Ullrich.

    2000

  * Dialog Issues for a Tutor System Incorporating Expert Problem Solvers. AAAI Fall Symposium on Building Dialog Systems for Tutorial Applications, E. Melis and H. Horacek

  *  The `Interactive Textbook' Project. E. Melis. CADE workshop on deduction and education.

  *  Extensions of Constraint Solving for Proof Planning. E. Melis and J. Zimmer and T. Mueller. ECAI-2000.

  *  Proof Planning with Multiple Strategies. E. Melis and A. Meier. CL-2000.

  *  Adaptive Course Generation and Presentation. ITS workshop on Adaptive and Intelligent Web-Based Education Systems.

  *  Domain Knowledge for Search Heuristics in Proof Planning. E. Melis and M. Pollet. AIPS workshop on Analyzing and Exploiting Domain Knowledge.

  *  On the Benefit of Expert Services in Mathematics Education Systems. E. Melis and A. Fiedler. ITS workshop on Modeling Human Teaching Tactics and Strategies.

  *  Concepts in Proof Planning. E.Melis and J.H. Siekmann,
    In: Intellectics and Computational Logic., pages: 263-276, Kluwer, 2000. pdf

    1999

  *  Knowledge-Based Proof Planning. E. Melis and J.H. Siekmann
    Artificial Intelligence Journal, 115(1): 65-105, 1999. preliminary version

  * Employing External Reasoners in Proof Planning, E. Melis and V. Sorge
    Calculemus'99 workshop, pages 123-134.

  * Omega - A Mathematical Assistant System. J.H. Siekmann, M. Kohlhase, and E. Melis
    In: Essays Dedicated to Johan van Benthem on the Occasion of his 50th Birthday , Amsterdam University Press, published on CDROM.

  *  A Proof Presentation Suitable for Teaching Proofs. E. Melis and U. Leron
    9th International Conference on Artificial Intelligence in Education, pages 483-490, 1999.

  *  Flexibly Interleaving Processes. E. Melis and C. Ullrich,
    International Conference on Case-Based Reasoning, pages 263-275, 1999.

  *  Analogy in Inductive Theorem Proving. preliminary version E. Melis and J. Whittle,
    Journal of Automated Reasoning 22(2): 117-147, 1999.

    1998

  * Proof Presentation Based on Proof Plans, E. Melis, technical report.

  *  Omega: Ein mathematisches Assistenzsystem. J.Siekmann, M. Kohlhase, E. Melis.
    In: Kognitionswissenschaft, 1998, (in German).

  *  Constraint Solving in Logic Programming and in Automated Deduction: a Comparison. A. Armando, E. Melis, and S. Ranise.
    International Conference on Artificial Intelligence: Methodology, Systems, and Applications (AIMSA-98)

  *  Reformulation in Case-Based Reasoning. E. Melis, J. Lieber, and A. Napoli.
    European Conference on Case-Based Reasoning 1998.

  *  Similarities and Reuse of Proofs in Formal Software Verification. E. Melis and A. Schairer.
    European Conference on Case-Based Reasoning 1998.

  *  AI-Techniques in Proof Planning. E. Melis,
    European Conference on Artificial Intelligence (ECAI-98).

  * An Argument for Derivational Analogy. E. Melis and J.G. Carbonell,
    Advances in Analogy Research, 1998.

  *  Case-Based Reasoning Applied to Planning. R. Bergmann, H. Munoz-Avila, M.M. Veloso, and E. Melis,
    In: Case-Based Reasoning Technology from Foundations to Applications. M. Lenz, B. Bartsch-Spörl, H-D. Burkhard, and S. Wess (eds.), Springer.

  * The "Limit" Domain E. Melis,
    Artificial Intelligence Planning Systems (AIPS'98).

  * The Heine-Borel Challenge Problem: In Honor of Woody Bledsoe. E. Melis,
    Journal of Automated Reasoning 20(3).

    1997

  * Progress in Proof Planning: Planning Limit Theorems Automatically. E. Melis, technical report, October 1997.

  * External Analogy in Inductive Theorem Proving. E. Melis and J. Whittle, German Annual Conference on Artificial Intelligence, 1997

  * Solution-Relevant Abstractions Constrain Retrieval and Adaptation. E. Melis, In: Case-Based Reasoning Research and Development, D. Leake and E. Plaza, 1997.

  * Beweisen durch Analogie. E. Melis, Kognitionswissenschaft (journal), in German, 1997.

  * Analogy as a Control Strategy in Theorem Proving. E. Melis, J. Whittle, FLAIRS conference, 1997

    1996

  * Island Planning and Refinement, E. Melis Technical report, SR-96-10, 1996.

  * Internal Analogy in Theorem Proving, E. Melis, J. Whittle, Conference on Automated Deduction (CADE-96).

  * Two Kinds of Non-monotonic Analogical Inference, M. Kerber, E. Melis, In: Practical Reasoning, LNAI 1085, 361-374.

  * When to Prove Theorems by Analogy? E. Melis. German Annual Conference on Artificial Intelligence, 1996.

  *  Planning and Proof Planning. E. Melis and A. Bundy, ECAI-96 Workshop on Cross-Fertilization in Planning, 1996.

    1995

  *  Analogy in Clam. E. Melis, DAI Research Paper 766, Edinburgh 1995.

  *  A Model of Analogy-Driven Proof-Plan Construction, E. Melis, International Joint Conference on Artificial Intelligence (IJCAI-95).

  *  Theorem Proving by Analogy - A Compelling Example E. Melis, Progress in Artificial Intelligence.

  *  Using Exemplary Knowledge for Justified Analogical Reasoning, M. Kerber and E. Melis, WOCFAI-95.

    1994

  *  Analogy Makes Proofs Feasible. E. Melis and M. Veloso, CBR-workshop AAAI-94.

  *  Decomposition Techniques and their Applications. E. Melis, International Conference on Artificial Intelligence: Methodology, Systems, and Applications (AIMSA-94).

  *  How Mathematicians Prove Theorems, E. Melis, Sixteens Annual Conference of the Cognitive Science Society 1994.

  *  Representing and Transferring Diagonal Methods. Technical Report CMU-CS-94-174.

    1993 and before

  *  System and Processing View in Similarity Assessment. S. Wess, D. Janetzko, and E. Melis, Proceedings of the First European Workshop on CBR, 1993.

  *  Analogous Proofs - A Case Study, E. Melis, SEKI-Report SR-93-13.

  *  Goal-Driven Similarity Assessment. D. Janetzko, S. Wess, and E. Melis. Proceedings of the 16th German AI-Conference (GWAI-92), LNAI 671.

  *  Grundlagen von Analogiebildung - Zu Perspektiven ihrer Rechnerstuetzung. Communication and Cognition 22/1989.

  *  Das Geheimnis der Wendeltreppe. E. Melis and Ch. Melis Wissenschaft und Fortschritt 39/1989.

  *  Some Considerations about Formalization of Analogical Reasoning. E. Melis and Ch. Melis, In: Analogical and Inductive Inference, LNAI 265, 1987.[/publication]

some links

<IMAGE src="http://www.ags.uni-sb.de/Counter/counter.cgi?melis-main" alt=""/> since June 1, 99
