<b>Angus Roberts</b>

<b>Home</b>
<b>Research</b>
<b>Utils & Notes</b>
<b>Software</b>
<b>Etc</b>

<b>About</b>
<b>Contact</b>
<b></b>
<b></b>
<b></b>

[pic]<IMAGE src="./images/angus.jpg" alt="Angus Roberts"/>[/pic]

[introduction]I am a [position]PhD student[/position] in the [affiliation]Natural Language Processing Group, Department of Computer Science, at Sheffield University[/affiliation].

My Supervisor is Rob Gaizauskas

My PhD is associated with the [interests]CLEF project[/interests]. I used to work for another CLEF partner, the Medical Informatics Group at Manchester University.[/introduction]

Last modified: Thu 22 Sep 2005 09:50:58
