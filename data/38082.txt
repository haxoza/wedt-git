[pic]<IMAGE src="if2004small.jpg" alt=""/>[/pic]


[contactinfo]<font size=4>Ichiro Fujinaga</font>

[position]Associate Professor[/position]
[affiliation]Music Technology Area
Centre for Interdisciplinary Research in Music Media and Technology (CIRMMT)
Schulich School of Music, McGill University[/affiliation]
<font size=2>[address]555 Sherbrooke Street West, Montreal, QC H3A 1E3[/address]
Tel: [phone]514.398.4535x00944[/phone] Fax: [fax]514.398.8061[/fax] Email: [email]ich.at.music.mcgill.ca[/email]</font>[/contactinfo]


<b><font size=4>Research (wiki) </font></b>

  * Distributed Digital Music Archives and Libraries (DDMAL)

  * McGill Audio Preservation Project (MAPP)

  * Gamera (framework for building custom document recognition systems)

  * Autonomous Classification Engine (ACE)

  * [resinterests]Research interests: [interests]optical music recognition[/interests], [interests]lazy learning (exemplar-based learning)[/interests], [interests]digital signal processing[/interests], [interests]music information retrieval[/interests], [interests]pattern recognition[/interests], [interests]music perception[/interests].[/resinterests]


[education]<b><font size=4>Education</font></b>

Ph.D. [phdmajor]Music Technology[/phdmajor] [phduniv]McGill University[/phduniv]
M.A. [msmajor]Music Theory[/msmajor] [msuniv]McGill University[/msuniv]
B.Mus. [bsmajor]Music Theory and Percussion[/bsmajor] [bsuniv]University of Alberta[/bsuniv]
B.S. [bsmajor]Mathematics[/bsmajor] [bsuniv]University of Alberta[/bsuniv][/education]

<b><font size=4>Current Classes</font></b>

  * Seminar: Music Information Acquisition, Preservation, and Retrieval (MUMT611)

  * Advanced Multimedia Development (MUMT402)


Other Research Publications

Previous Classes Literature Search

Other links

cv.pdf

<IMAGE src="amnesty_blue_banner.gif" alt="Amnesty International"/> <IMAGE src="hunger.gif" alt="the hunger site"/> <IMAGE src="rotate_rib.gif" alt="Free Speech Online"/>

<font size=2>Created: 2002/09/01</font><font size=2> Modified: Ichiro Fujinaga </font> <IMAGE src="mcrest_red.gif" alt="McGill Crest"/>
