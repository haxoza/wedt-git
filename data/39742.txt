Home Vitae Publications Research Personal Links



Li-San Wang's Homepage


Openings and research opportunities

  1.  Two postdoctoral positions available. Click here for the announcement.

  2.  Graduate and undergraduate students: if you are interested in rotation or research projects, please contact me for appointments, so I can learn more about your interests and discuss possible research topics.

  3.  Prospective applicants to PhD programs: please check the GCB program website for related information.


[introduction]Personal Information

Starting January 2007, I will join the [affiliation]Department of Pathology and Laboratory Medicine, University of Pennsylvania[/affiliation], as an [position]Assistant Professor[/position]. I will be also affiliated with the [affiliation]Institute on Aging and Penn Center for Bioinformatics (PCBI)[/affiliation].

My research interests include [interests]disease-related bioinformatic research (aging, neurodegenerative diseases, and cancer)[/interests], [interests]computational methods for microarray analysis and systems biology (semi-parametric and nonparametric analysis[/interests], [interests]transcription factor prediction and regulatory networks)[/interests], and [interests]computational phylogenetics (genome rearrangement phylogeny, phylogenetic multiple sequence alignment, nontree evolution)[/interests].[/introduction]

[contactinfo]<b>E-Mail:</b>
[email]lswang@mail.med.upenn.edu[/email]
<b>Tel:</b>
[phone](Mobile) (484) 5530284[/phone]
[phone](Lab) (215) 898-8395[/phone]
<b>Mailing address:</b>
(Office)
[address]Department of Biology
103I Lynch Laboratory
433 S University Avenue
University of Pennsylvania
Philadelphia, PA 19104 USA[/address][/contactinfo]

Visitors since August 2003:
<IMAGE src="http://s15.sitemeter.com/meter.asp?site=s15lswangpenn" alt="Site Meter"/>
