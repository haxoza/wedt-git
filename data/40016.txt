<b><font size=5>Paulo Cesar Masiero </font></b><b><font size=5>Contact:</font></b>

<font size=4>End. Elet.: masiero </font>

<font size=4>Phones: 55 16  3373 9696 (ICMC Office)</font>
<font size=4> 55 11  3091 6359 (CCI Office)</font>
<font size=4>     Fax: 55 16  3373 9751/8118</font>

[pic]<IMAGE src="MASIERO2.JPG" alt=""/>[/pic]

<b><font size=5>Current Positions</font></b>


[introduction]<font size=5> [position]Full Professor[/position] at the [affiliation]Instituto de Ciências Matemáticas e de Computação[/affiliation]</font>

<font size=5> [affiliation]Universidade de São Paulo, Brazil.[/affiliation]</font>

<font size=5> [position]Chaiman[/position] of the [affiliation]Information Systems?Course of the Escola de Arte, Ciências[/affiliation]</font>

<font size=5> [affiliation]e  Humanidades, Universidade de São Paulo, Brazil.[/affiliation]</font>[/introduction]

<font size=5> </font>

[education]<b><font size=5>Education</font></b>

  * <font size=5>Visiting Scholar: Technical University of Denmark, Dec.93/Ago.94</font>

  * <font size=5>Visiting Scholar: University of  Michigan, Feb.1985/Feb/86</font>

  * <font size=5>Visiting Scholar: University of Strathclyde, UK, Dec.89/Fev.90</font>

  * <font size=5>[phddegree]D. Sc.[/phddegree] [phdmajor]Business Administration (Area: Quantitative Methods)[/phdmajor], [phduniv]FEA-USP[/phduniv] [phddate]1984[/phddate]</font>

  * <font size=5>[msdegree]M.Sc.[/msdegree] [msmajor]Computer Science[/msmajor] [msuniv]ICMC-USP[/msuniv], [msdate]1979[/msdate]</font>

  * <font size=5>[bsdegree]B.Sc[/bsdegree] [bsmajor]Mathematics[/bsmajor]: [bsuniv]Universidade Estadual Paulista - UNESP[/bsuniv], [bsdate]1975[/bsdate]</font>[/education]

[resinterests]<font size=5>Research Interests:</font>

  * <font size=5> [interests]Software Engineering[/interests]</font>

    * <font size=5> [interests]Software Analysis and Design Methods[/interests]</font>

    * <font size=5> [interests]Software Patterns[/interests], [interests]Frameworks[/interests]</font>

    * <font size=5>[interests]Aspect Oriented Software Development[/interests] </font>

  * <font size=5>[interests]Information Systems[/interests]</font>

      * <font size=5> [interests]Analysis Pattern Languages[/interests]</font>

  * <font size=5> [interests]Ethics in Computing[/interests]</font>[/resinterests]

<font size=5> Group:    Software Engineering and Information Systems
Lab    :     LABES (Software Engineering Lab)
(<b>LAB</b>oratório de <b>E</b>ngenharia de <b>S</b>oftware)</font>

<b><font size=5>Publications:</font></b>

  * <font size=5>Selected Publications</font>

  * <font size=5>Essays</font>

    * <font size=5>A  USP e o Erro do Milênio</font>

    * <font size=5>Sistemas de Informação ou Análise de Sistemas ?</font>

    * <font size=5>A greve na rede</font>

    * <font size=5>A Ética em Computação e o Caso do Senado</font>

    * <font size=5>Proposta curricular sobre fundamentos de direito para a computação</font>

<b><font size=5> Books</font></b>

  [contactinfo]<font size=5>Ética em Computação
  Editora da Universidade de São Paulo, 2000, 212p.
  Em São Paulo: [email]lojas da EDUSP, Liv. Cultura, Liv. da Vila e FINAC[/email]
  Outros estados, comprar diretamente da EDUSP:
  www.usp.br/edusp
  Telemarketing: [phone]3818 4150[/phone] e [phone]3818 4008[/phone]</font>[/contactinfo]

  <font size=5>Análise Etruturada pelo Método de Jackson
  Editora Edgar Blücher, 1992, 230 p.</font>
  (Obs. esgotado)
