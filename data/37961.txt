[introduction]Noam Rinetzky's Home Page I'm a [position]PhD student[/position] at the [affiliation]Computer Science Dept. of Tel Aviv University[/affiliation].[/introduction]

[pic]<IMAGE src="gif/RinetzkyNoam.jpg" alt=""/>[/pic]


[contactinfo]Contacts

<IMAGE src="gif/bul-purple.gif" alt=""/> <b>University:</b>

<IMAGE src="gif/bul-cyan.gif" alt=""/> <b>Room:</b> Open Space; [address]Schriber Bld.; Tel Aviv University[/address]

<IMAGE src="gif/bul-cyan.gif" alt=""/> <b>Phone:</b> [phone]+972-3-6405358[/phone]

<IMAGE src="gif/bul-cyan.gif" alt=""/> <b>email</b>: [email]maon 'at' post 'dot' tau 'dot' ac 'dot' il[/email]


[position]Teaching Assistant[/position][/contactinfo]

<IMAGE src="gif/bul-purple.gif" alt=""/> Program analysis; Spring, 2003-2004

<IMAGE src="gif/bul-purple.gif" alt=""/> Workshop in Computer Science; Spring, 2004-2005

<IMAGE src="gif/bul-purple.gif" alt=""/> Program analysis; Spring, 2004-2005


Research


[resinterests]Research Intrests:

<IMAGE src="gif/bul-purple.gif" alt=""/> [interests]Program analysis[/interests]

<IMAGE src="gif/bul-purple.gif" alt=""/> [interests]Shape analysis[/interests]

<IMAGE src="gif/bul-purple.gif" alt=""/> [interests]Program understanding[/interests]

<IMAGE src="gif/bul-purple.gif" alt=""/> [interests]Software design[/interests][/resinterests]


Phd My thesis advisor is Mooly Sagiv.


[education][msdegree]MsC[/msdegree] I did my Masters degree in the [msmajor]Computer Science[/msmajor] Dept. of the [msuniv]Technion[/msuniv] under the supervision of Mooly Sagiv and Orna Grumberg.
My MsC. thesis topic was "Interprocedural Shape Analysis". [bib] [pdf][/education]


[publication]Publications

Conference Publications

  * <b>Interprocedural shape analysis for cutpoint-free programs</b>
    N. Rinetzky, M. Sagiv, and E. Yahav
    SAS '05: 12th International Static Analysis Symposium, London, September 7-9, 2005. ?Springer-Verlag
    [bib] [pdf] [ps] [talk]

  * <b>A semantics for procedure local heaps and its abstractions</b>
    N. Rinetzky, J. Bauer, T. Reps, M. Sagiv, and R. Wilhelm
    POPL '05: 32nd Annual ACM SIGPLAN-SIGACT Symposium on Principles of Programming Languages, Long Beach, California, January 12-14, 2005, pages: 296--309. ?ACM, (2005)
    [bib] [pdf] [ps] [talk]

  * <b>Towards an object store</b>
    A. Azagury, V. Dreizin, M. Factor, E. Henis, D. Naor, N. Rinetzky, O. Rodeh, J. Satran, A. Tavory, and L. Yerushalmi
    MSS '03: 20th IEEE/11th NASA Goddard Conference on Mass Storage Systems and Technologies, April 7-10, 2003, San Diego, California, USA, pages: 165--176. ?IEEE, (2003)
    [bib] [pdf]

  * <b>Interprocedural shape analysis for recursive programs</b>
    N. Rinetzky and M. Sagiv
    CC '01: Compiler Construction, 10th International Conference, CC 2001 Held as Part of the Joint European Conferences on Theory and Practice of Software, ETAPS 2001 Genova, Italy, April 2-6, 2001, pages: 133--149. ?Springer-Verlag
    [bib] [pdf] [talk]

Workshop Publications

  * <b>A Two-layered approach for securing an object store network</b>
    A. Azagury, R. Canetti, M. Factor, S. Halevi, E. Henis, D. Naor, N. Rinetzky, O. Rodeh, and J. Satran
    SISW '02: 1st International IEEE Security in Storage Workshop, Greenbelt, Maryland, USA, Greenbelt, Maryland, USA, pages: 10--23. ?IEEE, (2002)
    [bib] [pdf]

Technical Reports

  * <b>Componentized Heap Abstraction</b>
    N. Rinetzky, G. Ramalingam, M. Sagiv, and E. Yahav
    TR-164/06, School of Computer Science, Tel Aviv University, December 2006
    [bib] [pdf] [ps]

  * <b>Modular Shape Analysis for Dynamically Encapsulated Programs</b>
    N. Rinetzky, A. Poetzsch-Heffter, G. Ramalingam, M. Sagiv, and E. Yahav
    TR-407/06, School of Computer Science, Tel Aviv University, October 2006
    [bib] [pdf] [ps] (summarized version to appear in ESOP '07)

  * <b>Interprocedural shape analysis for cutpoint-free programs</b>
    N. Rinetzky, M. Sagiv, and E. Yahav
    TR-104/05, School of Computer Science, Tel Aviv University, April 2005
    [bib] [pdf] [ps] (summarized version appeared in SAS '05)

  * <b>Interprocedural functional shape analysis using local heaps</b>
    N. Rinetzky, M. Sagiv, and E. Yahav
    TR-26/04, School of Computer Science, Tel Aviv University, November 2004
    [bib] [pdf] [ps]

  * <b>A semantics for procedure local heaps and its abstraction</b>
    N. Rinetzky, J. Bauer, T. Reps, M. Sagiv, and R. Wilhelm
    AVACS Technical Report No. 1, SFB/TR 14 AVACS, October 2004
    [bib] [pdf] [ps] (summarized version appeared in POPL '05)


Talks

  *  <b>Interprocedural shape analysis for cutpoint-free programs</b>
    23-Jun-2005 Dagstuhl seminar 05251: Types for Tools: Applications of Type Theoretic Techniques [ppt]
    22-May-2005 Joint verification day (Tel Aviv University) [ppt] (with a very short introduction to interprocedural analysis)

  *  <b>A semantics for procedure local heaps and its abstractions</b>
    10-Aug-2005 Summer School Marktoberdorf 2005 (student's session) [ppt] (15 min.)
    13-Jan-2005 POPL'05 [ppt] (30 min.)
    10-Jan-2005 PL/SE Seminar (IBM WT.J. Watson research center) [ppt] (50 min.)

  *  <b>Interprocedural shape analysis for recursive programs</b>
    2-April-2001 CC '01 [ppt] (30 min.)[/publication]


Personal

<IMAGE src="gif/bul-gold.gif" alt=""/> My art

The page's <IMAGE src="gif/wc.gif" alt=""/> count says that you are visitor number <IMAGE src="http://counter.digits.com/wc/-d/4/-z/rinetzky" alt=""/>
