[pic]<IMAGE src="foto.jpg" alt=""/>[/pic] <font size=4> [contactinfo]<b>Prof. Dr. Klaus Weihrauch</b>
[affiliation]Theoretische Informatik I
FernUniversität Hagen[/affiliation]
[address]D-58084 Hagen
Germany[/address]

Tel. [phone]+49-2331-987-2722[/phone]
Fax [fax]+49-2331-987-319[/fax]
E-Mail </font>[/contactinfo]


[introduction]<font size=4> <b> Curriculum Vitae </b>

  *  Diplom 1970, Physics, University of Hamburg,

  *  PhD [phddate]1973[/phddate], [phdmajor]Computer Science[/phdmajor], [phduniv]University of Bonn[/phduniv],

  *  Habilitation 1975, Computer Science, University of Bonn,

  *  1976-1979 Professor for Computer Science at RWTH Aachen,

  *  since 1979 Professor for Computer Science at University of Hagen.[/introduction]


</font>

<b> Research Interests </b>

  *  [interests]Recursion theory[/interests],

  *  [interests]complexity theory[/interests],

  *  [interests]computable analysis[/interests].


<b> Links </b>

  * Publications,

  * Computable Analysis, Springer-Verlag Berlin/Heidelberg, 2000,

      * Errata, last update in January 2005,

  * CCA Net,

  *  Theoretische Informatik I.


<IMAGE src="/cgi-bin/nph-count?link=/thi1/klaus.weihrauch/index.html&show=NO" alt=""/>
