[pic]<IMAGE src="/mcb/pict/kam.jpg" alt="Zvi Kam"/>[/pic]


<font size=2>Home Page</font>
<font size=2>Life Science Open Day Book</font>

[contactinfo]<font size=4><b>Zvi Kam</b></font>

<font size=2>[position]Professor[/position]

The [position]Israel Pollak Professor[/position] of Biophysics </font>

[affiliation]Department of Molecular Cell Biology
Weizmann Institute of Science[/affiliation]
[address]Rehovot, Israel 76100[/address]

<font size=2>Tel: [phone](972) 8-934-3473[/phone]
Fax: [fax](972) 8-934-4125[/fax]</font>
<font size=2>Email</font> - [email]zvi.kam@weizmann.ac.il[/email]
<font size=2>[address]Wolfson Building, Room 616[/address]</font>[/contactinfo]
