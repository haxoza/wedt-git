[pic]<IMAGE src="st01.jpg" alt=""/>[/pic]
<font size=1></font>

[contactinfo]<font size=6> Saurabh Tewari</font>
[affiliation]UCLA Computer Science Department[/affiliation]
[address]Boelter Hall 3803E
Los Angeles, CA 90095[/address]
[email]stewari {at}cs.ucla.edu[/email][/contactinfo]

<font size=5></font><b>CV </b>(pdf)

[resinterests]Research Interests

[interests]Peer-to-Peer Networks[/interests], [interests]Networking[/interests], [interests]Performance Modeling[/interests][/resinterests]

<b>Advisor:</b>
Dr. Leonard Kleinrock

[publication]<b>Publications</b>

<b> Conference Proceedings </b>

  *  Saurabh Tewari and Leonard Kleinrock, "Analytical Model for BitTorrent-based Live Video Streaming", to appear in Proceedings of IEEE NIME 2007 Workshop, Las Vegas, NV, Jan 2007. PDF


  *  Shirshanka Das, Saurabh Tewari and Leonard Kleinrock, "The Case for Servers in a Peer-to-Peer World", in Proceedings of IEEE ICC 2006, June 2006. PDF


  *  Saurabh Tewari and Leonard Kleinrock, "Optimal Search Performance in Unstructured Peer-to-Peer Networks With Clustered Demands", in Proceedings of IEEE ICC 2006, June 2006. PDF


  *  Saurabh Tewari and Leonard Kleinrock, "Proportional Replication in Peer-to-Peer Networks", in Proceedings of IEEE INFOCOM 2006, April 2006. PDF


  *  Alok Nandan, Saurabh Tewari, Shirshanka Das, Mario Gerla and Leonard Kleinrock, "AdTorrent: Delivering Location Cognizant Advertisements to Car Networks", in Proceedings of 3rd IEEE/IFIP Annual Conference on Wireless On-demand Network Systems and Services (WONS'06), January 2006. PDF


  *  Alok Nandan, Saurabh Tewari, Shirshanka Das and Leonard Kleinrock, "Modeling Epidemic Query Dissemination in AdTorrent Network", in Proceedings of 2nd IEEE International Workshop on Networking Issues in Multimedia Entertainment (NIME'06), January 2006. PDF


  *  Saurabh Tewari and Leonard Kleinrock, "Search Time in Unstructured Peer-to-Peer Networks with Clustered Demands", in Proceedings of IEEE GLOBECOM 2005, November 2005. PDF


  *  Saurabh Tewari and Leonard Kleinrock, "Analysis of Search and Replication in Unstructured Peer-to-Peer Networks", in Proceedings of ACM SIGMETRICS 2005, Banff, Canada, June 2005. PDF


  *  Saurabh Tewari and Leonard Kleinrock, "On Fairness, Optimal Download Performance and Proportional Replication in Peer-to-Peer Networks", in Proceedings of IFIP Networking 2005, Waterloo, Canada, May 2005. PDF


  *  Saurabh Tewari and Jon M. Peha, "Competition Among Telecommunications Carriers That Offer Multiple Services," Proceedings of 23rd Telecommunications Policy Research Conference (TPRC), Solomons, Maryland, October 1995. PDF


<b> Journals </b>

  *  Saurabh Tewari and Leonard Kleinrock, "Optimal Search Performance in Unstructured Peer-to-Peer Networks With Clustered Demands", in IEEE Journal on Selected Areas in Communications, to appear January 2007. PDF


  *  J. M. Peha, S. Tewari, "The Results of Competition Between Integrated-Services Telecommunications Carriers," Information Economics and Policy, Special Issue on Multimedia, March 1998. PDF


<b> Technical Reports </b>

  *  Tewari, S. and Kleinrock, L., "Analysis of Search and Replication in Unstructured Peer-to-Peer Networks", UCLA Computer Science Dept Technical Report UCLA-CSD-TR050006, March 2005. PDF


  *  Tewari, S. and Kleinrock, L., "Optimal Search Performance in Unstructured Peer-to-Peer Networks With Clustered Demands", UCLA Computer Science Dept Technical Report UCLA-CSD-TR050040, September 2005. PDF


  *  Tewari, S. and Kleinrock, L., "Entropy and Search Distance in Peer-to-Peer Networks", UCLA Computer Science Dept Technical Report UCLA-CSD-TR050049, November 2005. PDF[/publication]
