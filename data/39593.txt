"The Italian Scholar", painting by Kim Itkonen [pic]<IMAGE src="./jeannethedean.jpg" alt="[Painting, The Italian Scholar, Kim Iktonen]"/>[/pic]


[contactinfo]Jeanne Ferrante
[position]Professor and Associate Dean[/position] of the [affiliation]Jacobs School of Engineering[/affiliation]

[affiliation]Computer Science and Engineering Department[/affiliation]
[address]CSE Building, Room 3102
9500 Gilman Drive, Mail Stop 0404[/address]
[affiliation]The University of California[/affiliation], [address]San Diego
La Jolla, CA 92093-0404 USA[/address]

Short bio


Contact Information

  <b>CSE Office </b>

  <b> CSE Email </b>

  <b> CSE Phone </b>

  <b> CSE Fax </b>

  <b> CSE Assistant and Emergency Contact </b>

  <b> CSE Office Hours </b>

  [address]CSE 3102[/address] [email]ferrante@cs.ucsd.edu[/email]
  [phone]858-534-8406[/phone] [fax]858-534-7029[/fax] Sheila Manalo Tel: [phone]858-534-8873[/phone] Email: [email]shmanalo@ucsd.edu[/email] By Appointment

  <b>Dean's Office </b>

  <b> Assoc. Dean Email </b>

  <b> Assoc. Dean Phone </b>

  <b> Assoc. Dean Fax </b>

  <b> Executive Assistant and Dean's Office Contact </b>

  [address]EBU I 7311[/address] [email]jferrant@soe.ucsd.edu[/email] [phone]858-822-0232[/phone] [fax]858-822-3904[/fax] Cheryl Eng Tel: [phone]858-822-4814[/phone] Email: [email]caeng@ucsd.edu[/email][/contactinfo]


[resinterests]Compiler technology provides the necessary interface between programming languages and architectures, and as such is intimately tied to new developments in both. Jeanne Ferrante's work has centered on the development of [interests]compiler technology[/interests], with particular interest in [interests]exploiting parallelism and optimizing data movement to achieve high performance[/interests]. Her current research interests also include [interests]autonomous application scheduling for large-scale distributed systems[/interests], and [interests]program information interfaces[/interests].[/resinterests]


Resume


Classes


UCSD TIES (Teams In Engineering Service)
An academic program initiated in fall, 2004 that partners multidisciplinary student teams with non-profit organizations in the community


Students, sign up for MentorNet! More information can be found at MentorNet's web page


[publication]Recent Papers


PLDI 2007 at FCRC, San Diego


NSF/INRIA Workshop: Scheduling for Large-Scale Distributed Platforms, November 12-14, 2005[/publication]


Women's Leadership Alliance


Women In Engineering


Women In Computing Here are some pictures from WIC lunches at the CUPS coffee cart, and the Grace Hopper Celebration, 2006.


Jeanneology


Pictures from my Leave/Sabbatical Academic Year 2000-01

``Life is very short, and there's no time
For fussing and fighting, my friend.''

from We Can Work It Out, by John Lennon and Paul Mc Cartney

[contactinfo][email]ferrante@cs.ucsd.edu[/email][/contactinfo]


<IMAGE src="/common_images/library.jpg" alt="[ucsd web site]"/>
