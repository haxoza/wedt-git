David J. Wilner

[introduction][pic]<IMAGE src="images/lizlinder_davidwilner1002_small.jpg" alt=""/>[/pic] I am an [position]Astrophysicist[/position] at the [affiliation]Smithsonian Astrophysical Observatory[/affiliation], part of the [affiliation]Harvard-Smithsonian Center for Astrophysics[/affiliation], and I am also a [position]Lecturer[/position] in the [affiliation]Astronomy Department of Harvard University[/affiliation]. My main research interests are (1) the [interests]formation of stars and planets[/interests] and (2) the development [interests]of aperture synthesis techniques[/interests]. Part of my science program is associated with the Submillimeter Array near the summit of Mauna Kea, Hawaii.[/introduction]

[contactinfo]Contact Information:

  * Tel: <b>[phone](617) 496-7623[/phone]</b>

  * FAX: <b>[fax](617) 495-7345[/fax]</b>

  * Email: [email]dwilner@cfa.harvard.edu[/email]

  * Address: [address]60 Garden St., MS 42, Cambridge, MA 02138 USA[/address][/contactinfo]

  * Travel schedule


Research:

  * Curriculum Vitae (pdf)

  * Publications: query ADS

  * Graduate Students

      *  Meredith Hughes, Harvard University, 2005-

      * Jeff Wagg (INAOE), SAO Predoctoral Fellow, 2005-2006

      *  Daniel W.A. Harvey, Harvard University, Ph.D. 2003 (thesis: pdf or ps)

  * Submillimeter Array

  *  IAU JD9 (2000-Aug-14): Cold Gas and Dust at High Redshift

  * Media Coverage

last update: 2006-Dec-01
