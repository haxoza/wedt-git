<IMAGE src="unobluebar.gif" alt=""/>

<b>

[education]Guijun Wang

[position]Associate Professor ?Organic & Bioorganic Chemistry[/position]

[bsdegree]B.S.[/bsdegree], [bsuniv]Tsinghua University[/bsuniv] [bsdate]1990[/bsdate]

[phddegree]Ph.D.[/phddegree], [phduniv]Michigan State University[/phduniv] [phddate]1999[/phddate][/education]

[contactinfo]email: [email]gwang2@uno.edu[/email]

phone: [phone](504) 280-1258[/phone]

office: [address]CBS 240[/address][/contactinfo]

Group Page

</b>

<b>

[pic]<IMAGE src="FacPic/gw.jpg" alt=""/>[/pic]</b>

<b>

[resinterests]Research Interests

Our main research interests are the synthesis of biologically interesting compounds including the [interests]design and synthesis of functional small molecules[/interests] and the total [interests]synthesis of complex natural products[/interests]. We are also interested in [interests]synthesizing and developing self-assembling supramolecular systems[/interests]. The following descriptions are overviews of our current research projects:

Chiral scaffolds and chiral drug-like libraries[/interests]

Despite the structural diversity of medicinal compounds, there are key substructures that frequently appear in several different classes of drugs. For instance, oxazolidinones and piperazines are core substructures in antibacterial (1,2) and antiviral agent (3) respectively. We are developing synthetic strategies for important chiral small molecules and drug-like compound libraries. Currently we are focusing on the synthesis of compound libraries that could lead to novel antimicrobial and anti-cancer agents.

<IMAGE src="FacPic/gw2.jpg" alt=""/>

Design and synthesis of thrombosis inhibitors

Aeruginosins are serine protease inhibitors isolated from Microcystis aeruginosa. They are small linear peptides that contain a new bicyclic amino acid core structure containing 2-carboxy-6-hydroxyoctahydroindole (CHOI). Aeruginosins are thrombin and factor VIIa inhibitors and this new amino acid is <IMAGE src="FacPic/gw3.gif" alt=""/> important for their biological activity. In order to discover novel thrombosis inhibitors with better optimized inhibitory and pharma- cokinetics, we are synthesizing analogs of the natural aeruginosins based on X-ray crystal structures of blood clotting factors and thrombin-aeruginosin complex. Combinatorial libraries of non-peptide and peptido- mimetics will be synthesized and screened to facilitate the discovery of new thrombosis inhibitors.

Carbohydrate based self-assembling systems

Carbohydrates contain all of the important elements needed to prepare highly functional and synthetically flexible compounds. The creation of novel functional biocompatible materials from abundant carbohydrates is very important for the advancement of carbohydrate chemistry, material science, biotechnology and the biomedical field. In this research, novel carbohydrate based low molecular weight hydrogelators and other functional materials are synthesized and characterized. Their applications in enzyme immobilization and biosensors as well as controlled release media are being explored.  The research effort should contribute significantly to biomaterials research in tissue engineering, drug delivery, and bimolecular recognition.[/resinterests]

</b>

<b>

[publication]Selected Publications

"A New Synthesis of Chiral 1,3-oxazinan-2-ones from Carbohydrate Derivatives," J.-R. Ella-Menye, V. Sharma, G. Wang, J. Org. Chem. <b>2005</b>, 70, 463-469.

"Synthesis of a Ring Oxygenated Variant of the 2-Carboxy-6-hydroxyoctahydroindole Core of Aeruginosin 298-A from Glucose" X. Nie and G. Wang, J. Org. Chem. <b>2005</b>, 70, 8687-8692.

"Synthesis and Characterization of Monosaccharide Lipids as Novel Hydrogelators," G. Wang, S. Cheuk, K. Williams, V. Sharma, L. Dakessian and Z. Thorton. Carbohydrate Research, <b>2006</b>, 341, 705-716.

"Synthesis and Antibacterial Activities of Chiral 1,3-Oxazinan-2-one Derivatives," G. Wang, J. R. Ella-Menye, V. Sharma,  Bioorg. Med. Chem. Lett. <b>2006</b>, 16, 2177-2181.

"Synthesis and Self-assembling Properties of Diacetylene Containing Glycolipids."  X. Nie and G. Wang, J. Org. Chem. <b>2006</b>, 71, 4734-4741.[/publication]

</b>
