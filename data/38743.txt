<b><font size=4>[pic]<IMAGE src="pictures/mt_washington.jpg" alt=""/>[/pic]</font></b><b><font size=4>
Matthew Lease</font></b><b><font size=4>
</font></b>Mt. Washington, New Hampshire

Contact ?Professional ?Personal

[introduction]I am a [position]PhD candidate[/position] in [phdmajor]Computer Science[/phdmajor] at [affiliation]Brown University[/affiliation]
My area is [interests]statistical natural language processing (NLP)[/interests] / [interests]computational linguistics[/interests]
I am advised by Eugene Charniak and Mark Johnson
I work in the [affiliation]Brown Laboratory for Linguistic Information Processing (BLLIP)[/affiliation]
I also serve on the [affiliation]Brown University Community Council[/affiliation] (BUCC) and [affiliation]Graduate Student Council[/affiliation] (GSC)

On the lighter side, learn how a Smart Elevator has changed our way of life forever.[/introduction]

------------------------------------------------------------------------------------------------------------------------

[contactinfo]<b>Contact Information</b>

[email]firstname_lastname@brown.edu[/email]

[affiliation]Department of Computer Science
Brown University[/affiliation], [address]Box 1910
115 Waterman Street, 4th Floor
Providence, RI 02912-1910[/address]

Office:  [address]CIT 527[/address]

Phone:  [phone]401-863-7667[/phone]

Fax:   [fax]401-863-7657[/fax][/contactinfo]

Mobile SMS

------------------------------------------------------------------------------------------------------------------------

[introduction]<b>Brief Bio</b>

I received my Bachelor's Degree in [bsmajor]CS[/bsmajor] from the [bsuniv]University of Washington[/bsuniv] in [bsdate]1999[/bsdate], attending as a Washington Scholar. While at UW, I worked on radiation treatment planning with Ira Kalet. From 1999-2001 I worked at LizardTech helping to develop emerging JPEG2000 technology. During 2001-2002, I worked on ubiquitous computing at Intel Research Seattle. I received my MSc in [msmajor]CS[/msmajor] from [msuniv]Brown[/msuniv] in [msdate]May 2004[/msdate]. Thanks to Vance Faber, I also have a pseudo Erdös number of 2.[/introduction]

<b>Honors</b>

Post-Workshop Research Award, Johns Hopkins Center for Language and Speech Processing, 2005 (1-year fellowship)
University Fellowship, Brown University, 2002 (1-year fellowship)

[resactivity]<b>Service</b>

Volunteer, HLT/NAACL'06
Program Committee, Student Research Workshop at COLING/ACL'06
Volunteer, HLT/EMNLP'05
Volunteer, 23rd Intl. Conf. on Distributed Computing Systems (ICDCS), 2003
Reviewer and Volunteer, Fourth Intl. Conf. on Ubiquitous Computing, 2002[/resactivity]

[publication]<b>Publications</b>

<b>2006</b>

Recognizing Disfluencies in Conversational Speech
Matthew Lease, Mark Johnson, Eugene Charniak
IEEE Transactions on Audio, Speech and Language Processing, 2006
[ TO APPEAR ]

PCFGS With Syntactic and Prosodic Indicators of Speech Repairs
John Hale, Izhak Shafran, Lisa Yung, Bonnie Dorr, Mary Harper, Anna Krasnyanskaya, <b>Matthew Lease</b>, Yang Liu, Brian Roark, Matthew Snover and Robin Stewart
COLING-ACL'06
[ PDF ]

A Look At Parsing and Its Applications
Matthew Lease, Eugene Charniak, Mark Johnson, and David McClosky
AAAI'06
[ PDF ]

Early Deletion of Fillers In Processing Conversational Speech
Matthew Lease and Mark Johnson
HLT-NAACL'06
[ PDF ]

Linguistic Resources for Speech Parsing
Ann Bies, Stephanie Strassel, Haejoong Lee, Kazuaki Maeda, Seth Kulick, Yang Liu, Mary Harper, <b>Matthew Lease</b>
LREC'06
[ PDF ]

SParseval: Evaluation Metrics for Parsing Speech
Brian Roark, Mary Harper, Eugene Charniak, Bonnie Dorr, Mark Johnson, Jeremy Kahn, Yang Liu, Mari Ostendorf, John Hale, Anna Krasnyanskaya, <b>Matthew Lease</b>, Izhak Shafran, Matthew Snover, Robin Stewart, Lisa Yung
LREC'06
[ PDF ]

Reranking for Sentence Boundary Detection in Conversational Speech
Brian Roark, Yang Liu, Mary Harper, Robin Stewart, <b>Matthew Lease</b>, Matthew Snover, Izhak Shafran, Bonnie Dorr, John Hale, Anna Krasnyanskaya, Lisa Yung
ICASSP'06
[ PDF ]

<b>2005</b>

Effective Use of Prosody in Parsing Conversational Speech
Jeremy G. Kahn, Matthew Lease, Eugene Charniak, Mark Johnson and Mari Ostendorf
Human Language Technology / Empirical Methods in Natural Language Processing (HLT/EMNLP), 2005
[ PDF BIB ]

Parsing Biomedical Literature
Matthew Lease and Eugene Charniak
Second International Joint Conference on Natural Language Processing (IJCNLP'05)
[ PDF ]

Parsing and its Applications for Conversational Speech
Matthew Lease, Eugene Charniak, and Mark Johnson
IEEE International Conference on Acoustics, Speech, and Signal Processing (ICASSP'05)
[ PDF ]

<b>2004</b>

An Improved Model for Recognizing Disfluencies in Conversational Speech
Mark Johnson, Eugene Charniak, and Matthew Lease
Rich Transcription Fall Workshop 2004 (RT-04F)
[ PDF ]

<b>Ubiquitous Computing</b>

SmartElevator: Revitalizing A Legacy Device through Inexpensive Augmentation
Matthew Lease and Guy Eddon
23rd International Conference on Distributed Computing Systems (ICDCS) Workshops (IWSAWC: Smart Appliances and Wearable Computing), 2003, pp. 254-259.
[PDF]

Plan-Aware Behavioral Modeling
Matthew Lease
Extended abstract. Fourth International Conference on Ubiquitous Computing, 2002. Intel Research, IRS-TR-02-013
[PDF] (if link broken, try looking here)

PlantCare: An Investigation in Practical Ubiquitous Systems
A. LaMarca, W. Brunette, D. Koizumi, M. Lease, S. Sigurdsson, K. Sikorski, D. Fox, G. Borriello
Fourth International Conference on Ubiquitous Computing, 2002. Intel Research, IRS-TR-02-007
[PDF] (if link broken, try looking here)

Making Sensor Networks Practical with Robots
A. LaMarca, W. Brunette, D. Koizumi, M. Lease, S. Sigurdsson, K. Sikorski, D. Fox, G. Borriello
2002 International Conference on Pervasive Computing. Intel Research, IRS-TR-02-004
[PDF] (if link broken, try looking here)

<b>
</b><b>Radiation Teatment Planning</b>

Anatomical Information in Radiation Treatment Planning
Kalet, Ira J., Ph.D., Jonn Wu, M.D., Matthew Lease, James F. Brinkley, M.D., Ph.D., Mary M. Austin-Seymour, M.D., and Cornelius Rosse, M.D., D.Sc.
Proceedings of the American Medical Informatics Association (AMIA) Fall Symposium 1999, pp. 291-295
[PDF]

Radiation Therapy Planning: an Uncommon Application of Lisp
Kalet, Ira J., Robert S. Giansiracusa, Craig Wilcox, and Matthew Lease
Proceedings of the Conference on the 40th Anniversary of Lisp, Berkeley, CA, November 1998. R. Gabriel, ed.
[PDF][/publication]

------------------------------------------------------------------------------------------------------------------------

<b>Personal</b>

<IMAGE src="pictures/Climbing.jpg" alt=""/>

My first rappel. Enchantments, WA, Summer 1997. Derek Stuart snaps a pic as Mark Swanson mans the rope.

The polar plunge at Ingalls lake...

For fun I swim, bike, run, and maybe soon all three. Stay tuned.
