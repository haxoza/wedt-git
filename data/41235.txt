<font size=3> [pic]<IMAGE src="valaee2.jpg" alt=""/>[/pic]</font>

<font size=3> </font><IMAGE src="index_files/image001.gif" alt="Text Box: "/>

<IMAGE src="research.jpg" alt=""/>

<IMAGE src="teaching.jpg" alt=""/>

<IMAGE src="projects.jpg" alt=""/>

<IMAGE src="team.jpg" alt=""/>

<IMAGE src="publications.jpg" alt=""/>

<IMAGE src="wirlab.jpg" alt=""/>

<IMAGE src="prospectivestudents.jpg" alt=""/>

<IMAGE src="links.jpg" alt=""/>

<font size=3>
</font>

<font size=3>
</font>

<IMAGE src="index_files/image002.gif" alt="Text Box: [name]Shahrokh Valaee[/name]

 

 [contactinfo][affiliation]Nortel Institute Junior Chair of Communication Networks[/affiliation]

 

[affiliation]Department of Electrical and Computer Engineering

University of Toronto[/affiliation]

[address]10 King's College Road, Toronto, ON, Canada, M5S 3G4[/address]

 

Phone: [phone](416) 946-8032[/phone] (office)

                 [phone](416) 978-1611[/phone] (WIRLab)

Fax: [fax](416) 978-4425[/fax]

Email: [email]valaee -at- comm -dot- utoronto -dot- ca[/email]

 

Office: [address]BA4124

Bahen Center for Information Technology

40 St George Street[/address][/contactinfo]

(map)

 

"/>

[introduction]I am an [position]associate professor[/position] of the [affiliation]Communications Group in the Edward S. Rogers Sr. Department of Electrical and Computer Engineering, University of Toronto[/affiliation] and [position]holder[/position] of [affiliation]Nortel Institute Junior Chair of Communication Networks[/affiliation]. I am also the <b> [position]Graduate Coordinator[/position]</b> of the Communications Group. 

My recent research is focused on the [interests]Quality-of-Service (QoS)[/interests] provisioning for Wireless Local Area Networks (WLAN) and wired high-speed networks. I am specially interested in [interests]Mobile Ad Hoc and Sensor networks[/interests]. Currently, we are working on multiple interesting projects on [interests]Wireless Access in Vehicular Environment[/interests] (<b>WAVE</b>), also known as <b>IEEE 802.11p</b>, and mesh networks, <b>IEEE 802.11s</b>. We have also recently proposed the concept of <b>Space-Time Scheduling</b> for high bandwidth connections to high speed vehicles such as maglev trains. We call this approach information raining were packets are decomposed into a number of smaller fragments and then the fragments are "rained" upon the vehicle from closely placed relay elements. Please visit my Research Interests for details on the research direction of my group.

We are now building a new lab for research in wireless networks in the Department of Electrical and Computer Engineering at the University of Toronto. Please visit Wireless and Internet Research Laboratory (WIRLab) for details.[/introduction] 

<font size=5><b>NEW (accepting graduate students)</b></font>

WIRLab is now hiring graduate students for <b>January 2007</b>. Priority will be given to students who have applied to our graduate program for Fall 2006, and the students who are now residing in Canada. If interested, please send an email with the subject line "January 2007 admission" to valaee-at-comm-dot-utoronto-dot-ca.

<b> <font size=5>Wireless and Internet Research Laboratory (WIRLab) </font></b>

WIRLab is a quarter million dollar facility dedicated to wireless and internet research at the University of Toronto. WIRLab hosts an array of high-end routers and switches, multiple wireless networks, soft routers, server racks, top quality desktops,  notebooks, PDAs, sensors, mobile robots, and DSRC devices. The current research direction in WIRLab is on <b> mesh/sensor networks</b>, <b> mobile ad hoc networks, and Cellular Networks,</b> addressing the critical issues in the upcoming IEEE 802.11s and 802.11p standards. Our research investigates both analytical approaches to network performance studies and implementation aspects. The researchers in WIRLab have strong analytical background and are involved in realization and engineering of novel networking solutions.

<IMAGE src="wirlab/Picture%20008-25p.jpg" alt=""/><IMAGE src="wirlab/Picture%20002-25p.jpg" alt=""/>

<IMAGE src="wirlab-topology.jpg" alt=""/>

<IMAGE src="wirlab-laptop.jpg" alt=""/>

Globecom 2006, Wireless Communications and Networks

<IMAGE src="ieeecrest.gif" alt=""/>

<font size=2></font>

Symposium on Next Generation Mobile Networks?-  International Conference on Wireless Communications and Mobile Computing (ICWCMC 2006)

<font size=2>

</font>

Wireless Communications and Mobile Computing <b>

Radio Link and Transport Protocol Engineering for Future-Generation Wireless Mobile Data Networks

</b> <IMAGE src="Drawing1.jpg" alt=""/>

IEEE Wireless Communication Magazine <b>

Toward Seamless Internetworking of Wireless LAN and Cellular Networks

</b> <IMAGE src="ieeecrest.gif" alt=""/>
