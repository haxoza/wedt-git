[pic]<IMAGE src="/image/ohn2005s2.jpg" alt=""/>[/pic]
in Japanese <b>


[introduction]Dr. Atsushi OHNISHI, [position]Professor[/position]</b>

  * [position]Head[/position] of [affiliation]Software Engineering Laboratory, Dept. Computer Science[/affiliation]

  * <b>[position]Associate Dean[/position]</b>, [affiliation]Faculty of Information Science and Engineering[/affiliation]

  * <b>Lectures:</b> Software Development, Specification techniques, Software management techniques, Advanced Software Engineering, etc.[/introduction]

  * <b>Research Projects:</b>

      1.  [interests]Software Requirements Definition Environment: CARD[/interests]

      2.  [interests]Visual Requirements Language[/interests]

      3.  [interests]Object Oriented Software Development[/interests]

      4.  [interests]Formal Specification Technique[/interests]

      5.  [interests]Software Reusing[/interests]

      6.  [interests]Logic based Software Specification[/interests]

      7.  [interests]Intellectualized Software Systems[/interests]

      8.  [interests]Visual Programming method for sequence programs[/interests]

  [resactivity]*  [position]Member[/position] of [affiliation]IEEE Computer Society (TCSE)[/affiliation], [affiliation]ACM[/affiliation], [affiliation]IPS Japan[/affiliation], [affiliation]JSSST[/affiliation]

  *  IEEE 12th International Requirements Engineering Conference ( RE'04), Program committee, Local arrangement chair

  *  Chair of Requirements Engineering Working Group, SIG Software Engineering, IPS Japan (1998-2004)

  *  The 4th World Conference on Integrated Design and Process Technology (IDPT'99), Session Organizer (Session: Requirements Engineering),(1999)[/resactivity]

Back to selab's home page
