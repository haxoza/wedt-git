gobics.de [Burkhard Morgenstern]

University of Göttingen - Faculty of Biology - Institute of Microbiology and Genetics - Department of Bioinformatics

[pic]<IMAGE src="pictures/bg1.jpg" alt=""/>[/pic]

Prof. Dr. Burkhard Morgenstern
[contactinfo][affiliation]Universität Göttingen
Institut für Mikrobiologie und Genetik[/affiliation]
[address]Abteilung für Bioinformatik
Goldschmidtstr. 1 (Raum 228)
37077 Göttingen
Germany[/address]

My spam-protected E-mail is:
<IMAGE src="pictures/mailbox.gif" alt=""/> [email]burkhard AT gobics DOT de[/email]

Phone: [phone]+49 551 39-14628[/phone]

Secretary: [phone]+49 551 39-14966[/phone]

FAX: [fax]+49 551 39-14929[/fax][/contactinfo]

<IMAGE src="pictures/amb.gif" alt=""/>
Visit our new journal: Algorithms for Molecular Biology


[publication]<b>Publications:</b>

  * Some recent papers:

      *  M. Stanke, A. Tzvetkova, B. Morgenstern (2006)
        AUGUSTUS+ at EGASP: using EST, protein and genomic alignments for improved gene prediction in the human genome.
        Genome Biology 7, S11.

      * A.-K. Schultz, M. Zhang, T. Leitner, C. Kuiken, B. Korber, B. Morgenstern, M. Stanke (2006)
        A jumping profile Hidden Markov Model and applications to recombination sites in HIV and HCV genomes
        BMC Bioinformatics 7, 265.

      * B. Morgenstern, S.J. Prohaska, D. Pöhler, P.F. Stadler (2006)
        Multiple sequence alignment with user-defined anchor points
        Algorithms for Molecular Biology 1, 6.

      *  M. Stanke , O. Schöffmann , B. Morgenstern, S. Waack (2006)
        Gene prediction in eukaryotes with a generalized hidden Markov model that uses hints from external sources
        BMC Bioinformatics 7, 62.

      *  M. Stanke , O. Keller, I. Gunduz, A. Hayes, S. Waack, B. Morgenstern (2006)
        AUGUSTUS: ab initio prediction of alternative transcripts
        Nucleic Acids Res. 34, W435 - W439.

      *  A.R. Subramanian, J. Weyer-Menkhoff, M. Kaufmann, B. Morgenstern (2005)
        DIALIGN-T: An improved algorithm for segment-based multiple sequence alignment
        BMC Bioinformatics 6, 66.

  * BM's publication list

  * Department publication list[/publication]



<b>Teaching </b>


<b>Software</b>


<b>Short CV</b>

Created by Burkhard Morgenstern. Last modified: November 2006
