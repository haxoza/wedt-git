



[contactinfo]<font size=5>Jim Gemmell</font>

<font size=2>[email]JGemmell@microsoft.com[/email]</font><font size=2>
[address]One Microsoft Way, Redmond, WA, 98052-6399[/address]
Phone: [phone](425)722.8520[/phone]
Fax: [fax](425)936.7329[/fax] (put "to jgemmell" on fax) </font>[/contactinfo]

[introduction]I am a [position]senior researcher[/position] in [affiliation]Microsoft's Next Media research group[/affiliation]. My current research is focused on [interests]MyLifeBits[/interests], part of the CARPE research community, whose first and second workshops I was proud to chair. My research interests include [interests]personal media management/enhancement[/interests], [interests]telepresence[/interests], and [interests]reliable multicast[/interests]. I produced the on-line version of ACM97 and co-authored the PGM RFC. I serve on the editorial boards of the ACM/Springer Multimedia Systems Journal and Computer Communications. My program committee service includes the ACM Multimedia conference and the ICDE eChronicles workshop. (here is my standard bio).

I led the administration of the Digital Memories (Memex) RFP funding/software/hardware package. Winners are now posted.[/introduction]

Papers IETF RFCs Presentations Lecture Diary MyLifeBits in the News

<b>Projects:   </b> MyLifeBits PGM Fcast GazeMaster Multicast PowerPoint Add-in MPEGstats

[pic]<IMAGE src="pics/Jim08-2005.jpg" alt=""/>[/pic]


[publication]<b>Papers</b>

<font size=2>Wang, Zhe, and Gemmell, Jim, Clean Living: Eliminating Near-Duplicates in Lifetime Personal Storage, Microsoft Research Technical Report MSR-TR-2006-30, March 2006 Word (0.1 MB) PDF(0.1 MB) Abstract </font>

<font size=2>Jim Gemmell, Gordon Bell and Roger Lueder, MyLifeBits: a personal database for everything, Communications of the ACM, vol. 49, Issue 1 (Jan 2006), pp. 88.95. PDF Extended version published as Microsoft Research Technical Report MSR-TR-2006-23 Word (3MB) PDF (1MB) Abstract</font>

<font size=2>Mary Czerwinski, Douglas W. Gage, Jim Gemmell, Catherine C. Marshall, Manuel A. Pérez-Quiñonesis, Meredith M. Skeels, Tiziana Catarci, Digital memories in an era of ubiquitous computing and abundant storage, Communications of the ACM, vol. 49, Issue 1 (Jan 2006), pp. 44-50. PDF</font>

<font size=2>Gemmell, Jim, Aris, Aleks, and Lueder, Roger, Telling Stories With MyLifeBits, ICME 2005, Amsterdam, July 6-8, 2005 ,  PDF (1 MB) Word (2.4 MB)</font>

<font size=2>Czerwinski, Mary, Gage, Doug, Gemmell, Jim, Catarci, Tiziana, Marshall, Cathy, Perez-Quinones, Manuel, and Skeels, Meredith M., Digital Memories & Ubiquitous Computing, breakout report, NSF Personal Information Management (PIM) Workshop, Jan. 27-29, 2005, Seattle, WA. PDF (58 KB)</font>

<font size=2>Gemmell, Jim, Williams, Lyndsay, Wood, Ken, Bell, Gordon and Lueder, Roger, Passive Capture and Ensuing Issues for a Personal Lifetime Store, Proceedings of The First ACM Workshop on Continuous Archival and Retrieval of Personal Experiences (CARPE '04), Oct. 15, 2004, New York, NY, USA, pp. 48-55. Word (2 MB) </font> <font size=2>PDF (1 MB)</font>

<font size=2>Mayer-Patel, Ketan and Gemmell, Jim, MPEG2Event: A Framework for Developing Analysis Tools for Compressed Video, Microsoft Research Technical Report MSR-TR-2004-111, October 2004 Word (4 MB) PDF (0.4 MB) </font> <font size=2>Abstract</font>

<font size=2>Aris, Aleks, Gemmell, Jim and Lueder, Roger, Exploiting Location and Time for Photo Search and Storytelling in MyLifeBits, Microsoft Research Technical Report MSR-TR-2004-102, October 2004 Word (1.5MB) PDF (0.8 MB) </font> <font size=2>Abstract</font>

<font size=2>Roberts, John, Lank, Edward, and Gemmell, Jim, Histogram-based Visualizations for Large Time Dependent Datasets, Proceedings of HCI 2004: Design for Life, Sept. 6-10, 2004, Leeds Metropolitan University, UK, Vol. 2, pp. 101-104 </font> <font size=2>PDF (0.1 MB)</font>

<font size=2>Gemmell, Jim, Lueder, Roger, and Bell, Gordon, The MyLifeBits Lifetime Store, Proceedings of ACM SIGMM 2003 Workshop on Experiential Telepresence (ETP 2003), November 7, 2003, Berkeley, CA. Word (1.5 MB) </font> <font size=2>PDF (1.5 MB)</font>

<font size=2>Gemmell, Jim, Lueder, Roger, and Bell, Gordon, Living With a Lifetime Store, ATR Workshop on Ubiquitous Experience Media, Sept. 9-10, 2003, Keihanna Science City, Kyoto, Japan, pp.69-76. Word (1.5MB) PDF (1.5MB) Slides (PPT 7.3 MB) </font> <font size=2>Erratum: In Table 1, "290 hours" should be "1.6K hours" (corrected in the versions posted here)</font>

<font size=2>Gemmell, Jim, Lueder, Roger, Blumenstock, Joshua, Solomon, Evan and Bell, Gordon, Telephone, Television and Radio in the Home of the Future, Internet and Multimedia Systems and Applications (IMSA) 2003, August 13-15, 2003, Honolulu, Hawaii, USA, pp. 263-268. Word (2.5MB) </font> <font size=2>PDF (1.7MB)</font>

<font size=2>Gemmell, Jim, Montgomery, Todd, Speakman, Tony, Bhaskar, Nidhi, and Crowcroft, Jon, The PGM Reliable Multicast Protocol, IEEE Network special issue on "Multicasting: An Enabling Technology", Vol. 17, No. 1, Jan/Feb 2003, pp. 16-22. Word (232KB) </font> <font size=2>PDF (266 KB)</font>

<font size=2>Gemmell, Jim, Bell, Gordon, Lueder, Roger, Drucker, Steven, and Wong, Curtis, MyLifeBits: Fulfilling the Memex Vision, ACM Multimedia '02, December 1-6, 2002, Juan-les-Pins, France, pp. 235-238. Word (1.4 MB) PDF (297 KB) </font> <font size=2>Erratum: In Table 1, "290 hours" should be "1.6K hours" (corrected in the versions posted here)</font>

<font size=2>Gemmell, Jim, and Zhu, Dapeng, Implementing Gaze-Corrected VideoConferencing, Communications, Internet and Information Technology (CIIT) 2002, November 18-20, 2002, St. Thomas, US Virgin Islands, pp. 382-387. Word (522KB) </font> <font size=2>PDF (397KB)</font>

<font size=2>Bell, Gordon and Gemmell, Jim, A Call For The Home Media Network, Communications of the ACM, 45(7), July 2002, pp. 71-75. PDF (277KB) Word (not quite as published - 2+MB) An early version of this was published as: Microsoft Research Technical Report, MSR-TR-2001-52, May 2001. Word </font> <font size=2>abstract</font>

<font size=2>Gemmell, Jim, and Lueder, Roger, Progressive Layered Media: "It gets better every time", Internet and Multimedia Systems and Applications (IMSA) 2002, August 12-14, 2002, Kauai, USA, pp. 342-347. Also published as Microsoft Research Technical Report MSR-TR-2002-26, March 10, 2002. Word (2.9MB) Postscript (1.5MB) abstract </font> <font size=2>PowerPoint (1MB)</font>

<font size=2>Feris, Rogério Schmidt, Gemmell, Jim, Toyama, Kentaro, and Krueger, Volker, Facial Feature Detection Using A Hierarchical Wavelet Face Database, Microsoft Research Technical Report MSR-TR-2002-05, January 2002. Word (2.21 MB) PDF (1.37 MB)</font>

<font size=2>Feris, Rogério Schmidt, Gemmell, Jim, Toyama, Kentaro, and Krueger, Volker, Hierarchical Wavelet Networks for Facial Feature Localization,  The 5th International Conference on Automatic Face and Gesture Recognition, May 20-21, 2002 Washington D.C., USA. </font> <font size=2>PDF</font>

<font size=2>Gemmell, Jim, Zitnick, C. Lawrence, Kang, Thomas, Toyama, Kentaro, and Seitz, Steven, Gaze-awareness for Videoconferencing: A Software Approach, IEEE Multimedia, Vol. 7, No. 4, Oct-Dec 2000, pp. 26-35 Word (2MB) </font> <font size=2>Postscript (1.4MB)</font>

<font size=2>Gemmell, Jim, Scalable Multicast File Distribution Using Fcast, Dr. Dobb's Journal, Vol. 25, Issue 5, May 2000, pp. 82-89. HTML </font>

<font size=2>Gemmell, Jim, Schooler, Eve, and Gray, Jim, Fcast Multicast File Distribution, IEEE Network, Vol. 14, No. 1 (Jan 2000), pp. 58-68. Word </font> <font size=2>postscript</font>

<font size=2>Kang, Thomas, Gemmell, Jim, Toyama, Kentaro, A Warp-Based Feature Tracker, Microsoft Research Technical Report, MSR-TR-99-80, October 1999. Word (541 KB) Postscript (6729 KB) </font> <font size=2>Abstract</font>

<font size=2>Gemmell, Jim, Schooler, Eve, and Gray, Jim, Fcast multicast file distribution: "Tune in, download, and drop out", Proceedings of the IASTED International Conference on Internet and Multimedia Systems and Applications (IMSA '99), Oct. 18-21, 1999, Nassau, Bahamas, Published by ACTA Press, Editor: B. Furht, ISBN: 0-88986-269-9, pp. 371-377. Word </font> <font size=2>Postscript</font>

<font size=2>Zitnick, C. Lawrence, Gemmell, Jim, and Toyama, Kentaro, Manipulation of Video Eye Gaze and Head Orientation for Video Teleconferencing, Microsoft Research Technical Report, MSR-TR-99-46, June 1999. Postscript </font> <font size=2>Word</font>

<font size=2>Gemmell, Jim, Schooler, Eve, and Gray, Jim, Fcast Scalable Multicast File Distribution: Caching and Parameters Optimizations, Microsoft Research Technical Report, MSR-TR-99-14, June 1999. Postscript </font> <font size=2>Word</font>

<font size=2>Gemmell, Jim, Schooler, Eve, and Kermode, Roger, An Architecture for Multicast Telepresentations, Journal of Computing and Information Technology, Vol. 6, No. 3, Sept 1998, pp. 255-272. Word Postscript </font> <font size=2>abstract</font>

<font size=2>Gemmell, Jim, Schooler, Eve, and Kermode, Roger, A Scalable Multicast Architecture for One-to-many Telepresentations, IEEE International Conference on Multimedia Computing Systems (ICMCS '98), pp. 128-139. Word Postscript PPT slides from presentation </font> <font size=2>abstract</font>

<font size=2>Schooler, Eve, and Gemmell, Jim, Using Multicast FEC to Solve the Midnight Madness Problem, Microsoft Research Technical Report, MSR-TR-97-25, Sept. 1997. Word </font> <font size=2>Postscript</font>

<font size=2>Gemmell, Jim, Liebeherr, Jorg, and Bassett, Dave, An API for Scalable Reliable Multicast, International Conference on Computer Communications and Networks, Sept. 22-25, 1997 (Las Vegas, Nevada), IEEE Computer Society, pp. 60-42. Word postscript </font> <font size=2>PPT Slides from presentation</font>

<font size=2>Gemmell, Jim, Scalable Reliable Multicast Using Erasure-Correcting Re-sends, Microsoft Research Technical Report, MSR-TR-97-20, June 1997. Word </font> <font size=2>postscript</font>

<font size=2>Gemmell, Jim, Liebeherr, Jorg, and Bassett, Dave, In Search of an API for Scalable Reliable Multicast, Microsoft Research Technical Report, MSR-TR-97-17, June 1997. Word </font> <font size=2>postscript</font>

<font size=2>Gemmell, D. James, and Bell, C. Gordon, Non-collaborative Telepresentations Come of Age, Communications of the ACM, Vol. 40, No. 4, April 1997, pp. 79-89. Word </font> <font size=2>pdf</font>

<font size=2>Bell, Gordon, and Gemmell, Jim, On-ramp Prospects for the Information Superhighway Dream, Communications of the ACM, July 1996, Vol. 39, No. 7, pp. 55-61. PDF (361KB) </font> <font size=2>abstract</font>

<font size=2>Gemmell, D. James, Disk Scheduling For Continuous Media, Multimedia Information Storage and Management, Soon M. Chung, Ed., Kluwer Academic Publishers, Boston, August 1996, pp. 1-21. (cannot post on Internet due to copyrights).</font>

<font size=2>Gemmell, D. James, Vin, Harrick M., Kandlur, Dilip D., Rangan, P. Venkat, and Rowe, Lawrence A., Multimedia Storage Servers: A Tutorial, COMPUTER, Vol. 28, No. 5 (May 1995), pp. 40-49. postscript abstract </font>

<font size=2>Gemmell, D. James, Support For Continuous Media In File Servers, PhD thesis, Simon Fraser University, April 1995. Word </font> <font size=2>Postscript</font>

<font size=2>Gemmell, D. James, Beaton, Richard, Han, Jaiwei, and Christodoulakis, Stavros, Delay Sensitive Multimedia on Disks, IEEE Multimedia, Vol. 1, No. 3 (Fall 1994), pp. 56-67.Word postscript </font> <font size=2>abstract</font>

<font size=2>Gemmell, D. James and Han, Jiawei, Multimedia Network File Servers: Multi-Channel Delay Sensitive Data Retrieval, Multimedia Systems, Vol. 1, No. 6 (1994), pp. 240-252. Word </font> <font size=2>postscript</font>

<font size=2>Gemmell, D. James, Multimedia Network File Servers: Multi-Channel Delay Sensitive Data Retrieval, Proceedings of ACM Multimedia '93,(Anaheim, CA, August 1-6 1993), Association for Computing Machinery, New York, 1993, pp. 243-250. Word </font> <font size=2>abstract</font>

<font size=2>Gemmell, D. James, and Christodoulakis, Stavros, Principles of Delay Sensitive Multimedia Data Storage and Retrieval, ACM Transactions on Information Systems, vol. 10, no. 1 (Jan. 1992) pp. 51-90. </font> <font size=2>PDF</font>

<font size=2>Gemmell, D. James, and Christodoulakis, Stavros, Principles of Delay Sensitive Multimedia Data Retrieval, Proceedings of the International Conference on Multimedia Information Systems '91 (Singapore), McGraw-Hill, New York, 1991, pp. 147-158. (on-line version not available). </font>


<b>IETF RFCs</b>

<font size=2>Speakman, T., Crowcroft, J., Gemmell, J., Farinacci, D. , Lin, S., Leshchiner, D., Luby, M., Montgomery, T. , Rizzo, L., Tweedly, A., Bhaskar, N., Edmonstone, R., Sumanasekera, R., Vicisano, L., PGM Reliable Transport Protocol Specification, RFC 3208, December 2001. rfc3208.txt also available at ftp://ftp.rfc-editor.org/in-notes/rfc3208.txt</font>

<font size=2>Asynchronous Layered Coding (ALC) Protocol Instantiation, Luby, M., Gemmell, J., Vicisano, L., Rizzo, L., and Crowcroft, J., RFC 3450, December 2002. rfc3450.txt also available at ftp://ftp.rfc-editor.org/in-notes/rfc3450.txt </font>

<font size=2>Layered Coding Transport (LCT) Building Block, Luby, M., Gemmell, J., Vicisano, L., Rizzo, L., Handley, M., and Crowcroft, J., RFC 3451 , December 2002. rfc3451.txt also available at </font> <font size=2>ftp://ftp.rfc-editor.org/in-notes/rfc3451.txt</font>

<font size=2>Forward Error Correction (FEC) Building Block, Luby, M., Vicisano, L., Gemmell, J., Rizzo, L., Handley, M., Crowcroft, J., RFC 3452, December 2002 rfc3452.txt also available at </font> <font size=2>ftp://ftp.rfc-editor.org/in-notes/rfc3452.txt</font>

<font size=2>The Use of Forward Error Correction (FEC) in Reliable Multicast, Luby, M., Vicisano, L.. Gemmell, J., Rizzo, L., Handley, M., Crowcroft, J., RFC 3453, December 2002 rfc3453.txt also available at </font> <font size=2>ftp://ftp.rfc-editor.org/in-notes/rfc3453.txt</font>

<b>History: D</b><b>rafts leading up to these RFCs</b>


Presentations

MyLifeBits talk given at a number of universities, Feb 2005 version PowerPoint (10 MB) Feb 2004 version PowerPoint (10 MB), Oct 2003 version PowerPoint (10 MB)

Experiential systems panel at CIVR 2003 - my statement (43 KB Word)& my presentation (2.6 MB PowerPoint)

Gaze-corrected video conferencing talk given at at UCSB, Cambridge U., U. Virginia, UCB and Stanford HTML PowerPoint

Fcast: Reliable Multicast File Transfer - talk given at Cal Tech (PowerPoint)

Scalable Reliable Multicast Telepresentations - talk given at Stanford (PowerPoint)

Telepresentations and Scalable Reliable Multicast - A lecture given at a number of universities.

Reliable, Scalable Multicast: A lecture given to Larry Rowe's class at UC Berkeley, with a focus on SRM. PowerPoint (450 KB) HTML

MBONE for the Masses - Presentation from the May 96 WWW conference MBONE panel.[/publication]


Links

Multicast information including ECSRM, Multicast PowerPoint Add-in, Mping test utility, MBONE videoconferencing tools, and lots of related links

Talks from ACM 97 "The Next 50 years"

.wav files using different compression types


Lecture Diary

<font size=2>4/7/2006 - eChronicles workshop - panelist statement
11/30/2005 U. Washington iSchool - MyLifeBits
8/19/2005 MSR Faculty Summit, Redmond WA - Digital Memories RFP
2/17/2005 Princeton U. - MyLifeBits
2/15/2005 U. Maryland - MyLifeBits
2/9/2005 TTI Vanguard - MyLifeBits
11/30/2004 Ricoh Innovations - MyLifeBits
10/29/2004 UC Berkeley Database Seminar (w. Roger Lueder) - MyLifeBits
10/15/2004 ACM Workshop on Continuous Archival and Retrieval of Personal Experiences ?MyLifeBits
10/13/2004 Columbia University - MyLifeBits (w. Gordon Bell)
9/16/2004 Intelligent Systems conference, Perugia, Italy - MyLifeBits
7/29/2004 NPUC workshop, IBM Almaden Research Lab ?MyLifeBits (w. Gordon Bell)
6/10/2004 NASA Ames Research Center ?MyLifeBits (w. Gordon Bell)
6/4/2004 MSR Cambridge ?MyLifeBits
6/4/2004 Cambridge University - MyLifeBits
2/11/2004 General Print Office of US Federal Government (at Microsoft Silicon Valley campus) ?MyLifeBits
1/19/2004 SDForum Distinguished Speaker series ?MyLifeBits (w. Gordon Bell)
11/19/2003 Georgia Tech ?MyLifeBits
10/2003 San Francisco State U. MyLifeBits
10/2003 Arizona State U. MyLifeBits
10/2003 Microsoft Research Technical Advisory Board (at Microsoft Silicon Valley campus)
9/2003 ATR Workshop on Ubiquitous Experience Media - MyLifeBits
7/2003 International Conference on Image and Video Retrieval ?Panelist statement on Experiential Computing
5/2003 UCLA MyLifeBits
5/2003 U. North Carolina MyLifeBits
4/2003 UC Davis MyLifeBits
12/2002 ACM Multimedia Conference ?MyLifeBits Poster presentation
11/2002 CIIT ?GazeMaster paper presentation
10/2002 U. Virginia ?MyLifeBits
10/2002 U. Minnesota ?MyLifeBits
10/2002 U. Illinois ?MyLifeBits
8/2002 IMSA ?PLM paper presentation
4/2002 MSR Cambridge ?Reliable Multicast
4/2002 Cambridge University - GazeMaster
2/26/2002 San Francisco State U. ?GazeMaster
4/2000 Stanford U. ?GazeMaster
3/2000 UC Berkeley ?GazeMaster
10/1999 IMSA ?Fcast
10/1999 U. Virginia ?Fcast & GazeMaster
2/1999 Cal Tech ?Fcast
11/1998 Stanford ?Multicast Telepresentations
3/1998 UC Berkeley - Telepresentations
2/1998 U. British Columbia ?Telepresentations
2/1998 Simon Fraser U. ?Telepresentations
12/1997 Xerox PARC ?Telepresence/multicast
10/1997 CMU - Telepresence/multicast
10/1997 MIT - Telepresence/multicast
10/1997 NY Polytechnic - Telepresence/multicast
10/1997 Columbia U. - Telepresence/multicast
9/1997 ICCCN ?Reliable Multicast APIs
11/1996 UC Berkeley </font>


MyLifeBits In The News

<font size=2>How Microsofts Gordon Bell is Reengineering Human Memory (and Soon, Your Life and Business)</font><font size=2>, Fast Company, Nov 2006.
Digital age may bring total recall in future, CNN 10/16/2006.
El hombre que guarda todos los recuerdos de su vida en bits, La Crónica de Hoy (Mexico), 7/16/2006.
That's My Life, Aria Magazine April 2006.
The ultimate digital diary The Dominion Post 5/31/2006
In 2021 You'll Enjoy Total Recall Popular Science 5/18/2006
The Memory Machine, Varsity.co.uk, 3/2/2006
Life Bytes, NPR Radio "Living on Earth" show, 1/20/2006
The man with the perfect memory - just don't ask him to remember what's in it The Guardian, 12/28/2005
Bytes of my life, Hindustan Times, 11/17/2005
Total Recall, IEEE Spectrum, 11/1/2005 Podcast on IEEE Spectrum Radio (Choose arrow on October 2005 show and select "MyLifeBits -- the digitized life of Gordon Bell")
Turning Your Life Into Bits, Indexed, Los Angeles Times 7/11/2005
Wouldn't It Be Nice The Wall Street Journal 5/23/2005
Life Bits IEEE Spectrum Online May 2005
How To Be A Pack Rat, Forbes.com 4/29/2005 - see also blog entry by Thomas Hawk at eHomeUpgrade
Computer sage cuts paperwork, converts his life to digital format The Seattle Time 4/9/2005
Channel 9 video interviews 8/21/2004 Intro Gemmell Lueder
Slices of Life Spiked-Online 8/19/2004
Next-generation search tools to refine results CNET 8/9/2004
Life in byte-sized pieces The Age, 7/18/2004
Removable Media For Our Minds TheFeature 3/25/2004
This is Your Life San Jose Mercury News 3/6/2004
Navigating Digital Home Networks New York Times 2/19/2004
Offloading Your Memories New York Times Magazine Year in Ideas issue 12/14/2003  "Bright notions, bold inventions, genius schemes and mad dreams that took off (or tried to) in 2003"
Logged on for life Toronto Star 9/8/2003
This is your life--in bits U.S. News & World Report 6/23/2003
My Life in a Terabyte IT-Analysis.com 5/14/2003
How MS will know ALL about you ZD AnchorDesk 4/18/2003
Memories as Heirlooms Logged Into a Database The New York Times 3/20/2003
Microsoft Fair Forecasts Future AP 2/27/2003 (This story ran on many newspapers and news sites, including USA Today, The Globe and Mail, The San Jose Mercury News, and ABC News)
This Is Your Brain on Digits ABC News 2/5/2003
A life in bits and bytes c|net News.com 1/6/2003 (run also by ZDNet)|
Your Life - On The Web Computer Research & Technology 12/20/2002
Saving Your Bits for Posterity Wired 12/6/2002
Microsoft works to create back-up brain Knowledge Management 11/25/2002
Microsoft Creating Virtual Brain NewsFactor Network 11/22/2002
Microsoft solves "giant shoebox problem" Geek.com 11/22/2002
Would you put your life in Microsoft's hands? Silicon.com (run also by ZDNet News) 11/21/2002
Microsoft Plans Digital Memory Box, a Step Toward "Surrogate Brain" BetterHumans 11/21/2002
E-hoard with Microsoft's life database vnunet.com IT Week 11/21/2002
Microsoft plans online life archive BBC News 11/20/2002
Software aims to put your life on a disk New Scientist 11/20/2002</font>


