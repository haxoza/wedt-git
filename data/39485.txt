Xiaoqiu Huang


[contactinfo][pic]<IMAGE src="xqhuang.jpg" alt=""/>[/pic]
Xiaoqiu Huang, [position]Associate Professor[/position]
[affiliation]Department of Computer Science, Iowa State University[/affiliation]
[address]226 Atanasoff Hall, Ames, IA 50011[/address]
Email: [email]xqhuang@cs.iastate.edu[/email] Phone: [phone]515-294-2432[/phone] Fax: [fax]515-294-0258[/fax][/contactinfo]


Teaching

  1.  Discrete Computational Structures: ComS 330

  2.  Computational Techniques for Genome Assembly and Analysis: ComS/BCB 551


[resinterests]Research


My research in bioinformatics focuses on three computational problems in
[interests]genome sequencing and analysis[/interests].

  1.  [interests]Assembly of DNA fragments into longer sequences[/interests].

  2.  [interests]Identification of genes in genomic DNA sequences[/interests].

  3.  [interests]Comparison of genomic DNA sequences[/interests].[/resinterests]


  [publication]Selected Publications

  Huang, X. and Miller, W. (1991)
  A Time-efficient, Linear-Space Local Similarity Algorithm. Advances in Applied Mathematics, 12:337-357.

  Huang, X., Adams, M.D., Zhou, H. and Kerlavage, A.R. (1997)
  A Tool for Analyzing and Annotating Genomic Sequences. Genomics, 46: 37-45.

  Huang, X. and Madan, A. (1999)
  CAP3: A DNA Sequence Assembly Program. Genome Research, 9: 868-877.

  Huang, X. and Chao, K.-M. (2003)
  A Generalized Global Alignment Algorithm. Bioinformatics, 19: 228-233.

  Huang, X., Wang, J., Aluru, S., Yang, S.-P. and Hillier, L. (2003)
  PCAP: A Whole-Genome Assembly Program. Genome Research, 13: 2164-2170.

  Ye, L. and Huang, X. (2005)
  MAP2: Multiple Alignment of Syntenic Genomic Sequences. Nucleic Acids Research, 33: 162-170.

  Huang, X., Yang, S.-P., Chinwalla, A., Hillier, L., Minx, P., Mardis, E. and Wilson, R. (2006)
  Application of a Superword Array in Genome Assembly. Nucleic Acids Research, 34: 201-205.[/publication]


  Computer Programs

  We have developed a number of computer programs for analysis of DNA and protein sequences.
  The programs below are used by scientists around the world in their research.
  SIM: A local sequence alignment program,
  MAP: A multiple sequence alignment program,
  CAP3 and PCAP: Sequence and genome assembly programs (downloading),
  AAT: A set of programs for finding genes in DNA sequences (downloading).
