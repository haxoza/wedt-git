[pic]<IMAGE src="Pictures/ddlee.gif" alt=""/>[/pic] <b><font size=4>Daniel D. Lee, Ph.D.</font></b>

[contactinfo]<b>[position]Associate Professor[/position]
</b><b> [affiliation]Dept. of Electrical and Systems Engineering
Dept. of Bioengineering (Secondary)
GRASP (General Robotics, Automation, Sensing, Perception) Lab[/affiliation]</b>

[address]203B Moore/6314
University of Pennsylvania
200 S. 33rd Street
Philadelphia, PA 19104[/address]
[phone]215-898-8112[/phone]
[fax]215-573-2068[/fax](FAX)

Email: [email]ddlee@seas.upenn.edu[/email][/contactinfo]


[introduction]Background I am currently an [position]Associate Professor[/position] in the [affiliation]School of Engineering and Applied Science at the University of Pennsylvania[/affiliation]. I studied [bsmajor]physics[/bsmajor] receiving my A.B. from [bsuniv]Harvard[/bsuniv] in [bsdate]1990[/bsdate], and my Ph.D. in [phdmajor]condensed matter physics[/phdmajor] from [phduniv]MIT[/phduniv] in [phddate]1995[/phddate]. After completing my studies, I joined Bell Labs, the research and development arm of Lucent Technologies, where I was a researcher in the Theoretical Physics and Biological Computation departments. After six years in industrial research, I joined the faculty at Penn in 2001 where I am currently doing research and teaching in the [affiliation]Electrical and Systems Engineering Department[/affiliation] and at the [affiliation]GRASP Lab[/affiliation].


Research Why is it that if computers have gotten so much faster and cheaper, they have not become any better at understanding what we want them to do? Some of the tasks we take for granted in vision and language are still too difficult for the fastest supercomputers to handle. To us, a picture may be worth a thousand words, but to a machine both are just seemingly random jumbles of numbers. How can we get machines to intelligently process this kind of information? I believe that we can learn much from the way biological systems compute and learn. My research focuses on [interests]applying knowledge about biological information processing systems[/interests] to building better artificial sensorimotor systems that can adapt and learn from experience. Thus, research in my lab looks at [interests]computational neuroscience models[/interests], [interests]theoretical foundations of machine learning algorithms[/interests], as well as [interests]constructing real-time intelligent robotic systems[/interests].[/introduction]


[publication]Publications Here are some representative publications from my lab's research. A more complete list can be found on my CV in HTML or PDF format.

  * A kernel view of the dimensionality reduction of manifolds, J. H. Ham, D. D. Lee, and S. Mika, and B. Schoelkopf, Proceedings of the International Conference on Machine Learning (2004).

  * Nonnegative deconvolution for time of arrival estimation, Y. Lin, D. D. Lee and L. K. Saul, International Conference on Acoustics, Speech, and Signal Processing (2004).

  * Short-term memory in orthogonal neural networks, O. L. White, D. D. Lee and H. Sompolinsky, Phys. Rev. Lett. 92, 148102 (2004).

  * Spontaneous eye movements in goldfish: oculomotor integrator performance, plasticity, and dependence on visual feedback, B. D. Mensh, E. Aksay, D. D. Lee, H. S. Seung and D. W. Tank, Vision Research 44, 711-726 (2004).

  * Learning high dimensional correspondences with low dimensional manifolds, J. H. Ham, D. D. Lee, and L. K. Saul, Proceedings of the ICML 2003 Workshop on The Continuum from Labeled to Unlabeled Data in Machine Learning and Data Mining, 34-41 (2003).

  * Equilibrium properties of temporally asymmetric Hebbian plasticity. J. Rubin, D. D. Lee and H. Sompolinsky, Phys. Rev. Lett <b>86</b>, 364 (2001).

  * Stability of the memory of eye position in a recurrent network of conductance-based model neurons. H. S. Seung, D. D. Lee, B. Y. Reis, D. W. Tank, Neuron <b>26</b>, 259 (2000).

  * The manifold ways of perception, H. S. Seung and D. D. Lee, Science 290, 2268-2269 (2000).

  * Learning the parts of objects with nonnegative matrix factorization. D. D. Lee and H. S. Seung, Nature <b>401</b>, 788 (1999).[/publication]


CoursesHere is a list of courses that I have taught over the past few years. You can find more information about them using guest access at Penn's online course site: http://courseweb.upenn.edu.

  * EE531: Digital Signal Processing (Fall 2001, Fall 2002)

  * EE400: Microcontroller Laboratory (Spring 2002, Spring 2003)

  * ESE350: Embedded Systems/Microcontroller Lab (Spring 2004)

  * ESE650: Learning in Robotics (Fall 2004)


Miscellaneous I also lead the Univ. of Pennsylvania's soccer legged robot team, the UPennalizers, in the annual Robocup competition. I live with my wife Lisa, three year old son Jordan, and one year old daughter Jessica in Leonia, New Jersey.
