Skip to content | Change text size<IMAGE src="http://infotech.monash.edu/assets/images/infotechlogo.gif" alt="Information Technology"/> <IMAGE src="/assets/images/header/banner-01.jpg" alt=""/>

Home | About Us | Courses | Units | Support and Services | Research

IT Support | Staff directory | A-Z index | Site mapSEARCH

  * About the Faculty of IT

  * Message from the Dean

  * Decanal and senior management

  * Schools

  * Staff profiles

  * News & events

  * Contact us

Monash University > Information Technology > About the Faculty > Staff >



[contactinfo]Dr Peta Darke[position]Senior Lecturer[/position]


<b>Phone </b> [phone]+61 3 990 32416[/phone]

<b>Email </b>

[email]<IMAGE src="data/images/pdarke.jpg" alt="Peta Darke"/>[/email][/contactinfo]

Back to staff list

Update your details

Copyright ?2005 Monash University ABN 12 377 614 012 - Caution - CRICOS Provider Number: 00008C
Last updated: 30 June 2005 - Maintained by Online Services - Privacy - Accessibility information Request an IT or web service
