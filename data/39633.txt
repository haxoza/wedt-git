[pic]<IMAGE src="MyPhoto.jpg" alt=""/>[/pic]


[contactinfo]Fernando Pedone  [position]Assistant Professor[/position]

 [affiliation]University of Lugano (USI)
 Faculty of Informatics[/affiliation]
 [address]Via Giuseppe Buffi 6
 CH-6904 - Lugano
 Switzerland[/address]

 E-mail: [email]firstname.lastname(at)unisi.ch[/email]

 Phone: [phone]+41 58 666 4695[/phone]
 Fax: [fax]+41 58 666 4536[/fax][/contactinfo] Curriculum
Vitae


[resinterests]Research

I'm interested in problems related to theoretical and practical aspects of [interests]distributed systems[/interests] and [interests]distributed data management systems[/interests]. Theoretical work has addressed algorithmic issues of reliable distributed systems, such as defining new abstractions for building reliable systems, creating algorithms implementing these abstractions and proving them correct, and establishing related lower bounds. Systems work has considered using group communication abstractions to build replicated data management systems. To a large extent, I have been interested in the interplay between theory and practice.[/resinterests]


[publication]Recent publications

N. Schiper, R. Schmidt and F. Pedone
Optimistic Algorithms for Partial Database Replication
10th International Conference on Principles of Distributed Systems (OPODIS'2006)
[BibTeX] [PDF]

J. Grov, L. Soares, A. Correia, J. Pereira, R. Oliverira and F. Pedone
A Pragmatic Protocol for Database Replication in Interconnected Clusters
12th IEEE International Symposium on Pacific Rim Dependable Computing (PRDC'2006)
[BibTeX] [PDF]

N. Schiper, R. Schmidt and F. Pedone
Optimistic Algorithms for Partial Database Replication, Brief announcement
20th International Symposium on Distributed Computing (DISC'2006)
[BibTeX] [PDF]

L. Camargos, F. Pedone and R. Schmidt
A Primary-Backup Protocol for In-Memory Database Replication
5th IEEE International Symposium on Network Computing and Applications (NCA'2006)
[BibTeX] [PDF]

L. Camargos, E. Madeira and F. Pedone
Optimal and Practical WAB-Based Consensus Algorithms
European Conference on Parallel Computing (EuroPar'2006)
[BibTeX] [PDF]

S. Elnikety, S. Dropsho and F. Pedone
Tashkent: Uniting Durability with Transaction Ordering for High-Performance Scalable Database Replication
EuroSys 2006
[BibTeX] [PDF][/publication]

All publications by year: 2006, 2005, 2004, 2003, 2002, 2001, 2000, 199x

From other sources: DBLP, Google Scholar, CiteSeer


Research group Lásaro Camargos (PhD student)
Nicolas Schiper (PhD student)
Rodrigo Schmidt (PhD student)
Marija Stamenkovic (PhD student)
Marcin Wieloch (PhD student)
Vaide Zuikeviciute (PhD student) <IMAGE src="RGroup/DSC00076.jpg" alt=""/>


Projects Sprint: [interests]adaptive data management for main-memory database clusters[/interests]
[interests]Gorda: open replication of databases[/interests]
[interests]Swift: system support for distributed dynamic content web services[/interests]


Teaching Algorithms and Data Structures (Winter 2005)
Distributed Computing (Summer 2005, Summer 2006)
Readings in Distributed Systems (Summer 2005, Winter 2005)
Discrete Structures (Winter 2004, Summer 2006)


Events 8th Brazilian Workshop on Tests and Fault Tolerance (WTF 2007)
4th International Conference on Autonomic and Trusted Computing (ATC 2007)
International Workshop on Global and Peer-to-Peer Computing (GP2PC2007)
Workshop on Large Scale and Volatile Desktop Grids (PCGrid 2007)
2nd International Conference on Availability, Reliability and Security (ARES 2007)
2nd Dependable and Adaptive Distributed Systems, ACM Symposium on Applied Computing (DADS 2007)
3rd Latin-American Symposium on Dependable Computing (LADC 2007)


Miscellaneouspedone.org



last update: December 4, 2006 Research
Publications
Research group
Projects
Teaching
Events
Misc

<IMAGE src="Lugano/slides/2.JPG" alt=""/>

Lugano in pictures


News:

Funded PhD student positions at the University of Lugano
