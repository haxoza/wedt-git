Amit Kumar

[contactinfo]<b> [position]Associate Professor[/position]
[affiliation]Department of Computer Science and Engineering
Indian Institute of Technology[/affiliation]
[address]Hauz Khas, New Delhi - 110016[/address] </b>[/contactinfo]

[pic]<IMAGE src="Amit-Sing.JPG" alt=""/>[/pic]

<b> <font size=5> Teaching</font> </b>


<b> <font size=4> CSL 865: Machine learning and Data Mining </font> </b>
<b> <font size=4> CSL 201 : Data-Structures </font> </b>

[publication]<b> <font size=5> Publications </font> </b>


<b> <font size=4> Approximation Algorithms </font> </b>

  *  <b> Better algorithms for minimizing average flow-time on related machines. </b>
    (with Naveen Garg)
    ICALP, 2006.

  *  <b> Minimizing average flow-time on related machines </b>
    (with Naveen Garg )
    ACM Symposium on Theory of Computing, 2006

  * <b> Where's the Winner ? (Max-finding and Sorting with Metric Comparision Costs) </b>
    (with A. Gupta )
    APPROX 2005.

  * <b> On a bidirected relaxation for the multiway cut problem </b>
    (with Chandra Chekuri and Anupam Gupta)
    To appear in Discrete Applied Mathematics

  * <b> Linear Time Algorithms for Clustering Problems in any dimensions </b>
    (with Sandeep Sen and Yogish Sabharwal)
    ICALP, 2005.

  *  <b> A simple linear time (1+epsilon)-approximation algorithm </b>
    for k-means clustering in any dimensions.
    (with Sandeep Sen and Yogish Sabharwal)
    IEEE Foundations of Computer Science, 2004.

  * <b> Maximum Coverage Problem with Group Budget Constraints. </b>
    (with C. Chekuri )
    APPROX 2004.

  *  <b> Multi-processor Scheduling for Minimizing Flow Time with </b>
    epsilon-Resource Augmentation.
    (with A. Goel , C. Chekuri , S. Khanna )
    ACM Symposium on Theory of Computing, 2004.

  *  <b> Approximation via Cost-Sharing : A Simple Approximation
    Algorithm for the Multi-commodity Rent-or-Buy Problem. </b>
    (with A. Gupta , M. Pal , T. Roughgarden )
    IEEE Foundations of Computer Science, 2003.

  *  <b> Simpler and better algorithms for network design. </b>
    (with A. Gupta , T. Roughgarden )
    ACM Symposium on Theory of Computing, 2003.

  * <b> A Constant Factor Approximation Algorithm for the
    Multicommodity Rent-or-Buy Problem. </b>
    (with A. Gupta , T. Roughgarden )
    IEEE Foundations of Computer Science, 2002.

  *  <b> Primal-dual Algorithms for Connected Facility Location Problems. </b>
    (with C. Swamy )
    APPROX 2002.

  * <b> Approximation Algorithms for the Unsplittable Flow Problem. </b>
    (with A. Chakrabarti , C. Chekuri , A. Gupta )
    APPROX 2002.

<b> <font size=4> Network Management and Routing</font> </b>

  * <b> On Configuring BGP Route Reflectors </b>
    (with Yuri Breitbart, Minos Garofalakis and Rajeev Rastogi)
    COMSWARE 2007

  *  <b> Exploring the trade-off between label size and stack depth in MPLS Routing. </b>
    (with A. Gupta and R. Rastogi )
    IEEE INFOCOM, 2003

  * <b> Building edge-failure resilient networks. </b>
    (with C. Chekuri , A. Gupta , S. Naor , D. Raz )
    IPCO 2002.

  * <b> Optimal configuration of OSPF aggregates. </b>
    (with Y. Breitbart , M. Garofalakis , R. Rastogi )
    IEEE INFOCOM, 2002.

  * <b> Routing Issues in MPLS (Or, How to Travel with a Pez Dispenser). </b>
    (with A. Gupta , R. Rastogi )
    IEEE Symposium on Foundations of Computer Science, 2001.

  *  On configuring BGP route reflectors.
    (with Y. Breitbart , M. Garofalakis , A. Gupta , R. Rastogi )
    Bell Labs technical Report

  * <b> Algorithms for provisioning VPNs in the hose model . </b>
    (with R. Rastogi , A. Silberschatz , B. Yener )
    ACM SIGCOMM, 2001.

  * <b> Provisioning a virtual private network : A network design problem for multicommodity flow. </b>
    (with A. Gupta , J. Kleinberg , R. Rastogi , B. Yener )
    ACM Symposium on Thoery of Computing, 2001.

  * <b> Wavelength Conversion in optical networks. </b>
    (with J. Kleinberg )
    ACM-SIAM Symposium on Discrete Algorithms, 1999.

<b> <font size=4> Databases </font> </b>

  * <b> Join-Distinct Aggregate Estimation over Update Streams </b>
    (with Sumit Ganguly, M. Garofalakis and Rajeev Rastogi)
    ACM PODS, 2005

  * <b> Deterministic Wavelet Thresholding for Maximum-Error Metrics. </b>
    (with M. Garofalakis )
    ACM PODS, 2004

  *  <b> Correlating XML Data Streams Using Tree-Edit Distance Embeddings. </b>
    (with M. Garofalakis )
    ACM PODS, 2003

<b> <font size=4> Fairness </font> </b>

  * <b> Fairness measures for resource allocation. </b>
    (with J. Kleinberg )
    IEEE Symposium on Foundations of Computer Science, 2000.

<b> <font size=4> Miscellaneous </font> </b>

  * <b> Sorting and Selection with Structured Costs. </b>
    (with A. Gupta )
    IEEE Symposium on Foundations of Computer Science, 2001.

  * <b> Connectivity and inference problems for temporal networks. </b>
    (with D. Kempe , J. Kleinberg )
    ACM Symposium on Thoery of Computing, 2000.[/publication]

[contactinfo]<b> <font size=4> Contact Information : </font> </b>
<b> email : </b> [email]amitk@cse.iitd.ernet.in[/email]
<b> Phone : </b> [phone]+91-11-26591286[/phone][/contactinfo]

<b> <font size=4> Biographical Sketch </font> </b>

<b> <font size=4> Links : </font> </b>

  *  Google

  *  Compendium of NP hard optimization problems

  *  Theoretical Computer Science on the Web
