[pic]<IMAGE src="UIUC_dcai.jpg" alt=""/>[/pic]



Deng Cai <IMAGE src="cd.gif" alt=""/>
[contactinfo]<b>Email:</b>    [email]cai_deng<b> AT </b>yahoo<b> DOT </b>com[/email]
[email]dengcai2<b> AT </b>cs<b> DOT </b>uiuc<b> DOT </b>edu[/email]
[email]dengcai<b> AT </b>gmail<b> DOT </b>com[/email][/contactinfo]

Xiaofei He |   Shipeng Yu |  


About me

[introduction]I am currently a [position]Ph.D student[/position] at the [affiliation]Computer Science Department, University of Illinois at Urbana Champaign[/affiliation]. My Ph.D. advisor is Prof. Jiawei Han.

I worked at Information Management & System Group of Microsoft Research Asia as a visiting student from Sep. 2001 to July 2004.

I received my Master degree in The Department of [msmajor]Automation[/msmajor], at [msuniv]Tsinghua University[/msuniv] in [msdate]2003[/msdate]. Before this, I received my B.E. degree in the same department in [bsdate]2000[/bsdate].

My research interests include [interests]machine learning[/interests], [interests]information retrieval[/interests], [interests]data mining and computer vision[/interests].[/introduction]

Curriculum Vitae [pdf]


[resinterests]Research Interests My main research interests are in the fields of [interests]Information Processing[/interests], especially on the Web. Related topics include [interests]data mining[/interests], [interests]information retrieval[/interests], [interests]machine learning[/interests], [interests]pattern recognition[/interests]... I'd like to help people get what they need more easily. Let the computer do more for us with less help from us, learn from experience, adapt effortlessly, and discover new knowledge. We need computers that reduce the information overload by extracting the important patterns from masses of data. And we need computer understand what we need. This poses many deep and fascinating scientific problems: How can a computer decide autonomously which representation is best for target knowledge? How can it tell genuine regularities from chance occurrences? How can pre-existing knowledge be exploited? How can learned results be made understandable by us?[/resinterests]

My research addresses these and related questions. [resinterests]Research topics that I'm working on, or have recently worked on, include:

<b><font size=3>1. [interests]VIsion-based Page Segmentation (VIPS)[/interests]</font></b> (More details)

Traditionally, link analysis has attracted much attention for web search. However, the layout information of the web page has not been fully explored. By noticing the fact that a single web page always contains multiple semantics, I am working on a fundamental problem that how to extract these semantics based on <b>visual perception</b>.

<b><font size=3>2. [interests]Web Information Retrieval[/interests]</font></b>

In contrast to traditional document retrieval, a web page as a whole is not a good information unit to search because it often contains multiple topics and a lot of irrelevant information from navigation, decoration, and interaction part of the page. I am working on the problem that how to enhance web information retrieval using web page segmentation.

Particularly, we have proposed a new algorithm, called "Block Level Link Analysis". The new algorithm partitions the web pages into blocks. By extracting the page-to-block, block-to-page relationships from link structure and page layout analysis, we can construct a semantic graph over the WWW such that each node exactly represents a single semantic topic. We further developed two ranking algorithms, block level PageRank and block level HITS.

<b><font size=3>3. [interests]Web Image Searching and Clustering[/interests]</font></b>

With VIsion-based Page Segmentation, a web page can be partitioned into blocks, each containing semantically coherent information, and the textual and link information of an image can be accurately extracted within each image block. The textual information is used for image representation. A large image graph can obtained from block-level link analysis. This method is less sensitive to noisy links than previous method like PicASHOW, and hence the image graph can to some extent reflect the semantic relationship between images. By spectral techniques, the obtained image graph can be partitioned into clusters which are used to enhance the search results.

<b><font size=3>4. [interests]Locality Preserving Criteria[/interests]</font></b>

Supervised and unsupervised learning are two major directions in machine learning. How to achieve the similar result of supervised learning under unsupervised mode? How to utilize the unlabeled data in learning process? Based on the assumption that neighboring points probably belong to the same underlying class, one may discover the discriminative structures of the data space in unsupervised mode. We called it <b>Locality Preserving Criteria</b>. Based on this criteria, we have been developing efficient and effective learning algorithms for unsupervised learning and semi-supervised learning.

<b><font size=3>5. [interests]Face representation and recognition[/interests]</font></b>

An face image is intrinsically an matrix. The relationship between the column vectors of the image matrix and that between the row vectors should not be ignored. We are developing more efficient and effective face representation and recognition method based on this.[/resinterests]

<b><font size=3>6. Learning on Graph</font></b>


Some datasets and codes


[publication]Publication


Patent

  *  Method and System for Identifying Image Relatedness Using Link and page Layout Analysis. filed May 2004.

  *  Vision-Based Document Segmentation. filed July 2003.[/publication]


Other Interests

  *  Dance, International Standard Style.

  *  All kinds of sports: especially swimming.

  *  Reading, science fiction.

  *  Music

Department of Computer Science, University of Illinois at Urbana Champaign

Microsoft Research Asia, Beijing

Department of Automation, Tsinghua University, Beijing

<IMAGE src="http://m1.nedstatbasic.net/n?id=ADG3DgqrbOJIW6UBZXNAdxGtCYRg" alt="Nedstat Basic - Free web site statistics
Personal homepage website counter"/>
Free counter Last modified: July 3 2005
