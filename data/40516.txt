<IMAGE src="/library/toolbar/3.0/gradient.aspx?a=4B92D9&b=1E77D3&w=250&h=22&d=ltr&c=eFLbOmyDquZL7nbgTLBSftxozLo%3d" alt="*"/>

Quick Links | Home | Worldwide

<IMAGE src="/library/toolbar/3.0/images/banners/ms_masthead_ltr.gif" alt="Microsoft"/> <IMAGE src="/library/toolbar/3.0/gradient.aspx?a=0A6CCE&b=FFFFFF&w=250&h=42&d=ltr&c=NAbKOqnrunJiULJyhm6k3VulBy4%3d" alt="*"/>


Search:

<IMAGE src="/library/mnp/2/gif/arrowLTR.gif" alt="*"/>Microsoft Research Home<IMAGE src="/library/mnp/2/gif/arrowLTR.gif" alt="*"/>About Microsoft Research<IMAGE src="/library/mnp/2/gif/arrowLTR.gif" alt="*"/>Research Areas<IMAGE src="/library/mnp/2/gif/arrowLTR.gif" alt="*"/>People<IMAGE src="/library/mnp/2/gif/arrowLTR.gif" alt="*"/>Worldwide Labs<IMAGE src="/library/mnp/2/gif/arrowLTR.gif" alt="*"/>University Relations<IMAGE src="/library/mnp/2/gif/arrowLTR.gif" alt="*"/>Publications<IMAGE src="/library/mnp/2/gif/arrowLTR.gif" alt="*"/>Downloads<IMAGE src="/library/mnp/2/gif/arrowLTR.gif" alt="*"/>Conferences and Events<IMAGE src="/library/mnp/2/gif/arrowLTR.gif" alt="*"/>Lectures Online<IMAGE src="/library/mnp/2/gif/arrowLTR.gif" alt="*"/>Careers<IMAGE src="/library/mnp/2/gif/arrowLTR.gif" alt="*"/>Visiting Microsoft Research<IMAGE src="/library/mnp/2/gif/arrowLTR.gif" alt="*"/>Press Resources <IMAGE src="/images/rss.gif" alt=""/>

[contactinfo][address]Beijing, China
5/F, Beijing Sigma Center
No.49, Zhichun Road, Hai Dian District
Beijing China 100080[/address][/contactinfo]

[pic]<IMAGE src="mjli.jpg" alt="Mingjing Li"/>[/pic] <b>[introduction]Mingjing Li</b>
[position]Researcher[/position]
[affiliation]Microsoft Research China[/affiliation]

<font size=3> </font>

Dr. Mingjing Li, an expert in Chinese handwriting recognition, has joined [affiliation]Microsoft Research China[/affiliation] as a [position]researcher[/position]. He comes to Microsoft from his post as an associate professor at Institute of Automation, Chinese Academy of Sciences. He designed and built a Chinese handwriting recognition engine that was licensed to Microsoft for use in Chinese Windows CE operating system in 1998. 

Dr. Li has designed and developed several first-class Chinese handwriting recognition engines. His off-line writer-dependent engine was the best one available in China during his doctoral studies, which was authenticated by Chinese Academy of Sciences upon his graduation. His off-line writer-independent engine had the highest recognition rate and the fastest recognition speed at a contest held by the Experts Group of National 863 Program in April 1998. Its correct rate for Chinese text is 95%, which is 15% higher than the second best system; its speed is 89 characters per second, which is 8 times as fast as the second best. In May 1999, he developed a new multi-classifier handwriting recognition engine, which achieved a 45 percent error rate reduction compared with the best one of 1998.

Dr. Li worked as an engineer at Peking University Founder Group Corporation after graduation. He became an associate professor at Institute of Automation, Chinese Academy of Sciences in 1997, where he took charge of the research and development of character recognition. His research areas included both [interests]on-line and off-line Chinese handwriting recognition[/interests], [interests]handwritten numeral recognition and printed Chinese character recognition[/interests]. He joined Microsoft in July 1999. 

He received his [bsdegree]BS[/bsdegree] in [bsmajor]electrical engineering[/bsmajor] from the [bsuniv]University of Science and Technology of China[/bsuniv] in [bsdate]1989[/bsdate], and his [phddegree]Ph.D.[/phddegree] from the Institute of Automation, [phduniv]Chinese Academy of Sciences[/phduniv] in [phddate]1995[/phddate].[/introduction] 


Manage Your Profile |Contact Us?007 Microsoft Corporation. All rights reserved. Terms of Use |Trademarks |Privacy Statement

<IMAGE src="http://m.webtrends.com/dcsslvlng89k7may281d5dedm_8n8e/njs.gif?dcsurl=/nojavascript&WT.js=No" alt=""/><IMAGE src="http://c.microsoft.com/trans_pixel.asp?source=research&TYPE=PV&p=users_mjli&URI=%2fusers%2fmjli%2fdefault.aspx&GUID=1F4FC18C-F71E-47FB-8FC9-612F8EE59C61&lc=en-us" alt=""/>
