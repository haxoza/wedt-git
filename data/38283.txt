<b>[position]Graduate Program Director[/position]</b>
<b>Xudong Yu, Ph. D.</b>
[pic]<IMAGE src="images/Faculty_Xudong_Yu.jpg" alt="Dr. Xudong Yu"/>[/pic]

[contactinfo]<b>Contact Information</b>
<IMAGE src="/images/red_dot.gif" alt="-"/>Office: [address]EB 3039[/address]
Phone: [phone]618-650-2321[/phone]
E-mail:
Website: [homepage]http://www.cs.siue.edu/xyu[/homepage][/contactinfo]

[education]<b>Education</b>
<IMAGE src="/images/red_dot.gif" alt="-"/>[phddate]1992[/phddate] - PhD in [phdmajor]Computer Science[/phdmajor] at [phduniv]Vanderbilt University[/phduniv]
[msdate]1989[/msdate] - MS in [msmajor]Computer Science[/msmajor] at [msuniv]Vanderbilt University[/msuniv]
[bsdate]1984[/bsdate] - BE in [bsmajor]Computer Science[/bsmajor] at [bsuniv]Shanghai JiaoTong University[/bsuniv], China[/education]

[resinterests]<b>Areas of Interest</b>
<IMAGE src="/images/red_dot.gif" alt="-"/>[interests]Integrated diagnostic systems[/interests]
[interests]Robotics[/interests]
[interests]Teachable Agents[/interests]
[interests]Database and Data Warehouse[/interests]
[interests]Data Mining and Knowledge Discovery[/interests][/resinterests]

<b>Courses Taught</b>
<IMAGE src="/images/red_dot.gif" alt="-"/>CS140 - Introduction to Computing I
CS150 - Introduction to Computing II
CS240 - Introduction to Computing III
CS330 - Programming Languages
CS340 - Data Structure and Algorithm
CS425 - Senior Project: Software Design
CS434 - Database Management Systems
CS438 - Introduction to Artificial Intelligence
CS499 - Senior Project II - Implementation
CS584 - Expert Systems
CS590 - Data Mining and Data Warehouse

SIUE Computer Science CS Faculty
