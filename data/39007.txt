[pic]<IMAGE src="adp3.jpg" alt=""/>[/pic]

<b><font size=5>Alberto Pettorossi</font></b>
[contactinfo]<b>[position]Professor[/position] of Theoretical Computer Science</b>

<IMAGE src="maplogo.gif" alt=""/>



[affiliation]<b><font size=5>MAP  Research Center on </font></b>
<b><font size=5>Advanced Programming Methodologies</font></b>[/affiliation]

<b>Alberto Pettorossi</b>
[affiliation]<b>Dipartimento di Informatica, Sistemi e Produzione</b>
<b>Universit? di Roma Tor Vergata</b>[/affiliation]
<b>[address]Via del Politecnico 1</b>
<b>I-00133 Roma,  Italy[/address]</b>
<b>Phone: [phone]+39 06 7259 7379[/phone]</b>
<b>email: </b>[email]<IMAGE src="email_R2.jpeg" alt=""/>[/email]
<b>c/o  [affiliation]Istituto di Analisi dei Sistemi ed Informatica "A. Ruberti"</b>
<b> Consiglio Nazionale delle Ricerche</b>[/affiliation]
<b> [address]Viale Manzoni 30 </b>
<b> I-00185 Roma,  Italy[/address]</b>
<b>Phone: [phone]+39 06 7716 427[/phone]    Fax: [fax]+39 06 7716 461[/fax] </b>
<b>email: </b>[email]<IMAGE src="email_iasi.jpeg" alt=""/>[/email][/contactinfo]

<b><font size=4> </font></b><b><font size=4>Research</font></b> <b><font size=4> </font></b><b><font size=4>Teaching</font></b> <b><font size=4> Links</font></b>
Maurizio Proietti <b><font size=4> Events</font></b> <b><font size=4> Associations</font></b>


<b><font size=4>***</font></b> Book in honor of Prof. Robert Paige (1947-1999)

<b><font size=4>*** </font></b><font size=5>Special Issue of FUNDAMENTA INFORMATICAE on Program Transformation</font>

<b><font size=4>
</font></b>
