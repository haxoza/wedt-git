<IMAGE src="/images/uq-logo.gif" alt="Go to The University of Queensland Homepage"/> <IMAGE src="/images/top-links.gif" alt=""/> <IMAGE src="/images/my-uq.gif" alt="Go to my.UQ"/>

<IMAGE src="/images/header.gif" alt="Welcome to the UQ Business School Homepage"/> <IMAGE src="/images/uq-shield.gif" alt="You are at the UQ Business School website"/>

[pic]<IMAGE src="/staff/staff_images/xud.jpg" alt=""/>[/pic]

<IMAGE src="/images/header-onthissite.gif" alt="Header: On this site"/>

  * ?School Profile

  * ?Current Students

  * ?Prospective Students

  * ?Staff Profiles

  * ?Research and Centres

  * ?Business Portal

  * ?News and Media Lounge

  * ?Alumni Association

  * ?Upcoming Events

  * ?Contact Us

  * ?Sitemap

<IMAGE src="/images/logos/enterprize.gif" alt="Enterprize website"/>
<IMAGE src="/images/logos/aacsb_accrediation_small.jpg" alt=""/>


Home ?Staff (Academic) ?Dongming Xu


[introduction]Dr Dongming Xu

<IMAGE src="/images/icons/arrow2.gif" alt=""/> [position]Lecturer[/position] in Information Systems
<IMAGE src="/images/icons/arrow2.gif" alt=""/> [affiliation]Business Information Systems Cluster[/affiliation]

PhD [phduniv]City University of Hong Kong[/phduniv] [phddate]2004[/phddate][/introduction]

[contactinfo]<b>Contact Details</b>
[address]Room 440 Colin Clark 39[/address]
Telephone: [phone]3365 6290[/phone]
Facsimile: [fax]3365 6988[/fax]
Email: [email]d.xu@business.uq.edu.au[/email][/contactinfo]


<b>Course Coordinator </b>
Semester 2, 2006: MGTS2202 Data and Information Systems, St Lucia Campus
Semester 2, 2006: INFS7210 Electronic Commerce Fundamentals, St Lucia Campus
Semester 2, 2006: IBUS7314 International Study in Asian Business, St Lucia Campus (Intensive)


<b>Lecturer</b>
Semester 2, 2006: INFS7210 Electronic Commerce Fundamentals, St Lucia Campus

<b><IMAGE src="/images/headings/relatedlinks.gif" alt=""/></b>

<font size=1><IMAGE src="/images/icons/arrow2.gif" alt=""/></font> <font size=1>Download complete staff profile </font>

<font size=1><IMAGE src="/images/icons/arrow2.gif" alt=""/></font> <font size=1>Email Dongming Xu</font>

<font size=1><IMAGE src="/images/icons/reversearrow.gif" alt=""/></font> <font size=1>Staff</font>

Brief profile

[introduction]Dr. Dongming Xu is a [position]lecturer[/position] in [affiliation]Business of Information Systems at the UQ Business School[/affiliation] and has a PhD in the area of [phdmajor]Information Systems[/phdmajor] from the [phduniv]City University of Hong Kong[/phduniv] in [phddate]2004[/phddate]. Her research interests include [interests]technology-mediated learning[/interests], [interests]knowledge management[/interests], [interests]electronic commerce and business intelligence systems[/interests]. Her research usually combines theoretical model building, laboratory and field experiments and the development of prototype systems.

Currently she is working in the area of [interests]technology-mediated learning focusing on the use of advanced technologies in virtual learning environments[/interests] and its [interests]impact on eLearning effectiveness[/interests]. Knowledge management research is a natural and logical extension of the work in the area of technology-mediated learning, since learning is the process by which information is converted into knowledge. She focuses in the area of the investigation of the role of information and communication technologies in development and implementation of knowledge strategies and knowledge management systems, and the impact on organization performance. She is also studying the area of business intelligence systems on variety contexts, such as, theoretical foundations, applications, and technologies, such as intelligent agents, data mining etc. In particular, she is working on financial monitoring systems, knowledge management systems, workflow management systems, etc.[/introduction]

<font size=2>[return]</font>




Feedback

UQ Business School ?001 The University of Queensland, Australia

The University of Queensland ABN: 63 942 912 684

Brisbane, Queensland 4072 Australia Authorised by: Head of School

Phone: +61 (7) 3365 6475, 3365 6283 Maintained by: webmaster

Fax: +61 (7) 3365 6988, 3365 6788 Last Updated: 21 Jul, 2006

Email: webmaster@business.uq.edu.au
