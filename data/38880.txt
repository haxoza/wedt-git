[pic]<IMAGE src="Pic/234-274_sin_in_sun.png" alt=""/>[/pic]

"Make everything as simple as possible, but not simpler." - Albert Einstein

[contactinfo]<b>Gang Zhou</b>
Fifth Year [position]Graduate Student[/position] of Computer Science

[affiliation]Department of Computer Science
University of Virginia[/affiliation]
[address]Charlottesville, VA 22903[/address]

My advisor is Professor John A. Stankovic

Email: [email]gzhou@cs.virginia.edu[/email] Office Phone: [phone]434-982-2294[/phone][/contactinfo]

<b> Publications Services CV Personal</b>

[resinterests]<b><font size=4>Areas of Interest</font></b>

My research interests lie broadly in [interests]wireless networking[/interests], [interests]embedded system and real-time systems[/interests]. My recent focus is [interests]wireless sensor networks[/interests], including [interests]reality-aware sensor network protocols[/interests], [interests]integrated sensor network systems[/interests], [interests]power control in sensor networks[/interests], [interests]programming paradigms in sensor networks[/interests], [interests]hardware and tools in sensor networks[/interests], [interests]data storage in sensor networks[/interests], [interests]secure sensor networks[/interests], and [interests]content distribution networks[/interests].[/resinterests]

[education]<b><font size=4>Education</font></b>

PH.D. in [phdmajor]Computer Science[/phdmajor] Department, [phduniv]University of Virginia[/phduniv], [phddate]June 2007[/phddate] (Expected)

M.CS. in [msmajor]Computer Science[/msmajor] Department, [msuniv]University of Virginia[/msuniv], [msdate]August 2004[/msdate]

M.E. in [msmajor]Computer Science[/msmajor] Department, [msuniv]Nanjing University[/msuniv], China, [msdate]July 2002[/msdate]

B.S. in Computer Science Department, [bsuniv]Nanjing University[/bsuniv], China, [bsdate]July 1999[/bsdate][/education]

<IMAGE src="Pic/uva_06.gif" alt=""/>

[contactinfo][affiliation]Department of Computer Science
School of Engineering, University of Virginia[/affiliation]
[address]151 Engineer's Way, P.O. Box 400740
Charlottesville, Virginia 22904-4740[/address][/contactinfo]<IMAGE src="http://m1.nedstatbasic.net/n?id=ACUdPAoZZ7v1quiHpUMTMVdTCQGg" alt="Nedstat Basic - Free web site statistics

Personal homepage website counter"/>
Free counter
