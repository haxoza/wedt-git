[contactinfo]<IMAGE src="/images/i7-logo-ch.gif" alt="I7"/> <font size=6><b>[affiliation]Lehrstuhl für Informatik VII
Logik und Theorie diskreter Systeme[/affiliation]</b></font>

<IMAGE src="/images/rwthlogo.gif" alt="RWTH"/>

<IMAGE src="/images/spacer.gif" alt=""/>


[pic]<IMAGE src="/images/photo.jpg" alt="Photo"/>[/pic]


Prof. Dr. Dr.h.c. Wolfgang Thomas

[affiliation]RWTH Aachen
Lehrstuhl Informatik VII[/affiliation]
[address]Ahornstr. 55
52056 Aachen[/address]

Email: [email]thomas@informatik.rwth-aachen.de[/email]

Phone: [phone]+49-241-80 21700[/phone]

Fax: [fax]+49-241-80 22215[/fax][/contactinfo]

Office Hour: here


<IMAGE src="/images/spacer.gif" alt=""/>


[resinterests]Research My main interest is the development of [interests]automata theory[/interests] as a framework for modelling, analyzing, verifying, and synthesizing systems. This includes

  * [interests]Generalized models of automata[/interests] (over trees, pictures, relational structures)

  * [interests]Automata over infinite words and trees[/interests]

  * The [interests]relation between automata and logical systems[/interests]

  * The [interests]effective theory of infinite games[/interests]

  * [interests]Infinite automata and applications in the verification of infinite systems[/interests]

Other interests are questions of the [interests]history and methodology of computer science[/interests].

<IMAGE src="/images/spacer.gif" alt=""/>[/resinterests]


Selected Publications

<IMAGE src="/images/spacer.gif" alt=""/>


Teaching My courses for 3rd or 4th year students are

  * Applied Automata Theory

  * Automata and Reactive Systems

  * Temporal Logic and Model-Checking

  * History of Ideas of Informatics

  * Recursion Theory


<IMAGE src="/images/spacer.gif" alt=""/>


[resactivity]Journals ([position]Member of Editorial Board[/position])

  * [affiliation]ACM - Transactions of Computational Logic[/affiliation]

  * [affiliation]RAIRO - Theoretical Informatics and Applications[/affiliation]

  * [affiliation]DMTCS - Discrete Mathematics & Theoretical Computer Science[/affiliation]


<IMAGE src="/images/spacer.gif" alt=""/>


Further Activities

  * [position]Dean[/position] of [affiliation]Faculty of Mathematics, Informatics and Natural Sciences[/affiliation] (since 2004)

  * Vice-Dean of Faculty of Mathematics, Informatics and Natural Sciences (2002-2004)

  * Speaker of the Group of Computer Science, RWTH Aachen (2000-2002)

  * Delegate of Computer Science of RWTH at IDEA-League
    Report of IDEA WG "Computer Science" (2001-2005)

  * [position]Member[/position] of [affiliation]Council of EATCS - European Association for Theoretical Computer Science[/affiliation]

  * Chairman of Referee Board of Computer Science, German Science Foundation DFG (2000-2004)

  * Member of the Board of GIBU (GI-Beirat der Universitätsprofessoren) (1995-2003)

  * Member of the Advisory Board of the MINERVA John von Neumann Research Center "Fo rm al Verification of Reactive Systems", Rehovot, Israel (1998-2005)

  * Member of Gödel Prize Committee 1998-2001, chairman 2001

  * Chairman of GI-Fachausschuss "Theoretische Informatik" (1998-2001)

  * Member of Steering Committee of DLT - Developments in Language Theory (1995-2005)

  * Member of Steering Committee of ETAPS (1997-2000)[/resactivity]


<IMAGE src="/images/spacer.gif" alt=""/>


[introduction]Curriculum Vitae

1947 born in Naumburg/Saale

1954-66 School education in Göttingen, Hannover, Celle (Lower Saxony)

1967-68 Military service

1968-71 Study of Mathematics, Physics, Philosophy at University of Freiburg

1971-[msdate]72[/msdate] M.Sc. Course in [msmajor]Mathematical Logic[/msmajor], [msuniv]University of Bristol[/msuniv], UK
M.Sc. of [msuniv]University o f Bristol[/msuniv]

1973-82 Scientific employee at University of Freiburg

1975 Promotion to Dr. rer. nat., University of Freiburg

1980 Habilitation in Mathematics, University of Freiburg

1982-89 Assoc. Professor of Computer Science, RWTH Aachen

1989-98 Full Professor of Computer Science, University of Kiel

1998- [position]Full Professor[/position] of Computer Science, [affiliation]RWTH Aachen[/affiliation]

2005 Doctor honoris causa of Ecole Normale Supérieure de Cachan[/introduction]



<IMAGE src="/images/spacer.gif" alt=""/>


<IMAGE src="/images/i7bull.gif" alt=""/>
Zurück zur Homepage

<IMAGE src="/images/spacer.gif" alt=""/>

25. 1. 2006 Webmaster <b>Haftungsausschluss</b>
