<font size=6><b> Jiri Srba
[introduction][position]Associate Professor[/position] at [affiliation]BRICS[/affiliation]
[pic]<IMAGE src="srba.gif" alt=""/>[/pic]</b></font>



Jiri Srba I am associated with [affiliation]BRICS in Aalborg[/affiliation].[/introduction]

[resinterests]The main area of my interest is within concurrency theory, in particular:

  *  [interests]process algebras[/interests]

  *  [interests]timed Petri nets and timed automata[/interests]

  *  [interests]decidability and complexity issues for various classes of infinite state systems[/interests]

  *  [interests]software verification[/interests]

  *  [interests]partial order semantics[/interests], [interests]logics over partial orders[/interests]

  *  [interests]verification of cryptographic protocols[/interests][/resinterests]

Links:

  * List of publications.

  * Roadmap of infinite results.

  * Curriculum vitae.

Events:

  * INFINITY'05

Pictures:

  * Karneval in Aalborg 2005


[contactinfo]Jiri Srba
[affiliation]University of Aalborg
Department of Computer Science[/affiliation]
[address]Fredrik Bajersvej 7B
9220 Aalborg East
Denmark[/address]

Office: [address]B2-204[/address]
Phone: [phone]+45 96 35 98 51[/phone]
Fax : [fax]+45 96 35 05 10[/fax]
Mobil: [phone]+45 20 45 35 14[/phone]
Mobil: [phone]+420 608 222962[/phone] (during summer and winter holidays)
Email: [email]srba@brics.dk[/email][/contactinfo]

Last modified: 2006-08-02 by Jiri Srba.            Back to my homepage
