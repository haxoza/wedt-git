<IMAGE src="img/1-1.gif" alt=""/> <IMAGE src="img/1-4-new.gif" alt=""/>

<IMAGE src="img/2-1-tiled.gif" alt=""/> <IMAGE src="img/2-2-tiled.gif" alt=""/> <font size=5><b> General Information :: Members <b> </b></b></font> <IMAGE src="img/2-4-tiled-new.gif" alt=""/>

<IMAGE src="img/3-1.gif" alt=""/> <font size=2> <b> </b> </font> <IMAGE src="img/3-3.gif" alt=""/> <IMAGE src="img/3-4-new.gif" alt=""/>




<IMAGE src="img/en/item_general.gif" alt=""/>

Whats New

Members

<IMAGE src="img/transpixel.gif" alt=""/> <IMAGE src="img/en/item_research.gif" alt=""/> <IMAGE src="img/transpixel.gif" alt=""/> <IMAGE src="img/en/item_students.gif" alt=""/> <IMAGE src="img/transpixel.gif" alt=""/> <IMAGE src="img/en/item_contact.gif" alt=""/> <IMAGE src="img/transpixel.gif" alt=""/> <IMAGE src="img/transpixel.gif" alt=""/> <IMAGE src="img/en/item_internal.gif" alt=""/>

<b>[ Personal Information ... ] </b> <IMAGE src="img/b_pijltje0.gif" alt=""/>


  [pic]<IMAGE src="img/people/Peter.Rigole.jpg" alt=""/>[/pic]

  [contactinfo]Name: <b> Peter Rigole </b>

  Address: [address]Celestijnenlaan 200A B-3001 Leuven Belgium[/address]

  Office: [address]A02.28[/address]



  Phone: [phone]+32 (0)16 32 78 54[/phone]

  Fax: [fax]+32 (0)16 32 79 96[/fax]



  E-mail: Peter Rigole

  Homepage: [homepage]http://www.cs.kuleuven.ac.be/~peterri[/homepage][/contactinfo]






[publication]<b>[ Publications in 2006 ] </b> <IMAGE src="img/b_pijltje0.gif" alt=""/>

  1. D. Preuveneers, Y. Vandewoude, P. Rigole, D. Ayed, and Y. Berbers, Context-aware adaptation for component-based pervasive computing systems, Advances in Pervasive Computing 2006. Adjunct Proceedings of the 4th International Conference on Pervasive Computing (Pfeifer, T. and Schmidt, A. and Woo, W. and Doherty, G. and Vernier, F. and Delaney, K. and Yerazunis, B. and Chalmers, M. and Kiniry, J., eds.), vol 207, pp. 125-128, 2006

  2. P. Rigole, and Y. Berbers, Resource-driven collaborative component deployment in mobile environments, Proceedings of the International Conference on Autonomic and Autonomous Systems (Dini, P. and Ayed, D. and Dini, C. and Berbers, Y., eds.), pp. 1-5, 2006

  3. P. Rigole, Task- and resource-aware component deployment in ambient intelligence environments, Ph.D. Thesis, Department of Computer Science, K.U.Leuven, Leuven, Belgium, November, 2006, 242 + xxviii pages

<b>[ Publications in 2005 ] </b> <IMAGE src="img/b_pijltje0.gif" alt=""/>

  1. T. Delaet, T. Clijsner, P. Rigole, W. Joosen, and Y. Berbers, A proposal for decentralized, scalable and automatic IP network configuration: SCNP, Department of Computer Science, K.U.Leuven, Report CW 408, Leuven, Belgium, April, 2005

  2. P. Rigole, C. Vandervelpen, K. Luyten, Y. Vandewoude, K. Coninx, and Y. Berbers, A component-based infrastructure for pervasive user interaction, Proceedings of Software Techniques for Embedded and Pervasive Systems (Varea, M. and Cortes, L., eds.), pp. 1-16, 2005

  3. D. Preuveneers, P. Rigole, Y. Vandewoude, and Y. Berbers, Middleware support for component-based ubiquitous and mobile computing applications, ACM/IFIP/USENIX 6th International Middleware Conference Workshop Proceedings, Demonstrations Extended abstracts (Joolia, A. and Jean, S., eds.), pp. 1-4, 2005

  4. Y. Berbers, P. Rigole, Y. Vandewoude, and S. Van Baelen, CoConES: An approach for components and contracts in embedded systems, Component-Based Software Development for Embedded Systems: An Overview of Current Research Trends, (Atkinson, C. and Bunse, C. and Gross, H-G. and Peper, C., eds., eds.), vol. 3778, Lecture Notes in Computer Science, Springer-Verlag Berlin Heidelberg, November, 2005, pp.209-231

<b>[ Publications in 2004 ] </b> <IMAGE src="img/b_pijltje0.gif" alt=""/>

  1. P. Rigole, Y. Berbers, and T. Holvoet, Bluetooth enabled interaction in a distributed camera surveillance system, Proceedings of the Thirty-Seventh Annual Hawaii International Conference on System Sciences (Sprague, R.H. Jr., ed.), pp. 1-10, 2004

  2. Y. Berbers, P. Rigole, S. Van Baelen, and Y. Vandewoude, Components and contracts in software development for embedded systems, Proceedings of the first European Conference on the Use of Modern Information and Communication Technologies (De Backer, Luc, ed.), pp. 219-226, 2004

  3. P. Rigole, Y. Berbers, and T. Holvoet, Mobile adaptive tasks guided by resource contracts, Middleware'2004 Companion Proceedings (Blair, G. and Endler, M. and Guerraoui, R., eds.), pp. 1-4, 2004

  4. D. Preuveneers, J. Van den Bergh, D. Wagelaar, A. Georges, P. Rigole, T. Clerckx, Y. Berbers, K. Coninx, V. Jonckers, and K. De Bosschere, Towards an extensible context ontology for ambient intelligence, Ambient Intelligence: Second European Symposium (Markopoulos, P. and Eggen, B. and Aarts, E. and Crowley, J., eds.), vol 3295, Lecture Notes in Computer Science, pp. 148-159, 2004

  5. A. Wils, P. Rigole, Y. Berbers, and K. De Vlaminck, Ambient computing using component resource contracts, Proceedings of the IASTED International Conference on Advances in Computer Science and Technology (Sahni, S., ed.), pp. 1-6, 2004

  6. P. Rigole, Y. Berbers, and T. Holvoet, Component-based adaptive tasks guided by resource contracts, Workshop on component-oriented approaches to context-aware systems (Dobson, S. and Nixon, P., eds.), pp. 1-5, 2004

<b>[ Publications in 2003 ] </b> <IMAGE src="img/b_pijltje0.gif" alt=""/>

  1. P. Rigole, and Y. Berbers, The working of the SEESCOA common test case, Department of Computer Science, K.U.Leuven, Report CW 354, Leuven, Belgium, January, 2003[show files]

  2. E. Steegmans, P. Rigole, T. Holvoet, and Y. Berbers, Intelligent Buildings: A Multi-Agent System Approach, Artificial Intelligence and Applications Proceedings (Hamza, M.H., ed.), pp. 771-776, 2003[show files]

  3. P. Rigole, Y. Berbers, and T. Holvoet, A UPnP software gateway towards EIB home automation, Proceedings of the IASTED International Conference on Computer Science and Technology (Cosnard, M. and Ibarra, O.H. and Jaja, J. and Kumar, V. and Sarrafzadeh, M., eds.), pp. 253-258, 2003[show files]

  4. P. Rigole, Y. Berbers, and T. Holvoet, Design and run-time bandwidth contracts for pervasive computing middleware, Middleware2003 Companion (Urarahy, C. and Sztajnberg, A. and Cerqueira, R., eds.), pp. 5-12, 2003[show files]

  5. Y. Vandewoude, P. Rigole, D. Urting, and Y. Berbers, Draco : An adaptive runtime environment for components, Department of Computer Science, K.U.Leuven, Report CW 372, Leuven, Belgium, , 2003

<b>[ Publications in 2002 ] </b> <IMAGE src="img/b_pijltje0.gif" alt=""/>

  1. D. Urting, Y. Berbers, S. Van Baelen, T. Holvoet, Y. Vandewoude, and P. Rigole, A Tool for Component Based Design of Embedded Software, Objects for Internet, Mobile and Embedded Applications (Noble, J. and Potter, J., eds.), vol 10, Conferences in Research and Practica in Information Technology, pp. 159-168, 2002[show files]

  2. P. Rigole, T. Holvoet, and Y. Berbers, Using Jini to integrate home automation in a distributed software-system, DCW (Plaice, J. and Kropf, P. and Schulthess, P. and Slonim, J., eds.), vol 2468, Lecture Notes in Computer Science, pp. 291-303, 2002[show files]

  3. P. Rigole, Y. Berbers, and T. Holvoet, Distributable lightweight components in a resource-aware ubiquitous computing environment, Department of Computer Science, K.U.Leuven, Report CW 352, Leuven, Belgium, October, 2002[show files][/publication]









<font size=2><IMAGE src="img/stripesgrey.jpg" alt=""/> Copyright ?2003
K.U.Leuven, Dept. of Computerscience, DistriNet Research Group
Content : dnetw3c
</font>
