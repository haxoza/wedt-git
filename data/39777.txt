<font size=2>CURRICULUM VITAE | CURRENT RESEARCH ACTIVITIES | PROFESSIONAL ACCOMPLISHMENTS | PRESENTATIONS | WRITINGS</font>


[contactinfo][pic]<IMAGE src="face.jpg" alt=""/>[/pic]
Robert Fourer [position]Professor[/position]
[affiliation]Dept. of Industrial Engineering & Management Sciences
Northwestern University[/affiliation]
[address]2145 Sheridan Road, Room C234
Evanston, IL 60208-3119, U.S.A.[/address]

[phone]847/491-3151[/phone] (voice)
[fax]847/467-1828[/fax] or [fax]847/491-8005[/fax] (fax)

<b><font size=4>[email]4er@iems.northwestern.edu[/email]</font></b>[/contactinfo]


Curriculum Vitae

  <font size=4>in PDF (178K)</font>


Current Research Activities

  *  [interests]Cyberinfrastructures for optimization and operations research[/interests]. See the NSF workshop report, "An Operations Cyberinfrastructure: Using Cyberinfrastructure and Operations Research to Improve Productivity in the Enterprise."

  * [interests]Next-generation servers for optimization as an Internet resource[/interests], in collaboration with Jorge Mor? Todd Munson, Sven Leyffer, and others of Argonne National Laboratory, Jason Sarich of Argonne/Northwestern, and Dominique Orban of l'École Polytechnique de Montréal. Current projects include enhancements and investigations of the NEOS Server in the areas of problem representation, analysis, scheduling,and benchmarking.


  *  The [interests]AMPL modeling language and system for large-scale linear, integer, and nonlinear programming[/interests], in collaboration with David Gay of Sandia National Laboratories and Brian Kernighan of Princeton University.  Ongoing projects include extensions for discrete optimization problems via constraint programming and for stochastic programming.


Professional Accomplishments


[education]Degrees

  *  MS, PhD, [phdmajor]Operations Research[/phdmajor], [phduniv]Stanford University[/phduniv]

  *  MS, [msmajor]Statistics[/msmajor], [msuniv]Stanford University[/msuniv]

  *  BS, [bsmajor]Mathematics[/bsmajor], [bsuniv]Massachusetts Institute of Technology[/bsuniv][/education]


[introduction]Positions

  * [affiliation]Northwestern University, Department of Industrial Engineering and Management Sciences[/affiliation], Evanston, IL:  chair (1989-1995); [position]professor[/position] (1993-), associate professor (1985-1993); assistant professor (1979-1985)

  * Bell Laboratories, Computing Sciences Research Center, Murray Hill, NJ:  visiting member of technical staff (1985-1986 and 1995-1996)

  *  National Bureau of Economic Research, Cambridge, MA:  research analyst in mathematical programming (1974-1977)[/introduction]


Awards

  *  2004 INFORMS Fellow Award, for significant contributions to the advancement of operations research and the management sciences.

  *  2004 Medallion Award of the Institute of Industrial Engineers, in recognition of activities that have made a notable impact on the industrial engineering profession.

  *  2003 Beale-Orchard-Hays Prize for Excellence in Computational Mathematical Programming (with E.D. Dolan, J.J. Mor?and T.S. Munson), awarded by the Mathematical Programming Society for "Optimization on the NEOS Server"

  *  2002 John Simon Guggenheim Memorial Foundation Fellowship, to support studies of languages and systems for large-scale optimization

  *  1993 ORSA/CSTS Prize (with D.M. Gay and B.W. Kernighan), awarded by the Computer Science Technical Section of the Operations Research Society of America, for writings on the design of mathematical programming systems and the AMPL modeling language.


[publication]PresentationsCopies of transparencies for the following talks are available in
PDF, postscript, and gzip-compressed postscript formats.


Tutorials & Surveys R. Fourer, Assigning People in Practice, Session WC02, CORS/INFORMS Joint International Meeting, May 16-19, 2004.  B A A

R. Fourer and L.B. Lopes, StAMPL: A Filtration-Oriented Modeling Tool for Stochastic Programming. Technical report, Department of Industrial Engineering and Management Sciences, Northwestern University (2003).

E.D. Dolan, R. Fourer, J.-P. Goux and T.S. Munson, Kestrel: An Interface from Modeling Systems to the NEOS Server. Technical report, Department of Industrial Engineering and Management Sciences, Northwestern University (2002).

R. Fourer and D.M. Gay, Extending an Algebraic Modeling Language to Support Constraint Programming. Technical Report, Department of Industrial Engineering and Management Sciences, Northwestern University (2001); based on a shorter version that appeared in the Proceedings of the Third International Workshop on Integration of AI and OR Techniques in Constraint Programming for Combinatorial Optimization Problems (CP-AI-OR'01), Ashford, Kent, United Kingdom (2001). 

G. Dutta and R. Fourer, Database Structures for a Class of Multi-Period Mathematical Programming Models. Technical report, Department of Industrial Engineering and Management Sciences, Northwestern University (2000).

C. Coullard and R. Fourer, Interdependence of Methods and Representations in Design of Software for Combinatorial Optimization. Technical Report 95-67, Department of Industrial Engineering and Management Sciences, Northwestern University (1995); presented at the First International Joint Workshop on Artificial Intelligence and Operations Research, Timberline, OR (1995). 

R. Fourer, Notes on the Dual Simplex Method.  Draft report (1994). 

R.E. Bixby and R. Fourer, Finding Embedded Network Rows in Linear Programs II: Augmentation Heuristics. Preliminary draft report (1987).


Refereed Articles R. Fourer and L.B. Lopes, A Management System for Decompositions in Stochastic Programming.<IMAGE src="new.gif" alt="new"/>Annals of Operations Research <b>142</b> (2006) 99-118.

R. Fourer, L.B. Lopes and K. Martin, LPFML: A W3C XML Schema for Linear and Integer Programming. INFORMS Journal on Computing <b>17</b> (2005) 139-158.

A. Nareyek, R. Fourer, E.C. Freuder, E. Giunchiglia, R.P. Goldman, H. Kautz, J. Rintanen and A. Tate, Constraints and AI Planning. IEEE Intelligent Systems <b>20</b> (2005) 62-72.

G. Dutta and R. Fourer, An Optimization-Based Decision Support System for Strategic and Operational Planning in Process Industries. Optimization and Engineering <b>5</b> (2004) 295-314.

R. Fourer and D.M. Gay, Extending an Algebraic Modeling Language to Support Constraint Programming.  INFORMS Journal on Computing <b>14</b> (2002) 322-344.

O.B. Sawaya, D.L. Doan, A. Ziliaskopoulos and R. Fourer, Multistage Stochastic System Optimum Dynamic Traffic Assignment Program with Recourse for Incident Traffic Management. Transportation Research Record <b>1748</b> (2001) 116?24.

R. Fourer and D.M. Gay, Hooking a Constraint Programming Solver to an Algebraic Modeling Language. Proceedings of the 3rd International Workshop on Integration of AI and OR Techniques in Constraint Programming for Combinatorial Optimization Problems, Ashford, United Kingdom (2001).

G. Dutta and R. Fourer, A Survey of Mathematical Programming Applications in Integrated Steel Plants. Manufacturing & Service Operations Management <b>3</b> (2001) 387-400.

R. Fourer and J.-P. Goux, Optimization as an Internet Resource.  Interfaces <b>31:</b>2 (March-April 2001) 130-150. 

R. Fourer and D.M. Gay, Conveying Problem Structure from an Algebraic Modeling Language to Optimization Algorithms. In Computing Tools for Modeling, Optimization and Simulation: Interfaces in Computer Science and Operations Research, M. Laguna and J.L. Gonzlez Velarde, eds., Kluwer Academic Publishers, Dordrecht, The Netherlands (2000) 75-89. 

M.C. Ferris, R. Fourer and D.M. Gay, Expressing Complementarity Problems in an Algebraic Modeling Language and Communicating Them to Solvers.  SIAM Journal on Optimization <b>9</b> (1999) 991-1009.

R. Fourer, Predictions for Web Technologies in Optimization.  INFORMS Journal on Computing<b> 10</b> (1998) 388-389.

R. Fourer, Extending a General-Purpose Algebraic Modeling Language to Combinatorial Optimization: A Logic Programming Approach. <b> </b>In D.L. Woodruff, ed., Advances in Computational and Stochastic Optimization, Logic Programming, and Heuristic Search: Interfaces in Computer Science and Operations Research, Kluwer Academic Publishers, Dordrecht, The Netherlands (1998) 31-74.

J. Czyzyk, R. Fourer and S. Mehrotra, Using a Massively Parallel Processor to Solve Large Sparse Linear Programs by an Interior-Point Method.  SIAM Journal on Scientific Computing<b> 19</b> (1998) 553-565.

R. Fourer, Database Structures for Mathematical Programming Models.  Decision Support Systems <b>20</b> (1997) 317-344.

J.J. Bisschop and R. Fourer, New Constructs for the Description of Combinatorial Optimization Problems in Algebraic Modeling Languages. Computational Optimization and Applications <b>6</b> (1996) 83-116.

J. Czyzyk, R. Fourer and S. Mehrotra, A Study of the Augmented System and Column-Splitting Approaches for Solving Two-Stage Stochastic Linear Programs by Interior-Point Methods. ORSA Journal on Computing <b>7</b> (1995) 474-490.

R. Fourer and D.M. Gay, Expressing Special Structures in an Algebraic Modeling Language for Mathematical Programming. ORSA Journal on Computing <b>7</b> (1995) 166-190.

R. Fourer and D.M. Gay, Experience with a Primal Presolve Algorithm. In Large Scale Optimization: State of the Art, W.W. Hager, D.W. Hearn and P.M. Pardalos, eds., Kluwer Academic Publishers (Dordrecht, 1994) 135-154.

R. Fourer and S. Mehrotra, Solving Symmetric Indefinite Systems in an Interior-Point Method for Linear Programming. Mathematical Programming <b>62</b> (1993) 15-39.

R. Fourer and R.E. Marsten, Solving piecewise-linear programs: Experiments with a simplex approach. ORSA Journal on Computing <b>4</b> (1992) 16-31.

R. Fourer, A Simplex Algorithm for Piecewise-Linear Programming, III: Computational Analysis and Applications. Mathematical Programming <b>53</b> (1992) 213-235.

R. Fourer, D.M. Gay, and B.W. Kernighan, A Modeling Language for Mathematical Programming. Management Science<b>36</b> (1990) 519-554.

R. Fourer, A Simplex Algorithm for Piecewise-Linear Programming, II: Finiteness, Feasibility and Degeneracy. Mathematical Programming <b>41</b> (1988) 281-315.

R.E. Bixby and R. Fourer, Finding Embedded Network Rows in Linear Programs I: Extraction Heuristics. Management Science <b>34</b> (1988) 342-376.

R. Fourer, A Simplex Algorithm for Piecewise-Linear Programming, I: Derivation and Proof. Mathematical Programming <b>33</b> (1985) 204-233.

R. Fourer, Staircase Matrices and Systems. SIAM Review <b>26</b> (1984) 1-70.

R. Fourer, Modeling Languages versus Matrix Generators for Linear Programming. ACM Transactions on Mathematical Software <b>9</b> (1983) 143-183.

R. Fourer, Solving Staircase Linear Programs by the Simplex Method, 2: Pricing. Mathematical Programming <b>25</b> (1983) 251-292.

R. Fourer, Solving Staircase Linear Programs by the Simplex Method, 1: Inversion. Mathematical Programming <b>23</b> (1982) 274-313.

R. Fourer, J.B. Gertler and H.J. Simkowitz, Optimal Fleet Sizing and Allocation for Improved Rail Service in the Northeast Corridor. Transportation Research Record <b>656</b> (1978) 40-45.

R. Fourer, J.B. Gertler and H.J. Simkowitz, Models of Railroad Passenger-Car Requirements in the Northeast Corridor. Annals of Economic and Social Measurement <b>6</b> (1977) 367-398.[/publication]

Updated May 22, 2006
