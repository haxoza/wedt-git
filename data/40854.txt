Lei Ying - Assistant Professor



[contactinfo]<b>Contact Information</b>

Office Address:
[address]EMS 1085 
3200 North Cramer Street
Milwaukee, WI 53211[/address]
Phone: [phone]414- 229-5907[/phone]
Fax: [fax]414-229-2769[/fax]
Email: [email]leiying@uwm.edu[/email]
WWW: 

Mailing Address:
[address]P.O. Box 784
Department of Electrical Engineering and Computer Science
University of Wisconsin ?Milwaukee
Milwaukee, WI 53201[/address][/contactinfo]

[pic]<IMAGE src="../assets/images/LYing.jpg" alt="Lei Ying"/>[/pic]

[education]<b>Education</b>

Ph.D. [phdmajor]Electrical Engineering[/phdmajor], [phduniv]University of Illinois at Urbana-Champaign[/phduniv], [phddate]May 2003[/phddate]
M.S. [msmajor]Electrical Engineering[/msmajor], [msuniv]University of Illinois at Urbana-Champaign[/msuniv], [msdate]Aug. 1999[/msdate]
B. Eng. [bsmajor]Electronics Engineering[/bsmajor], [bsuniv]Tsinghua University[/bsuniv], [bsdate]July 1997[/bsdate][/education]

<b>Research Interests</b>

[interests]Medical imaging[/interests], [interests]Magnetic Resonance Imaging[/interests], [interests]Signal and Image Processing[/interests], [interests]Interpolation[/interests], [interests]Multidimensional Signal Reconstruction[/interests].

[publication]<b>Selected Publications</b>

  *  L. Ying, D. C. Munson, Jr., B. Frey, and R. Koetter, Multibaseline digital elevation mapping: A dynamic programming approach? Proceedings of IEEE International Conference on Image Processing, Sept. 2003

  *  L. Ying, and et al., A robust and efficient method to unwrap MR phase images? Proceedings of International Society of Magnetic Resonance in Medicine Scientific Meeting, vol. 11, p. 782, July 2003

  *  L. Ying, B. Frey, R. Koetter, and D. C. Munson, Jr., Analysis of an iterative dynamic programming approach to 2-D phase unwrapping? Proceedings of IEEE International Conference on Image Processing, vol. 3, pp. 829-832, Sept. 2002

  *  L. Ying, B. Frey, R. Koetter, and D. C. Munson, Jr., An iterative dynamic programming approach to 2-D phase unwrapping? Proceedings of IEEE International Geoscience and Remote Sensing Symposium 2002, vol.1, pp. 469-471, June 2002

  *  M. R. Fetterman, E. Tan, L. Ying, and et al., Tomographic imaging of foam? Optics Express, vol. 7, No. 5, pp. 186-197, Aug. 2000

  *  L. Ying and D. C. Munson, Jr., Approximation of minmax interpolator? Proceedings of IEEE International Conference on Acoustics, Speech and Signal Processing 2000, vol.1, pp. 328-331, June 2000[/publication]
