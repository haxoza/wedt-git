Wellcome to My Home Page
Japanesehere

Name :


      Toshiro Matsumura


[pic]<IMAGE src="face.gif" alt=""/>[/pic]

[contactinfo]Occupation :

      [position]Professor[/position]

      [affiliation]Department of Electrical Engineering and Computer Science

      Graduate School of Engineering

      Nagoya University[/affiliation]
      Address :

      [address]Furo-cho, Chikusa-ku, Nagoya, 464-8603, Japan[/address]

      TEL : [phone]052-789-3316[/phone], FAX : [fax]052-789-3149[/fax]

      E-Mail : |[email]matumura(at)nuee.nagoya-u.ac.jp[/email]|[/contactinfo]
      Sex :

      Male
      Date of birth :

      1st May, 1951
      Nationality :

      Japanese

      [education]Degrees:

      Bachelor of Eng., [bsmajor]Electrical Eng.[/bsmajor], [bsuniv]Nagoya University[/bsuniv], [bsdate]1974[/bsdate]

      Master of Eng., [msmajor]Electrical Eng.[/msmajor], [msuniv]Nagoya University[/msuniv], [msdate]1976[/msdate]

      Doctor of Eng., [phdmajor]Electrical Eng.[/phdmajor], [phduniv]Nagoya University[/phduniv], [phddate]1981[/phddate][/education]

      [introduction]Employment Record and Experiences:

      1979-1987 : Assistant Prof., Nagoya University

      1987-1989 : Associate Prof., Nagoya University

      1989-1991 : Associate Prof., Kyoto University

      1992-1995 : Associate Prof., Nagoya University

      1995- : [position]Professor[/position], [affiliation]Nagoya University[/affiliation][/introduction]

      [resactivity]Institution Membership:

      [position]Member[/position] of the [affiliation]Institute of Electrical Engineers of Japan[/affiliation]

      [position]Member[/position] of the [affiliation]Institute of Electrical and Electronics Engineers, Inc.[/affiliation]

      etc.[/resactivity]

      |Matsumura Lab. Home Page (Japanese)|

      |Matsumura Lab. Home Page (English)|

      |List of Matsumura Lab. Member (Japanese)|

      |List of Matsumura Lab. Member (English)|


      [publication]Publications


      Arcs and Circuit Breaker

      Arcs and Circuit Breaker

      Electric Power System and Energy

      Power Apparatus and Superconductivity


      Arcs and Circuit Breaker 1) T. Matsumura Y. Yokomizu,
      "Conductance Decaying Process of Wall-Stabilized Arc with CF3I Gas Blast (In Japanese)",
      Journal of IAPS, Vol.13, pp.29-34 (2005)

      2) Y.Yokomizu S.Matsumoto S.Hirata T.Matsumura A.Ishikawa T.Furuhata K.Mitsukuchi,
      "Arc Behavior in Rotary-arc Type of Load-break Switch and its Current-interrupting Capability for Different Environmentally Benibn Gases and Electrode Materials",
      IEEJ Trans. PE., Vol.125 No.11 pp.1070-1076 (2005)

      3)T.Matsumura Y.Yokomizu P.Almiron K.Yamamoto D.Ohta M.Shibuya,
      "Breakdown Voltage of CO2 at Temperatures around 4000 K and in Range from 300 to 700K",
      IEEJ Trans. PE., Vol.125 No.11 pp.1063-1069 (2005)

      4) R.Hemmmi Y.Yokomizu T.Matumura,
      "Anode-fall Voltages of Air Arcs Between Electrodes of Copper,Silver and Tungsten at Currents up to 1500 A",
      IEEJ Trans. PE., Vol.124 No.1 pp.143-149(2004)

      5) R. Hemmi Y. Yokomizu T. Matsumura,
      "Anode-fall and Cathode-fall Voltages of Air Arcs in Atmosphere Between Electrodes of Tungsten, Copper and Silver in Current Range up to 1500 A (In Japanese)",
      J. IEIE Japan, Vol.23 No.12 pp.90-98 (2003)

      6) K. Nakayama Y. Yokomizu T. Matsumura,
      "Anode and Cathode Fall Voltage for Atmospheric Air Arc between Copper Electrodes Estimated from Energy for Initiating Electrode Erosion (In Japanese)",
      Journal of IAPS, Vol.11 pp.17-21 (2003)

      7) T.Matsumura S.Hirata E.Calixte Y.Yokomizu,
      "Current interruption capability of H2-air hybrid model circuit breaker",
      VACUUM, Vol.73 pp.481-486 (2004)

      8) K. Nakayama Y. Yokomizu T. Matsumura E. Kanamori K. Kuwamura,
      "Arc Behavior Analysis and Fractional Decomposition of Voltage Rise of High-Current Air Arc Affected by Deion Plates (In Japanese)",
      IEEJ Trans. PE., Vol.123 No.9 pp.1091-1096 (2003)

      9) T. Matsumura Y. Yokomizu T. Watanabe K. Ito,
      "Equilibrium Composition of High-Temperature Argon Contaminated with Fly-Ash Metals in Consideration of Phase Transformation and its Thermodynamic and Transport Properties (In Japanese)",
      IEEJ Trans. FM., Vol.123 No.7 pp.637-643 (2003)

      10) Y. Yokomizu K. Ito T. Matsumura,
      "Approach to Decaying Process of Arc Conductance after Current Zero on the basis of Thermodynamic and Transport Properties of High Temperature Gases (In Japanese)",
      IEEJ Trans. PE., Vol.123 No.4 pp.450-456 (2003)

      11) R.Hemmmi Y.Yokomizu T.Matumura,
      "Anode-Fall and Cathode-fall Voltages of Air Arc in Atmosphere between Silver Electrodes",
      J. Phys. D: Appl. Phys., No.36 pp.1097-1106 (2003)

      12) Y.Yokomizu T.Matsumura A.Matsuda H.Ohno,
      "Dependence of Arc Interrupting Capability on Spatial Distribution of Airflow Velocity in Air-Blast Flat-Type Quenching Chamber",
      IEEE Tansactions on Power Delivery, Vol.18 No.1 pp.101-106 (2003)

      13) K. Nakayama Y. Yokomizu T. Matsumura E. Kanamori K. Kuwamura,
      "Mechanism on Voltage Rise of High-Current Arc at Atmospheric Pressure due to Deion Plates (In Japanese)",
      Trans. IEE of Japan, Vol.122-B No.9 pp.1016-1021 (2002)

      14) T.Matsumura Y.Yokomizu S. Arisawa,
      "Experimental Study on Threshold Conditions of Arcing Phenomenon in Hot Air",
      Vacuum, Vol.65, pp.335-339 (2002)

      15) J.He T.Matsumura,
      "3-Dimensional Simulation on Electrical Conductivity of CuCr Contact Materials",
      Trans. IEE of Japan, Vol.121-B No.8 pp.924-929 (2001)

      16) Y. Yokomizu T. Matsumura H. Ohno,
      "Decaying Processes of Electron Density of Arc around Current Zero and its Variation due to Spatial Distribution of Airflow Velocity in Flat-Type Quenching Chamber (In Japanese)",
      Trans. IEE of Japan, Vol.121-B No.7 pp.853-859 (2001)

      17) Y. Yokomizu K. Ito T. Kojima T. Matsumura,
      "Decaying Processes of Conductance around Current Zero for Wall-Sabilized Arcs in Various Gases (In Japanese)",
      Trans. IEE of Japan, Vol.121-B No.6 pp.767-774 (2001)

      18) S.Tanaka T.Matsumura,
      "Characteristic dynamic behavior of dc arc near graphite bar electrodes with short gap",
      JOURAL OF APPLIED PHYSICS, Vol.89 No.8 pp.4247-4254 (2001)

      19) T. Matsumura Y. Yokomizu K. Ito,
      "Decaying Process of Arc Conductance in Gases for Mitigating Global Warming (In Japanese)",
      Journal of IAPS, Vol.8 pp.138-143 (2000)

      20) H. Ohno H. Ito T. Tange K. Naito Y. Yokomizu T. Matsumura,
      "Traqvelling Characteristics of Electromagnetically Driven Arc Burning between the Edges of Parallel Flat Electrodes in Atmospheric Pressure (In Japanese)",
      Trans. IEE of Japan, Vol.120-B No.11 pp.1504-1512 (2000)

      21) T.Matsumura Y.Yokomizu H.Ohno,
      "Influence of air flow velocity distribution on current interruption in flat-type arc quenching chamber",
      VACUUM, Vol.59 No.1 pp.98-105 (2000)

      22) Y.Yokomizu T.Matsumura,
      "Arcing phenomena through hot gas ejected from moulded-case cricuent breakers during the high-current interruption process",
      J. Phys. D: Appl. Phys., No.34 pp.116-123 (2000)

      23) H. Ohno T. Tange H. Ito K. Naito Y. Yokomizu T. Matsumura,
      "Effects of Nozzle Shapes and their Positions on Axial Distribution of Electron Density around Nozzle Throat and on Arc Interrupting Capability in Flat-type Air-blast Quenching Chamber (In Japanese)",
      Trans. IEE of Japan, Vol.120-B No.5 pp.739-745 (2000)

      24) G J Cliteur, K Suzuki, Y Tanaka, T Sakuta, T Matsubara, Y Yokomizu and T Matsumura,
      "On the determination of the multi-temperature SF6 plasma composition",
      J. Phys. D: Appl. Phys. "Vol.32, pp.1851-1856 (1999)

      25) Y.Yokomizu A.Matsuda T.Matsumura H.Ohno Y.Kito,
      "Correlation between Interrupting Capability and Airflow Velocity",
      Trans. IEE of Japan, Vol.119-B, No.8, pp.988-993 (1999)

      26) Y. Yokomizu T. Matsumura Y. Kito,
      "Short-Circit Phenomenon Through Hpt Gas Ejected From Molded-Case Circuit Breakers during High Current Interruption Process and Prevention of its Pccurence (In Japanese)",
      Trans. IEE of Japan, Vol.119-B No.7 pp.834-839 (1999)

      27) H. Aichi T. Matsumura I. Miyachi,
      "Characteristics of Contact Resistance for Ag, Cu and Al Spot Contact under DC Current Flow of 300 A (In Japanese)",
      Trans. IEE of Japan, Vol.118-B No.7/8 pp.825-830 (1998)

      28) Y.Yokomizu T.Matsumura W.Y.Sun J.J.Lowke,
      "Electrode sheath voltages for helium arcs between non-thermionic electrodes of iron, copper and titanium",
      J.Phys.D: Appl.Phys., Vol.31 No.7 pp.880-883 (1998)

      29) Y.Yokomizu T.Matsumura R.Henmi Y.Kito,
      "Electrode-fall Voltages of SF6 and Air Arcs Between Electrodes of Fe, W, Cu-W, Cu and Ti in the Current Range from 10A to 20kA",
      European Transactions on Electrical Power, Vol.8 No.2 pp.111-115 (1998)

      30) Y.Tanaka Y.Yokomizu M.Ishikawa T.Matsumura,
      "Particle Composition of High-Pressure SF6 Plasma with Electron Temperature Greater than Gas Temperature",
      IEEE Transactions on Plasma Science, Vol.25 No.5 pp.991-995 (1997)

      31) Y.Tanaka Y.Yokomizu M.Ishikawa T.Matsumura Y.Kito,
      "Dominant Spectra of Background Radiation in an SF6 Post-Arc Channel",
      IEEE Transactions on Plasma Science, Vol.25 No.5 pp.986-990 (1997)

      32) T.Matsumura Y.Yokomizu W.Y.Sun,
      "Radiation Power of High Pressure SF6 Arc Plasma",
      Proc. of ISAPS'97, pp.31-36 (1997)

      33) Y.Tanaka Y.Yokomizu T.Matsubara T.Matsumura,
      "Particle Composition of Two-Temperature SF6 Plasma in Pressure Range from 0.1 to 1 Mpa",
      Proc. of the XII Int. Conf. on Gas Discharges and their Applications (Greifswald), Vol.II pp.566-569 (1997)

      34) H.Ohno H.Ito H.Naganawa Y.Yokomizu T.Matsumura Y.Kito,
      "Erosion of Parallel Bar Electrodes due to Traveling Arc Driven by Electromagnetic Force",
      Proc. of the XII Int. Conf. on Gas Discharges and Their Applications (Greifswald), Vol.I pp.127-130 (1997)

      35) Y.Yokomizu T.Matsumura A.Matsuda H.Ohno Y.Kito,
      "Relationships between Interrupting Capability and Distribution of Gas Flow Velocity in Flat-Type Arc Quenching Chamber",
      Proc. of the XII Int. Conf. on Gas Discharges and Their Applications (Greifswald), Vol.I pp.111-114 (1997)

      36) T.Matsumura Y.Yokomizu T.Matsubara Y.Tanaka,
      "Electrical Conductivity and Enthalpy of SF6 Plasma in Two-Temperature State",
      Proc of the XII Int. COnf. on Gas Discharges and Their Applications (Greifswald), Vol.I pp.94-97 (1997)

      37) Y.Yokomizu A.Matsuda H.Ohno T.Matsumura Y.Kito,
      "Effects of NOzzle Shape on Air Flow Velocity Distribution and Interrupting Capability in Flat-Type Arc Quenching Chamber",
      Proc. of the Int. Conf. on Electrical Engineering '97 (Matsue), pp.730-733 (1997)

      38) Y.Tanaka Y.Yokomizu M.Ishikawa T.Matsumura,
      "Electrical Conductivity and Enthalpy of Two Temperature SF6 Plasma at a Pressure of 0.1 Mpa",
      Proc. of the Int. Conf. on Electrical Engineering '97 (Matsue), pp.726-729 (1997)

      39) Y.Tanaka Y.Yokomizu T.Matsumura Y.Kito,
      "The Opening Process of Thermal Plasma Contacts in a Post-Arc Channel after Current Zero in a Flat-Type SF6 Gas-Blast Quenching Chamber",
      J.Phys.D: Appl.Phys., Vol.30 pp.407-416 (1997)

      40) Y.Yokomizu T.Matsumura Y.Kito,
      "Approximate Numerical Calculation Method for Deriving an Asymmetrical Temperature Distribution in a Cross-Section of a Gas-Blasted Arc",
      European Transactions on Electrical Power, Vol.7 No.2 pp.91-97 (1997)

      41) Y.Yokomizu T.Matsumura N.Umeda W.Y.Sun Y.Kito,
      "Radiation Power of High Current SF6 Arc at Pressure of 0.4MPa",
      Proc. of 3rd Int. Conf. on Electrical Contacts, Arcs, Apparatus and their Applications (Xi'an), pp.143-148 (1997)

      42) Y.Yokomizu T.Matsumura R.Henmi Y.Kito,
      "Total Voltage Drops in Electrode Fall Regions of SF6 Arcs between Electrodes of Cu-W, Cu, Fe, Ti, C and W in Current Range from 10 to 20000A",
      Trans. of IEE Japan,Vol.116-B No.10 pp.1197-1203 (1996)

      43) Y. Tanaka Y. Yokomizu M. Ishikawa T. Matsumura Y. Kito,
      "Opening Process of Fictitious Contacts Made of Themal Plasma after Current Zero in Flat-Type SF6 Gas-Blast Quenching Chamber (In Japanese)",
      Trans. IEE of Japan, Vol.116-B No.10 pp.1204-1211 (1996)

      44) Y.Yokomizu T.Matsumura Y.Kito,
      "Radiation Power of SF6 Arc at Currents up to 20kA",
      12th Symposium on Physics of Switching Arc (Brno), Vol.1, pp.152-155 (1996)

      45) Y.Tanaka Y.Yokomizu T.Matsumura Y.Kito,
      "Transient Distribution of Metallic Vapour Concentration in a Post-Arc Channel after Current Zero along Nozzle Axis in a Flat-Type SF6 Gas-Blast Quenching Chamber",
      J.Phys.D:Appl.Phys.,Vol.29 pp.1540-1550 (1996)

      46) Y.Yokomizu T.Matsumura R.Henmi Y.Kito,
      "Total Voltage Drops in Electrode Fall Regions of SF6, Argon And Air Arcs in Current Region from 10 to 20000A",
      J.Phys .D: Appl. Phys., Vol.29 pp.1260-1267 (1996)

      47) Y.Yokomizu T.Matsumura K.Shimizu Y.Kito S.Takayama Y.Aoyama,
      "Conductance Diagnostics in Hot Gas Ejected from a Molded Case Circuit Breaker during High Current Arc Interruption",
      Trans. of IEE Japan,Vol.116-B No.3 pp.338-345 (1996)

      48) Y.Tanaka Y.Yokomizu T.Matsumura Y.Kito,
      "Transient Behaviour of Axial Temperature Distribution in Post-Arc Channel after Current Zero around Nozzle Throat in Flat-Type SF6 Gas-Blast Quenching Chamber",
      J.Phys D:Appl. Phys.,Vol.28 pp.2095-2103 (1995)

      49) T.Matsumura Y.Yokomizu K.Shimizu Y.Kito S.Takayama Y.Aoyama,
      "Thermal Model Criteria of Short-Circuit Fault Induced in Hot Gas Ejected from a Molded-Case Circuit Breaker",
      Proc. of 11th Int. Conf. on Gas Discharges and Their Applications (Tokyo),Vol.1 pp.58-61 (1995)

      50) H.Ohno H.Nagonawa Y.Yokomizu T.Matsumura Y.Kito,
      "Dynamic Voltage and Current Characteristics of AC Air Arc Blasted by Ablated PTFE Gas",
      Proc. of 11th Int. Conf. on Gas Discharges and Their Applications (Tokyo),Vol.1 pp.62-65 (1995)

      51) Y.Yokomizu T.Matsumura K.Shimizu Y.Kito S.Takayama Y.Aoyama,
      "Transient Resistance of Hot Gas Ejected from a Molded Case Circuit Breaker in High Arc Interruption",
      Proc. of 6th International Symposium on Short Circuit Currents in Power Systems,pp.4.9.1-4.9.7 (1994)

      52) T. Matsumura Y. Yokomizu H. Futamata Y. Kito,
      "Total Radiation Power of 20kA-Class SF6 Gas-Blast Arcs (In Japanese)",
      Trans. IEE of Japan, Vol.114-A No.9 pp.585-590 (1994)

      53) Y.Yokomizu T.Matsumura Y.Kito,
      "An Approximate Numerical Calculation to Derive an Asymmetrical Distribution of Emission Coefficient in a Cross-Section of a Gas Blasted Arc",
      Proc. of 10th Int. Conf. on Gas Discharges and Their Applications (Swansea), Vol.1 pp.222-225 (1992)

      54) T. Matsumura Y. Ikuma Y. Kito,
      "Improvement of Current Limiting Performance of a Molded-Case Circuit Breaker by Mounting an Air-Buffer Chamber (In Japanese)",
      Trans. IEE of Japan, Vol.110-B No.5 pp.420-426 (1990)

      55) T.Matsumura W.T.Oppenlander C.A.Shmidt-Harms A.D.Stokes,
      "Prediction of Circuit-Breaker Performance Based on a Refined Cybernetic Model",
      IEEE. Trans. Plasma Science,Vol.PS-14 No.4 pp.435-443 (1986)

      56) T. Matsumura Y. Kito,
      "A Numerical Model of Wall-Stabilized Arc Sustained in the Forced Axial Air Flow (In Japanese)",
      Trans. IEE of Japan, Vol.104-A No.5 pp.241-246 (1984)

      57) T.Matsumura T.Sakakibara Y.Kito I.Miyachi,
      "Response of a Wall-Stabilized Air Arc to Step and Sinusoidal Change in Current",
      IEEE Trans. on Plasma Science, Vol.PS-8 No.3 pp.248-252 (1980)

      58) T.Matsumura Y.Kito I.Miyachi,
      "Transient Aspects of ArcConductance in Wall-Stabilized Air and SF6 Arc Disturbed by Step Change in Current",
      Proc. 5th Int.Conf. on Gas Discharges, (Liverpool)", pp.138-141 (1978)


      Electric Power System and Energy 1) K.Mizuno K.Ichiyanagi N.Takatsuka Y.Goto K.Yukita T.Matsumura Y.Kawashima,
      "Prediction of Total Amount of Rivr Flow Rate on Upper District of Dam for Hydro Power Plant by Using Radar Echo Data",
      WSEAS TRANSACTION ON POWER SYSTEMS, Issue2,Volume 1,February (2006)

      2) D. Iioka K. Sakakibara Y. Yokomizu T. Matsumura N. Idehara,
      "Distribution Voltage Rise at Dense Photovoltaic Power Generation Area and its Suppression by SVC (In Japanese)",
      IEEJ Trans. PE., Vol.126, No.2, pp.153-158(2006)

      3) E.Calixte Y.Yokomizu H.Shimizu T.Matsumura,
      "Theoretical expression of rate of rise of recovery voltage across a circuit breaker connected with fault current limiter",
      Electric Power Systems Research, Vol.75,pp.1-8 (2005)

      4) E.Calixte, Y.Yokomizu H.Shimizu T.Matsumura H.Fujita,
      "Interrupting Condition Imposed on a Circuit Breaker Cohaidenn nnected with Fault Current Limiter under Short Line Fault Regime",
      International Journal of Power and Energy Systems, Vol. 25, No.2, pp.75-81 (2005)

      5) E.Calixte Y.Yokomizu T.Matsumura H.Fujita,
      "Reduction in Severity of Circuit Breaker Duty under Generator-Fed Fault Condition by Means of Resistive Fault Current Limiter",
      International Journal of Power and Energy Systems PowerCON Special Issue, No.434-083 pp.24-31 (2004)

      6) E.Calixte Y.Yokomizu T.Matsumura H.Fujita,
      "Evaluation of Breaking Capability of a Hybrid Fault Current-Limiting Circuit Breaker Based on Mayr-Type Arc Model",
      IEEJ Trans. PE., Vol.124 No.6 pp.843-850 (2004)

      7) K. Ichiyanagi K. Mizuno K. Nakajima H. Yamada K. Yukita Y. Goto T. Matsumura Y. Kawashima,
      "Estimation of Runoff Ratio on Upper District of Dam for Hydro Power Plant by Using Radar Echo Data (In Japanese)",
      IEEJ Trans. PE., Vol.124 No.2 pp.229-236(2004)

      8) E.Calixte Y.Yokomizu T.Matsumura H.Fujita,
      "Reduction of Rating Required for Circuit Breakers by Employing Series-Connected Falut Current Limiters",
      IEE Proc.- Gener Transm Distrib, Vol.151 No.1 pp.36-42(2004)

      9) D. Iioka H. Shimizu Y. Yokomizu M. Goto T. Matsumura,
      "Influence of Output Power out of Synchronous Generator Installed in Customer System on Current Limiting Effect due to Fault Current Limiter (In Japanese)",
      IEEJ Trans. PE., Vol.123 No.7 pp.814-821 (2003)

      10) H. Shimizu Y. Yokomizu M. Goto T. Matsumura,
      "Steady State Current Limiting Characteristics of Fault Current Limiter at Three-Phase Short Circuit Fault in Electric Power System (In Japanese)",
      IEEJ Trans. PE., Vol.123 No.4 pp.457-465 (2003)

      11) D. Iioka H. Shimizu Y. Yokomizu M. Goto T. Matsumura I. Kano,
      "Suppressing Effect of Fault Current Limiter Installed in Incoming Feader to Consumer System with Synchronous Generator on Short-Circit Current in Dstribution System (In Japanese)",
      Trans. IEE of Japan, Vol.122-B No.11 pp.1167-1173 (2002)

      12) Y.H. Guo Y. Yokomizu T. Matsumura H. Fujita,
      "Difference of Effect of Superconducting Fault Current Limiter Introduced into Electric Power System due to Resitive-Type, Reactive-Type and Their Introduction Location (In Japanese)",
      Trans. IEE of Japan, Vol.120-B No.6 pp.791-800 (2000)

      13) Y.H.Guo Y.Yokomizu T.Matsumura J.J.He,
      "Simulation of Fault Current Limiting Performance When Line-to-Line Fault Occurs in Distribution System",
      Journal, High Voltage Apparatus, Vol.36 No.2 pp.7-11 (2000)

      14) K. Mizuno N. Hayashi Y. Goto K. Yukita K. Ichiyanagi Y. Yokomizu T. Matsumura,
      "Investigation of Input Data for Estimation of Ground Rainfall Distribution by Using Artificial Neural Network (In Japanese)",
      Trans. IEE of Japan, Vol.120-B No.5 pp.665-671 (2000)

      15) Y.H. Guo Y. Yokomizu T. Matsumura,
      "Design Guideline of Flux-Lock Type Superconducting Fault Current Limier with AC Magnetic Field Coil for 6.6 kV Distribution System (In Japanese)",
      Trans. IEE of Japan, Vol.120-B No.3 pp.368-374 (2000)

      16) Y. Goto T. Niimi K. Yukita K. Mizuno K. Ichiyanagi Y.H. Guo Y. Yokomizu T. Matsumura,
      "Experimantal Studies on Effect of Superconducting Fault Current Limiters on Improvement of Power System Stability (In Japanese)",
      Trans. IEE of Japan, Vol.119-B No.11 pp.1241-1248 (1999)

      17) H.Shimizu Y.Yokomizu T.Kato Y.J.Tang T.Matsumura Y.Kito K.Satoh W.Satoh,
      "Transmission Loss of Prospective Power Transmission Model System Integrated under Superconducting Environment (PROMISE)",
      IEEE Transactions on Applied Superconductivity, Vol.7 No.2 pp.1033-1036 (1997)

      18) T.Matsumura Y.Suzuki Y.Yokomizu K.Mizuno Y.Goto K.Ichiyanagi,
      "Application of Artificial Neural Network to Data Processing of Meteorological Radar Echoes for Forecasting Flow Rate into Dam",
      Proc. of the IASTED Int. Conf. ARTIFICIAL INTELLIGENCE AND SOFT COMPUTING (Banf), pp389-392 (1997)

      19) K.Ichiyanagi Y.Goto Y.Yokomizu T.Matsumura Y.Kito,
      "A Prediction of River Flow Rate Into a Dam for a Hydro Power Plant by Artificial Neural Network Trained with Data Classified According to Total Amount of Rain",
      International Journal of Power & Energy Systems,Vol.16 No.1 pp.11-15 (1996)

      20) T.Kato N.Hayakawa Y.Yokomizu T.Matsumura Y.Kito Y.Goto,
      "Voltage Stabilizing Effect of Superconducting Cable Introduced in A Tree-Type Metropolitan Electric Power System",
      Proc. of the International Power Engineering Conference (Singapore),Vol.1 pp.85-90 (1995)

      21) K.Ichiyanagi Y.Goto K.Mizuno Y.Yokomizu T.Matsumura,
      "An Artificial Neural Network to Predict River Flow Rate into a Dam for a Hydro-Power Plant",
      Proc. Of 1995 IEEE Int. Conf. on Neural Networks (Perth),Vol.5 pp.2679-2682 (1995)

      22) T. Kato Y. Tao N. Hayakawa T. Matsumura Y. Kito,
      "Evaluation of Total Transmission Loss and Refrigerating Energy to Operate Superconducting Cables in a Future Metropolitan Electric Power System (In Japanese)",
      Trans. IEE of Japan, Vol.114-B No.12 pp.1303-1309 (1994)

      23) T. Kato N. Hayakawa Y. Goto T. Matsumura Y. Kito,
      "Reduction Effect on Instantaneous Voltage Dip in Advanced Electric Power System with Superconducting Power Transmission Cable (In Japanese)",
      Trans. IEE of Japan, Vol.114-B No.6 pp.609-616 (1994)

      24) Y.J. Tang Y. Yokomizu N. Hayakawa Y. Goto T. Matsumura H. Okubo Y. Kito,
      "Quench Current Level Coordination in Superconducting Power Transmission System (In Japanese)",
      Trans. IEE of Japan, Vol.113-B No.9 pp.981-986 (1993)

      25) K.Ichiyanagi H.Kobayashi T.Matsumura Y.Kito,
      "Application of Artificial Neural Network to Forecasting Methods of Time Variation of the Flow rate into a Dam for a Hydro-Power Plant",
      Proc. of 2nd Int. Forum on Applications of Neural Networks to Power Systems (Yokohama),pp.349-354 (1993)

      26) N. Hayakawa Y. Kito T. Matsumura,
      "Power Swing Analysis in a Future Metropolitan Electric Power System on a Contingent Trip of Cryogenic High Power Cables (In Japanese)",
      Trans. IEE of Japan, Vol.110-B No.10 pp.834-840 (1990)

      27) T. Matsumura K. Higuchi Y. Kito,
      "Development of a Quick Solving Method in Expert-System Portotype for Protection Coordination in Private Power Distribution Systems by Modeling the Inference Process of Engineers (In Japanese)",
      Trans. IEE of Japan, Vol.110-C No.8 pp.473-478 (1990)

      28) K. Ichiyanagi H. Kobayashi T. Matsumura Y. Kito,
      "A Forecasting System of Both Rainfall Pattern and River Flow Rate in Upper District of Dams for Hydro-Power Plants (In Japanese)",
      Trans. IEE of Japan, Vol.109-B No.6 pp.243-250 (1989)

      29) F. Yoshimori T. Matsumura Y. Kito,
      "Development of the Knowledge-Based System with PROLOG Language for Selecting Appropriate Switching Devices in Industrial Power Distribution Systems (In Japanese)",
      Trans. IEE of Japan, Vol.108-C No.8 pp.563-570 (1988)

      30) N. Hayakawa T. Matsumura Y. Kito,
      "Prospective Electric Power System Models for Future Large Cities in Japan as a Background System Prior to Introduction of Cryogenic Power Cables (In Japanese)",
      Trans. IEE of Japan, Vol.108-B No.8 pp.355-362 (1988)

      31) Y.Kito T.Matsumura N.Hayakawa S.Ito and T.Sakakibara,
      "Operational Feasibility of a Cryogenic Power Cables in Future Metroporitan Power Systems.",
      Proc. Research on Effective Utilization and Densification of Electric Energy (Tokyo), Vol.SPEY-24 pp.45-48 (1987)

      32) K. Ichiyanagi H. Kobayashi A. Shinoda T. Matsumura Y. Kito,
      "A Forecasting Method of Time Variation of Rainfall in Upper District of Dams for Hydro-Power Plants by AMeDAS Data Processing (In Japanese)",
      Trans. IEE of Japan, Vol.106-B No.9 pp.809-816 (1986)

      33) K. Ichiyanagi T. Matsumura Y. Kito Y. Yamada M. Suzuki,
      "Application of Meteorological Satellite Data to Rainfall Forecasting in the Upper District of the Dams for Hydro-Power Generation (In Japanese)",
      Trans. IEE of Japan, Vol.104-B No.7 pp.441-448 (1984)


      Power Apparatus and Superconductivity 1) T.Matsumura M.Sugimura K.Mutsuura Y.Yokomizu H.Shimizu M.Shibuya M.Ichikawa H.Kado,
      "Limiting Impedance of Flat Type Fault Current Limiter with High Tc Superconducting Plate",
      IEEJ Trans. PE., Vol.126, No.2, pp.147-152(2006)

      2) H. Shimizu Y. Yokomizu T. Matsumura,
      "Fundamental Performance of Superconducting Fault Current Limiter with Two Air Core Coil (In Japanese)",
      IEEJ Trans. PE, Vol.126, No.2, pp.134-140 (2006)

      3) T.Matsumura T.Aritake Y.Yokomizu H.Shimizu N.Murayama,
      "Performances of small fault current limiting breaker model with high Tc Superconductor",
      IEEE Transactions on Applied Superconductivity, Vol.15 No.2 pp.2114-2117 (2005)

      4) H.Kado M.Ichikawa M.Shibuya M.Kojima M.Kawahara T.Matsumura,
      "Inductive Type Fault Current Limiter Using Bi-2223 Thick Film on MgO Cylinder With Bi-2212 Buffer Layer",
      IEEE Transactions on Applied Superconductivity, Vol.15 No.2 pp.2051-2054 (2005)

      5) H.Shimizu K.Mutsuura Y.Yokomizu T.Matsumura,
      "Inrush-Current-Limiting With High Tc Superconductor",
      IEEE Transactions on Applied Superconductivity, Vol.15 No.2 pp.2071-2073 (2005)

      6) T.Matsumura M.Sugimura Y.Yokomizu H.Shimizu M.Shibuya M.Ichikawa H.Kado,
      "Generating Performance of Limiting Impedance in Flat Type of Fault Current Limiter With High Tc Superconducting Plate",
      IEEE Transactions on Applied Superconductivity, Vol.15 No.2 pp.2015-2018 (2005)

      7) K.Mutsuura H.Shimizu Y.Yokomizu T.Matsumura,
      "Flux Flow Resistance in Bi2223 Generated by Pulse Currents",
      IEEE Transactions on Applied Superconductivity, Vol.15 No.2 pp.2003-2006 (2005)

      8) H.Shimizu Y.Yokomizu T.Matsumura,
      "Comparison of Fundamental Performance of Different Type of Fault Current Limiters With Two Air Core Coils",
      IEEE Transactions on Applied Superconductivity, Vol.14 No.2 pp.807-810 (2004)

      9) T.Matsumura A.Kimura H.Shimizu Y.Yokomizu M.Goto,
      "Fundamental Performance of Flux-Lock Type fault Current Limiter with Two Air-Core Coils",
      IEEE Transactions on Applied Superconductivity, Vol.13 No.2 pp.2024-2027 (2003)

      10) H.Shimizu Y.Yokomizu M.Goto T.Matsumura N.Murayama,
      "A Study on Required Volume of Superconducting Element for Flux Flow Resistance Type Fault Current Limiter",
      IEEE Transactions on Applied Superconductivity, Vol.13 No.2 pp.2052-2055 (2003)

      11) T.Aritake T.Noda H.Shimizu Y.Yokomizu T.Matsumura N.Murayama,
      "Relation Between Critical Current Density and Flux Flow Resistivity in Bi2223 Bulk Element for Fault Current Limiter",
      IEEE Transactions on Applied Superconductivity, Vol.13 No.2 pp.2052-2055 (2003)

      12) M. Ichikawa H. Kado T. Matsumura,
      "Thermal Runaway and Propagation Process of Defects from Local Defects in the Superconducting Cylinder for Magnetic Shielding Fault Current Limiters (In Japanese)",
      Journal of the Cryogenic Sociaty of Japan, Vol.38 No.8 (2003)

      13) M. Ichikawa H. Kado M. Shibuya M. Kojima M. Kawahara T. Matsumura,
      "Characteristics of Bi-2223 Thich Films on an MgO Substrate Prepeared by a Coating Method (In Japanese)",
      Journal of the Cryogenic Sociaty of Japan, Vol.37 No.9 pp.479-484i2002j

      14) H.Shimizu Y.Yokomizu T.Matsumura N.Murayama,
      "Proposal of Flux Flow Resistance Type Fault Current Limiter Using Bi2223 High Tc Superconducting Bulk",
      IEEE Transactions on Applied Superconductivity, Vol.12, No.1, pp.876-879 (2002)

      15) T.Noda H.Shimizu Y.Yokomizu T.Matsumura N.Murayama,
      "Estimation of AC Transport Current Loss Generated in Bi2223 Bulk for Fault Current Limiter",
      Physica C 378-381 (2002)

      16) H. Shimizu K. Kato Y. Yokomizu T. Matsumura N. Murayama,
      "Analysis of Current Limiting Characteristics of Bi2223 High Tc Superconducting Bulk Element for Application to Fault Current Limiter (In Japanese)",
      Trans. IEE of Japan, Vol.121-B No.10 pp.1263-1269 (2001)

      17) H.Shimizu K.Kato Y.Yokomizu T.Matsumura N.Murayama,
      "Resistance rise in Bi2223 Superconducting Bulk after Normal Transition due to Overcurrent",
      IEEE Transactions on Applied Superconductivity. Vol.11 No.1 Part2 pp.1948-1951 (2001)

      18) T.Matsumura H.Shimizu Y.Yokomizu,
      "Design Guideline of Flux-Lock Type HTS Fault Current Limiter for Power System Application",
      IEEE Transactions on Applied Superconductivity, Vol.11 No.1 Part2 pp.1956-1959 (2001)

      19) K.Kato T.Noda H.Shimizu Y.Yokomizu T.Matsumura N.Murayama,
      "Increase in Transient Resistance of Bi2223 Superconducting Bulk by Applying External Magnetic Field",
      IEEE Transactions on Applied Superconductivity,Vol.11 No.1 Part2 pp.2094-2097 (2001)

      20) T.Satoh M.Yamaguchi S.Fukui K.Kaiho T.Matsumura H.Shimizu N.Murayama,
      "Three-phase Fault Current Limiter with One dc S/N Transition Element",
      IEEE Transactions on Applied Superconductivity, Vol.11 No.1 Part2 pp.2398-2401 (2001)

      21) K. Mimura H. Shimizu Y. Yokomizu T. Matsumura,
      "Analysis of Recovery Characteristic in Commutation Type of Superconducting Fault Current Limiter Considering Time-Current Characteristic of Protective Relay and System Impedance (In Japanese)",
      Trans. IEE of Japan, Vol.120-B No.5 pp.752-759 (2000)

      22) H.Shimizu T.Shiroki Y.Yokomizu T.Matsumura,
      "Dependence of quench current level on ramp rate in as superconducting windings under different fixing conditions",
      IEEE Transactions on Applied Superconductivity, Vol.10 No.1 pp.689-692 (2000)

      23) T.Kawasumi H.Shimizu Y.Yokomizu T.Matsumura,
      "Novel measurement methods of propagation velocity and direction of normal zone in ac superconducting wire",
      IEEE Transactions on Applied Superconductivity, Vol.10 No.1 pp.1247-1250 (2000)

      24) H.Shimizu T.Shiroki Y.Yokomizu T.Matsumura,
      "Dependence of quench current level of superconducting wire and cable on the winding tension",
      IEEE Transactions on Applied Superconductivity, Vol.9 No.2 pp.1129-1132 (1999)

      25) H. Shimizu T.Kamechi Y. Yokomizu T. Matsumura,
      "Recovery Time to Superconducting State after Quench in Superconducting Fault Current Limiter (In Japanese)",
      Trans. IEE of Japan, Vol.119-B No.1 pp.83-90 (1999)

      26) H.Shimizu K.Taketa Y.Yokomizu T. Matsumura,
      "Noncontact Measurement of Transient Potential Distribution in Superconducting Cable",
      Cryogenics, Vol.38 No.10 pp.977-982 (1998)

      27) H.Shimizu T.Kamechi Y.Yokomizu T.Matsumura,
      "Recovery Characterictics to Superconducting State after Quench in A.C. Nb-Ti Superconducting Cable",
      Proc. of the Int. Conf. on Electrical Engineering '97 (Matsue), pp.106-109 (1997)

      28) H.Shimizu Y.Yokomizu T.Matsumura T.Kato Y.J.Tang T.Nagafusa N.Hashimoto,
      "Position of Quench Initiation in 6kV-200A Class Superconducting Fault Current Limiter",
      IEEE Transactions on Applied Superconductivity, Vol.7 No.2 pp.997-1000 (1997)

      29) T.Matsumura Y.Yokomizu T.Uchii,
      "Development of Flux-Lock-Type Fault Current Limiter with High-Tc Superconducting Element",
      IEEE Transactions on Applied Superconductivity, Vol.7 No.2 pp.1001-1004 (1997)

      30) H.Shimizu Y.Yokomizu T.Kato Y.J.Tang T.Matsumura Y.Kito K.Satoh W.Sato,
      "Transmission Loss of Prospective Power Transmission Model System Integrated under Superconducting Envirnment (PROMISE)",
      IEEE Transactions on Applied Superconductivity, Vol.7 No.2 pp.1033-1036 (1997)

      31) T.Kato H.Shimizu Y.Tang N.Hayakawa Y.Yokomizu T.Matsumura,
      "Quench Current Level - Time Characteristics of AC Insulated Multi-Strand Superconducting Cables",
      IEEE Transactions on Applied Superconductivity, Vol.7 No.2 pp.191-194 (1997)

      32) T. Matsumura T. Uchii Y. Yokomizu,
      "Proposal of Flux-Lock-Type Fault Current Limiter with High Tc Superconducting Element (In Japanese)",
      Trans. IEE of Japan, Vol.117-B No.6 pp.851-856 (1997)

      33) T.Uchii Y.Yokomizu T.Matsumura,
      "High Tc Superconducting Fault Current Limiter with Flux-Lock Reactors and Ac Magnetic Field Coil",
      Proc. of the 9th Int. Symp. on Superconductivity (ISS'96), Vol.2 pp.1365-1368 (1996)

      34) H.Shimizu T.Kato Y.Tang Y.Yokomizu T.Matsumura Y.Kito,
      "Decrease in Quench Current Level of Multiply-Twisted A.C. Superconducting Cable due to Local Longitudinal Magnetic Field",
      Proc od the 9th Int. Symp. on Superconductivity (ISS'96), Vol.2 pp.960-972 (1996)

      35) H. Shimizu T. Kato Y.J. Tang Y. Yokomizu T. Matsumura Y. Kito,
      "Influence of Self-magnetic Field on the Quench of Superconducting Fault Current Limiter (In Japanese)",
      Trans. IEE of Japan, Vol.116-B No.7 pp.846-852 (1996)

      36) Y.J.Tang S.Ogura H.Shimizu T.Kato Y.Yokomizu T.Matsumura Y.Kito,
      "Quench Current-Time Characteristcs of Four Kinds of Superconducting Cables",
      Cryogenics, Vol.36 No.6 pp.447-455 (1996)

      37) Y.J.Tang Y.Yokomizu N.Hayakawa T.Matsumura H.Okubo Y.Kito,
      "Influence of Self-Magnetic Field on a.c. Quench Current Level of Superconducting Coils",
      Cryogenics, Vol.35 No.12 pp.861-866 (1995)

      38) T. Kato H. Shimizu Y.J. Tang N. Hayakawa Y. Yokomizu T. Matsumura, et.al,
      "First Trial of the Electric Power Transmission of 3.8kV-460kVA through the Prospective Power Transmission Model System Integrated under Superconducting Environment-PROMISE (In Japanese)",
      Trans. IEE of Japan, Vol.115-B No.8 pp.992-1000 (1995)

      39) Y.J.Tang Y.Yokomizu N.Hayakawa T.Matsumura H.Okubo Y.Kito,
      "Current Limiting Level-Time Characteristics of a Superconducting Fault Current Limiter",
      Cryogenics, Vol.35 No.7 pp.441-446 (1995)

      40) Y.J.Tang T.Kato N.Hayakawa Y.Yokomizu T.Matsumura H.Okubo Y.Kito K.Miyake T.Kumano W.Satoh K.Satoh,
      "Development of the Prospective Power Transmission Model System Integrated under Superconducting Environment-PROMISE",
      IEEE Transactions on Applied Superconductivity, Vol.5 No.3 pp.945-948 (1995)

      41) Y.J. Tang H. Shimizu T. Kato N. Hayakawa Y. Yokomizu T. Matsumura et.al,
      "The Performance of a 3000/6000V, 1000kVA Class Superconducting Transformer Developed for PROMISE (In Japanese)",
      Trans. IEE of Japan, Vol.115-B N0.4 pp.337-345 (1995)

      42) Y.Yokomizu T.Matsumura H.Okubo Y.Kito,
      "Current-Limiting Performance of a YBa2Cu3O7-x Superconductor by the Transition from Superconducting to Normal-Conducting State",
      European Transactions on Electrical Power Engineering,Vol.5 No.2 pp.99-105 (1995)

      43) Y. Yokomizu K. Ito T. Matsumura H. Okubo Y. Kito,
      "Current Distribution in Parallel-Connected YBCO High-Temperature Superconducting Element with Different Critical Currents (In Japanese)",
      Trans. IEE of Japan, Vol-113-B No.10 pp.1149-1154 (1993)

      44) F. Takase T. Matsumura Y. Ueda,
      "Develpment of a Synchronous Generator Model founded on a Permeance-based Air-Gap Flux Density Analysis (In Japanese)",
      Trans. IEE of Japan, Vol.111-D No.10 pp.865-872 (1991)

      45) T. Matsumura N. Hayakawa S. Ito Y. Kito,
      "Conceptual Design of the Fault Current Limiter with Vacuum Fuse Operating as Quick Commutation Medium for Underground Power Distribution Systems (In Japanese)",
      Trans. IEE of Japan, Vol.108-B No.12 pp.599-605 (1988)

      46) Y. Kito T. Matsumura H. Kato,
      "Rediction Effect of Cryoresistive Fault Current Limiter on Transient Recovery Voltage (In Japanese)",
      Trans. IEE of Japan, Vol.104-B No.8 pp.527-533 (1984)

      47) Y.Kito and T.Matsumura,
      "A Fault Current Limiter in Cryoresistive Power Transmission Cables.",
      Proc. Research on Effective Utilization and Densification of Electric Energy, (Tokyo), Vol.SPEY-10 pp.43-46 (1983)[/publication]


