<IMAGE src="../images/corner_image_cells_grn.gif" alt="DNA photo"/>

<IMAGE src="../images/spacer.gif" alt=" "/> <IMAGE src="../images/angle_globalnav_green.gif" alt=" "/> Children's Hospital Research <IMAGE src="../images/bullet_globalnav_green.gif" alt=" "/> Children's Hospital Labs <IMAGE src="../images/spacer.gif" alt=" "/>

Idith Haber, PhD <IMAGE src="../images/spacer.gif" alt=" "/> <IMAGE src="../images/chb_logo_green.gif" alt="Children's logo"/> <IMAGE src="../images/spacer.gif" alt=" "/> <IMAGE src="../images/harvard_logo_green.gif" alt="Harvard logo"/>

<IMAGE src="../images/spacer.gif" alt=" "/>

<IMAGE src="../images/spacer.gif" alt=" "/> <IMAGE src="../images/spacer.gif" alt=" "/>

<IMAGE src="../images/spacer.gif" alt=" "/>

<IMAGE src="../images/bullet_on.gif" alt=" "/> <b>Idith Haber PhD</b> <IMAGE src="../images/spacer.gif" alt=" "/>

<IMAGE src="../images/dotted_sep.gif" alt=" "/>

<IMAGE src="../images/spacer.gif" alt=" X"/> <IMAGE src="../images/spacer.gif" alt=" X"/>

<IMAGE src="Images/spacer.gif" alt=" "/>

[pic]<IMAGE src="Images/idithhaber3.jpg" alt="Image"/>[/pic]

<IMAGE src="../images/spacer.gif" alt=" "/>

[contactinfo]<b>Department</b> [affiliation]Cardiac Surgery, Cardiology[/affiliation]

<b>Hospital Title</b>

<b>Academic Title</b> [position]Instructor[/position] in Surgery

<b>Phone</b> [phone]617-919-2317[/phone]

<b>Fax</b> [fax]617-730-0214[/fax]

<b>Email</b> Idith Haber

<b>Location</b> [address]300 Longwood Avenue
Boston
MA 02115[/address][/contactinfo]

<IMAGE src="../images/bioangle_lft_green.gif" alt=" "/> <IMAGE src="../images/bioangle_rt_green.gif" alt=" "/>

<IMAGE src="../images/spacer.gif" alt=" "/>

<IMAGE src="../images/spacer.gif" alt=" "/>

Research Overview

<IMAGE src="../images/spacer.gif" alt=" "/>

[resinterests]My main research interests lie in the fields of [interests]cardiac MRI[/interests] and [interests]cardiac biomechanics[/interests]. In cardiac MRI, I am primarily interested in [interests]image analysis of tagged MR images[/interests]. The focus of this research is to segment the ventricles from the images and to track the 3D motion of both right and left ventricles through images of systole. Once I have a 3D model/description of cardiac motion, I am interested in accurately describing the [interests]kinematics the ventricle[/interests] using biomechanical methods. First, since little is known of the mechanics of the right ventricle, I am trying to characterize its normal motion. Second, I am studying the alteration of left ventricular mechanics during the progression of left ventricular hypertrophy.[/resinterests]

<IMAGE src="../images/spacer.gif" alt=" "/>

About Idith Haber

<IMAGE src="../images/spacer.gif" alt=" "/>

[education]Idith Haber received her BSE in [bsmajor]biomedical engineering[/bsmajor] from [bsuniv]Tulane University[/bsuniv] and her PhD from the [phduniv]University of Pennsylvania[/phduniv] in [phdmajor]bioengineering[/phdmajor].[/education]

<IMAGE src="../images/spacer.gif" alt=" "/>

<IMAGE src="../images/spacer.gif" alt=" "/>

[publication]Key Publications

<IMAGE src="../images/spacer.gif" alt=" "/>

  * Haber I, Metaxas DN, Axel L. Three-dimensional motion reconstruction and analysis of the right ventricle using tagged MRI. Medical Image Analysis. 2000; 4(4):335-55.

  * Kikinis R, Westin CF. Phase-driven finite element model for spatio- temporal tracking in cardiac tagged MRI. In: Niessen WJ,Viergever MA, editors. Medical Image Computing and Computer-Assisted Intervention-MICCAI; 2001 Oct 14-17.

  * Haber I, Metaxas DN, Geva T,Axel L. Three-dimensional systolic kinematics of the right ventricle. American Journal of Physiology: Heart & Circulatory Physiology. 2005; 289(5): H1826-33.[/publication]

<IMAGE src="../images/spacer.gif" alt=" "/>

<IMAGE src="../images/spacer.gif" alt=" X"/> <IMAGE src="../images/spacer.gif" alt=" X"/>

<IMAGE src="../images/angle_bottomlft_green.gif" alt=" "/> <IMAGE src="../images/spacer.gif" alt=" "/> <IMAGE src="../images/hex_bottom_green.gif" alt=" "/> <IMAGE src="../images/angle_bottomrt_green.gif" alt=" "/>

<IMAGE src="../images/spacer.gif" alt=" "/> <IMAGE src="../images/spacer.gif" alt=" "/> Copyright 2004-2006 Children's Hospital Boston <IMAGE src="../images/spacer.gif" alt=" "/>
