<font size=2>Home > People > Faculty > <b>Seda Ogrenci Memik</b></font>

<font size=6> </font>

[contactinfo][pic]<IMAGE src="images/smemik.jpg" alt=""/>[/pic]
<font size=5>Seda Ogrenci Memik </font>
<font size=2> [position]Assistant Professor[/position]</font>


<font size=3> Room: [address]L471[/address]
Telephone: [phone](847) 491-7378[/phone]
E-mail: [email]seda@ece.northwestern.edu[/email][/contactinfo]

Personal Website:
[homepage]www.ece.northwestern.edu/~seda[/homepage] </font>


<font size=3>

[introduction]Seda Ogrenci Memik <font size=3>joined the Department of Electrical and Computer Engineering in September, 2003. </font>She received her B.S. degree in [bsmajor]electrical engineering[/bsmajor] from [bsuniv]Bogazici University[/bsuniv], Istanbul, Turkey in [bsdate]1998[/bsdate]. She earned her M.S. degree in [msmajor]computer engineering[/msmajor] from [msuniv]Northwestern University[/msuniv] in [msdate]2000[/msdate] and her Ph.D. degree in [phdmajor]computer science[/phdmajor] from [phduniv]UCLA[/phduniv] in [phddate]July 2003[/phddate]. Dr. Memik has authored two book chapters and over 20 technical papers.[/introduction]

[resinterests]<b>Research Interests</b>
[interests]CAD and system design[/interests], [interests]design planning for embedded systems[/interests], [interests]synthesis[/interests], [interests]reconfigurable computing[/interests], [interests]applications and novel architectures[/interests].[/resinterests]

</font>


<font size=1>Copyright ?2002 Department of Electrical and Computer Engineering
Robert R. McCormick School of Engineering and Applied Science, </font><font size=1>
Northwestern University. All rights reserved.

Send questions and comments to the Webmaster. </font>
