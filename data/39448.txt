[pic]<IMAGE src="alex.jpg" alt=""/>[/pic]

[contactinfo]Alexandros Nanopoulos

[affiliation]Department of Informatics

Aristotle University of Thessaloniki[/affiliation]

[address]54124, Thessaloniki, Greece[/address]

<IMAGE src="Email3.gif" alt=""/>[email]ananopou (at) csd (dot) auth (dot) gr[/email]
<IMAGE src="obj02.gif" alt=""/>[phone]+30 2310 991929[/phone][/contactinfo]

[introduction]I am a [position]lecturer[/position] at the [affiliation]Department of Informatics of the Aristotle University[/affiliation] and [position]member[/position] of the [affiliation]Data Engineering Lab[/affiliation]. I got my PhD from [phduniv]Aristotle University[/phduniv] in [phddate]2003[/phddate].[/introduction]

[resinterests]<b>Research Interests</b>
<IMAGE src="l_blue.gif" alt=""/>[interests]Data Mining
Web-mining[/interests]; [interests]Biological data mining[/interests]; [interests]Association rules[/interests]; [interests]Clustering[/interests]; [interests]Ingegrating Data Mining and RDBMSs[/interests]; [interests]Mining Musical Databases[/interests]
<IMAGE src="l_green.gif" alt=""/>[interests]Spatial and Temporal Databases
Indexing[/interests], [interests]Clustering Trajectories[/interests], [interests]Similarity in Sequence Data[/interests][/resinterests]

<b>Resume</b>
Publications
Short Bio
CV

[resactivity]<b>Recent Profesional Activities</b>

<IMAGE src="l_blue.gif" alt=""/>PC member of the AWIC'04, 05
<IMAGE src="l_green.gif" alt=""/>PC member of the WEBKDD 2004, 2005
<IMAGE src="l_blue.gif" alt=""/>[position]Reviewer[/position] for [affiliation]IEEE TKDE[/affiliation], [affiliation]KAIS[/affiliation]
<IMAGE src="l_green.gif" alt=""/>[position]Member[/position] of the [affiliation]SIGKDD Greek Chapter[/affiliation][/resactivity]

Teaching

<IMAGE src="l_red.gif" alt=""/><b> </b><b>Course on </b><b>Data Warehouses and Data Mining</b> (pages in Greek)

Links

<b>Last Modified: Sep 8, 2004</b>
