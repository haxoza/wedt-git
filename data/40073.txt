<IMAGE src="/images/usc_bar.gif" alt="University of Southern California"/>

<IMAGE src="/images/department_name.gif" alt="department name"/> <IMAGE src="/images/viterbi_info.gif" alt="USC Viterbi School of Engineering"/>

<IMAGE src="/images/sep_left.gif" alt=""/> <IMAGE src="/images/about.gif" alt="about"/> <IMAGE src="/images/seperate.gif" alt=""/> <IMAGE src="/images/academics.gif" alt="academics"/> <IMAGE src="/images/seperate.gif" alt=""/> <IMAGE src="/images/research.gif" alt="research"/> <IMAGE src="/images/seperate.gif" alt=""/> <IMAGE src="/images/faculty_staff.gif" alt="faculty_staff"/> <IMAGE src="/images/seperate.gif" alt=""/> <IMAGE src="/images/news.gif" alt="news"/> <IMAGE src="/images/sep_right.gif" alt=""/>

<IMAGE src="/images/admission.gif" alt="admission"/> <IMAGE src="/images/students.gif" alt="students"/> <IMAGE src="/images/alumni.gif" alt="alumni"/>

<IMAGE src="/images/spacer.gif" alt=""/>

Site Home > Faculty & Staff > Faculty Directory > Igor Devetak Calendar    |    Site Map    |    Search <IMAGE src="/images/spacer.gif" alt=""/>

<b></b>

<b> </b>

Faculty Directory

Staff Directory


<IMAGE src="/images/side_faculty_staff.gif" alt=""/>

<b></b>

<b> </b>

<IMAGE src="/images/space.gif" alt=""/>

[introduction]Igor Devetak, Ph.D. - [affiliation]Assistant Professor[/affiliation]

Research Interests:<IMAGE src="../../../wysiwygpro/editor_files/images/image.gif" alt=""/>

Quantum information theory

Biographical Information:

  * [phddegree]Ph.D.[/phddegree] in [phdmajor]Electrical Engineering[/phdmajor], [phddate]2002[/phddate], [phduniv]Cornell University[/phduniv], Ithaca, NY.

Igor Devetak received his [phddegree]Ph.D.[/phddegree] in [phdmajor]Electrical Engineering[/phdmajor] from [phduniv]Cornell University[/phduniv] in [phddate]2002[/phddate]. Before coming to USC in January 2005 he completed a post-doctoral research appointment at IBM's T.J. Watson Research Center. He works in the area of [interests]quantum information theory[/interests], and is especially interested in [interests]quantum Shannon theory[/interests], the [interests]asymptotic theory of quantum communication[/interests].[/introduction]

[resinterests]Research Areas:

  * [interests]Communications[/interests][/resinterests]

    Research Centers and Institutes:

  * Communication Sciences Institute

<IMAGE src="/images/space.gif" alt=""/> <IMAGE src="/images/space.gif" alt=""/>

[pic]<IMAGE src="/assets/001/18112.jpg" alt=""/>[/pic]

[contactinfo]Contact Information:

[affiliation]Department of Electrical Engineering - Systems[/affiliation]
[address]EEB 532
Hughes Aircraft Electrical Engineering Building
3740 McClintock Ave.
Los Angeles, CA 90089-2562[/address]
Tel: [phone](213) 740-9264[/phone]

Fax: Email: [email]devetak@usc.edu[/email][/contactinfo]

Research web page

University of Southern California | Viterbi School of Engineering | Electrical Engineering Department | Site Map<IMAGE src="/images/spacer.gif" alt=""/>
