[pic]<IMAGE src="photo.jpg" alt=""/>[/pic]

[contactinfo]<font size=4>[position]Prof.[/position] <b>Anna Tramontano</b></font>

Biocomputing 

[affiliation]<font size=2>Department of Biochemical Sciences</font>

<font size=2>University of Rome "La Sapienza"</font>[/affiliation]

<font size=2>[address]P.le Aldo Moro, 5</font>

<font size=2>00185 Rome[/address]</font>

<font size=2>Tel. [phone]06 49910556[/phone]</font>

<font size=2>Fax: [fax]06 4440062[/fax]</font>

<font size=2>e-mail: [email]Anna.Tramontano@uniroma1.it[/email]</font>[/contactinfo]

<IMAGE src="line.gif" alt=""/>

<b>Curriculum Vitae</b>

<b><font size=4>Publications</font></b>


[introduction]<b>since 2001</b> [position]Professor[/position] of Biochemistry, I Medical Faculty , [affiliation]University of Rome "La Sapienza"[/affiliation]

<b>1996 - 2001</b> Director of the Computational Biology and Chemistry Department at  IRBM (Merck Research Laboratories in Pomezia, Rome)
and
Director of Information Technology and Research Information Systems at IRBM
and
Professor of Chimica Computazionale II at the University of Naples

<b>in 1994/95</b> Director of Biocomputing Unit of IRBM

<b>in 1990/93</b> Biocomputing Group Leader at  IRBM
and
Professor of Bioinformatics at the University of Milan

<b>in 1988/90</b> Staff member of the European Laboratory for Molecular Biology in Heidelberg (D) in the Biocomputing Programme. 

<b>in 1987</b> Research fellow at International Institute of Genetics and Biophysics in Naples

<b>in 1985/86</b> Consultant for Biosym Technologies Inc., San Diego, CA, USA

<b>in 1984/85</b> Post-doctoral research fellow at the Department of Biochemistry and Biophysics of the University of California, San Francisco (USA) 

<b>in 1981/84</b> Research fellow at the International Institute of Genetics and Biophysics in Naples 

<b>in 1980</b> Laurea in Physics, summa cum laude, at the University of Naples[/introduction]

<b><font size=4> Teaching duties</font></b>

  *  Biochimica II, Corso di laurea in Biotecnologie, Universita' "La Sapienza"

  *  Biochimica I, Corso di laurea in Medicina e Chirugia, canale A, Universita' "La Sapienza"

  *  Bioinformatica, Corso di laurea in Biotecnologie, Universita' "La Sapienza"

  *  Bioinformatica e Ingegneria proteica, corso di laurea in Biotecnologie mediche, Universita' "La Sapienza"

  *  Biochimica, Master in Bioinformatica: Applicazioni Biomediche e Farmaceutiche, Universita' "La Sapienza"

  *  Bioinformatica 2, Master in Bioinformatica: Applicazioni Biomediche e Farmaceutiche, Universita' "La Sapienza"

  *  Biochimica 2, Master in Tecnologie Bioinformatiche applicate alla medicina personalizzata Consorzio21, Pula, Cagliari

  *  Domini proteici, Corso di laurea in Biotecnologie, Universita' di Verona

  *  The experimental interface, Master in Bioinformatics, University of Oxford, UK

[resactivity]<b><font size=4> Memberships and awards</font></b>

  *  Premio Minerva per la ricerca scientifica 2005

  *  Premio speciale della cultura per le Scienze Naturali della Presidenza del Consiglio dei Ministri 2002

  *  Marotta Prize 2001 - National Academy of Science

  * EMBO

  * European Molecular Biology Laboratory Scientific Advisory Committee

  * Scientific Council of Institute Pasteur - Fondazione Cenci Bolognetti

  * ISCB (Board of directors and former vicepresident)

  * European Boinformatics Institute (EBI) Advisory Committee

  *  Associate Editor of Bioinformatics

  *  Associate Editor of Proteins

  *  Advisory Editorial Board of EMBO Journal and EMBO Reports

  *  Editorial Board The FEBS Journal

  *  Assessor for comparative modelling in CASP4 and CASP5

  *  Co-organizer CASP

  *  ISMB Program Committee (1997-2004)

  *  ISMB 2001 organising Committee

  *  ISMB/ECCB 2004 Steering Committee

  *  BioSapiens Network of Excellence Steering Committee

  * FEBS Publication Committee

  *  Societa' Italiana di Biochimica

  * Societa' Italiana di Biofisica e Biologia Molecolare

  *  Societa' Italiana di Chimica

  *  Societa' Italiana di Bioinformatica

<b>Other activities (partal list)</b>

  *  Director: Master in Bioinformatica: Applicazioni biomediche e farmaceutiche

  *  Director: Master in Tecnologie Bioinformatiche applicate alla Medicina personalizzata, Consorzio21 Pula, Cagliari

  *  Organizer: EMBO Sectoral Meeting in Bioinformatics 2004

  *  Organizer: Permanent European School in Bioinformatics

  *  Organizer: 1st ESF training course on Molecular Interactions, Verona, Italy, 2002

  *  Organizer: 2nd ESF training course on Molecular Interactions, Verona, Italy, 2002

  *  Coordinator: Bioinformatics Group Italian Society of Biochemistry and Molecular Biology

  *  Organiser: Third European Workshop in Drug Design, Siena, Italy, 2001

  *  Organiser: Euroconference on Protein Folding and Structure Prediction, Torino, Italy, 1998

  *  Organiser: Second European Workshop in Drug Design, Siena, Italy, 1998

  *  Organiser: FEBS Practical Course Frontiers of Protein Structure Prediction, IRBM ,Rome, Italy, 1997

  *  Organiser: EMBO Symposium on Structural Biology, EMBL, Heidelberg, Germany, 1997

  *  Organiser: Practical Course Frontiers of Protein Structure Prediction, IRBM ,Rome, Italy 1995

  *  Co-organiser: First European Workshop in Drug Design, Cortona, Italy, 199<b>5</b>[/resactivity]

<b>Invited to give seminars / lectures / conferences in:</b>
Amsterdam (NL), Ascona (CH), Asilomar (US), Braunschweig (D), Buenos Aires (AG), Cambridge (UK), Cardiff (UK), Copenhagen (DK), Dover (UK), Eraklion (GR), Havana (CU), Londra (UK), Lucknow (IN), Madrid (ES), Monaco (D), Mosca (RU), Njemegen (NL), Oxford (UK), Oslo (NOR), Parigi (F), Puschino (RU), Stoccolma (S), Strasburgo (FR), Rovinj (CR), La Coruna (E), Lorne (AU), Glasgow (UK) Rovinj (CR), Asilomar (USA), Istanbul (T), Rio de Janeiro (BR) and in several italian cities.

<b>Areas of research</b>

  *  Genome functional annotation

  *  Protein Structure Prediction

  *  Protein structure analysis

  *  Protein sequence analysis

  *  Protein design

  *  Antibody structure

  *  Viral proteins
