<font size=5>Welcome to Pengyu Hong's Homepage</font>

<IMAGE src="_derived/home_cmp_nature-by-hong000_hbtn_p.gif" alt="Home"/> <IMAGE src="Resume/_derived/resume.htm_cmp_nature-by-hong000_hbtn.gif" alt="Resume"/> <IMAGE src="Research/_derived/research_index.htm_cmp_nature-by-hong000_hbtn.gif" alt="Research"/> <IMAGE src="Research/_derived/publication.htm_cmp_nature-by-hong000_hbtn.gif" alt="Publication"/> <IMAGE src="_derived/software.htm_cmp_nature-by-hong000_hbtn.gif" alt="Software"/> <IMAGE src="_derived/Courses.htm_cmp_nature-by-hong000_hbtn.gif" alt="Courses"/> <IMAGE src="_derived/JobOpenings.htm_cmp_nature-by-hong000_hbtn.gif" alt="Openings"/>

<IMAGE src="_themes/nature-by-hong/narule.gif" alt=""/>

[pic]<IMAGE src="images/me/Hong.JPG" alt=""/>[/pic]

[introduction]<font size=4> I am an [position]Assistant Professor[/position] at [affiliation]Computer Science and the National Center of Behavioral Genomics, Brandeis University[/affiliation]. My main research interests include: [interests]computational systems biology[/interests], [interests]image-based functional genomics[/interests], and [interests]behavioral genomics[/interests].</font>[/introduction]

[contactinfo]<font size=3>Contact Information:</font>

[address]Volen Center for Complex Systems 135

<font size=3> MS 018, Brandeis University</font>

<font size=3>Waltham, MA 02454[/address]</font>

<font size=3> Phone: [phone]781-736-2729[/phone]</font>

<font size=3>Email: [email]<IMAGE src="email.gif" alt=""/>[/email]</font>[/contactinfo]
