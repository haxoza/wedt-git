<IMAGE src="logo1b.gif" alt="Departament D'Arquitecturade Computadors"/><IMAGE src="cab1.gif" alt=""/>


[contactinfo]<b><font size=7>Javier Zalamea León.</font></b>
[pic]<IMAGE src="javier.jpg" alt=""/>[/pic]
<b><font size=4>Postal Mail:</font></b>
<b>[address]Departament d'Arquitectura de Computadors (DAC)</b>
<b>Universitat Politècnica de Catalunya (UPC)</b>
<b>Campus Nord, Mòdul D-6 116</b>
<b>Jordi Girona 1-3</b>
<b>08034 Barcelona - Spain[/address]</b>






<b><font size=4> E-mail</font></b>

<IMAGE src="mail.gif" alt=""/> Please, send any comments or suggestions to: <font size=3>[email]jzalamea@ac.upc.es[/email]</font>[/contactinfo]


[introduction]<b><font size=4>About me:</font></b>

<font size=4>Hello!! Thank for visiting my home page. I was born in 1969 in Equador. I obtained my degree in Electrical Engeneering at the Universidad de Cuenca in 1995. Since October of 1997 I'm taking my [phddegree]doctored[/phddegree] at the Computer Architecture Departament in the [phduniv]Universitat Politècnica de Catalunya[/phduniv]. I'm studing under a grant from AECI (Agencia Española de Cooperación Internacional) that allows me to study for my Ph.D. and to do research. I'm working with High Performance Computing Group (HPC) doing research related to [interests]register requirements in VLIW processors for execution of numerical aplications[/interests]. Mainly, my job is concerned with [interests]software and hardware techniques[/interests] aimed at reducing the register pressure in software pipelined loops.</font>[/introduction]




<b><font size=4>Index:</font></b>

    * <b>Publications.</b>

    * <b>High Performance Computing  Group.</b>

    * <b>Hobbies.</b>


<font size=3>Última modificación: Enero 2001</font>
