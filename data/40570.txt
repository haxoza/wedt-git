[pic]<IMAGE src="lijia.jpg" alt=""/>[/pic]
[contactinfo]Dr. Jian Li

Phone: [phone](352) 392-2642[/phone]
FAX: [fax](352) 392-0044[/fax]
email: [email]li@dsp.ufl.edu[/email]

[position]Professor[/position]
[affiliation]Department of Electrical and Computer Engineering[/affiliation]

[address]NEB 437
P.O. Box 116130
University of Florida
Gainesville, FL 32611[/address][/contactinfo]

<IMAGE src="line_1.gif" alt=""/>

[introduction]Dr. Jian Li received the [msdegree]M.Sc.[/msdegree] and [phddegree]Ph.D.[/phddegree] degrees in [phdmajor]electrical engineering[/phdmajor] from [phduniv]The Ohio State University[/phduniv], Columbus, in [msdate]1987[/msdate] and [phddate]1991[/phddate], respectively.

From April 1991 to June 1991, she was an Adjunct Assistant Professor with the Department of Electrical Engineering, The Ohio State University, Columbus. From July 1991 to June 1993, she was an Assistant Professor with the Department of Electrical Engineering, University of Kentucky, Lexington. Since August 1993, she has been with the [affiliation]Department of Electrical and Computer Engineering, University of Florida[/affiliation], Gainesville, where she is currently a [position]Professor[/position]. Her current research interests include [interests]spectral estimation[/interests], [interests]statistical and array signal processing[/interests], and their [interests]applications[/interests].

Dr. Li is a [position]Fellow[/position] of [affiliation]IEEE[/affiliation] and a [position]Fellow[/position] of [affiliation]IEE[/affiliation]. She is a [position]member[/position] of [affiliation]Sigma Xi and Phi Kappa Phi[/affiliation]. She received the 1994 National Science Foundation Young Investigator Award and the 1996 Office of Naval Research Young Investigator Award. She was an Executive Committee Member of the 2002 International Conference on Acoustics, Speech, and Signal Processing, Orlando, Florida, May 2002. She was an Associate Editor of the IEEE Transactions on Signal Processing from 1999 to 2005 and an Associate Editor of the IEEE Signal Processing Magazine from 2003 to 2005. She has been a member of the Editorial Board of Signal Processing, a publication of the European Association for Signal Processing (EURASIP), since 2005. She is presently a [position]member[/position] of two of the [affiliation]IEEE Signal Processing Society technical committees[/affiliation]: the [affiliation]Signal Processing Theory and Methods (SPTM) Technical Committee[/affiliation] and the [affiliation]Sensor Array and Multichannel (SAM) Technical Committee[/affiliation]. She is a co-author of a paper on multi-static adaptive microwave imaging for early breast cancer detection that has received the Best Student Paper Award at the 2005 Annual Asilomar Conference on Signals, Systems, and Computers in Pacific Grove, California.

Dr. Li is the [position]director[/position] of the [affiliation]Spectral Analysis Laboratory of the Department of Electrical and Computer Engineering at University of Florida[/affiliation].[/introduction]

<IMAGE src="line_1.gif" alt=""/>

<b>Papers</b>

<IMAGE src="home.gif" alt=""/>(Back To SAL Home)
