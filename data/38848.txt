<IMAGE src="images/GB.gif" alt="english"/> index enseignement recherche publications administration

index


[pic]<IMAGE src="images/photo_petite.jpg" alt=""/>[/pic] <IMAGE src="images/logoUniversiteEvry-moyen.gif" alt=""/>

index

enseignement

recherche

publications

administration



Marc Aiguier


Maître de Conférences
Universit?d'Évry Val d'Essonne
LaMI - UMR 8042 du CNRS


<IMAGE src="images/enveloppe.gif" alt=""/>
[contactinfo][affiliation]Universit?d'Évry
LaMI - Département Informatique[/affiliation]
[address]Tour Évry 2
523 places des terrasses de l'Agora
91000 Evry
France[/address]

tél : [phone]01 60 87 39 14[/phone][/contactinfo]





Proposition de thèse : Une approche analytique fondée sur le raffinement algébrique pour analyser les propriétés émergentes dans le cadre de la modélisation des systèmes complexes
sujet de thèse



<IMAGE src="images/enveloppe.gif" alt=""/> index.php - dernière modification : Marc Aiguier, 26/06/2006
La responsabilit?du LaMI n'est en rien engagée par les informations publiées dans cette page <IMAGE src="images/top.gif" alt=""/>
