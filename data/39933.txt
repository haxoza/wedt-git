<IMAGE src="Sohler/new_page_files/HNI-logo.gif" alt="HEINZ NIXDORF INSTITUTE"/>



[introduction]<b>[affiliation]Computer Science Department[/affiliation]</b>



Algorithms and Complexity
<font size=1>
Christian Sohler</font>



<IMAGE src="Sohler/new_page_files/button-home-weiss.gif" alt="Home"/>
<IMAGE src="Sohler/new_page_files/button-research-blau.gif" alt="Research"/>
<IMAGE src="Sohler/new_page_files/lehre-blau_e.png" alt="Courses"/>
<IMAGE src="Sohler/new_page_files/button-publications-blau.gif" alt="Publications"/>


<font size=3>
</font>


















<IMAGE src="Sohler/new_page_files/nix.gif" alt=""/> Christian Sohler

<IMAGE src="Sohler/new_page_files/nix-gray.gif" alt=""/>

<IMAGE src="new_page_files/nix.gif" alt=""/>


Jun. Prof. Dr. Christian Sohler[pic]<IMAGE src="http://www.uni-paderborn.de/cs/ag-madh/WWW/BILDER/christiansohler.jpg" alt=""/>[/pic]

<font size=2> Christian Sohler is [position]assistant professor[/position] ([position]Junior professor[/position]) in the [affiliation]Algorithms and Complexity research group[/affiliation].</font>[/introduction]

[resactivity]Current activities:

  * Organizer of Dagstuhl seminar 05291 on "Sublinear Algorithms" together with A.Czumaj, S. Muthukrishnan, R. Rubinfeld

  * Organizer of the annual meeting of the DFG research cluster "Algorithms for large and complex networks"

  * Program committee ESA'05[/resactivity]


[contactinfo]<b>Address: </b> Heinz Nixdorf [affiliation]Institute & Computer Science Department
University of Paderborn[/affiliation]
[address]Fuerstenallee 11
33102 Paderborn
Germany[/address]

<b>Office: </b> [address]F1.119[/address]

<b></b>Phone:
[phone]+49-(0)52 51/60 6427[/phone]

<b>Fax: </b> [fax]+49-(0)52 51/60 6482[/fax]

Email:
[email]csohler (at) upb.de[/email][/contactinfo]


<IMAGE src="new_page_files/nix.gif" alt=""/>
