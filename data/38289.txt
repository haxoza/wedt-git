Tobias Mayr

<IMAGE src="_derived/home_cmp_modular110_vbtn_p.gif" alt="Home"/>
<IMAGE src="_derived/Research.htm_cmp_modular110_vbtn.gif" alt="Research"/>
<IMAGE src="_derived/Publications.html_cmp_modular110_vbtn.gif" alt="Publications"/>
<IMAGE src="_derived/Resume.htm_cmp_modular110_vbtn.gif" alt="Resume"/>
<IMAGE src="_derived/Links.htm_cmp_modular110_vbtn.gif" alt="Links"/>

<IMAGE src="_derived/index.htm_cmp_modular110_bnr.gif" alt="Tobias Mayr"/>

<b> </b>

<b> </b>

<b> </b>

<b> </b>

<b> </b>

<b>[pic]<IMAGE src="images/Tobias.jpg" alt="Tobias.jpg (24707 bytes)"/>[/pic]</b>

<b>

[contactinfo]Stefan Tobias Mayr
[address]4104 Upson Hall <font size=4>
Computer Science Department
Cornell University
Ithaca, NY 14853-7501[/address]
Tel.: [phone](607) 255-9537[/phone] (W)
[phone](607) 256-0951[/phone] (H)
e-mail: [email]mayr@cs.cornell.edu[/email] </font>[/contactinfo]

</b>

Preliminary versions of my dissertation (Zipped, MS-Word, Template)

<font size=1>Last updated, March, 13th, 2001 - Tobias Mayr </font>
