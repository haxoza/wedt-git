#! /usr/bin/env python

import sys

files_names = sys.argv[1:]
encodings = ('utf-8', 'iso-8859-15')

for file_name in files_names:
    read_file = open(file_name)
    data = read_file.read()
    for encoding in encodings:
        try:
            data = data.decode(encoding)
            break
        except UnicodeDecodeError:
            pass
    else:
        print('Problem with file: ' + file_name)
        sys.exit(-1)

    read_file.close()
    print('{} {}'.format(file_name, encoding))

    if encoding != 'utf-8':
        output_file = open(file_name, 'w')
        output_file.write(data.encode('utf-8'))
        output_file.close()

